# Transcript of Pepper&Carrot Episode 35 [ro]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Episodul 35: Reflecția

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Oh, te-ai trezit!
Pepper|2|False|Ți-e bine? Nu e prea frig?
Pepper|3|True|Scuze, o să rămân curând fără Rea,
Pepper|4|False|iar aura mea este prea slabă ca să ne protejeze de frigul de aici.

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Sunt pe cale să pierd hiperviteza,
Pepper|2|True|iar dragonul și călărețul au ținut pasul cu noi de ore bune.
Pepper|3|False|Ne va prinde imediat ce o să încetinesc.
Pepper|4|True|Ce idioată am fost!
Pepper|5|False|Eram sigură că o să renunțe după câteva ore!
Pepper|6|False|Dacă am putea să îl facem să ne piardă urma.
Pepper|7|True|NU. Trebuie să scăpăm de el!
Pepper|8|False|Înainte să ne prindă!
Pepper|9|True|Dar sunt și foarte...
Pepper|10|True|...foarte...
Pepper|11|False|...obosită...
Pepper|12|True|UPS!
Pepper|13|False|Trezirea, Pepper!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|False|Ai observat, da?
Arra|2|True|Da. Urmărirea asta se va termina curând.
Arra|3|False|Nu mai durează mult până va fi epuizată.
Torreya|4|False|Atacăm imediat ce rămâne fără magie.

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|False|BUM!
Pepper|2|False|POFTIM?!
Pepper|3|False|Au depășit viteza sunetului?!
Pepper|4|False|!!!
Sound|5|False|Vuuuș!!!
Pepper|6|True|Grrr!
Pepper|7|False|Ține-te bine, Carrot!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|OOOF!
Pepper|2|False|CE SĂ FAC?!
Pepper|3|False|Oh!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Peștera de pe Lacul Coarnelor!
Pepper|2|False|Carrot, asta e șansa noastră!
Pepper|3|False|Cunosc peștera asta.
Pepper|4|True|E un pasaj lung și întortocheat ce se termină într-o cameră care s-a prăbușit.
Pepper|5|True|Stalactitele din zona prăbușită s-au adunat într-un zid de țepușe. Este o capcană mortală!
Pepper|6|True|Are un spațiu îngust rămas, suficient de lat cât să ne permită nouă să ne strecurăm,
Pepper|7|False|dar dragonul nu o să aibă timp să oprească la timp, și o să loveasca zidul la viteză maximă! Muhahaha!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Grozav! Încă ne urmăresc!
Pepper|2|False|O să scăpăm curând de ei.
Pepper|3|True|Abia aștept să îi văd!
Pepper|4|False|Muhahaha!
Pepper|5|False|!!!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Stai un pic! Ce am devenit?
Pepper|2|True|Un ucigaș?
Pepper|3|False|O vrăjitoare rea?
Pepper|4|False|!!!

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|4|False|SWoosh!
Pepper|1|True|NU!
Pepper|2|False|Asta nu sunt eu!
Pepper|3|False|STOOOOP!!!
Pepper|5|False|MĂ PREDAU!

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|False|Știi că în momentul în care te predai Stăpânei Wasabi, nu o să îți fie bine, nu?
Pepper|2|False|Știu, dar eu tot cred că am o șansă să o conving – să o fac să înțeleagă.
Pepper|3|True|Până la urmă, nimic nu este ireparabil.
Pepper|4|False|Am învățat asta de la o prietenă veche.

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Îți mulțumesc pentru că i-ai permis lui Carrot să meargă să îmi aducă haine noi.
Torreya|2|True|Cu plăcere.
Torreya|3|False|Ne-ai ușurat treaba, așa că măcar atât puteam face să întorc favoarea.
Torreya|4|True|Dar, știi ceva, chiar ai supărat-o pe Wasabi.
Torreya|5|True|Ea cere întotdeauna perfecțiunea. Ai stricat asta cu murdăria și hainele tale rupte, precum și intrarea.
Torreya|6|False|Și ai atacat-o! Nu te va lăsa prea ușor.
Pepper|7|False|Știu.
Torreya|8|True|Oricum, trebuie să mă odihnesc un pic.
Torreya|9|True|Plecăm imediat ce mă trezesc.
Torreya|10|False|Fă ce vrei, atât timp cât nu pleci prea departe...
Sound|11|False|Pleosc
Torreya|12|False|Ce vrăjitoare ciudată.

### P12

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Așa, indiferent de pericole sau riscuri,
Pepper|2|True|O să fiu eu însămi.
Pepper|3|False|Promit!
Narrator|4|False|VA URMA...

### P13

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|18 iunie, 2021 Artă & scenariu: David Revoy. Cititori Beta: Arlo James Barnes, Craig Maloney, Karl Ove Hufthammer, Martin Disch, Nicolas Artance, Parnikkapore, Valvin. Versiune limba română Traducere: Florin Șandru Corectură: Nadia Șandru Bazat pe universul Hereva Creator: David Revoy. Administrator principal: Craig Maloney. Scriitori: Craig Maloney, Nartance, Scribblemaniac, Valvin. Corectori: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Software: Krita 4.4.2, Inkscape 1.1 on Kubuntu Linux 20.04 Licență: Creative Commons Attribution 4.0. www.peppercarrot.com
Pepper|3|True|Știai că?
Pepper|4|False|Pepper&Carrot este complet gratis, open-source și sponsorizat de către cititori.
Pepper|2|True|Mulțumiri pentru acest episod celor 1054 de patroni!
Pepper|5|True|Și tu poți deveni patron al Pepper&Carrot și vei avea numele trecut aici!
Pepper|7|True|Ne găsești pe Patreon, Tipeee, PayPal, Liberapay ...și altele!
Pepper|6|True|Vezi mai multe pe www.peppercarrot.com!
Pepper|8|False|Mulțumim!
