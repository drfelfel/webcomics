# Transcript of Pepper&Carrot Episode 35 [eo]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titolo|1|False|Ĉapitro 35a : La spegulaĵo

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pipro|1|True|Ho, vi jam vekiĝis !
Pipro|2|False|Ĉu fartas bone ? Ne tro malvarme ?
Pipro|3|True|Pardonu, mi ekmalhavas Reon,
Pipro|4|False|kaj mia aŭro tro maldikas por ŝirmi nin de vento ĉi tie.

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pipro|1|True|Baldaŭ mi perdos mian superrapidon,
Pipro|2|True|kaj tiuj ĉi flugisto kaj drako nin sukcese sekvas jam kelkajn horojn.
Pipro|3|False|Ili facile kaptos nin, kiam ni malrapidiĝos.
Pipro|4|True|Kia stultulo estis mi !
Pipro|5|False|Mi certis, ke ili rezignos jam antaŭ horoj !
Pipro|6|False|Se nur iel mi povus ilin forigi.
Pipro|7|True|NE. Mi devas forigi ilin !
Pipro|8|False|Antaŭ ol ili kaptos nin !
Pipro|9|True|Sed mi ankaŭ...
Pipro|10|True|... ege...
Pipro|11|False|… lacas…
Pipro|12|True|OJ OJ OJ !
Pipro|13|False|Koncentriĝu, Pipro !

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Nukstaksuso|1|False|Vi vidis tion, ĉu jes ?
Arao|2|True|Jes. La ĉaso baldaŭ finiĝos.
Arao|3|False|Ĝis nelonge ŝiaj kapabloj eluziĝos.
Nukstaksuso|4|False|Kaj tiam ni tuj atakos.

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sono|1|False|BUM !
Pipro|2|False|KIO ?!
Pipro|3|False|Ili rompis la sonmuron ?!
Pipro|4|False|!!!
Sono|5|False|PLUUUU !!!
Pipro|6|True|Grr !
Pipro|7|False|Tenu bone, Karoĉjo !

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pipro|1|True|AĈ !
Pipro|2|False|KION NI FARU ?!
Pipro|3|False|Ho !

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pipro|1|False|La kaverno de la Korna lago !
Pipro|2|False|Karoĉjo, tio estas nia ŝanco !
Pipro|3|False|Mi konas la kavernon.
Pipro|4|True|Ĝi estas longa koridoro, je kies fino troviĝas kolapsinta ĉambro.
Pipro|5|True|La pendŝtonoj en ĉi tiu kolapsinta parto amasiĝis al muro de pikaĵoj. Estas mortkaptilo !
Pipro|6|True|Plafone estas truo, kiu sufiĉe larĝas por ni, sed tiu drako ne povos ŝanĝi sian direkton,
Pipro|7|False|kaj trafos la muron je plena rapideco ! Ĥa ĥa ĥa ĥa ĥa ĥa !

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pipro|1|False|Bonege ! Ili sekvas nin !
Pipro|2|False|Baldaŭ ni seniĝos ilin.
Pipro|3|True|Mi ne povas atendi !
Pipro|4|False|Ĥa ĥa ĥa ĥa !
Pipro|5|False|!!!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pipro|1|True|Momenton… ! Kio iĝis mi ?
Pipro|2|True|Ĉu murdisto ?
Pipro|3|False|Ĉu malica sorĉistino ?
Pipro|4|False|!!!

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sono|4|False|BAlaaa !
Pipro|1|True|NE !
Pipro|2|False|Ne tio mi estas !
Pipro|3|False|HAAAALT !!!
Pipro|5|False|MI CEDAS !

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Nukstaksuso|1|False|Ĉu vi scias, ke cedi al majstrino Vasabio ne finiĝos bone por vi ?
Pipro|2|False|Mi scias, sed mi ankoraŭ sentas, ke devas esti maniero ŝin atingi – ŝin komprenigi.
Pipro|3|True|Finfine, ĉio estas riparebla.
Pipro|4|False|Tion mi lernis de malnova amiko.

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pipro|1|False|Dankon, ĉar vi lasis Karoĉjon porti al mi novajn vestaĵojn.
Nukstaksuso|2|True|Volonte.
Nukstaksuso|3|False|Vi faciligis nian taskon, do mi devus almenaŭ tion repagi.
Nukstaksuso|4|True|Sed sciu, vi vere kolerigis Vasabion.
Nukstaksuso|5|True|Ŝi postulas perfektecon. Vi tion difektis per via malpureco, via vesto, kaj via eniro.
Nukstaksuso|6|False|Kaj vi atakis ŝin ! Ŝi ne ĝentilos al vi.
Pipro|7|False|Mi scias.
Nukstaksuso|8|True|Ĉiuokaze, mi bezonas mallonge dormeti.
Nukstaksuso|9|True|Ni foriros kiam mi vekiĝos.
Nukstaksuso|10|False|Faru kion ajn, simple ne iru tro foren...
Sono|11|False|Plonĝ
Nukstaksuso|12|False|Kia interesa sorĉistino.

### P12

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pipro|1|True|Kontraŭ ĉiuj riskoj kaj danĝeroj,
Pipro|2|True|mi restos fidela al mia vera mio.
Pipro|3|False|Mi promesas !
Rakontanto|4|False|DAŬRIGOTE...

### P13

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Atribuintaro|1|False|18a de Junio, 2021 Arto kaj scenaro : David Revoy. Beta-legado : Arlo James Barnes, Craig Maloney, Karl Ove Hufthammer, Martin Disch, Nicolas Artance, Parnikkapore, Valvin. Esperanta versio Traduko : Tirifto Korekto : tuxayo Fasono : Navi, Tirifto Bazita sur la universo Hereva Kreinto : David Revoy. Ĉefa daŭrigonto : Craig Maloney. Redakto : Craig Maloney, Nartance, Scribblemaniac, Valvin. Korekto : Willem Sonke, Moini, Hali, CGand, Alex Gryson. Programaro : Krita 4.4.2, Inkscape 1.1 sur Kubuntu Linux 20.04 Permesilo : Krea Komunaĵo Atribuite 4.0. www.peppercarrot.com
Pipro|3|True|Ĉu vi sciis ?
Pipro|4|False|Pepper&Carrot estas tute libera, senpaga, malfermitkoda, kaj subtenata de siaj mecenataj legantoj.
Pipro|2|True|Ĉi tiu ĉapitro ricevis subtenon de 1054 mecenatoj !
Pipro|5|True|Vi ankaŭ povas iĝi mecenato de Pepper&Carrot kaj havi vian nomon skribita tie !
Pipro|7|True|Ni estas en Patreon, Tipeee, PayPal, Liberapay ... kaj aliaj !
Pipro|6|True|Venu al www.peppercarrot.com por pli da informoj !
Pipro|8|False|Dankon !
