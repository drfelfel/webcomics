# Transcript of Pepper&Carrot Episode 35 [en]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Episode 35: The Reflection

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Oh, you're awake!
Pepper|2|False|Are you OK? Not too cold?
Pepper|3|True|Sorry, I'm running out of Rea soon,
Pepper|4|False|and my aura is too thin to protect us from the cold up here.

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|I'm about to lose my hyperspeed,
Pepper|2|True|and this pilot and dragon have been keeping pace with us for hours.
Pepper|3|False|They'll have no trouble catching us once we slow down.
Pepper|4|True|What an idiot I was!
Pepper|5|False|I was certain they would have given up hours ago!
Pepper|6|False|If only I could get them off our tail.
Pepper|7|True|NO. I have to get rid of them!
Pepper|8|False|Before they catch us!
Pepper|9|True|But I am also...
Pepper|10|True|...very...
Pepper|11|False|...tired...
Pepper|12|True|WHOOPS!
Pepper|13|False|Keep it together, Pepper!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|False|You saw that, right?
Arra|2|True|Yes. This chase will be over soon.
Arra|3|False|It won't be long before her powers are exhausted.
Torreya|4|False|We'll attack as soon as she runs out.

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|False|BoOM!
Pepper|2|False|WHAT?!
Pepper|3|False|They broke the sound barrier?!
Pepper|4|False|!!!
Sound|5|False|WOoosH!!!
Pepper|6|True|Grrr!
Pepper|7|False|Hang on, Carrot!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|DRAT!
Pepper|2|False|WHAT TO DO?!
Pepper|3|False|Oh!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|The cave of the Lake of the Horns!
Pepper|2|False|Carrot, this is our chance!
Pepper|3|False|I know this cave.
Pepper|4|True|It's a long winding passageway that ends in a room that has caved in.
Pepper|5|True|The stalactites in this collapsed section piled up into a wall of spikes. It's a death trap!
Pepper|6|True|There's a small opening that's just wide enough for us to squeeze through,
Pepper|7|False|but that dragon won't be able to pull up in time and will hit the wall at full speed! Muhahaha!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Great! They are following us!
Pepper|2|False|We'll soon be rid of them.
Pepper|3|True|I can't wait to see it!
Pepper|4|False|Muhahaha!
Pepper|5|False|!!!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Wait! What have I become?
Pepper|2|True|A killer?
Pepper|3|False|A mean witch?
Pepper|4|False|!!!

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|4|False|SWoosh!
Pepper|1|True|NO!
Pepper|2|False|That's not who I am!
Pepper|3|False|STOOOOP!!!
Pepper|5|False|I SURRENDER!

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|False|You know that surrendering to Master Wasabi will not end well for you?
Pepper|2|False|I know, but I still feel there's some way to get through to her – to make her understand.
Pepper|3|True|After all, nothing is irreparable.
Pepper|4|False|I learned that from an old friend.

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Thank you for allowing Carrot to bring me a fresh change of clothes.
Torreya|2|True|My pleasure.
Torreya|3|False|You made our job easier, so the least I could do is return the favor.
Torreya|4|True|But you know, you really upset Wasabi.
Torreya|5|True|She demands perfection. You disturbed that with your filth, your clothing, and your entrance.
Torreya|6|False|And you attacked her! She won't go easy on you.
Pepper|7|False|I know.
Torreya|8|True|Anyway, I need to take a short nap.
Torreya|9|True|We'll leave when I wake up.
Torreya|10|False|Do what you want as long as you don't go far...
Sound|11|False|Sploosh
Torreya|12|False|What a curious witch.

### P12

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Now, whatever the risks and dangers,
Pepper|2|True|I'll stay true to who I am.
Pepper|3|False|I promise!
Narrator|4|False|TO BE CONTINUED...

### P13

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|June 18, 2021 Art & scenario: David Revoy. Beta readers: Arlo James Barnes, Craig Maloney, Karl Ove Hufthammer, Martin Disch, Nicolas Artance, Parnikkapore, Valvin. English version (original version) Proofreading: Arlo James Barnes, Craig Maloney, Karl Ove Hufthammer, Martin Disch. Based on the universe of Hereva Creator: David Revoy. Lead maintainer: Craig Maloney. Writers: Craig Maloney, Nartance, Scribblemaniac, Valvin. Correctors: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Software: Krita 4.4.2, Inkscape 1.1 on Kubuntu Linux 20.04 License: Creative Commons Attribution 4.0. www.peppercarrot.com
Pepper|3|True|Did you know?
Pepper|4|False|Pepper&Carrot is entirely free(libre), open-source and sponsored thanks to the patronage of its readers.
Pepper|2|True|For this episode, thanks go to 1054 patrons!
Pepper|5|True|You too can become a patron of Pepper&Carrot and get your name here!
Pepper|7|True|We are on Patreon, Tipeee, PayPal, Liberapay ...and more!
Pepper|6|True|Check www.peppercarrot.com for more info!
Pepper|8|False|Thank you!
