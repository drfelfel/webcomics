# Episode 27: Coriander's Invention

![cover of episode 27](https://www.peppercarrot.com/0_sources/ep27_Coriander-s-Invention/low-res/Pepper-and-Carrot_by-David-Revoy_E27.jpg)

## Comments from the author

This first part happens before the ceremony. It introduces a bit more Coriander. We already saw her on previous episode, but never really understood she was a princess (except last panel in "[The Pyjama party](https://www.peppercarrot.com/en/article338/episode-13-the-pyjama-party)".) I never introduced her passion for robots too. This episode tries to (finally) solve that. We also see Qualicity for the first time. I hope you'll like it.

From [Beta-reading Forum of episode 27](https://framagit.org/peppercarrot/webcomics/-/issues/69)
