# Transcript of Pepper&Carrot Episode 28 [de]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 28: Das Fest

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Erzähler|1|True|Die drei Monde von Hereva lagen auf einer Linie in jener Nacht...
Erzähler|2|False|...und ihr Licht sorgte für eine spektakuläre Szene in der Kathedrale von Zombiah.
Erzähler|3|False|Unter diesem bezaubernden Licht wurde Coriander, meine Freundin zur...
Erzähler|4|False|Königin von Qualicity.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Erzähler|1|False|Schon bald danach wurde gefeiert.
Erzähler|2|False|Ein riesiges Fest mit allen Magieschulen und hunderten von hoch-karätigen Gästen aus ganz Hereva.
Pepper|3|False|!?

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Ah! Du bist's!
Pepper|3|False|Ich war in Gedanken versunken.
Carrot|2|False|schmieg schmieg
Pepper|4|False|Ich gehe wieder rein. Es wird kalt da draußen.
Journalist|6|False|Beeilung! Sie ist hier!
Journalist|5|False|Schnell! Da ist sie!
Journalist|7|False|Frau Saffron! Haben Sie ein paar Worte für den Qualicity Tagesanzeiger?
Saffron|8|False|Ja, sicher doch!
Pepper|15|False|...
Pepper|16|False|Siehst du, Carrot? Ich glaube deshalb bin ich nicht in Stimmung.
Geräusch|9|False|BLITZ
Journalist|13|True|Frau Saffron! Hereva Mode-magazin.
Geräusch|10|False|BLITZ
Geräusch|11|False|BLITZ
Geräusch|12|False|BLITZ
Journalist|14|False|Welchen Designer tragen Sie heute Abend?

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Alle meine Freunde sind so erfolgreich und manchmal wünschte ich, ich wäre auch ein wenig wie sie.
Pepper|2|False|Coriander ist Königin.
Pepper|3|False|Saffron ein reicher Superstar.
Pepper|4|False|Sogar Shichimi sieht so perfekt aus mit ihrer Schule.
Pepper|5|False|Und ich? Was habe ich?
Pepper|6|False|Das.

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|False|Pepper?
Shichimi|2|False|Ich habe etwas Essen raus-geschmuggelt. Möchtest du?

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|True|Es ist komisch, dass ich mich verstecken muss, um was zu essen.
Shichimi|2|False|Aber unsere Lehrerin sagt, dass wir wie über-natürliche Wesen ohne körperliche Bedürfnisse aussehen müssen.
Saffron|3|False|Aha! Da seid ihr!
Saffron|4|True|Endlich! Ein Ort ohne Fotografen.
Saffron|5|False|Die machen mich noch wahnsinnig.
Coriander|6|False|Fotografen?
Coriander|7|False|Versuch mal, lang-weiligen Diskussionen mit Politikern aus dem Weg zu gehen mit dem Ding auf dem Kopf.
Geräusch|8|False|kratz kratz

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Coriander|1|True|Übrigens Pepper, ich habe endlich deine Patentanten kennengelernt.
Coriander|2|False|Sie sind wirklich außergewöhnlich.
Pepper|3|False|!!!
Coriander|4|True|Ich meine, du hast solches Glück!
Coriander|5|False|Sie lassen dich sicher machen, was du willst.
Coriander|6|False|Zum Beispiel, ganz un-diplomatisch auf langweilige Diskussionen zu verzichten.
Saffron|7|False|Oder Tanzen und Spaß zu haben, ohne sich darum zu kümmern, was andere denken.
Shichimi|8|False|Oder das leckere Essen am Buffet zu kosten, ohne Angst, gesehen zu werden.

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Erzähler|4|False|FORTSETZUNG FOLGT…
Shichimi|1|False|Oh! Pepper!? Haben wir was Falsches gesagt?
Pepper|2|True|Nein, gar nicht!
Pepper|3|False|Danke, meine Freunde!

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|5|True|Du kannst auch Spender von Pepper&Carrot werden und deinen Namen hier lesen!
Pepper|3|True|Pepper&Carrot ist vollständig frei(libre), Open Source und finanziert durch Spenden von Lesern.
Pepper|4|False|Für diese Episode danken wir 960 Spendern!
Pepper|7|True|Geh auf www.peppercarrot.com für mehr Informationen!
Pepper|6|True|Wir sind auf Patreon, Tipeee, PayPal, Liberapay ...und mehr!
Pepper|8|False|Dankeschön!
Pepper|2|True|Wusstest du schon?
Impressum|1|False|Januar, 2019 Illustration & Handlung: David Revoy. Beta-Leser: CalimeroTeknik, Craig Maloney, Martin Disch, Midgard, Nicolas Artance, Valvin. Deutsche Version Übersetzung: Martin Disch. Korrektur: Alina The Hedgehog. Basierend auf der Hereva-Welt Erstellung: David Revoy. Hauptbetreuer: Craig Maloney. Redakteure: Craig Maloney, Nartance, Scribblemaniac, Valvin. Korrektur: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Software: Krita 4.1.5~appimage, Inkscape 0.92.3 auf Kubuntu 18.04.1. Lizenz: Creative Commons Namensnennung 4.0. www.peppercarrot.com
