# Transcript of Pepper&Carrot Episode 28 [oc]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Títol|1|False|Episòdi 28 : Las festivitats

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrator|1|True|Las tres lunas d'Hereva èran alinhadas en aquela nuèit,
Narrator|2|False|e lor esclat balhava un espectacle fabulós dins la catedrala de Zombiah.
Narrator|3|False|Foguèt jos aquel lum magic que Coriandre, mon amiga, venguèt...
Narrator|4|False|Reina de Qualicity.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrator|1|False|Las festivitats comencèron pauc après.
Narrator|2|False|Un fèsta giganta amb totas las escòlas de magia, e de centenats de convidats prestigioses venguts d'Hereva tot.
Pepper|3|False|?!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Òu, siás tu !
Pepper|3|False|Èri perduda dins mas pensadas.
Carròt|2|False|fro fro
Pepper|4|False|Vau dedins. Comença de fa freg defòra.
Jornalista|6|False|Aviat !
Jornalista|5|False|Es aquí !
Jornalista|7|False|Damisèla Safran ! Qualques mots pel Diari de Qualicity ?
Safran|8|False|Òc, solide !
Pepper|15|False|...
Pepper|16|False|Veses, Carròt ? Pensi que compreni perqué soi pas dins l'ambient.
Son|9|False|FLASH
Jornalista|13|True|Damisèla Safran ! Gaseta Estile Hereva.
Son|10|False|FLASH
Son|11|False|FLASH
Son|12|False|FLASH
Jornalista|14|False|Qual costurièr portatz aqueste ser ?

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Totas mas amigas fan fièra e, de còps, m'agradariá d'aver lo quart de çò qu'an.
Pepper|2|False|Coriandre es una reina.
Pepper|3|False|Safran est rica e famosa.
Pepper|4|False|A mai Shichimi sembla perfièita amb son escòla.
Pepper|5|False|E ieu ? De qu'ai, ieu ?
Pepper|6|False|Aquò.

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|False|Pepper ?
Shichimi|2|False|Ai pres de qué manjar, ne vòls ?

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|True|Es estranh de me dever amagar per manjar.
Shichimi|2|False|Mas nòstre mèstre ditz que devem semblar d'esperits purs sens besonhs materials.
Safran|3|False|A ! Sètz aquí !
Safran|4|True|Enfin ! Un endreit per m'amagar dels fotografes !
Safran|5|False|Me fan venir cabra !
Coriandre|6|False|De fotografes ?
Coriandre|7|False|Ensajatz puslèu d'evitar las discussions anujosas amb los politicians quand avètz aquò sul cap.
Son|8|False|grat' grat'

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Coriandre|1|True|Tè, Pepper, fin finala ai rescontrat tas mairinas.
Coriandre|2|False|Son vertadièrament « especialas ».
Pepper|3|False|!!!
Coriandre|4|True|Vòli dire, siás plan astruga.
Coriandre|5|False|Soi segura que te daissan far çò que vòls.
Coriandre|6|False|Coma arrestar las posturas oficialas e las parlariás inutilas sens riscar un incident diplomatic.
Safran|7|False|O dançar e s'amusar sens se preocupar de l'agach dels autres.
Shichimi|8|False|O poder tastar lo manjar de la taulada davant tot lo monde !

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrator|4|False|DE SEGUIR…
Shichimi|1|False|Oï ! Pepper ?! Avèm dit quicòm que caliá pas ?
Pepper|2|True|Que non pas !
Pepper|3|False|Mercés, amigas !

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|5|True|Vos tanben, podètz venir mecèna de Pepper&Carrot e aver vòstre nom marcat aquí !
Pepper|3|True|Pepper&Carrot es completament liure, gratuit, open-source e esponsorizat mercés al mecenat de sos lectors.
Pepper|4|False|Aqueste episòdi a recebut lo sosten de 960 mecènas !
Pepper|7|True|Anatz sus www.peppercarrot.com per mai d'informacions !
Pepper|6|True|Sèm sus Patreon, Tipeee, PayPal, Liberapay ...e d'autres !
Pepper|8|False|Mercé!
Pepper|2|True|O sabiatz ?
Crèdits|1|False|Genièr de 2019 Art & scenari : David Revoy. Lectors de la version beta : CalimeroTeknik, Craig Maloney, Martin Disch, Midgard, Nicolas Artance, Valvin. Version occitana Traduccion : Aure Séguier. Basat sur l'univèrs d'Hereva Creator : David Revoy. Manteneire màger : Craig Maloney. Redactors : Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Correctors : Willem Sonke, Moini, Hali, CGand, Alex Gryson. Logicials : Krita 4.1.5~appimage, Inkscape 0.92.3 sus Kubuntu 18.04.1. Licéncia : Creative Commons Attribution 4.0. www.peppercarrot.com
