# Transcript of Pepper&Carrot Episode 17 [da]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 17: En ny begyndelse

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Spidskommen|1|False|Men Pepper... ...Kom tilbage...
Pepper|2|False|NEJ! JEG REJSER MIN VEJ!!
Pepper|3|False|I lærer mig ikke ægte hekseri! Jeg smutter!... Hen til Ah-heksene!
Lyd|4|False|Woooshh!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Nå, lad os flyve til månenedgangenes land.
Pepper|2|False|Shichimi vil fortælle os, hvordan vi kommer ind hos Ah-heksene.
Pepper|3|False|Carrot, find kortet og kompasset frem: der er for mange skyer og jeg kan ikke se, hvor jeg flyver.
Pepper|5|True|Pokkers!
Pepper|6|True|Turbulens!
Pepper|7|False|HOLD FAST!
Pepper|9|False|Åh nej!!!
Lyd|8|False|BrrOoooOoo!!
Skrift|4|False|N

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|1|False|KrrAsh!!
Lyd|2|False|KrrAsh!!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Monster|5|True|Piv!!
Monster|6|False|Piv!!
Pepper|1|False|Vær nu ikke bange, Carrot...
Cayenne|2|True|”Intens øjenkontakt er bedre end ubrugelige trylleformularer!...
Pepper|4|False|Jeg havde nu foretrukket at lære nogle gode angrebs-trylleformularer, men ok...
Pepper|7|True|Pokkers... hverken kost eller udstyr.
Pepper|8|False|Vejen bliver lang...
Cayenne|3|False|... Tæm dem. Dominér dem!”

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Spidskommen|4|False|"At kende spiselige planter er at vide hvor man finder færdiglavede eliksirer mod sult."
Pepper|6|False|Har hun i det mindste lært mig én?
Pepper|2|True|Jeg kan heller ikke mere...
Pepper|3|False|Og det er flere dage siden vi har spist.
Carrot|1|False|Rumle
Pepper|5|True|Men ægte eliksirer?...

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Timian|1|False|"En ægte Kaosah-heks har hverken brug for kort eller kompas under en stjernehimmel."
Pepper|3|True|Hold ud Carrot!
Pepper|4|False|Se, vi er her!
Pepper|2|False|... jeg ville hellere have haft spådomsundervisning!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|7|False|Licens: Creative Commons Kreditering 4.0. Værktøj: Krita, G'MIC, Inkscape, på Linux Ubuntu
Credits|6|False|Baseret på Hereva-universet skabt af David Revoy med bidrag af Craig Maloney. Rettelser af Willem Sonke, Moini, Hali, CGand og Alex Gryson.
Fortæller|4|False|- SLUT -
Credits|5|False|06/2016 – www.peppercarrot.com – Tegning og manuskript: David Revoy – Dansk oversættelse: Emmiline Alapetite
Pepper|2|False|Nu kender du hele historien.
Shichimi|3|False|...og du siger, du er her, fordi de ingenting har lært dig?
Pepper|1|True|...det var det.

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Pepper&Carrot er helt gratis, open-source, og sponsoreret af læsere. Denne episode kunne ikke være blevet til uden støtte fra de 719 tilhængere:
Credits|2|False|Støt næste episode af Pepper&Carrot på www.patreon.com/davidrevoy
