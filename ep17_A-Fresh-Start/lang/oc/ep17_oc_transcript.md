# Transcript of Pepper&Carrot Episode 17 [oc]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Títol|1|False|Episòdi 17 : Aviada

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Comin|1|False|Mas Pepper... ...Torna...
Pepper|2|False|NON ! ME'N VAU !!
Pepper|3|False|Ensenhatz pas de vertadièra mascariá ! Me'n vau !...en cò de las mascas de Ah !
Son|4|False|Zoooch !

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Bon, sèm partits pel país de las lunas colcas.
Pepper|2|False|Shichimi nos conselharà per dintrar en cò de las mascas de Ah.
Pepper|3|False|Carròt, sortís la mapa e la bossòla : i a tròp de nívols e sabi pas ont vau.
Pepper|5|True|ZUT !
Pepper|6|True|De turbuléncias !
Pepper|7|False|ARRAPA-TE !
Pepper|9|False|Ò non !!!
Son|8|False|BrrOoooOoo !!
Escritura|4|False|N

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Son|1|False|CrrAch !!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mostre|6|False|Hau !!
Mostre|5|True|Hiu !!
Pepper|1|False|Sustot, te'n fagas pas, Carròt...
Cayenne|2|True|« Un bon agach negre evita un fum de sòrtilègis inutiles !...
Pepper|7|False|A ieu, solide, m'auriá agradat melhor d'apréner qualques bons sortilègis d'ataca ; mas bon...
Pepper|8|True|Zut... pas pus d'escoba ni d'afars.
Pepper|9|False|La rota va èstre longa...
Cayenne|3|False|...Domda-los. Domina-los ! »

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Comin|4|False|« Conéisser las plantas comestiblas ? Es conéisser de pocions totas prèstas contra la talent »
Pepper|6|False|Me n'auriá ensenhat una sola, pensas ?
Pepper|2|True|Ieu tanben, soi aganida...
Pepper|3|False|E fa mantun jorn qu'avèm pas manjat res.
Carròt|1|False|Groo
Pepper|5|True|Mas de pocions vertadièras ?...

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Frigola|1|False|« Una vertadièra masca de Caosah a pas besonh de mapa, nimai de bossòla jos un cèl estelat »
Pepper|3|True|Coratge Carròt !
Pepper|4|False|Gaita, i sèm !
Pepper|2|False|...a ieu m'auriá agradat melhor un vertadièr cors de divinacion !

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crèdits|7|False|Licéncia : Creative Commons Attribution 4.0, Logicials : Krita, G'MIC, Inkscape sus Ubuntu
Crèdits|6|False|Basat sus l'univèrs d'Hereva creat per David Revoy amb las contribucions de Craig Maloney. Correccions de Willem Sonke, Moini, Hali, CGand e Alex Gryson.
Narrator|4|False|- FIN -
Crèdits|5|False|06/2016 - www.peppercarrot.com - Dessenh & Scenari : David Revoy
Pepper|2|False|Ara coneisses tota l'istòria.
Shichimi|3|False|...e me dises que siás aquí perque t'an pas res ensenhat ?
Pepper|1|True|...e vaquí.

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crèdits|1|False|Pepper&Carrot es completament liure, open source, e esponsorizat mercés al mecenat de sos lectors. Per aqueste episòdi, mercé als 719 mecènas :
Crèdits|2|False|Vos tanben, venètz mecèna de Pepper&Carrot sus www.patreon.com/davidrevoy
