# Transcript of Pepper&Carrot Episode 17 [tp]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
nimi|1|False|lipu nanpa luka luka luka tu: open sin

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Kumin|1|False|taso, jan Pepa o... kama sin...
jan Pepa|2|False|ALA! MI TAWA!!
jan Pepa|3|False|sina pana ala e sona lon pi wawa nasa! mi tawa kulupu Aa pi wawa nasa!
kalama|4|False|Taawaa !

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|True|pona a. mi tawa ma pi mun mute anpa.
jan Pepa|2|False|jan Sisimi li ken pana e sona ni: mi kama jan pi kulupu Aa pi wawa nasa kepeken nasin seme?
jan Pepa|3|False|soweli Kawa o pana e ilo nasin e lipu nasin tan ni: mi ken lukin ala tan ko kon sewi.
jan Pepa|5|True|PAKALA!
jan Pepa|6|True|kon li wawa!
jan Pepa|7|False|O AWEN!
jan Pepa|9|False|ike a !!!
kalama|8|False|KalaAmaAaa!!
sitelen|4|False|N

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
kalama|1|False|Pakala!!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
kon ike|6|False|Aaa!!
kon ike|5|False|Muu!!
jan Pepa|1|False|suli la, soweli Kawa o, pilin ala pi wile weka...
jan Kajen|2|True|"sina lukin kepeken ike wawa la, nimi pi wawa nasa li wawa ala! ...
jan Pepa|7|False|mi la, nimi utala pi wawa nasa li pona tawa tenpo ni. taso ni li ni...
jan Pepa|8|True|pakala... ilo mi en ijo mi li weka.
jan Pepa|9|False|nasin ni li wile kepeken tenpo a...
jan Kajen|3|True|...o lukin e ona. o anpa e ona.
jan Kajen|4|False|o lawa e ona! "

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Kumin|4|False|"sina sona e kasi moku la, sina sona e wawa tawa pilin pi wile moku!"
jan Pepa|6|False|sona ona o tawa mi. taso sona ala li tawa mi!
jan Pepa|2|True|wawa mi li kama lili kin, soweli Kawa o...
jan Pepa|3|False|mi tu li moku ala lon tenpo suno mute.
soweli Kawa|1|False|Mumoku
jan Pepa|5|True|taso telo pi wawa nasa la?...

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Tume|1|False|"jan sona pi kulupu Pakalaa li sona e nasin kepeken ilo ala, kepeken lipu ma ala. mun li pana e nasin"
jan Pepa|3|True|soweli Kawa o pilin pona!
jan Pepa|4|False|o lukin: mi kama!
jan Pepa|2|False|... taso wawa nasa la, mi wile sona e nasin pi tenpo kama a!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
mama|7|False|nasin lawa : Creative Commons Attribution 4.0, ilo: ilo Krita en ilo G'MIC en ilo Inkscape li kepeken ilo Ubuntu
mama|6|False|musi ni li tan pali pi ma Elewa. pali ni li tan jan David Revoy. jan Craig Maloney li pana e pona tawa ona. jan Willem Sonke en jan Moini en jan Hali en jan CGand en jan Alex Gryson li pona e musi ni.
sitelen toki|4|False|- PINI -
mama|5|False|tenpo 06/2016 - www.peppercarrot.com - musi sitelen & toki li tan jan David Revoy - ante toki pi toki pona li tan jan Ret Samys
jan Pepa|2|False|...ni la, sina kama ala kama lon ma mi kepeken sona ona ala?
jan Sisimi|3|False|sina sona e toki mi ale, e nasin mi ale.
jan Pepa|1|True|...ni li ale.

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
mama|1|False|jan Pepa&soweli Kawa li nasin jo pi jan ale, li nasin Free(libre), li nasin Open-source, li lon tan mani tan jan pona mute. jan 719 li pana e mani la, lipu ni li lon:
mama|2|False|sina ken pana kin e pona tawa jan Pepa&soweli Kawa lon www.patreon.com/davidrevoy
