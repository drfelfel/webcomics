# Transcript of Pepper&Carrot Episode 21 [en]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Episode 21: The Magic Contest

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Writing|1|False|By Popular Demand The City of Komona Presents its Magic Contest
Writing|2|False|Grand Prize of 50 000Ko
Writing|3|False|For the most impressive magical demonstration! Entrance: 150Ko The new giant and secured arena Komona Azarday, 10 AirMoon
Writing|4|False|SPECIAL INVITATION
Pepper|5|False|I'm ready! Come on, Carrot. Let's go!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Still sure you don't want to come with us?
Cayenne|2|False|I'm certain. We do sorcery in Chaosah, not spectacles!
Writing|3|False|The KOMONAN
Writing|4|False|OPENING CEREMONY OF THE NEW ARENA TODAY
Writing|5|False|RESIDENTS WORRIED ABOUT THEIR TAXES
Writing|6|False|TICKETS SOLD OUT: SUCCESS GUARANTEED!
Writing|7|False|POLITICAL TENSIONS IN LAND OF AH
Writing|8|False|AH CURRENTLY OCCUPIES 80% OF TERRITORIES
Writing|9|False|YOU HAVE EXCELLENT VISION!
Pepper|10|True|Thanks for letting me participate!
Pepper|11|True|I'll win! I promise!
Pepper|12|False|See you this evening!
Cayenne|14|False|...
Sound|13|False|blam!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Audience|2|False|Clap
Mayor of Komona|1|False|As the Mayor of Komona, I declare the second Grand Magic Contest open!
Mayor of Komona|3|True|I know that you've waited impatiently for this event!
Mayor of Komona|4|False|Thank you for having travelled from the four corners of Hereva to discover our grand, ultra-secured arena!
Mayor of Komona|5|False|Let's move on to introduce our participants!
Carrot|6|True|Z
Carrot|7|True|Z|nowhitespace
Carrot|8|False|Z ...|nowhitespace
Pepper|9|True|Carrot?! What do you think you're doing?!
Pepper|10|True|Wake up! It's beginning!
Pepper|11|False|Everyone is watching us!
Sound|12|False|Tap!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Audience|3|False|Clap
Mayor of Komona|1|True|Adept of the mortally revivifying magic of "Zombiah", we are honored to welcome...
Mayor of Komona|2|False|CORIANDER!
Mayor of Komona|4|True|The flora and fauna hide no secrets for this witch of "Hippiah"! Please welcome...
Mayor of Komona|5|False|CAMOMILE!
Mayor of Komona|6|False|And now, thanks to the technical prowess of Komona, here, in a world exclusive...
Sound|7|False|Flop!
Mayor of Komona|8|True|...a witch from the watery depths who masters the formidable magic of "Aquah"!
Mayor of Komona|9|False|SPIRULINA!
Pepper|10|False|Aquah?! Here?! Incredible!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Audience|9|False|Clap
Carrot|1|False|Groooo
Pepper|2|True|Carrot!
Pepper|3|True|It's nearly our turn!
Pepper|4|True|Be patient!
Pepper|5|True|Please!
Pepper|6|False|Just for today!
Mayor of Komona|7|True|Witch of the spiritual and ascetic magic of "Ah", it is a pleasure to welcome among us...
Mayor of Komona|8|False|SHICHIMI!
Pepper|10|False|Carrot! NO!!!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Audience|7|False|Ha!
Mayor of Komona|1|False|And now, it is time to turn to this mysterious participant, who anonymously entered to show off the flamboyant magic of "Magmah"...
Mayor of Komona|3|True|OH! What a surprise! Yes, it's her! None other than...
Mayor of Komona|4|False|SAFFRON!
Sound|2|False|Flap!
Mayor of Komona|6|False|Ah! It seems a participant already has a problem!
Sound|5|False|Sploosh!
Mayor of Komona|8|False|Miss!! Please return to your place!!
Pepper|9|False|Caaarrott! NO!
Sound|10|False|Dzing!
Pepper|11|False|?!
Sound|12|False|Splash!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Ha-ha! Thanks, Spirulina! And sorry...
Pepper|2|True|Where are you going?!
Pepper|3|False|GET BACK HERE!
Mayor of Komona|4|True|Ahem! Here then is our final participant with the uh... indescribable... magic of "Chaosah"...
Mayor of Komona|5|False|PEPPER!
Mayor of Komona|6|False|As the main prize winner of the potion contest, we've decided to offer her a special position!
Pepper|7|False|?!...
Mayor of Komona|8|False|As such she will be...
Mayor of Komona|9|False|... part of our jury!
Sound|10|False|Bam
Audience|11|True|Clap
Audience|12|True|Clap
Audience|13|True|Clap
Audience|14|True|Clap
Audience|15|False|Clap
Writing|16|False|Magic Contest Official Jury
Pepper|17|False|Yay.
Writing|18|False|Queen Aiel
Writing|19|False|Pepper
Writing|20|False|Lord Azeirf
Narrator|21|False|TO BE CONTINUED...

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|02/2017 - www.peppercarrot.com - Art & Scenario: David Revoy - Translation: Alex Gryson
Credits|2|False|Brainstorming: Craig Maloney, Quiralta, Nicolas Artance, Talime, Valvin.
Credits|3|False|Dialog Improvements: Craig Maloney, Jookia, Nicolas Artance and Valvin.
Credits|4|False|Special Thanks: Inkscape team and especially Mc.
Credits|5|False|Based on the universe of Hereva created by David Revoy with contributions by Craig Maloney. Corrections by Willem Sonke, Moini, Hali, CGand and Alex Gryson.
Credits|6|False|Software: Krita 3.2.1, Inkscape 0.91 on Linux Mint 18.1 XFCE
Credits|7|False|Licence: Creative Commons Attribution 4.0
Credits|9|False|You too can become a patron of Pepper&Carrot at www.patreon.com/davidrevoy
Credits|8|False|Pepper&Carrot is entirely free(libre), open-source and sponsored thanks to the patronage of its readers. For this episode, thanks go to 816 Patrons:
