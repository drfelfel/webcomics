# Transcript of Pepper&Carrot Episode 25 [oc]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Títol|1|False|Episòdi 25 : I a pas de travèrsa

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Escritura|1|False|CROQUETAS

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrator|13|False|- FIN -
Pepper|1|True|?
Pepper|2|True|?
Pepper|3|True|?
Pepper|4|False|?
Pepper|5|True|?
Pepper|6|True|?
Pepper|7|True|?
Pepper|8|False|?
Pepper|9|True|?
Pepper|10|True|?
Pepper|11|True|?
Pepper|12|False|?

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crèdits|7|False|Vos tanben, venètz mecèna de Pepper&Carrot sus www.patreon.com/davidrevoy
Crèdits|6|False|Pepper&Carrot es completament liure, open source, e esponsorizat mercés al mecenat de sos lectors. Per aqueste episòdi, mercé als 909 mecènas :
Crèdits|1|False|05/2018 - www.peppercarrot.com - Dessenh & Scenari : David Revoy - traduction : Nicolas Artance
Crèdits|3|False|Basat sus l'univèrs d'Hereva creat per David Revoy amb las contribucions de Craig Maloney. Correccions de Willem Sonke, Moini, Hali, CGand e Alex Gryson.
Crèdits|4|False|Logicials : Krita 4.0.0, Inkscape 0.92.3 sus Kubuntu 17.10
Crèdits|5|False|Licéncia: Creative Commons Attribution 4.0
Crèdits|2|False|Relectura pendent la version beta : Nicolas Artance, Imsesaok, Craig Maloney, Midgard, Valvin, xHire e Zveryok.
