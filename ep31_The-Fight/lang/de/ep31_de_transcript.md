# Transcript of Pepper&Carrot Episode 31 [de]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 31: Der Kampf

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Erzähler|1|False|Tenebrume, heiliger Hügel von Chaosāh.
Geräusch|2|False|FRummm!
Pepper|3|False|Ts!
Geräusch|4|False|FRuummm
Geräusch|5|False|Bruzzz!
Pepper|6|False|Nimm das!
Geräusch|8|False|Schh!
Cayenne|9|False|Anfängerin!
Pepper|10|False|!!
Geräusch|11|False|KNALL!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geräusch|1|False|Drzzaaff!
Geräusch|2|False|KRACH!!!
Geräusch|3|False|KRAACH!!!
Pepper|4|False|Von wegen!
Pepper|5|False|GRAVITATIONAS SCHILDUS!
Geräusch|6|False|Wuuuusch!!
Geräusch|7|False|Tchkschkk!!
Geräusch|8|False|Tchkschkk!!
Geräusch|9|False|Tock!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|CARROT!
Pepper|2|False|Plan 7-B!
Pepper|4|False|JPEGUS QUALITIS!
Geräusch|5|False|Brzamm !|nowhitespace
Geräusch|3|False|Schwirr
Cayenne|6|False|?!!
Cayenne|10|False|Argh!!
Geräusch|7|True|G
Geräusch|8|True|Z|nowhitespace
Geräusch|9|False|Z|nowhitespace
Pepper|11|False|QUALITIS MINIMALIS!
Cayenne|12|False|!!
Cayenne|13|False|Grrr...
Schrift|14|False|2019-12-20-E31P03_V15-final.jpg
Schrift|15|False|Fehler beim Laden
Geräusch|16|False|KRACH !!!|nowhitespace

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayenne|1|False|PYRO BOMBA ATOMICUS!
Geräusch|2|False|Frrzuuuf!
Geräusch|3|True|K
Geräusch|4|True|A|nowhitespace
Geräusch|5|True|B|nowhitespace
Geräusch|6|True|U|nowhitespace
Geräusch|7|True|M|nowhitespace
Geräusch|8|True|M|nowhitespace
Geräusch|9|True|!|nowhitespace
Geräusch|10|False|!|nowhitespace
Geräusch|11|True|BRRR
Geräusch|12|False|BRRR
Geräusch|13|False|Pschh...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayenne|1|False|Pfff...
Cayenne|2|True|Flucht in eine Mikrodimension? In deinem Alter?
Cayenne|3|False|Was für eine erbärmliche Niederlage...
Pepper|4|True|Falsch!
Pepper|5|False|Ein Wurmloch!
Geräusch|6|False|Bzz!!
Pepper|7|False|Danke für den Ausgang, Carrot!
Pepper|8|False|Schachmatt, Meisterin Cayenne!
Pepper|9|False|GURGES...
Pepper|10|False|...ATER!
Geräusch|12|False|Schwwwuuuuuuppppp!!!
Geräusch|11|False|WUSCH!!!
Thymian|13|False|HALT!!!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Thymian|1|True|HALT habe ich gesagt!
Thymian|2|False|Das reicht!
Geräusch|3|False|Schnapp!
Geräusch|4|False|PUFFF!
Geräusch|5|False|TSCHK!
Thymian|6|True|Alle beide!
Thymian|7|True|HERKOMMEN!
Thymian|8|False|Sofort!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geräusch|1|False|Tip!
Geräusch|2|False|Tap!
Thymian|3|True|Es ist schon drei Jahre her, seit du "nur einen letzten Test" wolltest...
Thymian|4|False|...wir werden nicht die ganze Nacht hier verbringen!
Thymian|5|False|Nun...?
Pepper|6|False|...
Cayenne|7|True|Na gut...
Cayenne|8|True|...sie kann ihren Abschluss haben...
Cayenne|9|False|...allerdings mit der Note "ausreichend".
Schrift|10|True|Abschluss
Schrift|11|True|von
Schrift|12|False|Chaosāh
Schrift|14|False|Kümmmel
Schrift|13|False|Cayenne
Schrift|15|False|Thymian
Schrift|16|False|~ für Pepper ~
Schrift|17|False|Offizielle Hexe
Erzähler|18|False|- ENDE -

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|5|True|Du kannst auch Gönner von Pepper&Carrot werden und deinen Namen hier lesen!
Pepper|3|True|Pepper&Carrot ist vollständig frei(libre), Open Source und finanziert durch Spenden von Lesern.
Pepper|4|False|Für diese Episode danken wir 971 Gönnern!
Pepper|7|True|Geh auf www.peppercarrot.com für mehr Informationen!
Pepper|6|True|Wir sind auf Patreon, Tipeee, PayPal, Liberapay ...und mehr!
Pepper|8|False|Dankeschön!
Pepper|2|True|Wusstest du schon?
Impressum|1|False|20. Dezember 2019 Illustration & Handlung: David Revoy. Beta-Leser: Craig Maloney, Martin Disch, Arlo James Barnes, Nicolas Artance, Valvin. Deutsche Version Übersetzung: Ret Samys. Korrektur: Martin Disch. Basierend auf der Hereva-Welt Erstellung: David Revoy. Hauptbetreuer: Craig Maloney. Redakteure: Craig Maloney, Nartance, Scribblemaniac, Valvin. Korrektur: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Software: Krita 4.2.6appimage, Inkscape 0.92.3 auf Kubuntu 18.04-LTS. Lizenz: Creative Commons Namensnennung 4.0. www.peppercarrot.com
