# Transcript of Pepper&Carrot Episode 12 [la]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titulus|1|False|Episodium XII : Quomodo res disponantur autumno

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sonus|9|True|Cling
Sonus|10|True|Clang
Sonus|11|False|Clong
Caienna|1|False|Potiones cachinnum commoventes
Caienna|2|False|Potiones mega capillum facientes
Caienna|3|False|Potiones putentes globos facientes
Caienna|4|False|Potiones « vita rosea »
Caienna|5|False|Potiones fumum generantes...
Caienna|6|False|... et caetera !
Piper|7|True|Ita, ita, scio:
Piper|8|False|"Non-oportet-veram-Chaosahis-magam-istas-potiones-facere".
Caienna|13|True|Debeo in forum Komonae ire.
Caienna|14|False|Me absente, remove omnia ista e conspectu. Damnum sit quod juvenes nequam cum ista ludant.
Caienna|12|False|Ita, et sic optime est.

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sonus|2|False|Ziuuum !|nowhitespace
Piper|3|False|Ecastor!
Piper|4|True|Omnia obruere ?!
Piper|5|False|Non modum longius erit sed etiam pustulas manis dabit!
Sonus|6|False|Pum
Piper|1|False|Removeam e conspectu? Facile! Ehem!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Piper|1|False|"Et noli magia uti: vera maga Chaosahis magia ad cotidiana opera conficienda non utetur."
Piper|2|False|Grrr !
Piper|3|True|CAROTA !
Piper|4|False|Septimum librum Chaosahis “de campis gravitatis” apellatur... ...cito!
Piper|5|False|Monstrabo isti frutici quid vera maga Chaosahis sit !
Sonus|6|True|Tam
Sonus|7|False|Tam

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sonus|2|False|ROOoooooooooo|nowhitespace
Caienna|3|False|... Foramen atrum Chaosahis ?!...
Piper|4|False|?!
Caienna|5|False|... Est quid intellexisti per “noli magia uti”?
Piper|6|False|... Attat... Non debeas in foro esse ?
Caienna|7|True|Minime vero!
Caienna|8|False|Et ratione feci quia sine custodia, non possumus te relinquere !
Piper|1|False|𐌂𐌏𐌓𐌂𐌄𐌔 𐌀𐌕𐌄𐌓!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sonus|1|False|vooovoovoovoo|nowhitespace
Sonus|7|False|vooovoovoovoo|nowhitespace
Sonus|6|False|Bam
Carota|10|False|?!
Caienna|2|False|... Ecce, specta istud..
Caienna|3|True|Massa non sufficiens !
Caienna|4|True|Campus gravitatis distinctus nimis gracilis!
Caienna|5|False|Etiam extrema stabilis circularis orbita nimis parva est!
Caienna|9|False|Si tamen horizontem eventuum paulo maiorem fecisses, optimus fuisset!
Caienna|8|False|Ecce: paene omnia gravitabunt in stabila orbita vel in punctis Lagrangianis et circum ibunt, fluctuantes.

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Piper|1|False|!!!|nowhitespace
Caienna|2|False|!!!|nowhitespace
Caienna|3|False|𐌂𐌏𐌓𐌔𐌏𐌔 𐌀𐌍𐌍𐌏𐌋𐌀𐌕𐌠𐌏𐌍𐌏𐌔 𐌑𐌀𐌢𐌉𐌑𐌏𐌔 !
Sonus|4|False|ChKlak !|nowhitespace
Narrator|5|False|- FINIS -
Credits|6|False|M.X A.MMXV - Dessin & Scénario : David Revoy, lingua latina translatio : Benjamin Touati, correctio: Valentine Guillocheau

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|3|False|https://www.patreon.com/davidrevoy
Credits|4|False|Licentia : Creative Commons Attribution 4.0 Fontes : inveniuntur apud www.peppercarrot.com Instrumenta : Hoc episodium omnino designatum est liberis programmatibus Krita 2.9.8, Inkscape 0.91, Blender 2.71, GIMP 2.8.14, G'MIC 1.6.7 in Linux Mint 17
Credits|1|False|Piper&Carota gratis constat, omnno apertum fontis est ac juvatur maecenatu lectorum ; in hoc episodium, gratias ago illis 575 Maecenatibus :
Credits|2|True|Tu quoque, potes fieri maecenas Piperis&Carotae sequenti episodio :
