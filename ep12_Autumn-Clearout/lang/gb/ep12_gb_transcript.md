# Transcript of Pepper&Carrot Episode 12 [gb]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titulo|1|False|Mon 12: Safegi fe Xuhamosem

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Soti|9|True|Kling
Soti|10|True|Klang
Soti|11|False|Klong
Kayena|1|False|Iksir fe Haha,
Kayena|2|False|Iksir fe Belo-Dayxunjan,
Kayena|3|False|Iksir fe Burnasacu-bulbula,
Kayena|4|False|Iksir fe Otimaismo,
Kayena|5|False|Iksir fe Dudan...
Kayena|6|False|... cel na zekaru sol bannumer to!
Pilpil|7|True|Si, mi jixi:
Pilpil|8|False|"Awtenti jumunyen of Kaosa ger no krea hin tipo fe iksir."
Kayena|13|True|Mi haja na idi cel bazar in Komona.
Kayena|14|False|Am awgi moy hinto durki mi awsen; to ger no sen bon eger lil humoryen ewreka hinto, ji ete karar na yuxi yon to.
Kayena|12|False|Preciso. To ingay na sen hinmaner.

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Soti|2|False|Ziooum!
Pilpil|3|False|Burxanse!
Pilpil|4|True|Kam na inturan moyto?!
Pilpil|5|False|Mas, dento ger dure plu satu, ji mi am no zekaru pifu-bulbula hu imi ger hare da fe fini!
Soti|6|False|Pum
Pilpil|1|False|Kam mi am awgi to? Asan! Hura!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pilpil|1|False|"Ji am no yongu magika fe he ban halu. Awtenti jumunyen of Kaosa no yongu magika cel moydinli ergomon."
Pilpil|2|False|Grrr!
Pilpil|3|True|KAROTE!
Pilpil|4|False|Kitabu sabe de Kaosa: "junluku-medan"... ...fori!
Pilpil|5|False|Mi xa onexa tas den lao ganxopune kraw ku awtenti jumunyen of Kaosa sen kepul te!
Soti|6|True|Tam
Soti|7|False|Tam

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Soti|2|False|VRUUuuuuuuuuuu
Kayena|3|False|... Syahe-hongu fe Kaosa?!
Pilpil|4|False|?!
Kayena|5|False|... Dento real sen to hu yu aham da yon: "am no yongu magika"?
Pilpil|6|False|... Hey... Kam yu no ingay na sen in bazar?
Kayena|7|True|Mimbay no!
Kayena|8|False|Ji maxpul, mi le sahiloga; to jandan sen nenible na resta yu nenyon estokal supraoko!
Pilpil|1|False|GURGES ATER!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Soti|1|False|wuuuwuuwuuwuu
Soti|7|False|wuuuwuuwuuwuu
Soti|6|False|Bam
Kayena|8|True|Ji prehay: Kriban moyto xa junluku cel andin weydao or Lagranjli tyan, ji to xa resta fe na weyflota.
Karote|10|False|?!
Kayena|2|True|... Am sol oko hinto.
Kayena|3|True|Nenkufi mase!
Kayena|4|True|Daif diferensyal fe junluku-medan!
Kayena|5|False|Hata maxim ner andin dayrapul weydao sen godomo lil!
Kayena|9|False|Hata sol lilmo maxmo day okurxey-ufuku ger le sen suksespul!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pilpil|1|False|!!!
Kayena|2|False|!!!
Kayena|3|False|CURSUS CANCELLARE MAXIMUS!
Soti|4|False|EsKlak!
Narrator|5|False|- FIN -
Credits|6|False|10/2015 - Art & Scenario: David Revoy, English Translation: Hector Ortega

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Pepper&Carrot is entirely free(libre), open-source and sponsored thanks to the patronage of its readers. For this episode, thank you to the 575 Patrons:
Credits|3|False|https://www.patreon.com/davidrevoy
Credits|2|True|You too can become a patron of Pepper&Carrot for the next episode at
Credits|4|False|License: Creative Commons Attribution 4.0 Source: available at www.peppercarrot.com Software: this episode was 100% drawn with libre software Krita 2.9.8, Inkscape 0.91, Blender 2.71, GIMP 2.8.14, G'MIC 1.6.7 on Linux Mint 17.2
