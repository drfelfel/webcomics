# Transcript of Pepper&Carrot Episode 12 [da]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 12: Efterårsoprydning

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|9|True|Kling
Lyd|10|True|Klang
Lyd|11|False|Klong
Cayenne|1|False|Latter-eliksirer
Cayenne|2|False|Megahårvækst-eliksirer
Cayenne|3|False|Stinkbombe-eliksirer
Cayenne|4|False|”Den lyse side”-eliksirer
Cayenne|5|False|Røg-eliksirer...
Cayenne|6|False|... og så videre!
Pepper|7|True|Ja ja, jeg ved det:
Pepper|8|False|”En rigtig Kaosah-heks skal ikke lave den slags eliksirer”
Cayenne|13|True|Jeg må tage en lille tur til markedet i Komona
Cayenne|14|False|Få det til at forsvinde mens jeg er borte; det ville være uheldigt hvis nogle små kanaljer legede med det
Cayenne|12|False|Præcis. Og det er meget bedre sådan.

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|2|False|Woosh!
Pepper|3|False|Pokkers!
Pepper|4|True|”Grave alt ned”?!
Pepper|5|False|Men det vil tage en krig; uden at nævne vabler på hænderne!
Lyd|6|False|Bong
Pepper|1|False|”Få det til at forsvinde”? Det er nemt! Juhuu!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|"Og selvfølgelig skal du ikke bruge magi. En ægte Kaosah-heks bruger ikke magi til de daglige pligter"
Pepper|2|False|Grrr!
Pepper|3|True|CARROT!
Pepper|4|False|Kaosahs syvende bog: ”Gravitationsfelterne”... ... og med det samme!
Pepper|5|False|Jeg skal vise den gamle krage hvad en ægte Kaosah-heks er!
Lyd|6|True|Tap
Lyd|7|False|Tap

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|2|False|VROOoooooooooo
Cayenne|3|False|... Et kaosiansk sort hul?! ...
Pepper|4|False|?!
Cayenne|5|False|... Er det virkelig hvad du forstår ved: "Brug ikke magi"?...
Pepper|6|False|… Øh… Skulle du ikke på marked?
Cayenne|7|True|Selvfølgelig ikke!
Cayenne|8|False|Og det gjorde jeg ret i; det er umuligt at lade dig uden opsyn!
Pepper|1|False|GURGES ATER!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|1|False|wooowoowoowoo
Lyd|7|False|wooowoowoowoo
Lyd|6|False|Bum
Cayenne|8|True|?!
Carrot|10|False|… Og se nu bare det dér
Cayenne|2|True|Utilstrækkelig masse!
Cayenne|3|True|Svag gravitationsfeltsgradient!
Cayenne|4|True|Selv den nærmeste stabile omløbsbane er for lille
Cayenne|5|False|En bare lidt bredere begivenheds-horisont ville have virket
Cayenne|9|False|Dér ser du: Næsten alt bliver fanget i den stabile omløbsbane eller i Lagrange-punkter og bliver ved med at flyde rundt.

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|!!!
Cayenne|2|False|!!!
Cayenne|3|False|INCANTATEM ANNULATIONUS MAXIMUS!
Lyd|4|False|ShKlak!
Fortæller|5|False|- Slut -
Credits|6|False|10/2015 – Tegning og manuskript: David Revoy – Oversættelse: Emmiline, Rikke & Alexandre Alapetite

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|https://www.patreon.com/davidrevoy
Credits|3|False|Licens: Creative Commons Kreditering 4.0 Kildematerialet: tilgængelige på www.peppercarrot.com Værktøj: denne episode er 100% designet med fri software: Krita 2.9.8, Inkscape 0.91, Blender 2.71, GIMP 2.8.14, G'MIC 1.6.7 på Linux Mint 17
Credits|2|True|Pepper&Carrot er helt gratis, open-source, og sponsoreret af læsere. Denne episode kunne ikke være blevet til uden støtte fra de 575 tilhængere:
Credits|4|False|Støt næste episode af Pepper&Carrot; hver en krone gør en forskel
