# Transcript of Pepper&Carrot Episode 34 [cn]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
标题|1|False|第34集：七味粉的册封仪式

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
旁白|1|False|当天晚上…
Hibiscus|2|False|…七味粉，今晚你将以 最年轻的骑士的身份 进入阿魔法界。
香菜叶|3|False|还没有 小辣椒 的消息么？
藏红花|4|True|没有。
藏红花|5|False|她最好是赶紧过来， 否则就错过七味粉的 册封感言了。
七味粉|6|True|谢谢。
七味粉|7|False|我想……

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
声音|1|False|嗖~~~~
声音|2|False|嗖~~~~~~~
小辣椒|3|True|前面危险！
小辣椒|4|True|小心！
小辣椒|5|False|小心！！！
声音|6|False|砰！~~~
小辣椒|7|False|哎呦喂！
小辣椒|8|False|大家都还好？ 没有人受伤？
七味粉|9|False|小辣椒！
小辣椒|10|True|好呀，七味粉！
小辣椒|11|True|抱歉，我不仅来晚了， 而且入场还有些戏剧性。
小辣椒|12|False|我今天跑了一整天， 说来话长。

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Hibiscus|1|False|这位是你邀请的？
七味粉|2|False|对。是我的朋友 小辣椒。 没问题的。
小辣椒|3|True|萝卜头， 还好吧？
小辣椒|4|False|不好意思， 超速飞行的降落 我掌握的还不够好。
小辣椒|5|True|再次抱歉打扰了会场，
小辣椒|6|False|还有我的衣着也……
七味粉|7|False|嘿嘿…
绿芥末|8|True|七味粉，
绿芥末|9|False|刚刚入场的这位年轻女士， 确实是你的朋友么？

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
七味粉|1|True|是的，殿下。
七味粉|2|False|她叫小辣椒， 来自混沌阿学校。
绿芥末|3|True|她的出现玷污了 本校圣洁的氛围。
绿芥末|4|False|立即将她 带出我的视线。
七味粉|5|True|可是…
七味粉|6|False|绿芥末导师…
绿芥末|7|True|可是什么？
绿芥末|8|False|你总不会 想被学校开除吧？
七味粉|9|False|!!!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
七味粉|1|True|对不起小辣椒， 你得离开这里。
七味粉|2|False|马上。
小辣椒|3|True|哎？
小辣椒|4|False|等等，等等。 我肯定这里有误会。
七味粉|5|False|求你了小辣椒。 别再为难我了。
小辣椒|6|False|嘿，坐在上面那位！你 要是和我有冤有仇， 就直接下来 亲自和我说！
绿芥末|7|False|呵……
绿芥末|8|True|七味粉， 给你十秒钟…
绿芥末|9|True|九….
绿芥末|10|True|八…
绿芥末|11|False|七…
七味粉|12|False|够了， 小辣椒， 给我走！
声音|13|False|闪光~~~

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
声音|9|True|啪 !
小辣椒|1|False|七味粉， 请冷静……
声音|2|False|砰~~~
七味粉|3|True|走呀！！！
七味粉|4|True|走呀！！！
七味粉|5|False|走呀！！
声音|6|False|卡啦啦~~~
小辣椒|7|True|哎呦！
小辣椒|8|False|嘿！这样就… 哎呦… 不够朋友了！
声音|10|True|砰 !
声音|11|True|砰 !
声音|12|False|啪 !
香菜叶|13|False|七味粉！小辣椒！ 你们住手呀！
藏红花|14|False|等会儿。
绿芥末|15|False|哼！
小辣椒|16|True|好！
小辣椒|17|False|这是你 自找的！
声音|18|False|嗡~~

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
小辣椒|1|False|大幅魔咒消除 ！！！
声音|2|False|飞出~~
小辣椒|3|False|哎呦！
声音|4|False|啪！
七味粉|5|False|就算你最厉害的 消除咒对我也无效！
七味粉|6|True|放弃吧，小辣椒， 走啊！
七味粉|7|False|不要逼我伤害你！
小辣椒|8|False|我的消除咒还是很有效的。 只不过我的目标不是你。
七味粉|9|True|嗯？
七味粉|10|False|你什么意思？
声音|11|True|噗嗤~~
声音|12|False|噗嗤~

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
七味粉|1|False|?!!
小辣椒|2|True|我的目标 是她！
小辣椒|3|False|我只不过消除了她 施加在自己身上的 让她看起来更年轻的 障眼法。
小辣椒|4|True|我刚到这里 就感到了 这个咒语。
小辣椒|5|False|而这也是 你教唆七味粉攻击我的 代价。
绿芥末|6|True|大胆！
绿芥末|7|True|你胆敢如此！
绿芥末|8|False|在众人面前
小辣椒|9|True|庆幸吧，
小辣椒|10|False|如果我的法力值更高， 结果将远远不止如此。

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
声音|1|False|变化~~
绿芥末|2|True|很好，你用更短的时间 到达了你上一任的等级, 这是我没有料到的…
绿芥末|3|False|这让我不得不提前下手， 但这是个好消息。
小辣椒|4|True|下手？
小辣椒|5|True|所以你就是在试探我， 跟我迟到这件事无关？
小辣椒|6|False|你真够变态！
绿芥末|7|True|嘿….
绿芥末|8|False|嘿。
绿芥末|9|True|你们这群人都在看什么呢？
绿芥末|10|False|我刚刚受到袭击， 你们一个个都无动于衷么？ 给我抓住她！！
绿芥末|11|False|要活口！
绿芥末|12|False|抓住她！！！
小辣椒|13|False|七味粉， 我们后会有期。
小辣椒|14|True|抱歉萝卜头，我们 不得不再一次 超速飞行了。
小辣椒|15|False|抓紧了。
声音|16|False|爬~~
声音|17|False|爬~
声音|18|False|飞起~~~

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
小辣椒|1|False|?!
绿芥末|2|False|抓住她！！！
小辣椒|3|False|糟糕。
藏红花|4|False|小辣椒，接住我的扫帚！
声音|5|False|飞出！
小辣椒|6|True|哦，太好了！
小辣椒|7|False|谢谢。 藏红花！
声音|8|False|抓住~
声音|9|False|冲出云霄！！！
旁白|10|False|未完待续……

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
参与者名单|1|False|2021年3月31日 绘画与脚本：David Revoy. 测试版读者 : Arlo James Barnes, Carotte, Craig Maloney, Efrat b, GunChleoc, Karl Ove Hufthammer, Martin Disch, Nicolas Artance, Parnikkapore, Valvin. 简体中文版 翻译与校对：庄冉 特别鸣谢：Nicolas Artance 基于艾河洼虚拟世界 创作 : David Revoy. 主维护 : Craig Maloney. 编辑 : Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. 校对 : Willem Sonke, Moini, Hali, CGand, Alex Gryson. 软件：Krita 4.4.1，Inkscape 1.0.2， Kubuntu Linux 20.04. 版权：Creative Commons Attribution 4.0. www.peppercarrot.com
小辣椒|2|True|您知道吗？
小辣椒|3|True|小辣椒与萝卜头是完全自由， 免费且开源的。 项目资金由读者捐助。
小辣椒|4|False|本集收到了1096位 捐助者！
小辣椒|5|True|您也可以成为小辣椒 与萝卜头的捐助者， 并在此看到您的名字！
小辣椒|6|True|我们的捐助平台有 Patreon, Tipeee, PayPal, Liberapay 及其它！
小辣椒|7|False|请关注网站 www.peppercarrot.com 获取更多信息 !
小辣椒|8|False|十分感谢！
