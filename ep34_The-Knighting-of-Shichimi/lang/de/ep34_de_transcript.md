# Transcript of Pepper&Carrot Episode 34 [de]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 34: Shichimis Abschluss

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Erzähler|1|False|Noch in der selben Nacht...
Hibiscus|2|False|...somit nehmen wir dich auf, Shichimi, als unsere jüngste Ritterin von Ah.
Coriander|3|False|Immer noch keine Spur von Pepper?
Saffron|4|True|Noch nicht.
Saffron|5|False|Wenn sie sich nicht beeilt, verpasst sie Shichimis Rede.
Shichimi|6|True|Danke.
Shichimi|7|False|Ich würde gern...

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geräusch|1|False|ZischhHHHHH
Geräusch|2|False|ZischHHHHH
Pepper|3|True|Ich komme!
Pepper|4|True|Aufgepasst!
Pepper|5|False|Achtung!!!
Geräusch|6|False|KRACH!
Pepper|7|False|Ups!
Pepper|8|False|Alles in Ordnung? Nichts gebrochen?
Shichimi|9|False|Pepper!
Pepper|10|True|Hallo Shichimi!
Pepper|11|True|Entschuldige den dramatischen Auftritt und die Verspätung!
Pepper|12|False|Bin den ganzen Tag rumgerannt, ist eine lange Geschichte.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Hibiscus|1|False|Hast du sie eingeladen?
Shichimi|2|False|Ja. Das ist meine Freundin Pepper. Alles gut.
Pepper|3|True|Carrot, alles in Ordnung?
Pepper|4|False|Die Bruchlandung tut mir leid. Leider habe ich das Hyper-tempo noch nicht ganz im Griff.
Pepper|5|True|Ich entschuldige mich nochmal für die Umstände
Pepper|6|False|und was meine Kleidung angeht...
Shichimi|7|False|hi hi
Wasabi|8|True|Shichimi,
Wasabi|9|False|diese junge Hexe, die gerade angekommen ist, ist sie wirklich eine Freundin von dir?

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|True|Ja, Eure Hoheit.
Shichimi|2|False|Ihr Name ist Pepper, von der Chaosāh-Schule.
Wasabi|3|True|Ihre Anwesenheit besudelt die Heiligkeit unserer Schule.
Wasabi|4|False|Schafft sie mir aus meinem Blickfeld, sofort.
Shichimi|5|True|Aber...
Shichimi|6|False|Meisterin Wasabi...
Wasabi|7|True|Aber was?
Wasabi|8|False|Würdest du lieber von unserer Schule verstoßen werden?
Shichimi|9|False|!!!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|True|Tut mir leid Pepper, du musst gehen.
Shichimi|2|False|Und zwar jetzt.
Pepper|3|True|Hä?
Pepper|4|False|Hey hey hey, Moment mal! Das ist doch bestimmt ein Missverständnis.
Shichimi|5|False|Pepper, bitte, mach es nicht noch schwieriger.
Pepper|6|False|He! Ihr da, auf dem Thron. Wenn Ihr ein Problem mit mir habt, kommt gefälligst runter und sagt es mir selbst!
Wasabi|7|False|Tsss...
Wasabi|8|True|Shichimi, du hast zehn Sekunden...
Wasabi|9|True|neun...
Wasabi|10|True|acht...
Wasabi|11|False|sieben...
Shichimi|12|False|ES REICHT, PEPPER! VERSCHWINDE!!!
Geräusch|13|False|ZZZZIiii!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geräusch|9|True|PAF!
Pepper|1|False|Shichimi, beruhige d...
Geräusch|2|False|BADuuM!
Shichimi|3|True|GEH WEG!!!
Shichimi|4|True|GEH WEG!!!
Shichimi|5|False|GEH WEG!!!
Geräusch|6|False|KREISCH!!!
Pepper|7|True|Aua!
Pepper|8|False|He! Das ist... n-NICHT... Auu... Nett!
Geräusch|10|True|POF!
Geräusch|11|True|POF!
Geräusch|12|False|PAF!
Coriander|13|False|SHICHIMI! PEPPER! BITTE HÖRT AUF!
Saffron|14|False|Warte.
Wasabi|15|False|Hmm!
Pepper|16|True|Grrr!
Pepper|17|False|Na gut, du hast es so gewollt!
Geräusch|18|False|BRUZZ!!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|CURSUS CANCELLARE MAXIMUS!!!
Geräusch|2|False|SCHAKA!
Pepper|3|False|Auu!
Geräusch|4|False|PAF!!
Shichimi|5|False|Sogar dein bester Aufhebungsfluch ist gegen mich wirkungslos!
Shichimi|6|True|Gib auf, Pepper, und verschwinde von hier!
Shichimi|7|False|Ich will dir nicht weiter wehtun!
Pepper|8|False|Oh, mein Aufhebungs-fluch klappt einwandfrei, dich wollte ich aber gar nicht treffen.
Shichimi|9|True|Was?
Shichimi|10|False|Wie meinst du das?!
Geräusch|11|True|Fschh...
Geräusch|12|False|Fschh...

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|False|?!!
Pepper|2|True|Der Fluch war für sie!
Pepper|3|False|Ich habe nur den Blendezauber auf-gehoben, durch den sie ihr junges Aussehen bewahrt.
Pepper|4|True|Den Zauber habe ich gleich bei meiner Ankunft bemerkt.
Pepper|5|False|Deshalb habe ich Euch einen kleinen Geschmack davon gegeben, was Ihr dafür verdient, Shichimi gegen mich kämpfen zu lassen!
Wasabi|6|True|FRECHHEIT!
Wasabi|7|True|Wie kannst du es wagen,
Wasabi|8|False|und das auch noch vor meiner Schule!
Pepper|9|True|Ihr könnt froh sein!
Pepper|10|False|Wenn ich all mein Rea hätte, wäre das nur der Anfang.

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geräusch|1|False|DZZUH!!
Wasabi|2|True|Verstehe. Deine Fähigkeiten sind denen deiner Vor-gängerinnen viel früher als erwartet ebenbürtig geworden...
Wasabi|3|False|Das beschleunigt zwar mein Vorhaben, aber es kommt mir gelegen.
Pepper|4|True|Euer Vorhaben?
Pepper|5|True|Das war also alles nur ein Test und hatte nichts damit zu tun, dass ich zu spät war?
Pepper|6|False|Ihr seid ja total hinterhältig!
Wasabi|7|True|hee...
Wasabi|8|False|hee.
Wasabi|9|True|WAS STARRT IHR SO RUM?!
Wasabi|10|False|ICH WURDE GERADE ANGEGRIFFEN UND IHR STEHT NUR DA UND MACHT NICHTS?! SCHNAPPT SIE!!!
Wasabi|11|False|ICH WILL SIE LEBEND!
Wasabi|12|False|AUF SIE!!!
Pepper|13|False|Shichimi, darüber müssen wir später reden!
Pepper|14|True|Tut mir leid, Carrot, aber wir brauchen wohl wieder das Hypertempo.
Pepper|15|False|Halt dich fest!
Geräusch|16|False|Tap!
Geräusch|17|False|Tap!
Geräusch|18|False|ZischHH!!

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|?!
Wasabi|2|False|SCHNAPPT SIE!!!
Pepper|3|False|Oh weh.
Saffron|4|False|Pepper, nimm meinen Besen!
Geräusch|5|False|Fizzz!
Pepper|6|True|Oh klasse!
Pepper|7|False|Danke, Saffron!
Geräusch|8|False|Tock!
Geräusch|9|False|zischHHH!
Erzähler|10|False|FORTSETZUNG FOLGT...

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Impressum|1|False|31. März, 2021 Illustration & Handlung: David Revoy. Beta-Leser: Arlo James Barnes, Carotte, Craig Maloney, Efrat b, GunChleoc, Karl Ove Hufthammer, Martin Disch, Nicolas Artance, Parnikkapore, Valvin. Deutsche Version Übersetzung: Martin Disch, Ret Samys. Korrektur: Alina The Hedgehog. Spezieller Dank: An Nartance für das Vertiefen des Charakters von Wasabi in seinen Fan-Fiction Geschichten. Wie er sie sich vorstellte, hatte einen grossen Einfluss auf die Darstellung in dieser Episode. Basierend auf der Hereva-Welt Erstellung: David Revoy. Hauptbetreuer: Craig Maloney. Redakteure: Craig Maloney, Nartance, Scribblemaniac, Valvin. Korrektur: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Software: Krita 4.4.1, Inkscape 1.0.2 auf Kubuntu Linux 20.04. Lizenz: Creative Commons Namensnennung 4.0. www.peppercarrot.com
Pepper|2|True|Wusstest du schon?
Pepper|3|True|Pepper&Carrot ist vollständig frei(libre), Open Source und finanziert durch Spenden von Lesern.
Pepper|4|False|Für diese Episode danken wir 1096 Gönnern!
Pepper|5|True|Du kannst auch Gönner von Pepper&Carrot werden und deinen Namen hier lesen!
Pepper|6|True|Wir sind auf Patreon, Tipeee, PayPal, Liberapay ...und mehr!
Pepper|7|False|Geh auf www.peppercarrot.com für mehr Informationen!
Pepper|8|False|Dankeschön!
