# Transcript of Pepper&Carrot Episode 34 [pt]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Título|1|False|Episódio 34: A Condecoração da Cavaleira Shichimi

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrador|1|False|Naquela mesma noite...
Hibiscus|2|False|...e então, nesta noite, nós te recebemos, Shichimi, como a nossa mais jovem Cavaleira de Ah.
Coriander|3|False|Ainda sem sinal da Pepper?
Saffron|4|True|Ainda não.
Saffron|5|False|É melhor ela se apressar, senão ela vai perder o discurso da Shichimi.
Shichimi|6|True|Obrigada.
Shichimi|7|False|Eu gostaria de...

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Som|1|False|ZiooOOOOO
Som|2|False|ZiooOOOOO
Pepper|3|True|Olha eu aí!
Pepper|4|True|Cuidado!
Pepper|5|False|CUIDADO!!!
Som|6|False|CRASH!
Pepper|7|False|Ops!
Pepper|8|False|Está todo mundo bem? Não quebrei nada?
Shichimi|9|False|Pepper!
Pepper|10|True|Oi Shichimi!
Pepper|11|True|Desculpa pela entrada dramática e pelo atraso!
Pepper|12|False|Eu estive correndo o dia todo - é uma longa história.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Hibiscus|1|False|Ela é uma das suas convidadas?
Shichimi|2|False|Sim. Ela é a minha amiga Pepper. Está tudo bem.
Pepper|3|True|Carrot, você está bem?
Pepper|4|False|Desculpe pela aterrissagem, eu ainda não dominei pousar enquanto uso a hipervelocidade.
Pepper|5|True|Desculpa de novo a todos pela confusão,
Pepper|6|False|e por como eu estou vestida...
Shichimi|7|False|Hi hi hi
Wasabi|8|True|Shichimi,
Wasabi|9|False|essa jovem bruxa que acabou de chegar, ela realmente é uma amiga sua?

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|True|Sim, vossa alteza.
Shichimi|2|False|O nome dela é Pepper, da Escola de Chaosah.
Wasabi|3|True|A presença dela aqui polui a natureza sagrada da nossa escola.
Wasabi|4|False|Tire ela da minha frente, imediatamente.
Shichimi|5|True|Mas...
Shichimi|6|False|Mestra Wasabi...
Wasabi|7|True|Mas o quê?
Wasabi|8|False|Ou você prefere ser banida da nossa escola?
Shichimi|9|False|!!!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|True|Desculpa Pepper, mas você tem que ir embora.
Shichimi|2|False|Agora.
Pepper|3|True|Hã?
Pepper|4|False|Ei ei ei, espera aí um minuto! Eu acho que deve ter havido algum mal-entendido.
Shichimi|5|False|Por favor Pepper, não torne isto difícil.
Pepper|6|False|Ei! Você aí no trono. Se você tiver algum problema comigo desça daí e me fale pessoalmente!
Wasabi|7|False|Tsss...
Wasabi|8|True|Shichimi, você tem dez segundos...
Wasabi|9|True|nove...
Wasabi|10|True|oito...
Wasabi|11|False|sete...
Shichimi|12|False|CHEGA, PEPPER! VÁ EMBORA!!!
Som|13|False|SHRRIiii!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Som|9|True|PAF!
Pepper|1|False|Shichimi, por favor se acal...
Som|2|False|BADuuM!
Shichimi|3|True|VÁ EMBORA!!!
Shichimi|4|True|VÁ EMBORA!!!
Shichimi|5|False|VÁ EMBORA!!!
Som|6|False|CREEEEE!!!
Pepper|7|True|Ai!
Pepper|8|False|Ei! Isso... n-NÃO... é... ai... Legal!
Som|10|True|POF!
Som|11|True|POF!
Som|12|False|PAF!
Coriander|13|False|SHICHIMI! PEPPER! PAREM POR FAVOR!
Saffron|14|False|Espera.
Wasabi|15|False|Hmm!
Pepper|16|True|Grrr!
Pepper|17|False|Tá bom, você pediu por isso!
Som|18|False|BRZOO!!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|CURSUS CANCELLARE MAXIMUS!!!
Som|2|False|SHKLAK!
Pepper|3|False|Aii!
Som|4|False|PAF!!
Shichimi|5|False|Nem o seu melhor feitiço de cancelamento tem efeito em mim!
Shichimi|6|True|Desista, Pepper, e vá embora!
Shichimi|7|False|Pare de fazer eu te machucar!
Pepper|8|False|Ah, o meu cancelamento funcionou perfeitamente, só que o alvo não era você.
Shichimi|9|True|Hã?
Shichimi|10|False|Como assim?!
Som|11|True|Pshh...
Som|12|False|Pshh...

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|False|?!!
Pepper|2|True|Ela era o alvo!
Pepper|3|False|Eu só cancelei o seu Feitiço de Glamour, que a faz parecer uma jovem.
Pepper|4|True|Eu percebi esse feitiço logo que eu cheguei aqui.
Pepper|5|False|Então, eu te dei uma pequena amostra do que você merece por fazer a Shichimi lutar comigo!
Wasabi|6|True|INSOLENTE!
Wasabi|7|True|Como ousa,
Wasabi|8|False|em frente de toda a minha escola!
Pepper|9|True|Considere-se sortuda!
Pepper|10|False|Se eu tivesse toda a minha Rea, eu teria ido além.

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Som|1|False|DRZOW!!
Wasabi|2|True|Eu vejo que você chegou no nível das suas antecessoras mais cedo do que eu antecipava...
Wasabi|3|False|Mas que notícia ótima, isso acelera os meus planos.
Pepper|4|True|Seus planos?
Pepper|5|True|Então você só estava me testando, e isso não tem nada a ver comigo estar atrasada?
Pepper|6|False|Você é doida mesmo!
Wasabi|7|True|He...
Wasabi|8|False|he.
Wasabi|9|True|O QUE VOCÊS TODOS ESTÃO OLHANDO?!
Wasabi|10|False|EU ACABEI DE SER ATACADA, E VOCÊS NÃO FAZEM NADA SENÃO FICAR PARADOS?! PEGUEM-NA!!!
Wasabi|11|False|EU A QUERO VIVA!
Wasabi|12|False|PEGUEM-NA!!!
Pepper|13|False|Shichimi, nós temos que conversar sobre isso mais tarde!
Pepper|14|True|Desculpa, Carrot, mas nós precisamos decolar de novo em hipervelocidade.
Pepper|15|False|Segura firme!
Som|16|False|Tap!
Som|17|False|Tap!
Som|18|False|ZiiooOO!!

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|?!
Wasabi|2|False|PEGUEM-NA!!!
Pepper|3|False|Ah, não.
Saffron|4|False|Pepper, pega minha vassoura!
Som|5|False|Fizzz!
Pepper|6|True|Epa!
Pepper|7|False|Obrigada, Saffron!
Som|8|False|Toc!
Som|9|False|ziooOOOO!
Narrador|10|False|CONTINUA...

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Créditos|1|False|31 de março de 2021 Arte e roteiro: David Revoy. Leitores beta: Arlo James Barnes, Carotte, Craig Maloney, Efrat b, GunChleoc, Karl Ove Hufthammer, Martin Disch, Nicolas Artance, Parnikkapore, Valvin. Versão em Português Tradução: Alexandre E. Almeida. Agradecimentos especiais: ao Nartance por explorar a personangem Wasabi em seus romances fan-fics. A maneira como ele a imaginou teve um grande impacto em como eu a retratei neste episódio. Baseado no universo de Hereva Criador: David Revoy. Mantenedor principal: Craig Maloney. Escritores: Craig Maloney, Nartance, Scribblemaniac, Valvin. Corretores: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Software: Krita 4.4.1, Inkscape 1.0.2 no Kubuntu Linux 20.04. Licença: Creative Commons Attribution 4.0. www.peppercarrot.com
Pepper|2|True|Você sabia?
Pepper|3|True|Pepper&Carrot é totalmente livre, com código aberto e patrocinada graças à gentil contribuição dos seus leitores.
Pepper|4|False|Para este episódio, agradeçemos a 1096 patronos!
Pepper|5|True|Você também pode se tornar um(a) patrono(a) de Pepper&Carrot e ter o seu nome aqui!
Pepper|6|True|Nós estamos no Patreon, Tipeee, PayPal, Liberapay ...e mais!
Pepper|7|False|Acesse www.peppercarrot.com para mais informações!
Pepper|8|False|Obrigada!
