# Transcript of Pepper&Carrot Episode 34 [kw]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Rann 34: Urdhyans Shichimi yn Marghogieth

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrator|1|False|An kethsam nos...
Hibiscus|2|False|...ytho, haneth, ni a'th tynnergh, Shichimi, avel agan yowynka Marghek a Ah.
Coriander|3|False|Arwodh vyth a Bepper hwath?
Saffron|4|True|Na hwath.
Saffron|5|False|Y tegoodh dhedhi fistena, ma na fyll hi klewes areth Shichimi.
Shichimi|6|True|Meur ras.
Shichimi|7|False|Y karsen vy...

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|False|ZooUUUUUU
Sound|2|False|ZoouUUUUUUU
Pepper|3|True|A-ughowgh!
Pepper|4|True|Bedhewgh war!
Pepper|5|False|BEDHEWGH WAR!!!
Sound|6|False|KRASH!
Pepper|7|False|Soweth!
Pepper|8|False|Yw pubonan OK? Tra vyth terrys?
Shichimi|9|False|Pepper!
Pepper|10|True|Hou Shichimi!
Pepper|11|True|Drog yw genev a'n entrans dramasek hag a vos diwedhes!
Pepper|12|False|My re beu ow ponya dres oll an jydh, mes henn yw hwedhel hir.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Hibiscus|1|False|Yw hi onan a'th westoryon?
Shichimi|2|False|Yw. Ow howethes Pepper yw hi. Splann yw puptra.
Pepper|3|True|Carrot, os ta OK?
Pepper|4|False|Drog yw genev a'n tirans, ny wrug hwath maystri warnodho ha my yn-dann wordoth.
Pepper|5|True|Hag ow diharesow dhe bubonan arta a'n trobel,
Pepper|6|False|hag ow tochya gis ow dillas...
Shichimi|7|False|ti hi
Wasabi|8|True|Shichimi,
Wasabi|9|False|an wragh yowynk ma neb re dheuth, yw hi kowethes dhiso yn tevri?

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|True|Yw, agas meuredh.
Shichimi|2|False|Pepper yw hy hanow, a'n Skol a Chaosah.
Wasabi|3|True|Hy fresens omma a dhefol gnas sans agan skol.
Wasabi|4|False|Kemmer hi mes a'm gwel, distowgh.
Shichimi|5|True|Mes...
Shichimi|6|False|Mester Wasabi...
Wasabi|7|True|Mes pyth?
Wasabi|8|False|A via gwell genes bos divres a'gan skol?
Shichimi|9|False|!!!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|True|Drog yw genev Pepper, mes res yw dhis mos.
Shichimi|2|False|Lemmyn.
Pepper|3|True|Pyth?
Pepper|4|False|Ay dar, gorta pols! Sur ov yma kammgonvedhes.
Shichimi|5|False|Pepper, my a'th pys, na wra hemma kales.
Pepper|6|False|Hou! Ty, ena, war an tron. Mara'th eus kaletter genev vy, deus war-nans ha'y dherivas orthiv dha honan!
Wasabi|7|False|Tsss...
Wasabi|8|True|Shichimi, ty a'th eus deg eylen...
Wasabi|9|True|naw...
Wasabi|10|True|eth...
Wasabi|11|False|seyth...
Shichimi|12|False|TAW, PEPPER! KE DHE-VES!!!
Sound|13|False|SHRRIiii!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|9|True|PAFF!
Pepper|1|False|Shichimi, mar pleg, koselha dha hon...
Sound|2|False|BADouM!
Shichimi|3|True|KE DHE-VES!!!
Shichimi|4|True|KE DHE-VES!!!
Shichimi|5|False|KE DHE-VES!!!
Sound|6|False|KRIIIIII!!!
Pepper|7|True|Ogh!
Pepper|8|False|Dar! N-NYNS... yw henna... Agh... Kuv!
Sound|10|True|POFF!
Sound|11|True|POFF!
Sound|12|False|PAFF!
Coriander|13|False|SHICHIMI! PEPPER! HEDHEWGH!
Saffron|14|False|Gorta.
Wasabi|15|False|Hmm!
Pepper|16|True|Grrr!
Pepper|17|False|OK, ty a dhendylas hemma!
Sound|18|False|BRZOU!!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|CURSUS CANCELLARE MAXIMUS!!!
Sound|2|False|SHKLAK!
Pepper|3|False|Ogh!
Sound|4|False|PAFF!!
Shichimi|5|False|Ny'm nas dha wella hus a dhileans vytholl!
Shichimi|6|True|Hepkor, Pepper, ha ke dhe-ves!
Shichimi|7|False|Hedh ow honstrina dhe'th shyndya!
Pepper|8|False|O, ow hus a dhileans a oberas yn ta, mes nyns es ta an gosten.
Shichimi|9|True|Pyth?
Shichimi|10|False|Pandr'a styrydh?!
Sound|11|True|Pshh...
Sound|12|False|Pshh...

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|False|?!!
Pepper|2|True|Hi o an gosten!
Pepper|3|False|My a naghas hy Hus Golok a venten hy semlant yowynk.
Pepper|4|True|My a verkyas an hus ma kettel dheuth vy.
Pepper|5|False|Ytho, my a ros dhis tokyn byghan a'n pyth a dhendylydh rag gul dhe Shichimi batalyas orthiv!
Wasabi|6|True|TONTETH!
Wasabi|7|True|Ass os ta bedhek,
Wasabi|8|False|hag a-rag ow skol dien!
Pepper|9|True|Omsyns feusik!
Pepper|10|False|A pe oll ow Rea dhymm, ny hedhsen vy ena.

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|False|DRZOW!!
Wasabi|2|True|My a gonvedh. Ty re dhrehedhas nivel dha ragresegydhesow fest skaffa dell dharganis...
Wasabi|3|False|Hemma a uskisha ow thowlennow, mes henn yw nowodhow da.
Pepper|4|True|Dha dowlennow?
Pepper|5|True|Ytho yth eses orth ow frevi hepken, ha nyns o hemma awos ow bos diwedhes?
Pepper|6|False|Ass os ta fell!
Wasabi|7|True|ti...
Wasabi|8|False|hi.
Wasabi|9|True|PANDR'ESOWGH OW LAGATTA ORTO?!
Wasabi|10|False|YTH ESA OMSETTYANS WARNAV NAMNYGEN, HA NY WREWGH TRA VYTH MARNAS SEVEL ENA?! SYNSEWGH HI!!!
Wasabi|11|False|MY A'S MYNN YN FEW!
Wasabi|12|False|KEVEWGH HI!!!
Pepper|13|False|Shichimi, res yw dhyn kewsel a-dro dhe hemma diwettha!
Pepper|14|True|Drog yw genev, Carrot, mes res yw dhyn yskynna orth gordoth arta.
Pepper|15|False|Syns yn strooth!
Sound|16|False|Tapp!
Sound|17|False|Tapp!
Sound|18|False|ZiiouUU!!

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|?!
Wasabi|2|False|KEVEWGH HI!!!
Pepper|3|False|Soweth.
Saffron|4|False|Pepper, kemmer ow skubell!
Sound|5|False|Fizzz!
Pepper|6|True|Marthys da!
Pepper|7|False|Meur ras, Saffron!
Sound|8|False|Tock!
Sound|9|False|ziouuUUU!
Narrator|10|False|DHE VOS YSTYNNYS...

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|mis Meurth 31, 2021 Art & hwedhel: David Revoy. Redyoryon beta: Arlo James Barnes, Carotte, Craig Maloney, Efrat b, GunChleoc, Karl Ove Hufthammer, Martin Disch, Nicolas Artance, Parnikkapore, Valvin. Treylyans Kernewek: Steve Harris. Provredyans: Steve Penhaligon. Synsys ov: dhe Nartance rag hwithra gnas Wasabi yn y hwedhlow fugieth-skoodhyer. An fordh a dybis ev anedhi a dhelenwis fatel y's portrayen vy y'n rann ma. Selys war ollvys Hereva. Gwrier: David Revoy. Pennmentenour: Craig Maloney. Skriforyon: Craig Maloney, Nartance, Scribblemaniac, Valvin. Ewnoryon: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Medhelweyth: Krita 4.4.1, Inkscape 1.0.2 on Kubuntu Linux 20.04. Leshyans: Creative Commons Attribution 4.0. www.peppercarrot.com
Pepper|2|True|A wodhes?
Pepper|3|True|Pepper&Carrot yw rydh/libre yn tien, fenten-ygor ha skoodhys dre weres kuv y redyoryon.
Pepper|4|False|Rag an dyllans ma synsys on dhe'n 1096 tasek!
Pepper|5|True|Y hyllir dos ha bos tasek Pepper&Carrot ha kavos dha hanow omma!
Pepper|6|True|Yth eson war Patreon, Tipeee, PayPal, Liberapay ...ha moy!
Pepper|7|False|Mir orth www.peppercarrot.com rag derivadow moy!
Pepper|8|False|Meur ras!
