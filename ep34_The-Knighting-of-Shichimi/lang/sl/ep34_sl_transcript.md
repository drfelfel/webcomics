# Transcript of Pepper&Carrot Episode 34 [sl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Naslov|1|False|Epizoda 34: Šičimin viteški naziv

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pripovedovalec|1|False|Še isto noč …
Oslezka|2|False|S tem, Šičimi, te imenujemo v našo najmlajšo vitezinjo Aha.
Koriandrika|3|False|Za Papriko še vedno ni sledu?
Žafranka|4|True|Ne še.
Žafranka|5|False|Če ne pohiti, bo zamudila Šičimin govor.
Šičimi|6|True|Hvala.
Šičimi|7|False|Rada bi …

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zvok|1|False|SiššŠŠŠŠŠ
Zvok|2|False|SišŠŠŠŠS
Paprika|3|True|Prihajam!
Paprika|4|True|Pozor!
Paprika|5|False|Umaknite se!!!
Zvok|6|False|TRESK!
Paprika|7|False|Ups!
Paprika|8|False|So vsi v redu? Nobenih zlomov?
Šičimi|9|False|Paprika!
Paprika|10|True|Živjo, Šičimi!
Paprika|11|True|Oprosti za zamudo in buren prihod!
Paprika|12|False|Že ves dan bežim. Dolga zgodba.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Oslezka|1|False|Si jo ti povabila?
Šičimi|2|False|Da. To je moja prijateljica Paprika. V redu je.
Paprika|3|True|Korenček, si dobro?
Paprika|4|False|Oprosti za neroden pristanek, hiperpogona še ne obvladam.
Paprika|5|True|Še enkrat se opravičujem.
Paprika|6|False|Kar se pa mojih oblačil tiče …
Šičimi|7|False|Hi hi
Vasabi|8|True|Šičimi,
Vasabi|9|False|je ta mlada čarovnica resnično tvoja prijateljica?

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Šičimi|1|True|Da, vaša visokost.
Šičimi|2|False|Ime ji je Paprika. Iz šole Kaosaha prihaja.
Vasabi|3|True|Njena navzočnost kazi sveto ravnovesje naše šole.
Vasabi|4|False|Pri tej priči jo spodi proč.
Šičimi|5|True|Ampak …
Šičimi|6|False|Mojstrica Vasabi …
Vasabi|7|True|Ampak kaj?
Vasabi|8|False|Bi raje tvegala izključitev iz šole?
Šičimi|9|False|!!!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Šičimi|1|True|Oprosti, Paprika, ampak oditi moraš.
Šičimi|2|False|Takoj zdaj.
Paprika|3|True|Kaj?
Paprika|4|False|Čakaj, no. Samo trenutek! To je gotovo nesporazum.
Šičimi|5|False|Paprika, prosim, ne povzročaj težav.
Paprika|6|False|Hej, vi tam na prestolu! Če vam kaj ni všeč, mi pridite to sami povedat!
Vasabi|7|False|Tc tc.
Vasabi|8|True|Šičimi, imaš deset sekund.
Vasabi|9|True|Devet.
Vasabi|10|True|Osem.
Vasabi|11|False|Sedem.
Šičimi|12|False|DOVOLJ, PAPRIKA! IZGINI!!!
Zvok|13|False|ZZZZIiii!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zvok|9|True|PAF!
Paprika|1|False|Šičimi, pomiri s…
Zvok|2|False|BADuuM!
Šičimi|3|True|POBERI SE!!!
Šičimi|4|True|POJDI ŽE!!!
Šičimi|5|False|POJDI PROČ!!!
Zvok|6|False|KREIŠŠ!!!
Paprika|7|True|Auu!
Paprika|8|False|Hej, to pa n-NI … Au! … lepo!
Zvok|10|True|POF!
Zvok|11|True|POF!
Zvok|12|False|PAF!
Koriandrika|13|False|ŠIČIMI! PAPRIKA! NEHAJTA, PROSIM!
Žafranka|14|False|Čakaj.
Vasabi|15|False|Hmm!
Paprika|16|True|Grrr!
Paprika|17|False|No, prav, če že tako hočeš!
Zvok|18|False|BRUZZ!!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|False|UROKUS PREKINITIS NAGLOS!!!
Zvok|2|False|ŠAKA!
Paprika|3|False|Auu!
Zvok|4|False|PAF!!
Šičimi|5|False|Celo tvoj najboljši protiurok mi ne more nič!
Šičimi|6|True|Odnehaj, Paprika, in odidi!
Šičimi|7|False|Nočem ti več škodovati!
Paprika|8|False|Moj protiurok že deluje, samo tebe nisem ciljala.
Šičimi|9|True|Kaj?
Šičimi|10|False|Kako to misliš?!
Zvok|11|True|Fšš…
Zvok|12|False|Fšš…

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Šičimi|1|False|?!!
Paprika|2|True|Urok sem namenila njej!
Paprika|3|False|Le odstranila sem urok privida, s katerim ohranja svoj mladostni videz.
Paprika|4|True|Opazila sem ga že ob prihodu.
Paprika|5|False|Tole je le delček tega, kar zaslužite, ker ste naju s Šičimi prisilili v boj!
Vasabi|6|True|PREDRZNOST!
Vasabi|7|True|Da si to dovoliš,
Vasabi|8|False|in še vpričo moje šole!
Paprika|9|True|Kar veseli bodite!
Paprika|10|False|Če bi imela ves svoj rês, bi bil to šele začetek.

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zvok|1|False|DZZUH!!
Vasabi|2|True|Že razumem. Tvoje sposobnosti so postale enakovredne tvojim predhodnicam veliko hitreje, kot sem pričakovala …
Vasabi|3|False|To sicer spremeni moje načrte, a nič ne dé.
Paprika|4|True|Vaše načrte?
Paprika|5|True|Se pravi ste me preizkušali in to nima veze z mojim zamujanjem?
Paprika|6|False|Zahrbtnost, vam rečem!
Vasabi|7|True|He
Vasabi|8|False|he.
Vasabi|9|True|KAJ SAMO STRMITE?!
Vasabi|10|False|PRAVKAR ME JE NAPADLA! NE STOJTE TAM KOT LIPOV BOG IN JO ZGRABITE!!!
Vasabi|11|False|HOČEM JO ŽIVO!
Vasabi|12|False|PO NJEJ!!!
Paprika|13|False|Šičimi, pozneje se pogovoriva!
Paprika|14|True|Oprosti, Korenček, ampak spet bova rabila hiperpogon.
Paprika|15|False|Trdno se primi!
Zvok|16|False|Tap!
Zvok|17|False|Tap!
Zvok|18|False|Siššš!!

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|False|?!
Vasabi|2|False|ZGRABITE JO!!!
Paprika|3|False|Ojoj.
Žafranka|4|False|Paprika, vzemi mojo metlo!
Zvok|5|False|Fizzz!
Paprika|6|True|O, mega!
Paprika|7|False|Hvala, Žafranka!
Zvok|8|False|Tok!
Zvok|9|False|z U U U U U M!|nowhitespace
Pripovedovalec|10|False|SE NADALJUJE …

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zasluge|1|False|31. marec, 2021 Piše in riše: David Revoy. Testni bralci: Arlo James Barnes, Carotte, Craig Maloney, Efrat b, GunChleoc, Karl Ove Hufthammer, Martin Disch, Nicolas Artance, Parnikkapore in Valvin. Prevedla: Andrej Ficko in Gorazd Gorup. Posebej se zahvaljujem Nartanceu za poglobitev Vasabijine osebnosti v svojih oboževalskih zgodbah. Njegova interpretacija njenega značaja je imela velik vpliv na Vasabijino uprizoritev v tem stripu. Dogaja se v vesolju Hereve Stvaritelj: David Revoy. Glavni vzdrževalec: Craig Maloney. Uredniki: Craig Maloney, Nartance, Scribblemaniac in Valvin. Popravki: Willem Sonke, Moini, Hali, CGand in Alex Gryson. Programska oprema: Krita 4.4.1 in Inkscape 1.0.2 na Kubuntu Linux 20.04. Licenca: Creative Commons Priznanje avtorstva 4.0. www.peppercarrot.com
Paprika|2|True|Ali veš?
Paprika|3|True|Strip Paprika in Korenček je popolnoma prost in odprtokoden, podpirajo pa ga njegovi bralci!
Paprika|4|False|Za to epizodo je zaslužnih 1096 podpornikov!
Paprika|5|True|Tudi ti lahko podpreš izdelavo tega stripa in si zagotoviš prostor na zgornjem seznamu imen!
Paprika|6|True|Smo na Patreonu, Tipeeeju, PayPalu, Liberapayju … in drugod!
Paprika|7|False|Obišči www.peppercarrot.com za več informacij!
Paprika|8|False|Hvalaaaa!
