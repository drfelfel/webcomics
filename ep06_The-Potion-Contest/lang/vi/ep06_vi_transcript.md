# Transcript of Pepper&Carrot Episode 06 [vi]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Tập 6: Cuộc Thi Thần Dược

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Hạt Tiêu|1|True|Ôi không, Mình lại ngủ với cửa sổ mở rồi..
Hạt Tiêu|2|True|... gió thổi nhiều quá...
Hạt Tiêu|3|False|... và tại sao Komona lại xuất hiện trước cửa sổ vậy nhỉ?
Hạt Tiêu|4|False|KOMONA!
Hạt Tiêu|5|False|Cuộc thi thần dược!
Hạt Tiêu|6|True|Chắc tại mình đã ngủ quên mất...
Hạt Tiêu|9|True|... nhưng?
Hạt Tiêu|10|False|Mình đang ở đâu đây?!?
Bird|12|False|cK?|nowhitespace
Bird|11|True|qua|nowhitespace

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Hạt Tiêu|1|False|!!!
Hạt Tiêu|2|False|Cà Rốt! Cậu thật dễ thương khi nghĩ cách đưa mình tới cuộc thi.
Hạt Tiêu|3|False|Tuyệt Vời !
Hạt Tiêu|4|True|Cậu còn mang cả một lọ thuốc, quần áo, và mũ của mình...
Hạt Tiêu|5|False|... hãy xem cậu đã mang loại thuốc gì nào...

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Hạt Tiêu|1|False|Cái gì thế này ?!!
Mayor of Komona|3|False|Là Thị trưởng của Komona, tôi chính thức tuyên bố cuộc thi... Bắt Đầu!
Mayor of Komona|4|False|Thị trấn của chúng ta hân hạnh chào đón bốn vị phù thủy cho cuộc thi lần thứ nhất này
Mayor of Komona|5|True|Xin hãy cho
Mayor of Komona|6|True|Thật Lớn!
Writing|2|False|Cuộc Thi Thần Dược Komona
Mayor of Komona|7|False|một trang pháo tay

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Audience|29|False|Clap
Mayor of Komona|1|True|Từ liên minh của các nhà kĩ thuật học, thật vinh dự khi chào đón Sắc Đẹp và Sự Khéo Léo
Mayor of Komona|3|True|... không thể không nhắc đến, vị phù thủy của Komona chúng ta
Mayor of Komona|5|True|... Thí sinh thứ ba của chúng ta đến từ cùng đất của mặt trăng khuất bóng,
Mayor of Komona|7|True|... và thí sinh cuối cùng của chúng ta, cô phù thủy đến từ khu rừng Squirrel's End,
Mayor of Komona|2|False|Coriander !
Mayor of Komona|4|False|Saffron !
Mayor of Komona|6|False|Shichimi !
Mayor of Komona|8|False|Hạt Tiêu !
Mayor of Komona|9|True|Hãy để cuộc thi bắt đầu...
Mayor of Komona|10|False|Các thí sinh được bình trọn bởi độ lớn khi vỗ tay của khán giả.
Mayor of Komona|11|False|Đầu tiên là phần biểu diễn của Coriander
Cây Rau Mùi|13|False|... không cần phải sợ cái chết nữa, cảm ơn đến liều thuốc của tôi ...
Cây Rau Mùi|14|True|... Thần Dược
Cây Rau Mùi|15|False|ZOMBIFICATION !
Audience|16|True|Clap
Audience|17|True|Clap
Audience|18|True|Clap
Audience|19|True|Clap
Audience|20|True|Clap
Audience|21|True|Clap
Audience|22|True|Clap
Audience|23|True|Clap
Audience|24|True|Clap
Audience|25|True|Clap
Audience|26|True|Clap
Audience|27|True|Clap
Audience|28|True|Clap
Cây Rau Mùi|12|False|Các Quý ông & Quý bà...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|False|Thật Tuyệt Vời!
Audience|3|True|Clap
Audience|4|True|Clap
Audience|5|True|Clap
Audience|6|True|Clap
Audience|7|True|Clap
Audience|8|True|Clap
Audience|9|True|Clap
Audience|10|True|Clap
Audience|11|True|Clap
Audience|12|True|Clap
Audience|13|True|Clap
Audience|14|True|Clap
Audience|15|True|Clap
Audience|16|False|Clap
Xà Phòng|18|True|Bởi vì đây, thần dược
Xà Phòng|17|True|... xin làm ơn, hãy dừng vỗ tay nào, người dân của Komona !
Xà Phòng|22|False|... cảm thấy ghen tỵ
Xà Phòng|19|False|Của Tôi!
Xà Phòng|25|False|Sang Trọng !
Xà Phòng|24|True|... Thần Dược
Xà Phòng|23|False|... Tất cả sẽ thành hiện thực chỉ với một giọt từ lọ thuốc tôi chế tạo...
Audience|26|True|Clap
Audience|27|True|Clap
Audience|28|True|Clap
Audience|29|True|Clap
Audience|30|True|Clap
Audience|31|True|Clap
Audience|32|True|Clap
Audience|33|True|Clap
Audience|34|True|Clap
Audience|35|True|Clap
Audience|36|True|Clap
Audience|37|True|Clap
Audience|38|True|Clap
Audience|39|True|Clap
Audience|40|False|Clap
Mayor of Komona|42|False|Loại thuốc này có thể làm cả Komona trở nên giàu có!
Mayor of Komona|41|True|Kì Diệu! Tuyệt vời !
Audience|44|True|Clap
Audience|45|True|Clap
Audience|46|True|Clap
Audience|47|True|Clap
Audience|48|True|Clap
Audience|49|True|Clap
Audience|50|True|Clap
Audience|51|True|Clap
Audience|52|True|Clap
Audience|53|True|Clap
Audience|54|True|Clap
Audience|55|True|Clap
Audience|56|True|Clap
Audience|57|True|Clap
Audience|58|True|Clap
Audience|59|True|Clap
Audience|60|False|Clap
Mayor of Komona|2|False|Coriander đã chống lại cái chết với loại thần dược tuyệt diệu này!
Xà Phòng|21|True|Lọ thuốc thật sự mà tất cả chúng ta đang mong đợi, lọ thuốc mà có thể khiến tất cả hàng xóm của bạn ...
Mayor of Komona|43|False|Có thể các bạn đã sai. Coriander đã bị vượt mặt

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|False|Màn trình diễn vừa rồi xem ra rất khó khăn cho Shichimi để vượt qua !
Thất Vị|4|True|Không được!
Thất Vị|5|True|Nó quá nguy hiểm khi sử dụng
Thất Vị|6|False|Tôi xin lỗi !
Mayor of Komona|3|False|... Thôi nào Shichimi, Mọi người đang đợi cô đấy
Mayor of Komona|7|False|Quý ông và quý bà xem ra Shichimi đã bỏ cuộc...
Xà Phòng|8|False|Đưa cho tui nào !
Xà Phòng|9|False|... Đừng ngại ngùng chứ, cô đang phá hỏng cuộc thi đó
Xà Phòng|10|False|Ai cũng biết là tôi sẽ chiến thắng cuộc thi này dù thuốc của cô có làm đi nữa ...
Thất Vị|11|False|!!!
Sound|12|False|BZZZIIOO
Thất Vị|15|False|Quái Vật Khổng Lồ !
Thất Vị|2|False|Tôi... Tôi không biết là phải biểu diễn lọ thuốc của mình
Thất Vị|13|True|Cẩn Thận!!!
Thất Vị|14|True|Đó là thần dược

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bird|1|False|CAW-CAW-Caaaaaaaawwww
Sound|2|False|BAM!
Hạt Tiêu|3|True|... được rồi !
Hạt Tiêu|5|False|... Ít nhất lọ thuốc của tôi cũng mang lại vài tiếng cười vì ....
Hạt Tiêu|4|False|đến lượt của tôi phải chứ ?
Mayor of Komona|6|True|Chạy đi đồ ngốc!
Mayor of Komona|7|False|Cuộc thi kết thúc rồi ! ... tự cứu lấy bản thân đi

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Hạt Tiêu|2|False|... Mọi người luôn bỏ đi khi đến lượt của chúng ta
Hạt Tiêu|1|True|Thấy chưa ...
Hạt Tiêu|4|True|Ít nhất mình nghĩ ra cách dùng "thần dược" của cậu, Cà Rốt
Hạt Tiêu|5|False|...Hãy thu dọn và chuẩn bị về nhà thôi.
Hạt Tiêu|7|True|Con chim
Hạt Tiêu|8|False|Xác sống-bự chảng-sang chảnh!
Hạt Tiêu|10|False|muốn thử lọ thuốc cuối này chứ? ...
Hạt Tiêu|11|False|... không đúng chứ ?
Hạt Tiêu|6|False|Ê.. !
Sound|9|False|CRACK !

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Hạt Tiêu|1|True|đúng rồi, hãy đọc kỹ cái nhãn xem ...
Hạt Tiêu|2|False|... Ta chỉ nói một lần thôi nếu ngươi không rời khỏi Komona ngay, ta sẽ đổ nó khắp người ngươi đó !
Mayor of Komona|3|True|Bởi vì cô ấy đã cứu thị trấn chúng ta khỏi nguy hiểm
Mayor of Komona|4|False|Giải nhất đã thuộc vê cô ấy cho thần dược ... ??!!
Hạt Tiêu|7|False|... huh... thật sự thì nó không phải là một lọ thuốc;nó chỉ là lọ nước tiểu của chú mèo của tôi sau lần khám sức khỏe lần trước!
Hạt Tiêu|6|True|... Haha! yep ...
Hạt Tiêu|8|False|... vậy thì khỏi cần biểu diễn nhỉ?...
Narrator|9|False|Tập 6: Cuộc Thi Thần Dược
Narrator|10|False|Hết
Writing|5|False|50,000 Ko
Credits|11|False|Tháng 3, 2015 - Truyện và vẽ bởi David Revoy - Dịch bời Binh Pham

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Hạt Tiêu&Cà Rốt hoàn toàn miễn phí và cảm ơn tới sự giúp đỡ bởi các nhà tài trợ. Tập này được tài trợ bởi 245 người này:
Credits|4|False|https://www.patreon.com/davidrevoy
Credits|3|False|Bạn cũng có thể ủng hộ cho Hạt Tiêu&Cà Rốt tập tiếp theo tại :
Credits|7|False|Công cụ : Tập này được vẽ với 100% các công cụ miễn phí Krita on Linux Mint
Credits|6|False|License : Creative Commons Attribution bạn có thể vẽ lại, chỉnh sửa, bán nó, đăng lại, vv...
Credits|2|False|Алексей ★ Глеб Бузало ★ 獨孤欣 & 獨弧悦 ★ Addison Lewis ★ A Distinguished Robot ★ Adrian Lord ★ Ahmad Ali ★ Aina Reich ★ Alandran ★ Alan Hardman ★ Albert Westra ★ Alcide ★ Alejandro Flores Prieto ★ Alex ★ Alexander Bülow Tomassen ★ Alexander Sopicki ★ Alexandra Jordan ★ Alex Lusco ★ Alex Silver ★ Alex Vandiver ★ Alfredo ★ Ali Poulton (Aunty Pol) ★ Allan Zieser ★ Andreas Rieger ★ Andrej Kwadrin ★ Andrew ★ Andrew Godfrey ★ Andrey Alekseenko ★ Angela K ★ Anna Orlova ★ Antan Karmola ★ Anthony Edlin ★ Antonio Mendoza ★ Ardash Crowfoot ★ Arjun Chennu ★ Arne Brix ★ Aslak Kjølås-Sæverud ★ Axel Bordelon ★ Axel Philipsenburg ★ Barbix ★ Ben Evans ★ Bernd ★ Betsy Luntao ★ Boonsak Watanavisit ★ Boris Fauret ★ Boudewijn Rempt ★ BoxBoy ★ Brett Smith ★ Brian Behnke ★ Brian Smith ★ Bryan Butler ★ Bui Dang Hai Trieu ★ Carlos Levischi ★ Charlotte Lacombe-bar ★ Chris Radcliff ★ Chris Sakkas ★ Christian Gruenwaldner ★ Christophe Carré ★ Christopher Bates ★ Clara Dexter ★ codl ★ Colby Driedger ★ Conway Scott Smith ★ Cuthbert Williams ★ Cyrille Largillier ★ Cyril Paciullo ★ Damien ★ Daniel ★ Daniel Björkman ★ Danny Grimm ★ David Tang ★ DiCola Jamn ★ Dmitry ★ Donald Hayward ★ Duke ★ Eitan Goldshtrom ★ Enrico Billich ★ Epsilon ★ Eric Schulz ★ Faolan Grady ★ Francois Schnell ★ Garret Patterson ★ Ginny Hendricks ★ GreenAngel5 ★ Grigory Petrov ★ Guillaume ★ Guillaume Ballue ★ Gustav Strömbom ★ Guy Davis ★ Happy Mimic ★ Helmar Suschka ★ Henning Döscher ★ Ilyas ★ Irina Rempt ★ Ivan Korotkov ★ James Frazier ★ Janusz ★ Jared Tritsch ★ JDB ★ Jean-Baptiste Hebbrecht ★ Jean-Gabriel LOQUET ★ Jeffrey Schneider ★ Jessey Wright ★ Jim ★ Jim Street ★ Jiska ★ Joachim Schiele ★ JoÃ£o Luiz Machado Junior ★ Joern Konopka ★ joe rutledge ★ Johanne Thomson ★ John ★ John Urquhart Ferguson ★ Jónatan Nilsson ★ Jonathan Leroy ★ Jonathan Ringstad ★ Jon Brake ★ Jorge Bernal ★ Joseph Bowman ★ Julien Duroure ★ Justus Kat ★ Kai-Ting (Danil) Ko ★ Kasper Hansen ★ Kate ★ Kathryn Wuerstl ★ Ken Mingyuan Xia ★ Kingsquee ★ Kroet ★ Levi Kornelsen ★ Liselle ★ Lorentz Grip ★ L S ★ Luc Stepniewski ★ Luke Hochrein ★ MacCoy ★ Magnus Kronnäs ★ Manuel ★ Marc & Rick ★ Marcus ★ Martin Owens ★ Mary Brownlee ★ Masked Admirer ★ Mathias Stærk ★ Mefflin Ross Bullis-bates ★ Michael ★ Michael Gill ★ Michael Pureka ★ Michelle Pereira Garcia ★ Mike Mosher ★ Miroslav ★ Muzyka Dmytro ★ Nataya Castillo ★ Nazhif ★ Nicholas DeLateur ★ Nicholas Terranova ★ Nicki Aya ★ Nicola Angel ★ Nicolae Berbece ★ Nicole Heersema ★ Nielas Sinclair ★ NinjaKnight Comics ★ Noble Hays ★ Noelia Calles Marcos ★ Nora Czaykowski ★ Nyx ★ Olivier Amrein ★ Olivier Brun ★ Omar Willey ★ Oscar Moreno ★ Öykü Su Gürler ★ Ozone S. ★ Pablo Lopez Soriano ★ Pat David ★ Patrick Gamblin ★ Paul ★ Pavel Semenov ★ Pet0r ★ Peter ★ Peter Moonen ★ Petr Vlašic ★ Philippe Jean Edward Bateman ★ Pierre Geier ★ Pierre Vuillemin ★ Pranab Shenoy ★ Pyves & Ran ★ Raghavendra Kamath ★ Rajul Gupta ★ Reorx Meng ★ Ret Samys ★ Rictic ★ RJ van der Weide ★ Roberto Zaghis ★ Roman ★ Rumiko Hoshino ★ Rustin Simons ★ Sally Bridgewater ★ Sami T ★ Samuel Mitson ★ Scott Petrovic ★ Sean Adams ★ Shadefalcon ★ ShadowMist ★ Shafak ★ Shawn Meyer ★ Simon Forster ★ Simon Isenberg ★ Sonny W. ★ Soriac ★ Stanislav Vodetskyi ★ Stephanie Cheshire ★ Stephen Bates ★ Stephen Smoogen ★ Steven Bennett ★ Stuart Dickson ★ Surt ★ TamaskanLEM ★ Tar8156 ★ Terry Hancock ★ TheFaico ★ Thomas Citharel ★ Thomas Courbon ★ Thomas Schwery ★ Tim Burbank ★ Tim J. ★ Tomas Hajek ★ Tom Demian ★ Tom Savage ★ Tracey Reuben ★ Travis Humble ★ Tree ★ Tyson Tan ★ Urm ★ Victoria ★ Victoria White ★ Vladislav Kurdyukov ★ Vlad Tomash ★ Westen Curry ★ Witt N. Vest ★ Xavier Claude ★ Yalyn Vinkindo ★ Yaroslav ★ Zeni Pong ★ Źmicier Kušnaroŭ
