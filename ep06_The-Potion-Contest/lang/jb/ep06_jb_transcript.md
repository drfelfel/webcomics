# Transcript of Pepper&Carrot Episode 06 [jb]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|.i 6 mo'o nu jivna

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
la .piper.|1|True|.i oi sai mi pu za'u re'u co'a sipna ca lo nu le canko cu kalri
la .piper.|2|True|.i le brife cu carmi
la .piper.|3|False|.i ni'i ma mi viska le nu la .komonan. cu trixe le canko
la .piper.|4|False|.i .oi la .komonan.
la .piper.|5|True|.i nu jivna fa le finti be lo mixre
la .piper.|6|False|.i ja'o mi snuti sipna be co'a
la .piper.|9|True|.i ku'i
la .piper.|10|False|mi zvati ma .a'u
Bird|12|False|k.|nowhitespace
Bird|11|True|.kua|nowhitespace

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
la .piper.|1|False|.ii
la .piper.|2|False|.i .u'e doi la .karot. do jai xamgu .i ja'e bo gau do mi klama le stuzi be le nu jivna
la .piper.|3|False|.i banli
la .piper.|4|True|.i ji'a do punji le mixre .e le taxfu .e le mapku vu'o pe mi
la .piper.|5|False|.i .ai facki le du'u ma me le mixre pe mi gi'e se punji do

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
la .piper.|1|False|ma .oi cai
Mayor of Komona|3|False|.i mi noi turni la .komonan. cu krati ri le ka jai gau djuno le du'u le 1 moi nu le'i finti be lo makfa mixre cu simxu le ka jivna co'a fasnu
Mayor of Komona|4|False|.i le xabju be le tcadu pe mi'a ca gleki le nu tigni fa le vo zukte be lo makfa
Mayor of Komona|5|True|.i .e'o do
Mayor of Komona|6|True|carmi
Writing|2|False|le 1 moi nu le'i finti be lo makfa
Mayor of Komona|7|False|rinsa

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|True|.i klama fi la minji fa le noi se sinma gi'e melbi gi'e stati vau se cmene be zo
Mayor of Komona|3|True|.i fi le nixli poi diklo mi'a cu .ei morji .i fi'i la
Mayor of Komona|5|True|.i le ci moi jivna cu klama fi le tumla pe le za'u dizlo lunra .i coi la
Mayor of Komona|7|True|.i .uo ro moi jivna gi'e xabju le noi foldi le tricu vau me la rebla be lo ricyratcu vau fa la
Mayor of Komona|2|False|.koriander.
Mayor of Komona|4|False|.safran.
Mayor of Komona|6|False|.citcimin.
Mayor of Komona|8|False|.piper.
Mayor of Komona|9|True|.i .e'e co'a jivna .i mi'o ba jdice le du'u
Mayor of Komona|10|False|ma kau jinga vau ta'i le nu mi'o cladu zanru
Mayor of Komona|11|False|.i pa mai tigni fa la .koriander.
la .koriander.|13|True|ko co'u terpa tu'a lo'e morsi .i ki'u bo mi co'i finti
la .koriander.|14|True|le mixre pe ja'e le nu
la .koriander.|15|False|no'e morsi
Audience|16|True|.io sai
Audience|17|True|.ui sai
Audience|18|True|.ui
Audience|19|True|.io
Audience|20|True|.i'e sai
Audience|21|True|.io sai
Audience|22|True|.io sai
Audience|23|True|.io sai
Audience|24|True|.ue sai
Audience|25|True|.ue sai
Audience|26|True|.io sai
Audience|27|False|.ue sai
la .koriander.|12|False|.i doi lei nobli

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|False|.i makfa
Audience|3|True|.io sai
Audience|4|True|.io sai
Audience|5|True|.io sai
Audience|6|True|.io sai
Audience|7|True|.io sai
Audience|8|True|.io sai
Audience|9|True|.io sai
Audience|10|True|.io sai
Audience|11|True|.io sai
Audience|12|True|.io sai
Audience|13|True|.io sai
Audience|14|True|.io sai
Audience|15|True|.io sai
Audience|16|False|.io sai
la .safran.|18|True|.i ti du le mixre poi
la .safran.|17|True|.i ku'i .e'o do na clira jdice doi lei nobli xabju be la .komonan.
la .safran.|22|False|gi'e jilra do
la .safran.|19|True|ba'e mi
la .safran.|25|False|to'e cumla ricfu
la .safran.|24|True|le mixre pe ja'e le nu
la .safran.|23|False|.i sa'u do curmi le nu farlu fa pa dirgo be
Audience|26|True|.io
Audience|27|True|.io
Audience|28|True|.io sai
Audience|29|True|.i'o sai
Audience|30|True|.io sai
Audience|31|True|.io sai
Audience|32|True|.io sai
Audience|33|True|.io sai
Audience|34|True|.io sai
Audience|35|True|.io sai
Audience|36|True|.io sai
Audience|37|True|.io sai
Audience|38|True|.io
Audience|39|True|.io sai
Audience|40|False|.io
Mayor of Komona|42|False|.i ba ku ri'a tu'a ti voi mixre vau ro xabju be la .komonan. cu ricfu
Mayor of Komona|41|True|.i na ka'e krici
Audience|44|True|.io
Audience|45|True|.io sai
Audience|46|True|.io
Audience|47|True|.io sai
Audience|48|True|.io
Audience|49|True|.io sai
Audience|50|True|.io sai
Audience|51|True|.io
Audience|52|True|.io sai
Audience|53|True|.io
Audience|54|True|.i'o
Audience|55|True|.io
Audience|56|True|.io sai
Audience|57|True|.io sai
Audience|58|True|.io sai
Audience|59|True|.io sai
Audience|60|False|.io
Mayor of Komona|2|False|.i gau la .koriander. ri'a tu'a le se manci mixre lo'e xadni co'u morsi
la .safran.|21|True|.i le mixre pe mi pu jai se kanpe do .i lo'e xabju be le jibni tumla ba manci le mixre
Mayor of Komona|43|False|.i .ie do tai krixa .i la .koriander. na jinga
la .safran.|20|False|finti

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|False|.i ju'o la .citcimin. ba se nandu le ka jivna la .safran.
la .citcimin.|4|True|vi'o nai do
la .citcimin.|5|True|.i mi na djica .i ckape
la .citcimin.|6|False|.i ko fraxu mi
Mayor of Komona|3|False|.i .e'e dai doi la .citcimin. .i ro da denpa tu'a do
Mayor of Komona|7|False|.i za'a dai doi lei nobli la .citcimin. cu rivbi
la .safran.|8|False|.i e'u mi ta jgari
la .safran.|9|False|.i ko co'u tarti le ka cumla .i va'o ku le nu tigni na zdile
la .safran.|10|False|.i ro da sanji le du'u mi jinga va'o ro ka'e se rinka be tu'a le mixre pe do
la .citcimin.|11|False|.ii
Sound|12|False|.BYJIION.
la .citcimin.|15|False|cizra danlu
la .citcimin.|2|False|.y.y.y. na pu djuno le du'u sarcu fa le nu pilno tigni
la .citcimin.|13|True|.i .o'i sai
la .citcimin.|14|True|.i le mixre cu jai rinka tu'a lo

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bird|1|False|.kax.kax.ka'a'a'a'a'a'a'ax.
Sound|2|False|.BAM.
la .piper.|3|True|.i cinri .i ja'o
la .piper.|5|False|.i ju'o le mixre pe mi ba jai xajmi .i ki'u bo
la .piper.|4|False|tcini le nu .ei mi tigni
Mayor of Komona|6|True|.i ko to'o bajra
Mayor of Komona|7|False|.i le nu jivna cu mulno .i gau ko ko snura

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
la .piper.|2|False|.i le nu ro da cliva cu fadni le ka fasnu vau le'i jai ca nu mi'o tigni
la .piper.|1|True|.i .i'a
la .piper.|4|True|.i o'u mi finti le zabna tadji be le nu pilno le za'e mixre pe do doi la .karot.
la .piper.|5|False|.i nu bu cu nu mi'o jai gau cnici fai ti gi'e ba bo klama le zdani
la .piper.|7|True|le .io nai
la .piper.|8|False|du'e va'e barda danlu
la .piper.|10|False|.i pei mi punji fi do fe ti voi ro moi mixre
la .piper.|11|False|.i xu .ue xo'o do na djica
la .piper.|6|False|ju'i
Sound|9|False|.KRAK.

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
la .piper.|1|True|.i ko jundi tcidu le notci pe le gacri
la .piper.|2|False|.i se va'o gi do na ca cliva la .komonan. gi mi pensi na ku gi'e zmiku le ka jai gau carvi fai ti do
Mayor of Komona|3|True|.i fi le ka jai gau snura fai le tcadu vau noi le nei pu pilno le makfa mixre ke'a
Mayor of Komona|4|False|se cnemu mi'a fa la .piper. .i ta'o do finti le mixre ma
la .piper.|7|False|sei mi stace ti na makfa mixre .i ti du lei pinca be le mlatu pe mi be'o noi pu ze'a zvati le zdani ba le nu mi'a klama le mikce be lo'e mlatu
la .piper.|6|True|.i .u'i
la .piper.|8|False|.i pei mi na pilno ti le ka tigni
Narrator|9|False|.i 6 mo'o nu jivna
Narrator|10|False|.i fanmo
Writing|5|False|rupnu li 50000
Credits|11|False|.i de'i li nanca bu 2015 masti bu 3 pu ku la'o gy.David Revoy.gy. finti le lisri gi'e finti le pixra .i la gleki cu fanva

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|i la'e la .piper. joi la .karot. cu fingubni i sarji le nu finti la'e bu kei fa le sidju pe la'e di'e
Credits|4|False|https://www.patreon.com/davidrevoy
Credits|3|False|i ko sarji la'e bu ta'i le nu do benji le rupnu be li pa bei le merko
Credits|7|False|i pu'i pilno fi le ka zbasu le dei lisri pe la'e bu vau fe le tutci noi fingubni zi'e voi noi me ke'a fa la krita e la'o gy.Linux Mint.gy.
Credits|6|False|i jaspu la'e bu fa la'o gy.Creative Commons Attribution to 'David Revoy'.gy.
Credits|2|False|Алексей ★ Глеб Бузало ★ 獨孤欣 & 獨弧悦 ★ Addison Lewis ★ A Distinguished Robot ★ Adrian Lord ★ Ahmad Ali ★ Aina Reich ★ Alandran ★ Alan Hardman ★ Albert Westra ★ Alcide ★ Alejandro Flores Prieto ★ Alex ★ Alexander Bülow Tomassen ★ Alexander Sopicki ★ Alexandra Jordan ★ Alex Lusco ★ Alex Silver ★ Alex Vandiver ★ Alfredo ★ Ali Poulton (Aunty Pol) ★ Allan Zieser ★ Andreas Rieger ★ Andrej Kwadrin ★ Andrew ★ Andrew Godfrey ★ Andrey Alekseenko ★ Angela K ★ Anna Orlova ★ Antan Karmola ★ Anthony Edlin ★ Antonio Mendoza ★ Ardash Crowfoot ★ Arjun Chennu ★ Arne Brix ★ Aslak Kjølås-Sæverud ★ Axel Bordelon ★ Axel Philipsenburg ★ Barbix ★ Ben Evans ★ Bernd ★ Betsy Luntao ★ Boonsak Watanavisit ★ Boris Fauret ★ Boudewijn Rempt ★ BoxBoy ★ Brett Smith ★ Brian Behnke ★ Brian Smith ★ Bryan Butler ★ Bui Dang Hai Trieu ★ Carlos Levischi ★ Charlotte Lacombe-bar ★ Chris Radcliff ★ Chris Sakkas ★ Christian Gruenwaldner ★ Christophe Carré ★ Christopher Bates ★ Clara Dexter ★ codl ★ Colby Driedger ★ Conway Scott Smith ★ Cuthbert Williams ★ Cyrille Largillier ★ Cyril Paciullo ★ Damien ★ Daniel ★ Daniel Björkman ★ Danny Grimm ★ David Tang ★ DiCola Jamn ★ Dmitry ★ Donald Hayward ★ Duke ★ Eitan Goldshtrom ★ Enrico Billich ★ Epsilon ★ Eric Schulz ★ Faolan Grady ★ Francois Schnell ★ Garret Patterson ★ Ginny Hendricks ★ GreenAngel5 ★ Grigory Petrov ★ Guillaume ★ Guillaume Ballue ★ Gustav Strömbom ★ Guy Davis ★ Happy Mimic ★ Helmar Suschka ★ Henning Döscher ★ Ilyas ★ Irina Rempt ★ Ivan Korotkov ★ James Frazier ★ Janusz ★ Jared Tritsch ★ JDB ★ Jean-Baptiste Hebbrecht ★ Jean-Gabriel LOQUET ★ Jeffrey Schneider ★ Jessey Wright ★ Jim ★ Jim Street ★ Jiska ★ Joachim Schiele ★ JoÃ£o Luiz Machado Junior ★ Joern Konopka ★ joe rutledge ★ Johanne Thomson ★ John ★ John Urquhart Ferguson ★ Jónatan Nilsson ★ Jonathan Leroy ★ Jonathan Ringstad ★ Jon Brake ★ Jorge Bernal ★ Joseph Bowman ★ Julien Duroure ★ Justus Kat ★ Kai-Ting (Danil) Ko ★ Kasper Hansen ★ Kate ★ Kathryn Wuerstl ★ Ken Mingyuan Xia ★ Kingsquee ★ Kroet ★ Levi Kornelsen ★ Liselle ★ Lorentz Grip ★ L S ★ Luc Stepniewski ★ Luke Hochrein ★ MacCoy ★ Magnus Kronnäs ★ Manuel ★ Marc & Rick ★ Marcus ★ Martin Owens ★ Mary Brownlee ★ Masked Admirer ★ Mathias Stærk ★ Mefflin Ross Bullis-bates ★ Michael ★ Michael Gill ★ Michael Pureka ★ Michelle Pereira Garcia ★ Mike Mosher ★ Miroslav ★ Muzyka Dmytro ★ Nataya Castillo ★ Nazhif ★ Nicholas DeLateur ★ Nicholas Terranova ★ Nicki Aya ★ Nicola Angel ★ Nicolae Berbece ★ Nicole Heersema ★ Nielas Sinclair ★ NinjaKnight Comics ★ Noble Hays ★ Noelia Calles Marcos ★ Nora Czaykowski ★ Nyx ★ Olivier Amrein ★ Olivier Brun ★ Omar Willey ★ Oscar Moreno ★ Öykü Su Gürler ★ Ozone S. ★ Pablo Lopez Soriano ★ Pat David ★ Patrick Gamblin ★ Paul ★ Pavel Semenov ★ Pet0r ★ Peter ★ Peter Moonen ★ Petr Vlašic ★ Philippe Jean Edward Bateman ★ Pierre Geier ★ Pierre Vuillemin ★ Pranab Shenoy ★ Pyves & Ran ★ Raghavendra Kamath ★ Rajul Gupta ★ Reorx Meng ★ Ret Samys ★ Rictic ★ RJ van der Weide ★ Roberto Zaghis ★ Roman ★ Rumiko Hoshino ★ Rustin Simons ★ Sally Bridgewater ★ Sami T ★ Samuel Mitson ★ Scott Petrovic ★ Sean Adams ★ Shadefalcon ★ ShadowMist ★ Shafak ★ Shawn Meyer ★ Simon Forster ★ Simon Isenberg ★ Sonny W. ★ Soriac ★ Stanislav Vodetskyi ★ Stephanie Cheshire ★ Stephen Bates ★ Stephen Smoogen ★ Steven Bennett ★ Stuart Dickson ★ Surt ★ TamaskanLEM ★ Tar8156 ★ Terry Hancock ★ TheFaico ★ Thomas Citharel ★ Thomas Courbon ★ Thomas Schwery ★ Tim Burbank ★ Tim J. ★ Tomas Hajek ★ Tom Demian ★ Tom Savage ★ Tracey Reuben ★ Travis Humble ★ Tree ★ Tyson Tan ★ Urm ★ Victoria ★ Victoria White ★ Vladislav Kurdyukov ★ Vlad Tomash ★ Westen Curry ★ Witt N. Vest ★ Xavier Claude ★ Yalyn Vinkindo ★ Yaroslav ★ Zeni Pong ★ Źmicier Kušnaroŭ
