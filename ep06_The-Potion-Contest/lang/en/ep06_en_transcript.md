# Transcript of Pepper&Carrot Episode 06 [en]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Episode 6: The Potion Contest

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Oh shoot, I fell asleep with the window open, again...
Pepper|2|True|...it's so windy...
Pepper|3|False|...and why can I see Komona through the window?
Pepper|4|False|KOMONA!
Pepper|5|False|The Potion Contest!
Pepper|6|False|I must've... must've accidentally fallen asleep!
Pepper|7|True|...but?
Pepper|8|False|Where am I ?!?
Bird|10|False|cK?|nowhitespace
Bird|9|True|qua|nowhitespace

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|!!!
Pepper|2|False|Carrot! You're so cute to have thought of bringing me to the Contest!
Pepper|3|False|Fan-tas-tic!
Pepper|4|True|You even thought of bringing a potion, my clothes, and my hat...
Pepper|5|False|...let's see which potion you brought...

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|WHAT?!!
Mayor of Komona|3|False|As Mayor of Komona, I declare the potion contest... Open!
Mayor of Komona|4|False|Our town is delighted to welcome no fewer than four witches for this first edition.
Mayor of Komona|5|True|Please give a
Mayor of Komona|6|True|huge
Writing|2|False|Komona Potion Contest
Mayor of Komona|7|False|round of applause:

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Audience|29|False|Clap
Mayor of Komona|1|True|All the way from the great Technologist's Union, it's an honor to welcome the ravishing and ingenious
Mayor of Komona|3|True|...not to forget our local girl, Komona's very own witch
Mayor of Komona|5|True|...Our third participant comes to us from the lands of the setting moons,
Mayor of Komona|7|True|...and finally, our last participant, from the forest of Squirrel's End,
Mayor of Komona|2|False|Coriander!
Mayor of Komona|4|False|Saffron!
Mayor of Komona|6|False|Shichimi!
Mayor of Komona|8|False|Pepper!
Mayor of Komona|9|True|Let the games begin!
Mayor of Komona|10|False|The vote will be by applaud-o-meter
Mayor of Komona|11|False|First up, Coriander's demonstration
Coriander|13|False|...fear death no more, thanks to my...
Coriander|14|True|...Potion of
Coriander|15|False|ZOMBIFICATION!
Audience|16|True|Clap
Audience|17|True|Clap
Audience|18|True|Clap
Audience|19|True|Clap
Audience|20|True|Clap
Audience|21|True|Clap
Audience|22|True|Clap
Audience|23|True|Clap
Audience|24|True|Clap
Audience|25|True|Clap
Audience|26|True|Clap
Audience|27|True|Clap
Audience|28|True|Clap
Coriander|12|False|Ladies & Gentlemen...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|False|FANTASTIC!
Audience|3|True|Clap
Audience|4|True|Clap
Audience|5|True|Clap
Audience|6|True|Clap
Audience|7|True|Clap
Audience|8|True|Clap
Audience|9|True|Clap
Audience|10|True|Clap
Audience|11|True|Clap
Audience|12|True|Clap
Audience|13|True|Clap
Audience|14|True|Clap
Audience|15|True|Clap
Audience|16|False|Clap
Saffron|18|True|Because here is
Saffron|17|True|...but please, save your applause, people of Komona!
Saffron|22|False|...make them jealous!
Saffron|19|True|MY
Saffron|25|False|POSHNESS!
Saffron|24|True|...Potion of
Saffron|23|False|...all this is possible with the simple application of a single drop of my...
Audience|26|True|Clap
Audience|27|True|Clap
Audience|28|True|Clap
Audience|29|True|Clap
Audience|30|True|Clap
Audience|31|True|Clap
Audience|32|True|Clap
Audience|33|True|Clap
Audience|34|True|Clap
Audience|35|True|Clap
Audience|36|True|Clap
Audience|37|True|Clap
Audience|38|True|Clap
Audience|39|True|Clap
Audience|40|False|Clap
Mayor of Komona|42|False|This potion could make all of Komona rich!
Mayor of Komona|41|True|Fantastic! Incredible!
Audience|44|True|Clap
Audience|45|True|Clap
Audience|46|True|Clap
Audience|47|True|Clap
Audience|48|True|Clap
Audience|49|True|Clap
Audience|50|True|Clap
Audience|51|True|Clap
Audience|52|True|Clap
Audience|53|True|Clap
Audience|54|True|Clap
Audience|55|True|Clap
Audience|56|True|Clap
Audience|57|True|Clap
Audience|58|True|Clap
Audience|59|True|Clap
Audience|60|False|Clap
Mayor of Komona|2|False|Coriander defies death itself with this mi-ra-cu-lous potion!
Saffron|21|True|The real potion you've all been waiting for: the one which will amaze your neighbors...
Mayor of Komona|43|False|Your applause can't be wrong. Coriander has already been eliminated.
Saffron|20|False|Potion!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|False|That last demonstration would now seem difficult to beat for Shichimi!
Shichimi|4|True|NO!
Shichimi|5|True|I cannot, it is too dangerous
Shichimi|6|False|SORRY!
Mayor of Komona|3|False|...come on Shichimi, everyone's waiting for you.
Mayor of Komona|7|False|It seems, Ladies & Gentlemen that Shichimi forfeits...
Saffron|8|False|Give me that!
Saffron|9|False|...and stop pretending to be shy, you're spoiling the show.
Saffron|10|False|Everyone already knows that I've won the contest no matter what your potion does...
Shichimi|11|False|!!!
Sound|12|False|BZZZIIOO
Shichimi|15|False|GIANT MONSTERS!
Shichimi|2|False|I... I did not know we had to give a demonstration.
Shichimi|13|True|CAREFUL!!!
Shichimi|14|True|It's a potion for

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bird|1|False|CAW-CAW-Caaaaaaaawwww
Sound|2|False|BAM!
Pepper|3|True|...huh, cool!
Pepper|5|False|...my potion will at least be worth a few laughs because...
Pepper|4|False|So it's my turn now?
Mayor of Komona|6|True|Run, idiot!
Mayor of Komona|7|False|The competition is over! ...save yourself!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|2|False|...as usual, everyone leaves just when it's our turn.
Pepper|1|True|There you go...
Pepper|4|True|At least I have an idea of what we'll be able to do with your "potion", Carrot.
Pepper|5|False|...put everything back in order here and head home!
Pepper|7|True|You
Pepper|8|False|Oversized-posh-zombie-canary!
Pepper|10|False|Wanna try one last potion? ...
Pepper|11|False|...not really, huh?
Pepper|6|False|HEY!
Sound|9|False|CRACK!

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Yeah, read the label, carefully...
Pepper|2|False|...I won't think twice before pouring it all over you if you don't get out of Komona right away!
Mayor of Komona|3|True|Because she saved our town when it was in peril
Mayor of Komona|4|False|we award first place to Pepper for her Potion of ... ??!!
Pepper|7|False|... huh... in fact, it's not really a potion ; it's my cat's pee sample from his last trip to the vet!
Pepper|6|True|... Haha! yep ...
Pepper|8|False|... no demo then ?...
Narrator|9|False|Episode 6: The Potion Contest
Narrator|10|False|FIN
Writing|5|False|50,000 Ko
Credits|11|False|March 2015 - Artwork and story by David Revoy - Translation by Alex Gryson

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Pepper&Carrot is entirely free(libre), open-source and sponsored thanks to the kind patronage of readers. For this episode, thank you to the 245 Patrons:
Credits|4|False|https://www.patreon.com/davidrevoy
Credits|3|False|You too can become a patron of Pepper&Carrot for the next episode:
Credits|7|False|Tools: This episode was 100% drawn with Free/Libre software Krita on Linux Mint
Credits|6|False|Open-source: all source files with layers and fonts, are available on the official site
Credits|5|False|License: Creative Commons Attribution You can modify, reshare, sell etc. ...
Credits|2|False|Алексей ★ Глеб Бузало ★ 獨孤欣 & 獨弧悦 ★ Addison Lewis ★ A Distinguished Robot ★ Adrian Lord ★ Ahmad Ali ★ Aina Reich ★ Alandran ★ Alan Hardman ★ Albert Westra ★ Alcide ★ Alejandro Flores Prieto ★ Alex ★ Alexander Bülow Tomassen ★ Alexander Sopicki ★ Alexandra Jordan ★ Alex Lusco ★ Alex Silver ★ Alex Vandiver ★ Alfredo ★ Ali Poulton (Aunty Pol) ★ Allan Zieser ★ Andreas Rieger ★ Andrej Kwadrin ★ Andrew ★ Andrew Godfrey ★ Andrey Alekseenko ★ Angela K ★ Anna Orlova ★ Antan Karmola ★ Anthony Edlin ★ Antonio Mendoza ★ Ardash Crowfoot ★ Arjun Chennu ★ Arne Brix ★ Aslak Kjølås-Sæverud ★ Axel Bordelon ★ Axel Philipsenburg ★ Barbix ★ Ben Evans ★ Bernd ★ Betsy Luntao ★ Boonsak Watanavisit ★ Boris Fauret ★ Boudewijn Rempt ★ BoxBoy ★ Brett Smith ★ Brian Behnke ★ Brian Smith ★ Bryan Butler ★ Bui Dang Hai Trieu ★ Carlos Levischi ★ Charlotte Lacombe-bar ★ Chris Radcliff ★ Chris Sakkas ★ Christian Gruenwaldner ★ Christophe Carré ★ Christopher Bates ★ Clara Dexter ★ codl ★ Colby Driedger ★ Conway Scott Smith ★ Cuthbert Williams ★ Cyrille Largillier ★ Cyril Paciullo ★ Damien ★ Daniel ★ Daniel Björkman ★ Danny Grimm ★ David Tang ★ DiCola Jamn ★ Dmitry ★ Donald Hayward ★ Duke ★ Eitan Goldshtrom ★ Enrico Billich ★ Epsilon ★ Eric Schulz ★ Faolan Grady ★ Francois Schnell ★ Garret Patterson ★ Ginny Hendricks ★ GreenAngel5 ★ Grigory Petrov ★ Guillaume ★ Guillaume Ballue ★ Gustav Strömbom ★ Guy Davis ★ Happy Mimic ★ Helmar Suschka ★ Henning Döscher ★ Ilyas ★ Irina Rempt ★ Ivan Korotkov ★ James Frazier ★ Janusz ★ Jared Tritsch ★ JDB ★ Jean-Baptiste Hebbrecht ★ Jean-Gabriel LOQUET ★ Jeffrey Schneider ★ Jessey Wright ★ Jim ★ Jim Street ★ Jiska ★ Joachim Schiele ★ JoÃ£o Luiz Machado Junior ★ Joern Konopka ★ joe rutledge ★ Johanne Thomson ★ John ★ John Urquhart Ferguson ★ Jónatan Nilsson ★ Jonathan Leroy ★ Jonathan Ringstad ★ Jon Brake ★ Jorge Bernal ★ Joseph Bowman ★ Julien Duroure ★ Justus Kat ★ Kai-Ting (Danil) Ko ★ Kasper Hansen ★ Kate ★ Kathryn Wuerstl ★ Ken Mingyuan Xia ★ Kingsquee ★ Kroet ★ Levi Kornelsen ★ Liselle ★ Lorentz Grip ★ L S ★ Luc Stepniewski ★ Luke Hochrein ★ MacCoy ★ Magnus Kronnäs ★ Manuel ★ Marc & Rick ★ Marcus ★ Martin Owens ★ Mary Brownlee ★ Masked Admirer ★ Mathias Stærk ★ Mefflin Ross Bullis-bates ★ Michael ★ Michael Gill ★ Michael Pureka ★ Michelle Pereira Garcia ★ Mike Mosher ★ Miroslav ★ Muzyka Dmytro ★ Nataya Castillo ★ Nazhif ★ Nicholas DeLateur ★ Nicholas Terranova ★ Nicki Aya ★ Nicola Angel ★ Nicolae Berbece ★ Nicole Heersema ★ Nielas Sinclair ★ NinjaKnight Comics ★ Noble Hays ★ Noelia Calles Marcos ★ Nora Czaykowski ★ Nyx ★ Olivier Amrein ★ Olivier Brun ★ Omar Willey ★ Oscar Moreno ★ Öykü Su Gürler ★ Ozone S. ★ Pablo Lopez Soriano ★ Pat David ★ Patrick Gamblin ★ Paul ★ Pavel Semenov ★ Pet0r ★ Peter ★ Peter Moonen ★ Petr Vlašic ★ Philippe Jean Edward Bateman ★ Pierre Geier ★ Pierre Vuillemin ★ Pranab Shenoy ★ Pyves & Ran ★ Raghavendra Kamath ★ Rajul Gupta ★ Reorx Meng ★ Ret Samys ★ Rictic ★ RJ van der Weide ★ Roberto Zaghis ★ Roman ★ Rumiko Hoshino ★ Rustin Simons ★ Sally Bridgewater ★ Sami T ★ Samuel Mitson ★ Scott Petrovic ★ Sean Adams ★ Shadefalcon ★ ShadowMist ★ Shafak ★ Shawn Meyer ★ Simon Forster ★ Simon Isenberg ★ Sonny W. ★ Soriac ★ Stanislav Vodetskyi ★ Stephanie Cheshire ★ Stephen Bates ★ Stephen Smoogen ★ Steven Bennett ★ Stuart Dickson ★ Surt ★ TamaskanLEM ★ Tar8156 ★ Terry Hancock ★ TheFaico ★ Thomas Citharel ★ Thomas Courbon ★ Thomas Schwery ★ Tim Burbank ★ Tim J. ★ Tomas Hajek ★ Tom Demian ★ Tom Savage ★ Tracey Reuben ★ Travis Humble ★ Tree ★ Tyson Tan ★ Urm ★ Victoria ★ Victoria White ★ Vladislav Kurdyukov ★ Vlad Tomash ★ Westen Curry ★ Witt N. Vest ★ Xavier Claude ★ Yalyn Vinkindo ★ Yaroslav ★ Zeni Pong ★ Źmicier Kušnaroŭ
