# Transcript of Pepper&Carrot Episode 06 [pl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tytuł|1|False|Odcinek 6: Konkurs eliksirów

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Kurka wodna, znowu zasnęłam przy otwartym oknie.
Pepper|2|True|Strasznie wieje...
Pepper|3|False|Czemu nie widać stąd Komony?
Pepper|4|False|Komona!
Pepper|5|False|Konkurs eliksirów!
Pepper|6|False|Musiałam... Widocznie musiałam zasnąć!
Pepper|8|True|Ale...?
Pepper|9|False|Gdzie ja jestem?!
Bird|10|False|kwa?|nowhitespace

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|!!!
Pepper|2|False|Carrot! Jak miło, że pomyślałeś o zabraniu mnie na konkurs!
Pepper|3|False|Fan-ta-sty-cznie!
Pepper|4|True|Nie zapomniałeś nawet o eliksirze, ubraniach i kapeluszu.
Pepper|5|False|Zobaczmy, którą miksturę wziąłeś.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Co?!
Mayor of Komona|3|False|Niniejszym, jako burmistrz Komony, rozpoczynam konkurs eliksirów!
Mayor of Komona|4|False|Nasze miasto zostało zaszczycone obecnością czterech czarownic. Zdarza się nam to po raz pierwszy.
Mayor of Komona|5|True|Proszę o
Mayor of Komona|6|True|duże
Napis|2|False|Komona Potion Contest
Mayor of Komona|7|False|brawa:

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Audience|29|False|klask
Mayor of Komona|1|True|Zaszczyciła nas, przybywając wprost z wielkiej Unii Technologów, przeurocza i pomysłowa
Mayor of Komona|3|True|Oczywyście nie wolno nam zapomnieć o lokalnej wiedźmie
Mayor of Komona|5|True|Trzecia uczestniczka, pochodząca z krainy zachodzącego Księżyca,
Mayor of Komona|7|True|I w końcu panienka z lasu Wiewiórczego Końca,
Mayor of Komona|2|False|Coriander!
Mayor of Komona|4|False|Saffron!
Mayor of Komona|6|False|Shichimi!
Mayor of Komona|8|False|Pepper!
Mayor of Komona|9|True|Niech rozpoczną się zawody!
Mayor of Komona|10|False|Głosować będziemy poprzez aplauz.
Mayor of Komona|11|False|Na początek pokaz Coriander.
Coriander|13|False|nie musicie bać się już śmierci, a wszystko dzięki mojej
Coriander|14|True|miksturze
Coriander|15|False|ZOMBIFIKACJI!
Audience|16|True|klask
Audience|17|True|klask
Audience|18|True|klask
Audience|19|True|klask
Audience|20|True|klask
Audience|21|True|klask
Audience|22|True|klask
Audience|23|True|klask
Audience|24|True|klask
Audience|25|True|klask
Audience|26|True|klask
Audience|27|True|klask
Audience|28|True|klask
Coriander|12|False|Panie i panowie,

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|False|FANTASTYCZNE!
Audience|3|True|klask
Audience|4|True|klask
Audience|5|True|klask
Audience|6|True|klask
Audience|7|True|klask
Audience|8|True|klask
Audience|9|True|klask
Audience|10|True|klask
Audience|11|True|klask
Audience|12|True|klask
Audience|13|True|klask
Audience|14|True|klask
Audience|15|True|klask
Audience|16|False|klask
Saffron|18|True|Oto mój eliksir!
Saffron|17|True|Mieszkańcy Komony, wstrzymajcie swe brawa.
Saffron|20|False|czyniąc ich zazdrosnymi.
Saffron|23|False|ELEGANCJI!
Saffron|22|True|Eliksiru
Saffron|21|True|Wszystko to jest możliwe dzięki pojedynczej kropelce mojego...
Audience|25|True|klask
Audience|26|True|klask
Audience|27|True|klask
Audience|28|True|klask
Audience|29|True|klask
Audience|30|True|klask
Audience|31|True|klask
Audience|32|True|klask
Audience|33|True|klask
Audience|34|True|klask
Audience|35|True|klask
Audience|36|True|klask
Audience|37|True|klask
Audience|38|True|klask
Audience|39|False|klask
Mayor of Komona|41|False|Dzięki niemu wszyscy mogą być szykowni!
Mayor of Komona|40|True|Fantastyczne! Niewiarygodne!
Audience|43|True|klask
Audience|44|True|klask
Audience|45|True|klask
Audience|46|True|klask
Audience|47|True|klask
Audience|48|True|klask
Audience|49|True|klask
Audience|50|True|klask
Audience|51|True|klask
Audience|52|True|klask
Audience|53|True|klask
Audience|54|True|klask
Audience|55|True|klask
Audience|56|True|klask
Audience|57|True|klask
Audience|58|True|klask
Audience|59|False|klask
Mayor of Komona|2|False|Dzięki tej cu-do-wnej miksturze, Coriander śmieje się śmierci w twarz!
Saffron|19|True|Mikstura, na którą wszyscy czekaliście. Ta, która zadziwi waszych sąsiadów,
Mayor of Komona|42|False|Wasz aplauz wskazuje na to, że Coriander została pokonana.

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|False|Czy Shichimi podoła zadaniu pokonania swej przedmówczyni?
Shichimi|4|True|Nie!
Shichimi|5|True|Nie mogę, to zbyt niebezpieczne!
Shichimi|6|False|Przepraszam!
Mayor of Komona|3|False|Dalej, Shichimi, wszyscy na ciebie czekają.
Mayor of Komona|7|False|Panie i panowie, wygląda na to, że Shichimi utra...
Saffron|8|False|Daj mi to!
Saffron|9|False|I nie udawaj, że jesteś taka wstydliwa, psujesz całą zabawę.
Saffron|10|False|Wszyscy wiedzą, że wygrałam, niezależnie od tego, co pokażesz.
Shichimi|11|False|!!!
Dźwięk|12|False|BZZZIIUU
Shichimi|15|False|wielkie monstrum!
Shichimi|2|False|Nie wiedziałam, że będziemy musiały demonstrować.
Shichimi|13|True|Uważaj!
Shichimi|14|True|To eliksir przemiany w

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bird|1|False|ĆWIR-ĆWIR-ĆWIR
Dźwięk|2|False|BAM!
Pepper|3|True|Fajne!
Pepper|5|False|Moja mikstura przynajmniej będzie śmieszna, bo...
Pepper|4|False|Teraz moja kolej?
Mayor of Komona|6|True|Uciekaj, głupia!
Mayor of Komona|7|False|Konkurs odwołany!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|2|False|Wszyscy jak zwykle wychodzą, gdy przychodzi nasza kolej.
Pepper|1|True|Tutaj jesteś...
Pepper|4|True|Przynajmniej mam pomysł na to, co możemy zrobić z twoją miksturą, Carrot.
Pepper|5|False|Przywrócimy wszystko do ładu i wracamy do domu.
Pepper|7|True|Ty
Pepper|8|False|wielki-elegancki-zombie-kanarku!
Pepper|10|False|Chcesz spróbować mojego eliksiru?
Pepper|11|False|Niezbyt, co?
Pepper|6|False|Hej!
Dźwięk|9|False|TRACH!

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Słuchaj, przeczytaj uważnie.
Pepper|2|False|Nie zawaham się go użyć, jeśli natychmiast stąd nie odlecisz.
Mayor of Komona|3|True|Ponieważ uratowała nas, gdy byliśmy w potrzebie,
Mayor of Komona|4|False|nagradzamy Pepper pierwszym miejscem, za jej eliksir... czego?
Pepper|7|False|Tak naprawdę to nie jest żaden eliksir, tylko próbka moczu mojego kota z ostatniej wizyty u weterynarza,
Pepper|6|True|Haha! No właśnie...
Pepper|8|False|więc może jej nie testujmy.
Narrator|9|False|Odcinek 6: Konkurs eliksirów
Narrator|10|False|KONIEC
Napis|5|False|50,000 Ko
Credits|11|False|Marzec 2015 - Rysunki i fabuła: David Revoy - Przekład: Sölve Svartskogen

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Pepper&Carrot jest całkowicie darmowy, open-source'owy oraz wspierany przez naszych czytelników. Odcinek ufundowało 245 patronów:
Credits|4|False|https://www.patreon.com/davidrevoy
Credits|3|False|Ty również możesz zostać patronem następnego odcinka Pepper&Carrot:
Credits|7|False|Narzędzia: odcinek ten został w 100% narysowany w wolnym oprogramowaniu Krita na Linux Mint
Credits|6|False|Open-source: wszystkie pliki źródłowe z warstwami oraz czcionki są dostępne na oficjalnej stronie
Credits|5|False|Licencja: Creative Commons Uznanie Autorstwa Możesz modyfikować, wrzucać na inne strony, sprzedawać itd...
Credits|2|False|Алексей ★ Глеб Бузало ★ 獨孤欣 & 獨弧悦 ★ Addison Lewis ★ A Distinguished Robot ★ Adrian Lord ★ Ahmad Ali ★ Aina Reich ★ Alandran ★ Alan Hardman ★ Albert Westra ★ Alcide ★ Alejandro Flores Prieto ★ Alex ★ Alexander Bülow Tomassen ★ Alexander Sopicki ★ Alexandra Jordan ★ Alex Lusco ★ Alex Silver ★ Alex Vandiver ★ Alfredo ★ Ali Poulton (Aunty Pol) ★ Allan Zieser ★ Andreas Rieger ★ Andrej Kwadrin ★ Andrew ★ Andrew Godfrey ★ Andrey Alekseenko ★ Angela K ★ Anna Orlova ★ Antan Karmola ★ Anthony Edlin ★ Antonio Mendoza ★ Ardash Crowfoot ★ Arjun Chennu ★ Arne Brix ★ Aslak Kjølås-Sæverud ★ Axel Bordelon ★ Axel Philipsenburg ★ Barbix ★ Ben Evans ★ Bernd ★ Betsy Luntao ★ Boonsak Watanavisit ★ Boris Fauret ★ Boudewijn Rempt ★ BoxBoy ★ Brett Smith ★ Brian Behnke ★ Brian Smith ★ Bryan Butler ★ Bui Dang Hai Trieu ★ Carlos Levischi ★ Charlotte Lacombe-bar ★ Chris Radcliff ★ Chris Sakkas ★ Christian Gruenwaldner ★ Christophe Carré ★ Christopher Bates ★ Clara Dexter ★ codl ★ Colby Driedger ★ Conway Scott Smith ★ Cuthbert Williams ★ Cyrille Largillier ★ Cyril Paciullo ★ Damien ★ Daniel ★ Daniel Björkman ★ Danny Grimm ★ David Tang ★ DiCola Jamn ★ Dmitry ★ Donald Hayward ★ Duke ★ Eitan Goldshtrom ★ Enrico Billich ★ Epsilon ★ Eric Schulz ★ Faolan Grady ★ Francois Schnell ★ Garret Patterson ★ Ginny Hendricks ★ GreenAngel5 ★ Grigory Petrov ★ Guillaume ★ Guillaume Ballue ★ Gustav Strömbom ★ Guy Davis ★ Happy Mimic ★ Helmar Suschka ★ Henning Döscher ★ Ilyas ★ Irina Rempt ★ Ivan Korotkov ★ James Frazier ★ Janusz ★ Jared Tritsch ★ JDB ★ Jean-Baptiste Hebbrecht ★ Jean-Gabriel LOQUET ★ Jeffrey Schneider ★ Jessey Wright ★ Jim ★ Jim Street ★ Jiska ★ Joachim Schiele ★ JoÃ£o Luiz Machado Junior ★ Joern Konopka ★ joe rutledge ★ Johanne Thomson ★ John ★ John Urquhart Ferguson ★ Jónatan Nilsson ★ Jonathan Leroy ★ Jonathan Ringstad ★ Jon Brake ★ Jorge Bernal ★ Joseph Bowman ★ Julien Duroure ★ Justus Kat ★ Kai-Ting (Danil) Ko ★ Kasper Hansen ★ Kate ★ Kathryn Wuerstl ★ Ken Mingyuan Xia ★ Kingsquee ★ Kroet ★ Levi Kornelsen ★ Liselle ★ Lorentz Grip ★ L S ★ Luc Stepniewski ★ Luke Hochrein ★ MacCoy ★ Magnus Kronnäs ★ Manuel ★ Marc & Rick ★ Marcus ★ Martin Owens ★ Mary Brownlee ★ Masked Admirer ★ Mathias Stærk ★ Mefflin Ross Bullis-bates ★ Michael ★ Michael Gill ★ Michael Pureka ★ Michelle Pereira Garcia ★ Mike Mosher ★ Miroslav ★ Muzyka Dmytro ★ Nataya Castillo ★ Nazhif ★ Nicholas DeLateur ★ Nicholas Terranova ★ Nicki Aya ★ Nicola Angel ★ Nicolae Berbece ★ Nicole Heersema ★ Nielas Sinclair ★ NinjaKnight Comics ★ Noble Hays ★ Noelia Calles Marcos ★ Nora Czaykowski ★ Nyx ★ Olivier Amrein ★ Olivier Brun ★ Omar Willey ★ Oscar Moreno ★ Öykü Su Gürler ★ Ozone S. ★ Pablo Lopez Soriano ★ Pat David ★ Patrick Gamblin ★ Paul ★ Pavel Semenov ★ Pet0r ★ Peter ★ Peter Moonen ★ Petr Vlašic ★ Philippe Jean Edward Bateman ★ Pierre Geier ★ Pierre Vuillemin ★ Pranab Shenoy ★ Pyves & Ran ★ Raghavendra Kamath ★ Rajul Gupta ★ Reorx Meng ★ Ret Samys ★ Rictic ★ RJ van der Weide ★ Roberto Zaghis ★ Roman ★ Rumiko Hoshino ★ Rustin Simons ★ Sally Bridgewater ★ Sami T ★ Samuel Mitson ★ Scott Petrovic ★ Sean Adams ★ Shadefalcon ★ ShadowMist ★ Shafak ★ Shawn Meyer ★ Simon Forster ★ Simon Isenberg ★ Sonny W. ★ Soriac ★ Stanislav Vodetskyi ★ Stephanie Cheshire ★ Stephen Bates ★ Stephen Smoogen ★ Steven Bennett ★ Stuart Dickson ★ Surt ★ TamaskanLEM ★ Tar8156 ★ Terry Hancock ★ TheFaico ★ Thomas Citharel ★ Thomas Courbon ★ Thomas Schwery ★ Tim Burbank ★ Tim J. ★ Tomas Hajek ★ Tom Demian ★ Tom Savage ★ Tracey Reuben ★ Travis Humble ★ Tree ★ Tyson Tan ★ Urm ★ Victoria ★ Victoria White ★ Vladislav Kurdyukov ★ Vlad Tomash ★ Westen Curry ★ Witt N. Vest ★ Xavier Claude ★ Yalyn Vinkindo ★ Yaroslav ★ Zeni Pong ★ Źmicier Kušnaroŭ
