# Transcript of Pepper&Carrot Episode 06 [br]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Rann 6: Ar genstrivadeg drammoù

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Chaous, aet on da'm gwele hep serriñ ar prenestr c'hoazh ...
Pepper|2|True|... nag a avel zo!
Pepper|3|False|... ha perak bennak e welan Komona dre ar prenestr?
Pepper|4|False|KOMONA!
Pepper|5|False|Ar genstrivadeg drammoù.
Pepper|6|True|'Michañs... Emichañs on manet kousket hep gouzout din!
Pepper|9|True|... arsa?
Pepper|10|False|Pelec'h emaon aze ?!?
Bird|12|False|k?|nowhitespace
Bird|11|True|kwa|nowhitespace

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|!!!
Pepper|2|False|Carrot! Moutig ken-ken out pa'c'h eus soñjet em c'has d'ar genstrivadeg!
Pepper|3|False|Dreist-da-gaer!
Pepper|4|True|Ha soñjet ec'h eus zoken e kemer un dramm, ma dilhad ha ma zog...
Pepper|5|False|... gwelomp 'ta peseurt dramm ec'h eus kemeret ...

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|PETRA?!!
Mayor of Komona|3|False|Me, Maer Komona, a ziskleir ez eo digor ar genstrivadeg drammoù!
Mayor of Komona|4|False|Stad zo en hor c'hêr o tegemer peder sorserez paneveken evit an taol kentañ-mañ.
Mayor of Komona|5|True|Ho trugarekaat a reomp da stlakañ ho taouarn
Mayor of Komona|6|False|kreñv-tre dezhe.
Writing|2|False|Kenstrivadeg drammoù Komona

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Audience|30|False|Stlak
Mayor of Komona|1|True|Deuet eus bro vras Unaniezh an Deknologourien, un enor eo dimp degemer ar goantenn leun a ijin,
Mayor of Komona|3|True|... hep ankouazh plac'h ar vro, ar sorserez eus Komona
Mayor of Komona|5|True|... an drede o kemer perzh a zeu eus bro al loarioù o vont da guzh
Mayor of Komona|7|True|... hag erziwezh, ar berzhiadez diwezhañ o tont eus Koad Toull ar Gwiñver
Mayor of Komona|2|False|Koriandrez!
Mayor of Komona|4|False|Safron!
Mayor of Komona|6|False|Shichimi!
Mayor of Komona|8|False|Pepper!
Mayor of Komona|9|True|Digor eo ar genstrivadeg!
Mayor of Komona|10|False|Votet e vo dre stlakañ an daouarn!
Mayor of Komona|11|False|Ha da gentañ, da Goriandrez da ziskouez galloud he died.
Coriander|13|False|Itronezed hag Aotrounez...
Coriander|14|True|... n'hoc'h eus ket da spontañ ken rak ar marv a-drugarez ...
Coriander|15|True|.. da'm Dramm
Coriander|16|False|ZOMBIEKADUR!
Audience|17|True|Stlak
Audience|18|True|Stlak
Audience|19|True|Stlak
Audience|20|True|Stlak
Audience|21|True|Stlak
Audience|22|True|Stlak
Audience|23|True|Stlak
Audience|24|True|Stlak
Audience|25|True|Stlak
Audience|26|True|Stlak
Audience|27|True|Stlak
Audience|28|True|Stlak
Audience|29|True|Sltak

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|True|BISKOAZH !
Mayor of Komona|2|False|Daeañ a ra Koriandrez an Ankoù e-unan gant an dramm-mañ ken bur-zhu-dus !
Audience|3|True|Stlak
Audience|4|True|Stlak
Audience|5|True|Stlak
Audience|6|True|Stlak
Audience|7|True|Stlak
Audience|8|True|Stlak
Audience|9|True|Stlak
Audience|10|True|Stlak
Audience|11|True|Stlak
Audience|12|True|Stlak
Audience|13|True|Stlak
Audience|14|True|Stlak
Audience|15|True|Stlak
Audience|16|False|Stlak
Saffron|18|True|Rak setu
Saffron|17|False|... Ac'hanta 'ta! Mirit ho stlakadennoù, pobl a Gomona!
Saffron|22|False|... lakaat gwarizi enne!
Saffron|19|True|MA
Saffron|25|False|BOURC'HIZEREZH!
Saffron|24|True|... dramm
Saffron|23|True|Hiviziken e c'hall bezañ graet dre skuilhañ un dakennig eus ma ...
Audience|27|True|Stlak
Audience|28|True|Stlak
Audience|29|True|Stlak
Audience|30|True|Stlak
Audience|31|True|Stlak
Audience|32|True|Stlak
Audience|33|True|Stlak
Audience|34|True|Stlak
Audience|35|True|Stlak
Audience|36|True|Stlak
Audience|37|True|Stlak
Audience|38|True|Stlak
Audience|39|True|Stlak
Audience|40|True|Stlak
Audience|42|False|Stlak
Mayor of Komona|44|False|Gant an dramm-se e c'hallfe ar vegenn eus Komona dont da binvidik!
Mayor of Komona|43|True|Biskoazh c'hoazh! Digredus!
Audience|46|True|Stlak
Audience|47|True|Stlak
Audience|48|True|Stlak
Audience|49|True|Stlak
Audience|50|True|Stlak
Audience|51|True|Stlak
Audience|52|True|Stlak
Audience|53|True|Stlak
Audience|54|True|Stlak
Audience|55|True|Stlak
Audience|56|True|Stlak
Audience|57|True|Stlak
Audience|58|True|Stlak
Audience|59|True|Stlak
Audience|60|True|Stlak
Audience|61|True|Stlak
Audience|62|False|Stlak
Mayor of Komona|45|False|Sklaer ha splann eo ho stlakadeg; skarzhet eo Koriandrez end-eeun.
Saffron|21|True|Ar gwir zramm emaoc'h holl o c'hortoz: an hini a c'hallit bamañ hoc'h holl amezeien gantañ ...
Saffron|20|False|Dramm

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|False|Diaes da Shichimi ober par!
Shichimi|4|True|Ne rin ket!
Shichimi|5|True|Ne c'hallan ket, re zañjerus.
Shichimi|6|False|DIGAREZ!
Mayor of Komona|3|False|... Ata Shichimi, emañ an holl o c'hortoz.
Mayor of Komona|7|False|War a seblant, Itronezed hag Aotrounez en em dennfe Shichimi ...
Saffron|8|False|Ro din an dra-mañ!
Saffron|9|False|Ha paouez d'ober da damm abaf ha na drenk ket an abadenn.
Saffron|10|False|Gouzout a oar an holl ez eo deuet ar maout ganin, ne vern eta ar pezh a ra da zramm...
Shichimi|11|False|!!!
Sound|12|False|BZZZIIOO
Shichimi|15|False|EUZHVIL RAMZEL!
Shichimi|2|False|Ne... ne ouien ket e rankjemp ober un diskouez.
Shichimi|13|True|DIWALL!!!
Shichimi|14|True|Bez' eo un dramm

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bird|1|False|KRiKRiKRiiiiiiiiiiiiiiiiiiiiii
Sound|2|False|DRAO!
Pepper|3|True|... eee fiskal!
Pepper|5|False|Gant ma dramm bepred e tlefec'h dirollañ da c'hoarzhin.
Pepper|4|False|Da'm zro eo bremañ eta?
Mayor of Komona|6|True|Tec'h, sodez!
Mayor of Komona|7|False|Echu eo ar genstrivadeg! Sach da groc'hen ganit!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|2|False|evel boaz ez a an holl kuit pa zeu hon tro.
Pepper|1|True|Hag adarre:
Pepper|4|True|Bezet pe vezet, ur soñj bennak am eus eus ar pezh a c'hallfemp ober gant da «dramm».
Pepper|5|False|Adlakaomp urzh amañ ha d'ar gêr en-dro!
Pepper|7|True|Te ar
Pepper|8|False|Pezh-nenig-zombi-bourc'hiz!
Pepper|10|False|Plijout a rafe dit kaout un tañva eus an dramm diwezhañ?...
Pepper|11|False|...'Rafe ket hañ?
Pepper|6|False|C'HE!
Sound|9|False|STRACK!

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Ya, lenn mat an dikedenn...
Pepper|2|False|... Skuilhet 'vo hardizh warnout ma ne skarzhez ket diouzhtu eus Komona!
Mayor of Komona|3|True|Peogwir he deus tennet hor c'hêr eus an drast
Mayor of Komona|4|False|ez a ar priz kentañ gant Pepper evit he Dramm ... ??!!
Pepper|7|False|... eee ... e gwirionez, n'eo ket un dramm gwir; standilhonoù troazh ma c'hazh eo, e weladenn vezeg!
Pepper|6|True|... Haha! ya ...
Pepper|8|False|... 'vo diskouez ebet, hañ?...
Narrator|9|False|Rann 6: Ar genstrivadeg drammoù
Narrator|10|False|ECHU
Writing|5|False|50 000 Ko
Credits|11|False|March 2015 - Tresadennoù ha Senario : David Revoy

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Pepper&Carrot zo digoust, mammenn-digor, ha sponsoriset a-drugarez da vesenerezh al lennerien, evit ar rann-mañ, trugarez d'ar 245 Mesen :
Credits|4|False|https://www.patreon.com/davidrevoy
Credits|3|False|C'hwi ivez, deuit da vezañ ur mesen eus Pepper&Carrot evit ar rann a zeu::
Credits|7|False|Ostilhoù : treset eo bet ar rann-mañ 100% gant meziantoù frank Krita war Linux Mint
Credits|6|False|Mammenn-digor: an holl vammennoù, fontoù skrivañ, restroù gant kalkoù a c'haller kaout da bellgargañ el lec'hienn gefridiel
Credits|5|False|Lañvaz : Creative Commons menegiñ an oberour 'David Revoy' gallout a rit cheñch, skignañ, gwerzhañ, etc...
Credits|2|False|Алексей ★ Глеб Бузало ★ 獨孤欣 & 獨弧悦 ★ Addison Lewis ★ A Distinguished Robot ★ Adrian Lord ★ Ahmad Ali ★ Aina Reich ★ Alandran ★ Alan Hardman ★ Albert Westra ★ Alcide ★ Alejandro Flores Prieto ★ Alex ★ Alexander Bülow Tomassen ★ Alexander Sopicki ★ Alexandra Jordan ★ Alex Lusco ★ Alex Silver ★ Alex Vandiver ★ Alfredo ★ Ali Poulton (Aunty Pol) ★ Allan Zieser ★ Andreas Rieger ★ Andrej Kwadrin ★ Andrew ★ Andrew Godfrey ★ Andrey Alekseenko ★ Angela K ★ Anna Orlova ★ Antan Karmola ★ Anthony Edlin ★ Antonio Mendoza ★ Ardash Crowfoot ★ Arjun Chennu ★ Arne Brix ★ Aslak Kjølås-Sæverud ★ Axel Bordelon ★ Axel Philipsenburg ★ Barbix ★ Ben Evans ★ Bernd ★ Betsy Luntao ★ Boonsak Watanavisit ★ Boris Fauret ★ Boudewijn Rempt ★ BoxBoy ★ Brett Smith ★ Brian Behnke ★ Brian Smith ★ Bryan Butler ★ Bui Dang Hai Trieu ★ Carlos Levischi ★ Charlotte Lacombe-bar ★ Chris Radcliff ★ Chris Sakkas ★ Christian Gruenwaldner ★ Christophe Carré ★ Christopher Bates ★ Clara Dexter ★ codl ★ Colby Driedger ★ Conway Scott Smith ★ Cuthbert Williams ★ Cyrille Largillier ★ Cyril Paciullo ★ Damien ★ Daniel ★ Daniel Björkman ★ Danny Grimm ★ David Tang ★ DiCola Jamn ★ Dmitry ★ Donald Hayward ★ Duke ★ Eitan Goldshtrom ★ Enrico Billich ★ Epsilon ★ Eric Schulz ★ Faolan Grady ★ Francois Schnell ★ Garret Patterson ★ Ginny Hendricks ★ GreenAngel5 ★ Grigory Petrov ★ Guillaume ★ Guillaume Ballue ★ Gustav Strömbom ★ Guy Davis ★ Happy Mimic ★ Helmar Suschka ★ Henning Döscher ★ Ilyas ★ Irina Rempt ★ Ivan Korotkov ★ James Frazier ★ Janusz ★ Jared Tritsch ★ JDB ★ Jean-Baptiste Hebbrecht ★ Jean-Gabriel LOQUET ★ Jeffrey Schneider ★ Jessey Wright ★ Jim ★ Jim Street ★ Jiska ★ Joachim Schiele ★ JoÃ£o Luiz Machado Junior ★ Joern Konopka ★ joe rutledge ★ Johanne Thomson ★ John ★ John Urquhart Ferguson ★ Jónatan Nilsson ★ Jonathan Leroy ★ Jonathan Ringstad ★ Jon Brake ★ Jorge Bernal ★ Joseph Bowman ★ Julien Duroure ★ Justus Kat ★ Kai-Ting (Danil) Ko ★ Kasper Hansen ★ Kate ★ Kathryn Wuerstl ★ Ken Mingyuan Xia ★ Kingsquee ★ Kroet ★ Levi Kornelsen ★ Liselle ★ Lorentz Grip ★ L S ★ Luc Stepniewski ★ Luke Hochrein ★ MacCoy ★ Magnus Kronnäs ★ Manuel ★ Marc & Rick ★ Marcus ★ Martin Owens ★ Mary Brownlee ★ Masked Admirer ★ Mathias Stærk ★ Mefflin Ross Bullis-bates ★ Michael ★ Michael Gill ★ Michael Pureka ★ Michelle Pereira Garcia ★ Mike Mosher ★ Miroslav ★ Muzyka Dmytro ★ Nataya Castillo ★ Nazhif ★ Nicholas DeLateur ★ Nicholas Terranova ★ Nicki Aya ★ Nicola Angel ★ Nicolae Berbece ★ Nicole Heersema ★ Nielas Sinclair ★ NinjaKnight Comics ★ Noble Hays ★ Noelia Calles Marcos ★ Nora Czaykowski ★ Nyx ★ Olivier Amrein ★ Olivier Brun ★ Omar Willey ★ Oscar Moreno ★ Öykü Su Gürler ★ Ozone S. ★ Pablo Lopez Soriano ★ Pat David ★ Patrick Gamblin ★ Paul ★ Pavel Semenov ★ Pet0r ★ Peter ★ Peter Moonen ★ Petr Vlašic ★ Philippe Jean Edward Bateman ★ Pierre Geier ★ Pierre Vuillemin ★ Pranab Shenoy ★ Pyves & Ran ★ Raghavendra Kamath ★ Rajul Gupta ★ Reorx Meng ★ Ret Samys ★ Rictic ★ RJ van der Weide ★ Roberto Zaghis ★ Roman ★ Rumiko Hoshino ★ Rustin Simons ★ Sally Bridgewater ★ Sami T ★ Samuel Mitson ★ Scott Petrovic ★ Sean Adams ★ Shadefalcon ★ ShadowMist ★ Shafak ★ Shawn Meyer ★ Simon Forster ★ Simon Isenberg ★ Sonny W. ★ Soriac ★ Stanislav Vodetskyi ★ Stephanie Cheshire ★ Stephen Bates ★ Stephen Smoogen ★ Steven Bennett ★ Stuart Dickson ★ Surt ★ TamaskanLEM ★ Tar8156 ★ Terry Hancock ★ TheFaico ★ Thomas Citharel ★ Thomas Courbon ★ Thomas Schwery ★ Tim Burbank ★ Tim J. ★ Tomas Hajek ★ Tom Demian ★ Tom Savage ★ Tracey Reuben ★ Travis Humble ★ Tree ★ Tyson Tan ★ Urm ★ Victoria ★ Victoria White ★ Vladislav Kurdyukov ★ Vlad Tomash ★ Westen Curry ★ Witt N. Vest ★ Xavier Claude ★ Yalyn Vinkindo ★ Yaroslav ★ Zeni Pong ★ Źmicier Kušnaroŭ
