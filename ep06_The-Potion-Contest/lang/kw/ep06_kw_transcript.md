# Transcript of Pepper&Carrot Episode 06 [kw]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Rann 6: An Kesstrif Ismek

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Molleth, my a godhas yn kosk gans an fenester ygor arta...
Pepper|2|True|...ass yw gwynsek...
Pepper|3|False|...ha fatel allav vy gweles Komona dres an fenester?
Pepper|4|False|KOMONA!
Pepper|5|False|An Kesstrif Ismek!
Pepper|6|False|Dell hevel... my a godhas yn kosk dre happ!
Pepper|9|True|...mes?
Pepper|10|False|Ple'th esov vy?!?
Bird|12|True|kwa
Bird|13|False|cK?|nowhitespace

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|!!!
Pepper|2|False|Carrot! Ass os ta hweg a dybi ahanav ha'm dri dhe'n Kesstrif!
Pepper|3|False|Mar-thys da!
Pepper|4|True|Ty a dybis a dhri ismegen, ow dillas, ha'm hatt...
Pepper|5|False|...gwelyn py ismegen a dhresys...

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|PYTH?!!
Mayor of Komona|3|False|Avel Mer Komona, y tiskleryav an kesstrif ismek... Ygor!
Mayor of Komona|4|False|Pur lowen yw agan tre dhe dhynerghi peder gwragh dhe'n kynsa tro ma.
Mayor of Komona|5|True|Rewgh mar pleg
Mayor of Komona|6|True|garm wormola
Mayor of Komona|7|False|kowrek:
Writing|2|False|Kesstrif Ismek Komona

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|True|Dhyworth an Unyans Teknologieth meur, enor yw dhe dhynnerghi an ombrofyores teg ha konnyk
Mayor of Komona|3|True|...heb ankevi agan mowes leel, gwragh Komona
Mayor of Komona|5|True|...An tressa a dheu dhyn dhyworth powyow an loryow ow sedhi
Mayor of Komona|7|True|...ha wortiwedh, agan diwettha ombrofyores dhyworth koos Penn Gwiwer,
Mayor of Komona|2|False|Coriander!
Mayor of Komona|4|False|Saffron!
Mayor of Komona|6|False|Shichimi!
Mayor of Komona|8|False|Pepper!
Mayor of Komona|9|False|Re dhallattho an gwariow!
Mayor of Komona|10|True|Votyewgh dre dackya diwla!
Mayor of Komona|11|False|Yn kynsa, displetyans Coriander
Coriander|13|True|...na berthewgh own a ankow namoy, awos ow...
Coriander|14|True|...Ismegen a
Coriander|15|False|ZOMBIEKHEANS !
Coriander|12|False|Benenes & Gwer...
Audience|16|True|Tack
Audience|17|True|Tack
Audience|18|True|Tack
Audience|19|True|Tack
Audience|20|True|Tack
Audience|21|True|Tack
Audience|22|True|Tack
Audience|23|True|Tack
Audience|24|True|Tack
Audience|25|True|Tack
Audience|26|True|Tack
Audience|27|True|Tack
Audience|28|True|Tack
Audience|29|False|Tack

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Audience|3|True|Tack
Audience|4|True|Tack
Audience|5|True|Tack
Audience|6|True|Tack
Mayor of Komona|1|True|MARTHYS!
Audience|7|True|Tack
Audience|8|True|Tack
Audience|9|True|Tack
Audience|10|True|Tack
Audience|11|True|Tack
Audience|12|True|Tack
Audience|13|True|Tack
Audience|14|True|Tack
Audience|15|True|Tack
Audience|16|False|Tack
Audience|30|True|Tack
Audience|31|True|Tack
Audience|32|True|Tack
Audience|33|True|Tack
Saffron|22|True|Ottomma
Saffron|21|False|...mes, mar pleg, gwithewgh agas gormola, tus a Komona!
Saffron|26|False|...ha gul dhedha perthi avi!
Saffron|23|True|OW
Mayor of Komona|2|False|Coriander a dhefi ankow y honan gans an ismegen varthys ma!
Saffron|25|True|An ismegen wir a wortowgh rygdhi: an huni a wra sowdhanas agas kentrevogyon...
Saffron|29|False|AFINANS!
Saffron|28|True|...Ismegen a
Saffron|27|True|...oll a henna yw possybyl dre asa kodha unn banna hepken a'm...
Audience|34|True|Tack
Audience|35|True|Tack
Audience|36|True|Tack
Audience|37|True|Tack
Audience|38|True|Tack
Audience|39|True|Tack
Audience|40|True|Tack
Audience|41|True|Tack
Audience|42|True|Tack
Audience|43|True|Tack
Audience|44|True|Tack
Audience|45|True|Tack
Audience|46|True|Tack
Audience|47|True|Tack
Audience|48|False|Tack
Mayor of Komona|50|False|An ismegen ma a alsa gul dhe oll a Komona bos rych!
Mayor of Komona|49|True|Splann! Marthys!
Audience|52|True|Tack
Audience|53|True|Tack
Audience|54|True|Tack
Audience|55|True|Tack
Audience|56|True|Tack
Audience|57|True|Tack
Audience|58|True|Tack
Audience|59|True|Tack
Audience|60|True|Tack
Audience|61|True|Tack
Audience|62|True|Tack
Audience|63|True|Tack
Audience|64|False|Tack
Mayor of Komona|51|False|Ny yll agas gormola bos kamm. Coriander re beu fethys seulabrys.
Saffron|24|False|ismegen vy

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|12|False|BZZZIIOO
Mayor of Komona|1|False|Dell hevel y fia kales rag an diwettha displetyans na dhe vos fethys gans Shichimi !
Shichimi|4|True|NA WRAV!
Shichimi|5|True|Ny allav, re beryllus yw.
Mayor of Komona|3|False|...deus yn-rag Shichimi, yma pubonan orth dha wortos.
Mayor of Komona|7|False|Dell hevel, benenes ha gwer, Shichimi dhe gelli...
Saffron|8|False|Ro honna dhymm!
Saffron|9|False|...ha hedhi omwul dhe vos gohelus, ty a wra namma an gwari.
Saffron|10|False|Pubonan a wor my dhe seweni y'n kesstrif, ny vern pandr'a wra dha ismegen...
Shichimi|11|False|!!!
Shichimi|15|False|EUTHVILES KOWREK!
Shichimi|2|False|Ny... Ny wodhyen y tal dhyn ri displetyans
Shichimi|13|True|BYDH WAR!!!
Shichimi|14|True|Ismegen yw rag
Shichimi|6|False|DROG YW GENEV!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bird|1|False|KRI-KRI-KRiiiiiiiiiiiiiiiiiiiiiiiiiiiii
Sound|2|False|BOMM!
Pepper|3|True|...splannadoudel!
Pepper|5|False|...ow ismegen a vydh talvedhys nebes hwarthow drefen bos...
Pepper|4|False|Yw ow thro lemmyn?
Mayor of Komona|6|True|Poon, ty wocki!
Mayor of Komona|7|False|Gorfennys yw an kesstrif! ...omwith!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|8|False|KRACK !
Pepper|2|False|...pubonan dhe asa yn kettermyn ha'gan tro nyni
Pepper|1|True|herwydh usadow...
Pepper|3|True|Dhe'n lyha yma tybyans dhymm a'n pyth dhe wul gans dha "ismegen", Carrot
Pepper|4|False|...kempenna puptra omma ha mos tre!
Pepper|6|True|Ty
Pepper|7|False|vudji-kowrek-afinys-zombi!
Pepper|9|True|A garses assaya unn diwettha ismegen? ...
Pepper|10|False|... A na vynnydh tejy?
Pepper|5|False|YOW!

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrator|9|False|Rann 6: An Kesstrif Ismek
Narrator|10|False|FIN
Writing|5|False|50,000 Ko
Credits|11|False|mis Meurth 2015 - Artweyth ha hwedhel gans David Revoy - Treylyans gans Steve Harris, provredyans gans Steve Penhaligon
Pepper|1|True|Ya, red an arwodhik yn ta...
Pepper|2|False|... Ny wrussen tybi diwweyth kyns y dhiveri dresos mar na yssi Komona distowgh !
Mayor of Komona|3|True|Drefen hi dhe sawya agan tre pan o yn peryl
Mayor of Komona|4|False|ni a re an pewas kynsa dhe Pepper rag hy Ismegen a...??!!
Pepper|7|False|... well... y'n gwiryonedh, nyns yw ismegen yn tevri; yth yw sampel pis ow hath dhyworth y dhiwettha viaj dhe'n medhek enyvales!
Pepper|6|True|... Haha! ya...
Pepper|8|False|... Displetyans vyth, ytho ?...

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Pepper&Carrot yw rydh(libre), fenten ygor, ha skoodhys dre weres kuv redyoryon. Skoodhys veu an dyllans ma gans 245 tasek:
Credits|4|False|https://www.patreon.com/davidrevoy
Credits|3|False|Y hyllir dos ha bos tasek Pepper&Carrot rag an nessa dyllans:
Credits|7|False|Toulys: Gwrys veu an dyllans ma gans toulys 100% rydh(libre) Krita war Linux Mint
Credits|6|False|Open-source : all source files with layers and fonts, are available on the official site
Credits|5|False|License : Creative Commons Attribution You can modify, reshare, sell etc. ...
Credits|2|False|Алексей ★ Глеб Бузало ★ 獨孤欣 & 獨弧悦 ★ Addison Lewis ★ A Distinguished Robot ★ Adrian Lord ★ Ahmad Ali ★ Aina Reich ★ Alandran ★ Alan Hardman ★ Albert Westra ★ Alcide ★ Alejandro Flores Prieto ★ Alex ★ Alexander Bülow Tomassen ★ Alexander Sopicki ★ Alexandra Jordan ★ Alex Lusco ★ Alex Silver ★ Alex Vandiver ★ Alfredo ★ Ali Poulton (Aunty Pol) ★ Allan Zieser ★ Andreas Rieger ★ Andrej Kwadrin ★ Andrew ★ Andrew Godfrey ★ Andrey Alekseenko ★ Angela K ★ Anna Orlova ★ Antan Karmola ★ Anthony Edlin ★ Antonio Mendoza ★ Ardash Crowfoot ★ Arjun Chennu ★ Arne Brix ★ Aslak Kjølås-Sæverud ★ Axel Bordelon ★ Axel Philipsenburg ★ Barbix ★ Ben Evans ★ Bernd ★ Betsy Luntao ★ Boonsak Watanavisit ★ Boris Fauret ★ Boudewijn Rempt ★ BoxBoy ★ Brett Smith ★ Brian Behnke ★ Brian Smith ★ Bryan Butler ★ Bui Dang Hai Trieu ★ Carlos Levischi ★ Charlotte Lacombe-bar ★ Chris Radcliff ★ Chris Sakkas ★ Christian Gruenwaldner ★ Christophe Carré ★ Christopher Bates ★ Clara Dexter ★ codl ★ Colby Driedger ★ Conway Scott Smith ★ Cuthbert Williams ★ Cyrille Largillier ★ Cyril Paciullo ★ Damien ★ Daniel ★ Daniel Björkman ★ Danny Grimm ★ David Tang ★ DiCola Jamn ★ Dmitry ★ Donald Hayward ★ Duke ★ Eitan Goldshtrom ★ Enrico Billich ★ Epsilon ★ Eric Schulz ★ Faolan Grady ★ Francois Schnell ★ Garret Patterson ★ Ginny Hendricks ★ GreenAngel5 ★ Grigory Petrov ★ Guillaume ★ Guillaume Ballue ★ Gustav Strömbom ★ Guy Davis ★ Happy Mimic ★ Helmar Suschka ★ Henning Döscher ★ Ilyas ★ Irina Rempt ★ Ivan Korotkov ★ James Frazier ★ Janusz ★ Jared Tritsch ★ JDB ★ Jean-Baptiste Hebbrecht ★ Jean-Gabriel LOQUET ★ Jeffrey Schneider ★ Jessey Wright ★ Jim ★ Jim Street ★ Jiska ★ Joachim Schiele ★ JoÃ£o Luiz Machado Junior ★ Joern Konopka ★ joe rutledge ★ Johanne Thomson ★ John ★ John Urquhart Ferguson ★ Jónatan Nilsson ★ Jonathan Leroy ★ Jonathan Ringstad ★ Jon Brake ★ Jorge Bernal ★ Joseph Bowman ★ Julien Duroure ★ Justus Kat ★ Kai-Ting (Danil) Ko ★ Kasper Hansen ★ Kate ★ Kathryn Wuerstl ★ Ken Mingyuan Xia ★ Kingsquee ★ Kroet ★ Levi Kornelsen ★ Liselle ★ Lorentz Grip ★ L S ★ Luc Stepniewski ★ Luke Hochrein ★ MacCoy ★ Magnus Kronnäs ★ Manuel ★ Marc & Rick ★ Marcus ★ Martin Owens ★ Mary Brownlee ★ Masked Admirer ★ Mathias Stærk ★ Mefflin Ross Bullis-bates ★ Michael ★ Michael Gill ★ Michael Pureka ★ Michelle Pereira Garcia ★ Mike Mosher ★ Miroslav ★ Muzyka Dmytro ★ Nataya Castillo ★ Nazhif ★ Nicholas DeLateur ★ Nicholas Terranova ★ Nicki Aya ★ Nicola Angel ★ Nicolae Berbece ★ Nicole Heersema ★ Nielas Sinclair ★ NinjaKnight Comics ★ Noble Hays ★ Noelia Calles Marcos ★ Nora Czaykowski ★ Nyx ★ Olivier Amrein ★ Olivier Brun ★ Omar Willey ★ Oscar Moreno ★ Öykü Su Gürler ★ Ozone S. ★ Pablo Lopez Soriano ★ Pat David ★ Patrick Gamblin ★ Paul ★ Pavel Semenov ★ Pet0r ★ Peter ★ Peter Moonen ★ Petr Vlašic ★ Philippe Jean Edward Bateman ★ Pierre Geier ★ Pierre Vuillemin ★ Pranab Shenoy ★ Pyves & Ran ★ Raghavendra Kamath ★ Rajul Gupta ★ Reorx Meng ★ Ret Samys ★ Rictic ★ RJ van der Weide ★ Roberto Zaghis ★ Roman ★ Rumiko Hoshino ★ Rustin Simons ★ Sally Bridgewater ★ Sami T ★ Samuel Mitson ★ Scott Petrovic ★ Sean Adams ★ Shadefalcon ★ ShadowMist ★ Shafak ★ Shawn Meyer ★ Simon Forster ★ Simon Isenberg ★ Sonny W. ★ Soriac ★ Stanislav Vodetskyi ★ Stephanie Cheshire ★ Stephen Bates ★ Stephen Smoogen ★ Steven Bennett ★ Stuart Dickson ★ Surt ★ TamaskanLEM ★ Tar8156 ★ Terry Hancock ★ TheFaico ★ Thomas Citharel ★ Thomas Courbon ★ Thomas Schwery ★ Tim Burbank ★ Tim J. ★ Tomas Hajek ★ Tom Demian ★ Tom Savage ★ Tracey Reuben ★ Travis Humble ★ Tree ★ Tyson Tan ★ Urm ★ Victoria ★ Victoria White ★ Vladislav Kurdyukov ★ Vlad Tomash ★ Westen Curry ★ Witt N. Vest ★ Xavier Claude ★ Yalyn Vinkindo ★ Yaroslav ★ Zeni Pong ★ Źmicier Kušnaroŭ
