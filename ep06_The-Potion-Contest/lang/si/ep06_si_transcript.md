# Transcript of Pepper&Carrot Episode 06 [si]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|6වන කතාංගය: මයාවඩි තරඟාවලිය

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|චිකේ... මම ආයෙත් ජනේලේ ඇරන් නිදාගෙන...
Pepper|2|True|...පුදුම හුලඟක්නේ...
Pepper|3|False|අනික,මට කොහොමද කොමෝනාව ජනේලයෙන් පේන්නේ?
Pepper|4|False|කොමෝනාව!
Pepper|5|False|මයාවඩි තරඟේ!
Pepper|6|True|මට වැරදිලා නින්ද යන්න ඇති!
Pepper|9|True|... ඒත්?
Pepper|10|False|මේ කොහෙද ?!?
Bird|12|False|ක්වෑක්?

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|!!!
Pepper|2|False|කැරොට්! ඔයානම් හරිම ෂෝක්, කොහොමහරි මාව තරඟෙට එක්කං ආවනේ!
Pepper|3|False|ෆැන්-ටෑ-ස්ටික් !
Pepper|4|True|මයාවඩිය, මඟේ ඇඳුම්, තොප්පිය,හැමදේම ඔයාට මතක් වෙලානේ...
Pepper|5|False|....බලමු, මොන මායාවඩියද ගෙනල්ල තියෙන්නේ කියලා...

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|මොනවා ?!?
Mayor of Komona|3|False|කොමෝනාවේ නඟරාධිපති වශයෙන්,මා සඳහන් කරන්නේ,මායාවඩි තරඟය... මෙතැන් සිට ආරම්භ වූ වගයි!
Mayor of Komona|4|False|ප්‍රථම වරට පවත්වන මේ තරඟය සඳහා සහභාගීවන සතරකට නොඅඩු මායාකාරියන් සියල්ලෝම, අපි සතුටු සිතින් පිළිගන්නෙමු.
Mayor of Komona|5|False|අපේ මායාකාරියන්ට උණුසුම් අත්පුඩි සන්නාදයක් දෙන්න:
Writing|2|False|කොමෝනා මායාවඩි තරඟාවලිය

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|True|මෙහෙන් පටන්ගත්තොත්, උසස් තාක්ෂණික සංගමයෙන් ආ,සිත් කුල්මත් කරවන උපායශීලී
Mayor of Komona|3|True|... අමතක කරන්නෙපා,අපේ පළාතේ කෙල්ල, කොමෝනාවේ කවුරුත් හඳුනන
Mayor of Komona|5|True|... අපේ තෙවන තරඟකාරිය, සඳබට නිමනයෙන් ආ,
Mayor of Komona|7|True|... අන්තිමට, අපේ අවසන් තරඟකාරිය, ලේනාන්ත වනයෙන් ආ,
Mayor of Komona|2|False|කොරියැන්ඩර්
Mayor of Komona|4|False|සැෆ්රෝන්!
Mayor of Komona|6|False|සිචිමි!
Mayor of Komona|8|False|පෙපර්!
Mayor of Komona|9|True|එහෙනං තරඟය පටාන්ගමු!
Mayor of Komona|10|False|ලැබෙන අත්පුඩි ප්‍රමාණය මත, ලකුණු පිරිණැමේ
Mayor of Komona|11|False|පළමුව කොරියැන්ඩර්ගේ නිරූපණයයි
Coriander|13|False|...තවදුරටත් මරණයට බයවෙන්න ඕනෑ නෑ, ඒ මගේ...
Coriander|15|False|මායා වඩිය නිසා!
Audience|16|True|අත්පුඩි
Audience|17|True|අත්පුඩි
Audience|18|True|අත්පුඩි
Coriander|12|False|නෝනාවරුනී, මහත්වරුනී...
Audience|19|True|අත්පුඩි
Audience|20|True|අත්පුඩි
Audience|21|True|අත්පුඩි
Audience|22|True|අත්පුඩි
Audience|23|True|අත්පුඩි
Audience|24|True|අත්පුඩි
Audience|25|True|අත්පුඩි
Audience|26|True|අත්පුඩි
Audience|27|True|අත්පුඩි
Audience|28|False|අත්පුඩි
Coriander|14|True|...පිල්ලිකරණ

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|True|අද්විතීයයි !
Audience|10|True|අත්පුඩි
Saffron|4|False|ඒ මොකද කිව්වොත්,මෙන්න මගේ මායාවඩිය
Saffron|3|True|...එත් නඟරවැසියනි, අනේ මටත් අත්පුඩියක් ඉතිරි කරගන්න!
Saffron|6|False|...ඔවුන්ට ඊරිසියාව ගේන දෙයයි!
Saffron|9|False|මායාවඩියෙන්!
Saffron|7|True|...මේ සියල්ල ඉෂ්ට සිද්ධ කරගතහැකිය, එක බිංදුවක් ආලේප ක‍රෝතිං මගේ මේ...
Mayor of Komona|28|False|මේ මයාවඩියට මුළු කොමෝනාවම පෝසත් කළ ඇහැකි!
Mayor of Komona|27|True|අද්විතීයයි!පුදුමාකාරයි!
Audience|11|True|අත්පුඩි
Audience|12|True|අත්පුඩි
Audience|13|True|අත්පුඩි
Mayor of Komona|2|False|මේ පුදුමාකාර මායාවඩියෙන්,මරණය විසින්ම මරණය පරදවන සැටි, කොරියැන්ඩර් අපට පෙන්නුවා
Saffron|5|True|ඔබ සියලුදෙනාම බලා සිටින සැබෑම මයා වඩිය, ඒ:ඔබේ අසල්වැසියන් මවිත කරවන දෙයයි...
Mayor of Komona|29|False|ඔබලාගේ අත්පුඩිය වරදින්න බෑ. දැනටමත් කොරියැන්ඩරි කැපිලා ඉවරයි.
Audience|14|True|අත්පුඩි
Audience|15|True|අත්පුඩි
Audience|16|True|අත්පුඩි
Audience|17|True|අත්පුඩි
Audience|18|True|අත්පුඩි
Audience|19|True|අත්පුඩි
Audience|20|True|අත්පුඩි
Audience|21|True|අත්පුඩි
Audience|22|True|අත්පුඩි
Audience|23|True|අත්පුඩි
Audience|24|True|අත්පුඩි
Audience|25|True|අත්පුඩි
Audience|26|False|අත්පුඩි
Audience|32|True|අත්පුඩි
Audience|33|True|අත්පුඩි
Audience|34|True|අත්පුඩි
Audience|35|True|අත්පුඩි
Audience|36|True|අත්පුඩි
Audience|37|True|අත්පුඩි
Audience|38|True|අත්පුඩි
Audience|39|True|අත්පුඩි
Audience|40|True|අත්පුඩි
Audience|41|True|අත්පුඩි
Audience|42|True|අත්පුඩි
Audience|44|True|අත්පුඩි
Audience|45|True|අත්පුඩි
Audience|46|True|අත්පුඩි
Audience|47|True|අත්පුඩි
Audience|48|True|අත්පුඩි
Audience|49|True|අත්පුඩි
Audience|50|True|අත්පුඩි
Audience|51|True|අත්පුඩි
Audience|52|True|අත්පුඩි
Audience|53|True|අත්පුඩි
Audience|54|True|අත්පුඩි
Audience|55|True|අත්පුඩි
Audience|56|False|අත්පුඩි
Saffron|8|False|... ජේත්තුකාර

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|False|කලින් කළ නිරූපණය අභිබවන එක, දැන් සිචිමිට අභියෝගයක් වගේ!
Shichimi|4|True|බැහැ!
Shichimi|5|True|මට බැහැ,ඒක හරිම අවදානම්
Mayor of Komona|3|False|...අයියෝ සිචිමි, හැමෝම ඔයා වෙනුවෙනුයි බලා ඉන්නේ
Saffron|8|False|කෝ ඕක මට දෙන්න!
Saffron|9|True|...බොරුවට ලැජ්ජා වෙන්නැතිව.ඔයා තරඟෙ කැත කරනවනේ
Saffron|10|False|...මේ මයාවඩියෙන් මොනවා වුණත් කමක් නෑ,හැමෝම දන්නවා මම දැනටමත් තරඟය දිනල ඉවරයි කියලා
Shichimi|11|False|!!!
Sound|12|False|බ්ස්ස්ස්සියෝ
Shichimi|2|False|මම... මම දැනන් හිටියෙනෑ,අපි නිරූපණයක් කරන්නෝනි කියලා
Shichimi|13|True|බලාගෙනයි!!!
Shichimi|14|True|මේ මායාවඩියෙන් හැදෙන්නේ,
Shichimi|15|False|යෝධ රාක්ෂයින්!
Mayor of Komona|7|False|නගරවැසියනි, සිචිමි තම අවස්ථාව අහිමි කරගන්නා සැටියක් අපට පෙනී යනවා...
Shichimi|6|False|සමාවෙන්න!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bird|1|False|කොව්-කොව්-කෝඕඕඕඕව්ව්ව්ව්
Sound|2|False|දඩෝං!
Pepper|3|True|...හහ් නියමයි!
Pepper|5|False|...මගේ මායාවඩියෙන් චුට්ටක් හරි හිනායාවි, මොකද...
Pepper|4|False|එහෙනම් දැන් මගේ වාරය නේද?
Mayor of Komona|6|True|දුවපියව්, මෝඩයෝ!
Mayor of Komona|7|False|තරඟේ හමාරයි! ...ඇඟ බේරගෙන දුවපියව්!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|2|False|...පුරුදු විදියටම, අපේ වාරේ එද්දී කවුරුවත් නෑ
Pepper|3|True|අඩුම තරමින්,ඔයාගේ “මායාවඩියෙන්“ අපිට කළහැකි දේ ගැනවත්,මට නිච්වියක් තියෙනවා
Pepper|4|False|...ඔක්කෝම අකුලාගෙන ගෙදර යන එක!
Pepper|5|False|ඒයි!
Sound|9|False|චරාස්!
Note|8|False|*කැනරි කුරුල්ලා: ලස්සන නාදයක් ඇති කුරුලු විශේෂයක්
Pepper|11|False|ඕනෙම නැද්ද, ආ...?
Pepper|10|True|තව එක සැරයක් මායාවඩියක් අරන් බලමුද?...
Pepper|1|True|ඔන්න ඔහෙ ගියාවේ...
Pepper|7|False|පත-පිල්ලි-ජේත්තුකාර-කැනරි කුරුල්ලා*!
Pepper|6|True|ඔව් ඔහේටයි කිව්වේ,

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|ඔව්, හොඳට ලේබලේ කියවන්න...
Pepper|2|False|...උඔ නගරෙන් තෝංචි වුණේ නැතිනම්, මේක උඔට හලන්න, ආයෙ මම දෙපාරක් හිතන්නෑ!
Mayor of Komona|3|True|නගරය අනතුරේ වැටුන වෙලාවේ ඇය අප බේරාගත් නිසා
Mayor of Komona|4|False|ප්‍රථම ස්ථානය ලබා දෙන්නේ, පෙපර් විසින් ඉදිරිපත් කළ ඇගේ... ??!!
Pepper|6|True|...හහ්හා! ඔව්...
Pepper|8|False|...ඉදින් කිසිම නිරූපණයක් නෑ...
Narrator|9|False|6වන කතාංගය:මයාවඩි තරඟාවලිය
Narrator|10|False|සමාප්තයි
Writing|5|False|කෝ. 50,000
Credits|11|False|2015 මාර්තු-කතාව සහ චිත්‍ර: David Revoy, පරිවර්තනය: තරින්ද දිවාකර
Pepper|7|False|...හහ්... ඇත්ත වශයෙන්ම කිව්වොත් මේක මායාවඩියක් නෙවෙයි; මේ මගේ පූසාගේ; සායනයට ගිය වෙලාවේ ගත්ත මුත්‍රා සාම්පලය;

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|විවෘත මෙවලම් වලින් නිර්මිත පෙපර් සහ කැරටි සම්පූර්ණයෙන්ම නොමිලයේ ඔබ වෙත ගෙන එයි.මේ සඳහා අන්‍රග්‍රහය දැක් වූ පාඨකයින්ට ස්තූතියි.මෙම කතාංගය සඳහා,අන්‍රග්‍රහය දැක්වූ 245ක් වූ පිරිස:
Credits|4|False|https://www.patreon.com/davidrevoy
Credits|3|False|You too can become a patron of Pepper&Carrot for the next episode :
Credits|7|False|Tools : This episode was 100% drawn with Free/Libre software Krita on Linux Mint
Credits|6|False|Open-source : all source files with layers and fonts, are available on the official site
Credits|5|False|License : Creative Commons Attribution You can modify, reshare, sell etc. ...
Credits|2|False|Алексей ★ Глеб Бузало ★ 獨孤欣 & 獨弧悦 ★ Addison Lewis ★ A Distinguished Robot ★ Adrian Lord ★ Ahmad Ali ★ Aina Reich ★ Alandran ★ Alan Hardman ★ Albert Westra ★ Alcide ★ Alejandro Flores Prieto ★ Alex ★ Alexander Bülow Tomassen ★ Alexander Sopicki ★ Alexandra Jordan ★ Alex Lusco ★ Alex Silver ★ Alex Vandiver ★ Alfredo ★ Ali Poulton (Aunty Pol) ★ Allan Zieser ★ Andreas Rieger ★ Andrej Kwadrin ★ Andrew ★ Andrew Godfrey ★ Andrey Alekseenko ★ Angela K ★ Anna Orlova ★ Antan Karmola ★ Anthony Edlin ★ Antonio Mendoza ★ Ardash Crowfoot ★ Arjun Chennu ★ Arne Brix ★ Aslak Kjølås-Sæverud ★ Axel Bordelon ★ Axel Philipsenburg ★ Barbix ★ Ben Evans ★ Bernd ★ Betsy Luntao ★ Boonsak Watanavisit ★ Boris Fauret ★ Boudewijn Rempt ★ BoxBoy ★ Brett Smith ★ Brian Behnke ★ Brian Smith ★ Bryan Butler ★ Bui Dang Hai Trieu ★ Carlos Levischi ★ Charlotte Lacombe-bar ★ Chris Radcliff ★ Chris Sakkas ★ Christian Gruenwaldner ★ Christophe Carré ★ Christopher Bates ★ Clara Dexter ★ codl ★ Colby Driedger ★ Conway Scott Smith ★ Cuthbert Williams ★ Cyrille Largillier ★ Cyril Paciullo ★ Damien ★ Daniel ★ Daniel Björkman ★ Danny Grimm ★ David Tang ★ DiCola Jamn ★ Dmitry ★ Donald Hayward ★ Duke ★ Eitan Goldshtrom ★ Enrico Billich ★ Epsilon ★ Eric Schulz ★ Faolan Grady ★ Francois Schnell ★ Garret Patterson ★ Ginny Hendricks ★ GreenAngel5 ★ Grigory Petrov ★ Guillaume ★ Guillaume Ballue ★ Gustav Strömbom ★ Guy Davis ★ Happy Mimic ★ Helmar Suschka ★ Henning Döscher ★ Ilyas ★ Irina Rempt ★ Ivan Korotkov ★ James Frazier ★ Janusz ★ Jared Tritsch ★ JDB ★ Jean-Baptiste Hebbrecht ★ Jean-Gabriel LOQUET ★ Jeffrey Schneider ★ Jessey Wright ★ Jim ★ Jim Street ★ Jiska ★ Joachim Schiele ★ JoÃ£o Luiz Machado Junior ★ Joern Konopka ★ joe rutledge ★ Johanne Thomson ★ John ★ John Urquhart Ferguson ★ Jónatan Nilsson ★ Jonathan Leroy ★ Jonathan Ringstad ★ Jon Brake ★ Jorge Bernal ★ Joseph Bowman ★ Julien Duroure ★ Justus Kat ★ Kai-Ting (Danil) Ko ★ Kasper Hansen ★ Kate ★ Kathryn Wuerstl ★ Ken Mingyuan Xia ★ Kingsquee ★ Kroet ★ Levi Kornelsen ★ Liselle ★ Lorentz Grip ★ L S ★ Luc Stepniewski ★ Luke Hochrein ★ MacCoy ★ Magnus Kronnäs ★ Manuel ★ Marc & Rick ★ Marcus ★ Martin Owens ★ Mary Brownlee ★ Masked Admirer ★ Mathias Stærk ★ Mefflin Ross Bullis-bates ★ Michael ★ Michael Gill ★ Michael Pureka ★ Michelle Pereira Garcia ★ Mike Mosher ★ Miroslav ★ Muzyka Dmytro ★ Nataya Castillo ★ Nazhif ★ Nicholas DeLateur ★ Nicholas Terranova ★ Nicki Aya ★ Nicola Angel ★ Nicolae Berbece ★ Nicole Heersema ★ Nielas Sinclair ★ NinjaKnight Comics ★ Noble Hays ★ Noelia Calles Marcos ★ Nora Czaykowski ★ Nyx ★ Olivier Amrein ★ Olivier Brun ★ Omar Willey ★ Oscar Moreno ★ Öykü Su Gürler ★ Ozone S. ★ Pablo Lopez Soriano ★ Pat David ★ Patrick Gamblin ★ Paul ★ Pavel Semenov ★ Pet0r ★ Peter ★ Peter Moonen ★ Petr Vlašic ★ Philippe Jean Edward Bateman ★ Pierre Geier ★ Pierre Vuillemin ★ Pranab Shenoy ★ Pyves & Ran ★ Raghavendra Kamath ★ Rajul Gupta ★ Reorx Meng ★ Ret Samys ★ Rictic ★ RJ van der Weide ★ Roberto Zaghis ★ Roman ★ Rumiko Hoshino ★ Rustin Simons ★ Sally Bridgewater ★ Sami T ★ Samuel Mitson ★ Scott Petrovic ★ Sean Adams ★ Shadefalcon ★ ShadowMist ★ Shafak ★ Shawn Meyer ★ Simon Forster ★ Simon Isenberg ★ Sonny W. ★ Soriac ★ Stanislav Vodetskyi ★ Stephanie Cheshire ★ Stephen Bates ★ Stephen Smoogen ★ Steven Bennett ★ Stuart Dickson ★ Surt ★ TamaskanLEM ★ Tar8156 ★ Terry Hancock ★ TheFaico ★ Thomas Citharel ★ Thomas Courbon ★ Thomas Schwery ★ Tim Burbank ★ Tim J. ★ Tomas Hajek ★ Tom Demian ★ Tom Savage ★ Tracey Reuben ★ Travis Humble ★ Tree ★ Tyson Tan ★ Urm ★ Victoria ★ Victoria White ★ Vladislav Kurdyukov ★ Vlad Tomash ★ Westen Curry ★ Witt N. Vest ★ Xavier Claude ★ Yalyn Vinkindo ★ Yaroslav ★ Zeni Pong ★ Źmicier Kušnaroŭ
