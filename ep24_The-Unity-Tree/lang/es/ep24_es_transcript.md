# Transcript of Pepper&Carrot Episode 24 [es]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Título|1|False|Episodio 24: El árbol de Unidad

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sonido|5|False|¡PAF!
Cayena|1|True|¿Que quieres un árbol de Unidad?
Cayena|4|False|Somos las Brujas del Caos; NO HACEMOS eso.
Cayena|6|False|¡Céntrate en estudiar tus «Hechizos de destrucción»!
Pimienta|7|True|¿Destrucción?
Pimienta|8|True|¿En esta época del año?
Pimienta|9|False|¿Por qué no aprendemos, para variar, hechizos de creación?
Cayena|10|False|El próximo Concilio de Las Tres Lunas es pronto y también tus exámenes. ¡Aprende! ¡Aprende! ¡¡¡APRENDE!!! ¡¡¡Y DEJA DE DISTRAERTE CON ESAS IDEAS ESTÚPIDAS!!!
Cayena|2|False|¿¡¿Aquí?!?
Cayena|3|True|¡NO!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sonido|1|False|¡Blam!
Pimienta|5|True|¡¿Un invierno sin árbol de Unidad?!
Pimienta|7|False|¡¿Qué tal si creo mi propio árbol desde cero con magia?!
Pimienta|8|True|¡Hey! Quizá podría combinar las magias de Hippiah y Chaosah. ¡Sería la primera vez que lo hago!
Pimienta|9|True|¡Un gran árbol verde, iluminado por estrellas y minisistemas solares!
Pimienta|10|False|¡Oh! ¡Sería perfecto!
Pimienta|11|True|¡Y además sería
Pimienta|12|False|creativo!
Pimienta|13|True|Pero no hay forma de hacer ese tipo de magia aquí...
Pimienta|14|False|...mis madrinas se darían cuenta inmediatamente si no estoy estudiando mis «Hechizos de Destrucción».
Zanahoria|3|False|tap
Sonido|2|False|Ffff
Pimienta|6|False|¡Nunca!
Pimienta|4|False|¡¡¡Grrrrr...!!!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sonido|3|False|Shrr
Sonido|17|False|ShiiING
Escritura|22|False|EL KOMONERO
Pimienta|1|True|A no ser que...
Pimienta|2|False|Creo recordar...
Pimienta|4|False|¡Sí! En el capítulo de los «Hechizos de Destrucción» Tomillo recomienda intentarlos en una microdimensión. Pero mira todos estos avisos:
Pimienta|16|False|¡Vamos a trabajar!
Cumino|19|False|¿Acaba de crear Pimienta una microdimensión?
Sonido|18|False|¡¡SCHuump!!
Tomillo|20|True|¡Bien!
Escritura|8|False|«NO volver con lo generado por motivos de seguridad»
Escritura|6|False|«Procura NO gastar todo tu Rea en la creación de la microdimensión.»
Escritura|11|True|«NO esperar ningún tipo de ayuda del exterior de la microdimensión...
Pimienta|7|False|Naturalmente.
Pimienta|10|False|¡Aquí está!
Pimienta|9|False|Pff... ¡Obvio!
Pimienta|5|False|Mm...
Escritura|12|False|...¡nadie puede ver qué estás haciendo!»
Tomillo|21|False|Espero que haya comprendido bien todos los avisos que escribí en ese capítulo sobre los «Hechizos de Destrucción».
Pimienta|15|True|¡Es perfecto!
Pimienta|13|True|¿Nadie?
Pimienta|14|True|¿¡De veras!?

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|True|¡Genial!
Pimienta|2|True|Es como estar en un contenedor con la parte de la biblioteca que necesito. Y está totalmente aislado del resto del mundo.
Pimienta|3|False|Es el sandbox perfecto para probar hechizos.
Pimienta|4|False|¡Ahora toca centrarse en mi obra maestra!
Sonido|5|False|ShiiING
Sonido|6|False|¡¡PLOooom!!
Sonido|8|False|¡¡Duuuff!!
Sonido|10|False|¡¡Ssting!!
Pimienta|7|False|Jeje. Eh..., no.
Pimienta|9|False|¡Puaf!
Pimienta|11|False|¡Sí! Este se ve bien, pero es demasiado pequeño...
Pimienta|13|False|¡ZANAHORIA! ¡NO LO TOQUES!
Zanahoria|12|False|¿?

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sonido|1|False|¡¡Plooom!!
Pimienta|3|True|No veo qué mal podría hacer si llevo este árbol fuera de la microdimensión.
Pimienta|2|True|¡Eso es! Redimensionado. ¡Ahora está perfecto!
Pimienta|5|True|Bueno,
Pimienta|6|True|es hora de desplegar esta microdimensión sobre la realidad y mostrarles cómo de creativo puede ser el Caos.
Sonido|8|False|¡¡DOooong!!
Tomillo|9|True|Mmm... Pimienta ha vuelto.
Tomillo|10|False|¡Bravo, Cayena! Tus métodos educativos están por fin dando resultados.
Cayena|11|False|Gracias, maestra.
Pimienta|7|False|¡Estoy impaciente por ver qué cara ponen mis madrinas!
Escritura|12|False|EL KOMONERO
Pimienta|4|False|Solo es un árbol de Unidad; no es como un «Hechizo de Destrucción».

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sonido|5|True|BRRrrr
Sonido|6|True|BRRr
Sonido|7|True|BRRrrr
Sonido|8|True|BRRrrr
Sonido|9|True|BRRrrr
Sonido|10|False|BRrr
Zanahoria|11|False|¿?
Zanahoria|13|False|¡¡!!
Sonido|14|True|BRRr
Sonido|15|False|BRRrrrr
Zanahoria|16|False|!!
Pimienta|1|True|¡ZANAHORIA!
Pimienta|2|True|Vuelvo en unos minutos con Tomillo, Cayena y Comino.
Pimienta|3|False|No toques el árbol hasta que vuelva, ¿entendido?
Pimienta|4|False|OK, ¡vuelvo enseguida!
Pimienta|17|True|¡Preparaos para quedaros asombradas y boquiabiertas! Nunca habéis visto nada parecido.
Pimienta|18|False|Por favor, venid conmigo arriba...
Sonido|12|False|¡¡Shruf!!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrador|6|False|- FIN -
Sonido|1|False|¡Shroooof!!
Sonido|4|False|¡¡¡CRASH!!!
Sonido|3|False|¡¡¡Fuuum!!!
Pimienta|2|False|¡¿?!
Pimienta|5|False|Parece que he descubierto un nuevo «Hechizo de Destrucción», ¿verdad?

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Créditos|2|False|12/2017 - www.peppercarrot.com - Dibujo & Guion: David Revoy - Traducción: TheFaico
Créditos|3|False|Mejora de diálogos: Craig Maloney, CalimeroTeknik y Jookia.
Créditos|5|False|Basado en el universo de Hereva creado por David Revoy con las contribuciones de Craig Maloney. Correcciones de Willem Sonke, Moini, Hali, CGand y Alex Gryson.
Créditos|6|False|Software: Krita 3.2.3, Inkscape 0.92.2 on Ubuntu 17.10
Créditos|7|False|Licencia: Creative Commons Attribution 4.0
Créditos|9|False|Tú también puedes ser mecenas de Pepper&Carrot en www.patreon.com/davidrevoy
Créditos|8|False|Pepper&Carrot es completamente gratuito, de código abierto y financiado gracias al mecenazgo de sus lectores. Para este episodio, gracias a 810 mecenas:
Créditos|4|False|Relectura/Críticas a la versión beta: Alex Gryzon, Nicolas Artance, Ozdamark, Zveryok, Valvin and xHire.
Créditos|1|False|¡Felices Fiestas!
