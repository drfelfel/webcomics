# Transcript of Pepper&Carrot Episode 24 [de]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 24: Der Einheitsbaum

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geräusch|5|False|PAFF!
Cayenne|1|True|Du willst einen Einheitsbaum?
Cayenne|4|False|Wir sind Hexen des Chaos, wir machen das NICHT!
Cayenne|6|False|Du solltest dich besser auf das Lernen der "Zauber der Zerstörung" konzentrieren!
Pepper|7|True|Zerstörung?
Pepper|8|True|Um diese Jahreszeit?
Pepper|9|False|Warum nicht zur Abwechslung Erschaffungszauber?
Cayenne|10|False|Der nächste Rat der Drei Monde rückt näher und deine Prüfungen auch. Lerne! Lerne! LERNE!!! UND HÖR AUF, DICH MIT DIESEN DUMMEN IDEEN ABZULENKEN!!!
Cayenne|2|False|Hier?!?
Cayenne|3|True|NEIN!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geräusch|1|False|Knall!
Pepper|5|True|Ein Winter ohne Einheitsbaum?!
Pepper|7|False|Warum erschaffe ich mir nicht meinen eigenen Baum mit Magie?!
Pepper|8|True|He! Ich könnte vielleicht sogar die Magie von Hippiāh und Chaosāh zum ersten Mal vereinen!
Pepper|9|True|Ein großer, grüner Baum, mit Sternen und kleinen Sonnensystemen!
Pepper|10|False|Ach! Es wäre perfekt!
Pepper|11|True|Und es wäre
Pepper|12|False|kreativ!
Pepper|13|True|Aber es wäre unmöglich, diese Art von Magie hier zu machen...
Pepper|14|False|...meine Tanten würden es sofort merken, wenn ich meine "Zauber der Zerstörung" nicht übe.
Carrot|3|False|tipp
Geräusch|2|False|Ffff
Pepper|6|False|Niemals!
Pepper|4|False|Grrrrr...!!!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geräusch|3|False|Shrr
Geräusch|17|False|SchiiING
Schrift|22|False|KOMONA-Anzeiger
Pepper|1|True|Es sei denn...
Pepper|2|False|Ich meine mich zu erinnern...
Pepper|4|False|Ja! Im Kapitel "Zauber der Zerstörung" empfiehlt Thymian, sie in einer Mikrodimension auszuprobieren. Aber schau dir mal all die Warnungen an:
Pepper|16|False|An die Arbeit!
Kümmel|19|False|Hat Pepper gerade eine Mikrodimension erschaffen?
Geräusch|18|False|SCHwump!!
Thymian|20|True|Gut!
Schrift|8|False|“Bringe KEINE Erfolge mit zurück, aus Sicherheitsgründen.”
Schrift|6|False|“Verbrauche NICHT all dein Rea für die Erschaffung der Mikrodimension.”
Schrift|11|True|“Erwarte KEINE Hilfe von außerhalb der Mikrodimension...
Pepper|7|False|Natürlich.
Pepper|10|False|Da ist es!
Pepper|9|False|Pff... Offensichtlich!
Pepper|5|False|Mm...
Schrift|12|False|...niemand kann sehen, was du tust!”
Thymian|21|False|Ich hoffe, dass sie all die Warnungen verstanden hat, die ich im Kapitel "Zauber der Zerstörung" geschrieben habe.
Pepper|15|True|Das ist perfekt!
Pepper|13|True|Niemand?
Pepper|14|True|Wirklich!?

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|So cool!
Pepper|2|True|Es ist wie ein Behälter, mit nur dem Teil der Bibliothek, den ich brauche. Und es ist von unserer eigenen Realität völlig isoliert.
Pepper|3|False|Es ist der ideale Ort, um meine Zauber auszutesten.
Pepper|4|False|Zeit, sich auf mein Meisterwerk zu konzentrieren!
Geräusch|5|False|SchiiING
Geräusch|6|False|BLUuusch!!
Geräusch|8|False|Blohrff!!
Geräusch|10|False|Chting!!
Pepper|7|False|Heh. Äh, nein.
Pepper|9|False|Ihhhh!
Pepper|11|False|Ja! Es sieht gut aus, aber es ist zu klein....
Pepper|13|False|CARROT! FASS ES NICHT AN!
Carrot|12|False|?

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geräusch|1|False|Floooff!!
Pepper|3|True|Ich sehe nichts Gefährliches daran, diesen Baum aus der Mikrodimension zu holen.
Pepper|2|True|So! Vergrößert. Jetzt sieht es perfekt aus!
Pepper|5|True|Also gut,
Pepper|6|True|es ist an der Zeit, diese Mikrodimension in die Realität zu versetzen und ihnen zu zeigen, wie schöpferisch das Chaos sein kann!
Geräusch|8|False|DOooong!!
Thymian|9|True|Mmm... Pepper ist zurück.
Thymian|10|False|Bravo, Cayenne. Deine Erziehungsmethoden bringen uns endlich Ergebnisse.
Cayenne|11|False|Danke, Schulleiterin.
Pepper|7|False|Ich kann es kaum erwarten, den Ausdruck auf ihren Gesichtern zu sehen!
Schrift|12|False|KOMONA-Anzeiger
Pepper|4|False|Es ist nur ein Einheitsbaum. Es ist ja nicht so, als ob es ein "Zauber der Zerstörung" wäre.

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geräusch|5|True|BRRrrr
Geräusch|6|True|BRRr
Geräusch|7|True|BRRrrr
Geräusch|8|True|BRRrrr
Geräusch|9|True|BRRrrr
Geräusch|10|False|BRrr
Carrot|11|False|?
Carrot|13|False|!!
Geräusch|14|True|BRRr
Geräusch|15|False|BRRrrrr
Carrot|16|False|!!
Pepper|1|True|CARROT!
Pepper|2|True|Ich bin in ein paar Minuten mit Thymian, Cayenne und Kümmel zurück.
Pepper|3|False|Mach nichts mit dem Baum, bis ich zurück bin, OK?
Pepper|4|False|OK, ich bin bald zurück!
Pepper|17|True|Bereitet euch darauf vor, verblüfft und erstaunt zu sein! So etwas habt ihr noch nie gesehen.
Pepper|18|False|Bitte folgt mir nach oben, um zu erfahren...
Geräusch|12|False|Fruum!!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Erzähler|6|False|- ENDE -
Geräusch|1|False|Schruup!!
Geräusch|4|False|KRACK!!!
Geräusch|3|False|Wuusch!!!
Pepper|2|False|?!!
Pepper|5|False|Es scheint, als hätte ich die "Zauber der Zerstörung" nun doch studiert!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Impressum|2|False|12/2017 - www.peppercarrot.com - Illustration & Handlung: David Revoy - Deutsche Übersetzung: Ret Samys
Impressum|3|False|Dialogoptimierung: Craig Maloney, CalimeroTeknik, Jookia, Sarah und Markus Börner
Impressum|5|False|Basierend auf der Hereva-Welt von David Revoy mit Beiträgen von Craig Maloney. Korrekturen von Willem Sonke, Moini, Hali, CGand und Alex Gryson.
Impressum|6|False|Software: Krita 3.2.3, Inkscape 0.92.3 auf Ubuntu 17.10
Impressum|7|False|Lizenz: Creative Commons Namensnennung 4.0
Impressum|9|False|Auch du kannst Pepper&Carrot unterstützen: www.patreon.com/davidrevoy
Impressum|8|False|Pepper&Carrot ist komplett frei, Open Source und wird durch seine Leserinnen und Leser unterstützt und finanziert. Für diese Episode geht der Dank an 810 Förderer:
Impressum|4|False|Korrektorat/Beta-Feedback: Alex Gryson, Nicolas Artance, Ozdamark, Zveryok, Valvin und xHire.
Impressum|1|False|Frohes Fest!
