# Transcript of Pepper&Carrot Episode 13 [de]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 13: Die Pyjama Party

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|4|False|Hier ist es einfach schön!
Pepper|5|False|Danke für die Einladung, Coriander!
Pepper|3|False|... überhaupt!
Pepper|1|True|Beste ...
Pepper|2|True|... Ferien ...

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Coriander|1|True|Danke!
Coriander|2|False|Ich hoffe, ihr seid von meinen Bediensteten nicht zu sehr eingeschüchtert.
Pepper|11|False|Keine Sorge, sie sind sehr gastfreundlich, wir fühlen uns wie zu Hause.
Shichimi|13|False|vornehm und zugleich gemütlich!
Pepper|14|False|Das stimmt!
Shichimi|12|True|Ich bin wirklich beeindruckt, von der Inneneinrichtung...
Monster|9|False|WoooOoh ! ! !|nowhitespace
Monster|10|False|WoOoh ! ! !|nowhitespace
Monster|8|False|WoooOoh ! ! !|nowhitespace
Geräusch|7|False|schwwwwwwiiing!
Geräusch|6|False|Klaf!
Geräusch|5|False|Klack!
Geräusch|3|False|zzziiipp ! ! !|nowhitespace
Geräusch|4|False|Schkak ! ! !|nowhitespace

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Coriander|2|True|… manchmal kommt mir alles so …
Shichimi|5|True|Ich mag dieses bequeme Leben,
Geräusch|7|False|Schiingz ! ! !|nowhitespace
Geräusch|4|False|bam!
Shichimi|8|False|..."Eine-echte-Hexe-von-Ah-darf-nicht-auf-diese-Art-leben".
Pepper|9|True|Ha ha!...
Pepper|10|True|Die Bürde der Tradition...
Pepper|11|False|So etwas würden meine Tanten sagen!
Shichimi|12|False|Wirklich?
Coriander|1|True|Schön, dass du mich dran erinnerst …
Coriander|3|False|… "gewöhnlich" vor?
Shichimi|6|False|aber ...

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Coriander|2|False|Los Mädels, konzentriert euch, wir sind fast am Ende!
Pepper & Shichimi|1|False|Tii-hi-hi-hi!
Pepper|3|True|Oh, schon in Ordnung ... !
Pepper|5|False|... und wir haben noch nicht mal...
Geräusch|6|False|PRroooOwwoww ! ! !|nowhitespace
Geräusch|9|False|Shhshh
Shichimi|8|False|?!!
Coriander|7|False|PEPPER!!!
Pepper|4|True|Ihr müsst zugeben, dieses Abenteuer ist super einfach...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geräusch|6|False|KNACK ! !|nowhitespace
Shichimi|8|False|Ach NEIN!
Coriander|9|False|Nicht schon wieder!
Pepper|11|False|Aus, lass los! Sofort!
Coriander|1|False|...Oh nein! Es ist zu spät! sie... sie ist...
Shichimi|2|False|Neiiiiiiin!!!
Monster|3|False|MUAH HAHA HAHA !!!|nowhitespace
Shichimi|4|False|GRR!!!
Coriander|5|False|DAFÜR WIRST DU BEZAHLEN! ...
Geräusch|7|False|KLIRRR ! !|nowhitespace
Geräusch|13|False|PLONK!!
Geräusch|12|False|PAFF!!
Pepper|10|True|CARROT!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Impressum|10|False|11/2015 - Grafik & Handlung: David Revoy - Übersetzung: Philipp Hemmer
Geräusch|1|False|Tock
Pepper|4|True|Ich weiß, du wolltest mich nur verteidigen...
Pepper|3|True|Komm schon, nicht schmollen!
Pepper|5|False|... aber es ist nicht nötig. Wir spielen doch nur!
Carrot|8|False|Grrrr
Schrift|6|False|Schlösser & Phönixe
Schrift|2|False|Prinzessin Coriander
Erzähler|9|False|- ENDE -
Schrift|7|False|Schlösser & Phönixe

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Impressum|2|True|Du kannst die nächste Episode von Pepper&Carrot hier unterstützen:
Impressum|1|False|Pepper&Carrot ist komplett frei, Open Source und wird durch die Leser unterstützt und finanziert. Für diese Episode geht der Dank an die 602 Förderer:
Impressum|3|False|https://www.patreon.com/davidrevoy
Impressum|4|False|Lizenz: Creative Commons Namensnennung 4.0 Quelldaten: verfügbar auf www.peppercarrot.com Software: Diese Episode wurde zu 100% mit freier Software erstellt Krita 2.9.9, Inkscape 0.91 auf Linux Mint 17
