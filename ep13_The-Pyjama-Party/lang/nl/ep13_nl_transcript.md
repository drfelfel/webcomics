# Transcript of Pepper&Carrot Episode 13 [nl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Aflevering 13: Het slaapfeestje

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|4|False|Dit is gewoon bijna te mooi om waar te zijn!
Pepper|5|False|Bedankt om ons uit te nodigen bij je thuis, Koriander!
Pepper|3|False|...ooit!
Pepper|1|True|Beste...
Pepper|2|True|...vakantie...

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Koriander|1|True|Dank je!
Koriander|2|False|Ik hoop dat mijn personeel je niet te veel afschrikt.
Pepper|11|False|Nee hoor, ze zijn heel gastvrij; je voelt je meteen op je gemak.
Shichimi|13|False|... zo verfijnd, maar toch ook gezellig!
Pepper|14|False|Ja, dat is waar!
Shichimi|12|True|Ik ben onder de indruk van de inrichting...
Monster|9|False|WoeeEeh ! ! !|nowhitespace
Monster|10|False|WoEeh ! ! !|nowhitespace
Monster|8|False|WoeeEeh ! ! !|nowhitespace
Geluid|7|False|Tsjwwwwwwiiing!
Geluid|6|False|Klaf!
Geluid|5|False|Klukf!
Geluid|3|False|Dzzziii ! ! !|nowhitespace
Geluid|4|False|Shkak! ! !|nowhitespace

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Koriander|2|True|ik merk dat ik het vaak zo...
Shichimi|5|True|Ik zou graag ook zo comfortabel wonen,
Geluid|7|False|Tsjiingz! ! !|nowhitespace
Geluid|4|False|Bam!
Shichimi|8|False|..."Een-echte-heks-van-Ah-moet-niet-op-zo'n-manier-leven".
Pepper|9|True|Ha ha!
Pepper|10|True|Al dat ouderwetse gezeur...
Pepper|11|False|Het klinkt als iets wat mijn peettantes ook zouden zeggen!
Shichimi|12|False|Echt waar?
Koriander|1|True|Het is aardig dat je me daaraan herinnert...
Koriander|3|False|... "normaal" vind?...
Shichimi|6|False|maar...

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Koriander|2|False|Kom op meisjes, we zijn er bijna!
Pepper & Shichimi|1|False|Hi hi hi!
Pepper|3|True|Oh, geen paniek!
Pepper|5|False|... en we zijn nog niet eens bij de ...
Geluid|6|False|VRoeeEwoeww ! ! !|nowhitespace
Geluid|9|False|Tssjjj
Shichimi|8|False|?!!
Koriander|7|False|PEPPER!!!
Pepper|4|True|Deze missie is toch super-simpel...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geluid|6|False|KRAAK ! !|nowhitespace
Shichimi|8|False|O NEE!
Koriander|9|False|Niet alweer!
Pepper|11|False|Laat los! En wel nu!
Koriander|1|False|... oh nee! Het is te laat! Ze... Ze is...
Shichimi|2|False|Neeeeee!!!
Monster|3|False|MOEHA HAHA HAHA !!!|nowhitespace
Shichimi|4|False|GRR!!!
Koriander|5|False|DAT ZETTEN WE JE BETAALD! ...
Geluid|7|False|KLING ! !|nowhitespace
Geluid|13|False|PLONK!!
Geluid|12|False|PAF!!
Pepper|10|True|CARROT!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Aftiteling|10|False|11/2015 - Tekeningen & verhaal: David Revoy - Vertaling: Midgard & Willem Sonke
Geluid|1|False|Plof
Pepper|4|True|Ik weet dat je me alleen maar wil beschermen...
Pepper|3|True|Niet zo mokken, Carrot!
Pepper|5|False|... maar tijdens een spel hoeft dat niet!
Carrot|8|False|Grrrr
Geschrift|6|False|Torens & Feniksen
Geschrift|2|False|Prinses Koriander
Verteller|9|False|- EINDE -
Geschrift|7|False|Torens & Feniksen

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Aftiteling|1|False|Pepper&Carrot is geheel vrij, open-bron en gesponsord door de giften van haar lezers. Voor deze aflevering bedank ik 602 patronen:
Aftiteling|2|True|Voor de volgende aflevering kun jij ook een patroon van Pepper&Carrot worden op
Aftiteling|3|False|https://www.patreon.com/davidrevoy
Aftiteling|4|False|Licentie: Creative Commons Naamsvermelding 4.0 Bronbestanden: beschikbaar op www.peppercarrot.com Software: deze aflevering is 100% gemaakt met vrije software Krita 2.9.9, Inkscape 0.91 on Linux Mint 17
