# Transcript of Pepper&Carrot Episode 04 [de]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 4: Das Genie

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Erzähler|1|False|Die Nacht vor dem Zaubertrank-Wettbewerb 2 Uhr morgens
Pepper|2|True|Endlich... mit diesem Zaubertrank kann mir nicht mal Saffron das Wasser reichen!
Pepper|3|False|Ich muss ihn aber noch an jemandem ausprobieren ...
Pepper|4|False|CAAAAAAAA~ ~AAARROT!!!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Schrift|14|False|CAT FOOD
Pepper|1|False|Carrot?
Pepper|2|False|Wie jetzt? Nicht mal unterm Bett?
Pepper|4|False|Carrot?
Pepper|5|False|Carrot?!
Pepper|6|False|Carrot?!!!
Pepper|3|False|Carrot!
Pepper|7|False|Ich hätte nicht gedacht, dass ich wirklich so weit gehen muss...
Pepper|8|False|Carrot!
Geräusch|9|True|Crrr
Geräusch|10|True|Crrr
Geräusch|11|True|Crrr
Geräusch|12|True|Crrr
Geräusch|13|False|Crrr
Geräusch|15|True|Shhh
Geräusch|16|True|Shhh
Geräusch|17|True|Shhh
Geräusch|18|True|Shhh
Geräusch|19|False|Shhh
Geräusch|21|False|Shhh Shhh Shhh Shhh Shhh
Geräusch|20|False|Crrr Crrr Crrr Crrr Crrr
Geräusch|23|False|Crrr Crrr Crrr Crrr Crrr
Geräusch|24|False|Shhh Shhh Shhh Shhh Shhh
Carrot|25|False|Knurr
Geräusch|26|False|Huuuuiiii!
Carrot|22|False|O Sole Miauuuu!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Oooooh, Carrot! Du süßes kleines Kätzchen bist von ganz alleine hergekommen!
Pepper|2|False|Gerade im richtigen Moment, wenn ich meinen Lieblingsassistenten brauche!
Pepper|3|False|Und hiermit präsentiere ich dir mein Meisterwerk.
Pepper|4|False|Den Genietrank.
Pepper|5|False|Nimm doch einen Schluck.
Pepper|6|False|Wenn er wie geplant wirkt, solltest du kurzzeitig zum Genie werden.

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geräusch|1|False|Klack
Pepper|3|False|WOW... Carrot! Unglaublich... ein richtiger Buchstabe ...
Pepper|5|False|Du...du kannst schreiben?
Pepper|6|False|E?
Pepper|7|False|Energie?
Pepper|8|False|Ewig?
Pepper|9|False|Emotion?
Geräusch|2|False|Kraaaaaatz
Geräusch|4|False|Shrr

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Was soll das sonst bedeuten?
Pepper|2|False|Einbetten?
Pepper|3|False|Entleert?
Pepper|4|False|Komm schon, Carrot, mir fallen keine Wörter mehr ein, und das hier ergibt wirklich keinen Sinn!
Pepper|5|False|...es ergibt überhaupt keinen Sinn...
Pepper|6|True|Grrrrrrr!!!
Pepper|7|False|Nie gelingt mir was!
Pepper|8|True|Pah ...
Pepper|9|False|Ich habe noch Zeit, einen neuen Trank zu erfinden.
Geräusch|10|False|Klirr
Pepper|11|False|Schlaf gut, Carrot.
Erzähler|12|False|Fortsetzung folgt…
Impressum|13|False|David Revoy 11/2014

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Impressum|3|False|Алексей - 獨孤欣 - Adrian Lord - Alan Hardman - Albert Westra - Alejandro Flores Prieto - Alexander Sopicki - Alex Lusco Alex Silver - Alex Vandiver - Alfredo - Alice Queenofcats - Ali Poulton (Aunty Pol) - Alison Harding - Allan Zieser Andreas Rieger - Andrew - Andrew Godfrey - Andrey Alekseenko - Angela K - Anna Orlova - anonymous - Antan Karmola Arco - Ardash Crowfoot - Arne Brix - Aslak Kjølås-Sæverud - Axel Bordelon - Axel Philipsenburg - barbix - Ben Evans Bernd - Boonsak Watanavisit - Boudewijn Rempt - Brian Behnke - carlos levischi - Charlotte Lacombe-bar - Chris Radcliff Chris Sakkas - Christophe Carré - Clara Dexter - Colby Driedger - Conway Scott Smith - Cyrille Largillier - Cyril Paciullo Daniel - david - Damien de Lemeny - Dmitry - Donald Hayward - Dubema Ulo - Eitan Goldshtrom - Enrico Billich-epsilon--Eric Schulz - Garret Patterson - Guillaume - Gustav Strömbom - Guy Davis - Happy Mimic - Helmar Suschka Ilyas Akhmedov - Inga Huang - Irene C. - Ivan Korotkov - Jared Tritsch - JDB - Jean-Baptiste Hebbrecht - Jessey Wright John - Jónatan Nilsson - Jon Brake - Joseph Bowman - Jurgo van den Elzen - Justin Diehl - Kai-Ting (Danil) Ko - Kasper Hansen Kathryn Wuerstl - Ken Mingyuan Xia - Liselle - Lorentz Grip - MacCoy - Magnus Kronnäs - Mandy - marcus - Martin Owens Masked Admirer - Mathias Stærk - mattscribbles - Maurice-Marie Stromer - mefflin ross bullis-bates - Michael Gill Michelle Pereira Garcia - Mike Mosher - Morten Hellesø Johansen - Muzyka Dmytro - Nazhif - Nicholas DeLateur Nick Allott - Nicki Aya - Nicolae Berbece - Nicole Heersema - Noah Summers - Noble Hays - Nora Czaykowski Nyx - Olivier Amrein - Olivier Brun - Omar Willey - Oscar Moreno - Öykü Su Gürler - Ozone S. - Paul - Paul Pavel Semenov - Peter - Peter Moonen - Petr Vlašic - Pierre Geier - Pierre Vuillemin - Pranab Shenoy - Rajul Gupta Ret Samys - rictic - RJ van der Weide - Roman - Rumiko Hoshino - Rustin Simons - Sami T - Samuel Mitson - Scott RussSean Adams - Shadefalcon - Shane Lopez - Shawn Meyer - Sigmundur Þórir Jónsson - Simon Forster - Simon IsenbergSonny W. - Stanislav Vodetskyi - Stephen Bates - Steven Bennett - Stuart Dickson - surt - Tadamichi Ohkubo - tar8156TheFaico - Thomas Schwery - Tracey Reuben - Travis Humble - Vespertinus - Victoria - Vlad Tomash - Westen CurryXavier Claude - Yalyn Vinkindo and special thanks to Amireeti for helping me with english corrections!
Impressum|1|False|Pepper&Carrot ist frei, Open Source und mit Hilfe von Spenden über Patreon.com finanziert. Diese Episode wurde durch 156 Unterstützer ermöglicht:
Impressum|6|False|https://www.patreon.com/davidrevoy
Impressum|5|False|Unterstütze das Projekt! Schon ein Dollar pro Episode kann Pepper&Carrot eine Menge helfen.
Impressum|9|False|Tools: Diese Episode wurde mit den kostenlosen Open Source Tools Krita, G'MIC, Blender, GIMP auf Ubuntu Gnome (GNU/Linux) erstellt
Impressum|8|False|Open Source: hochauflösende Quelldateien mit Ebenen und Schriften zum Herunterladen, verändern, verkaufen, übersetzen, u.s.w.
Impressum|7|False|Lizenz: Creative Commons Namensnennung als 'David Revoy' Du darfst verändern, teilen, verkaufen, abgeleitete Werke kreieren, u.s.w.
Carrot|4|False|Vielen Dank!
