# Transcript of Pepper&Carrot Episode 04 [en]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Episode 4: Stroke of Genius

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrator|1|False|The night before the Potion challenge 2:00 AM
Pepper|2|True|Finally... With this potion, No one, not even Saffron, can match my skill!
Pepper|3|False|I do need to test it on someone ...
Pepper|4|False|CAAAAAAAA~ ~AAARROT!!!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Writing|14|False|CAT FOOD
Pepper|1|False|Carrot?
Pepper|2|False|Amazing! Not even under the bed?
Pepper|4|False|Carrot?
Pepper|5|False|Carrot?!
Pepper|6|False|Carrot?!!!
Pepper|3|False|Carrot!
Pepper|7|False|I really didn't think I would have to use this trick...
Pepper|8|False|Carrot!
Sound|9|True|Crrr
Sound|10|True|Crrr
Sound|11|True|Crrr
Sound|12|True|Crrr
Sound|13|False|Crrr
Sound|15|True|Shhh
Sound|16|True|Shhh
Sound|17|True|Shhh
Sound|18|True|Shhh
Sound|19|False|Shhh
Sound|21|False|Shhh Shhh Shhh Shhh Shhh
Sound|20|False|Crrr Crrr Crrr Crrr Crrr
Sound|23|False|Crrr Crrr Crrr Crrr Crrr
Sound|24|False|Shhh Shhh Shhh Shhh Shhh
Carrot|25|False|Grooo
Sound|26|False|Pffioooo!
Carrot|22|False|O Sole Meowww!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Awww, Carrot! you sweet kitty cat, you've come all by yourself!
Pepper|2|False|At the very moment when I need my favorite assistant.
Pepper|3|True|Here, I present you my final masterpiece.
Pepper|4|False|The potion of Genius.
Pepper|5|False|Do please take a sip.
Pepper|6|False|If it works, you should have a stroke of genius!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|False|Poc
Pepper|3|False|WOW... Carrot! That's amazing... A real alphabet ...
Pepper|5|False|You... You write?
Pepper|6|False|E?
Pepper|7|False|Energy?
Pepper|8|False|Eternal?
Pepper|9|False|Emotion?
Sound|4|False|Shrrrrrrr
Sound|2|False|Shrr

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|What else can this mean?
Pepper|2|False|Embed?
Pepper|3|False|Empty?
Pepper|4|False|C'mon Carrot, I'm running out of words. This really doesn't make any sense!
Pepper|5|False|...this really doesn't make any sense...
Pepper|6|True|Grrrrrrr!!!
Pepper|7|False|Nothing ever works with me!
Pepper|8|True|Bah...
Pepper|9|False|I still have time to invent another potion.
Sound|10|False|CR...A...CKKKK!
Pepper|11|False|Sleep well, Carrot.
Narrator|12|False|To be continued...
Credits|13|False|David Revoy 11/2014

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|3|False|Алексей - 獨孤欣 - Adrian Lord - Alan Hardman - Albert Westra - Alejandro Flores Prieto - Alexander Sopicki - Alex Lusco Alex Silver - Alex Vandiver - Alfredo - Alice Queenofcats - Ali Poulton (Aunty Pol) - Alison Harding - Allan Zieser Andreas Rieger - Andrew - Andrew Godfrey - Andrey Alekseenko - Angela K - Anna Orlova - anonymous - Antan Karmola Arco - Ardash Crowfoot - Arne Brix - Aslak Kjølås-Sæverud - Axel Bordelon - Axel Philipsenburg - barbix - Ben Evans Bernd - Boonsak Watanavisit - Boudewijn Rempt - Brian Behnke - carlos levischi - Charlotte Lacombe-bar - Chris Radcliff Chris Sakkas - Christophe Carré - Clara Dexter - Colby Driedger - Conway Scott Smith - Cyrille Largillier - Cyril Paciullo Daniel - david - Damien de Lemeny - Dmitry - Donald Hayward - Dubema Ulo - Eitan Goldshtrom - Enrico Billich-epsilon--Eric Schulz - Garret Patterson - Guillaume - Gustav Strömbom - Guy Davis - Happy Mimic - Helmar Suschka Ilyas Akhmedov - Inga Huang - Irene C. - Ivan Korotkov - Jared Tritsch - JDB - Jean-Baptiste Hebbrecht - Jessey Wright John - Jónatan Nilsson - Jon Brake - Joseph Bowman - Jurgo van den Elzen - Justin Diehl - Kai-Ting (Danil) Ko - Kasper Hansen Kathryn Wuerstl - Ken Mingyuan Xia - Liselle - Lorentz Grip - MacCoy - Magnus Kronnäs - Mandy - marcus - Martin Owens Masked Admirer - Mathias Stærk - mattscribbles - Maurice-Marie Stromer - mefflin ross bullis-bates - Michael Gill Michelle Pereira Garcia - Mike Mosher - Morten Hellesø Johansen - Muzyka Dmytro - Nazhif - Nicholas DeLateur Nick Allott - Nicki Aya - Nicolae Berbece - Nicole Heersema - Noah Summers - Noble Hays - Nora Czaykowski Nyx - Olivier Amrein - Olivier Brun - Omar Willey - Oscar Moreno - Öykü Su Gürler - Ozone S. - Paul - Paul Pavel Semenov - Peter - Peter Moonen - Petr Vlašic - Pierre Geier - Pierre Vuillemin - Pranab Shenoy - Rajul Gupta Ret Samys - rictic - RJ van der Weide - Roman - Rumiko Hoshino - Rustin Simons - Sami T - Samuel Mitson - Scott RussSean Adams - Shadefalcon - Shane Lopez - Shawn Meyer - Sigmundur Þórir Jónsson - Simon Forster - Simon IsenbergSonny W. - Stanislav Vodetskyi - Stephen Bates - Steven Bennett - Stuart Dickson - surt - Tadamichi Ohkubo - tar8156TheFaico - Thomas Schwery - Tracey Reuben - Travis Humble - Vespertinus - Victoria - Vlad Tomash - Westen CurryXavier Claude - Yalyn Vinkindo and special thanks to Amireeti for helping me with english corrections!
Credits|1|False|Pepper&Carrot is totally free(libre), open-source, and sponsored thanks to the help of patrons. This episode was sponsored by 156 patrons:
Credits|6|False|https://www.patreon.com/davidrevoy
Credits|5|False|Support the project, a single dollar donation per webcomic can help Pepper&Carrot a lot!
Credits|9|False|Tools: This episode was made with 100% free(libre) and open-source tools Krita, G'MIC, Blender, GIMP on Ubuntu Gnome (GNU/Linux)
Credits|8|False|Open-source: high resolution layered source files for printing, with fonts available to download, sell, modify, translate, etc...
Credits|7|False|License: Creative Commons Attribution to 'David Revoy' you can do derivations, modifications, repost, sell-it, etc...
Carrot|4|False|Thank you!
