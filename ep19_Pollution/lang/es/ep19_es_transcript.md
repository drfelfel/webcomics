# Transcript of Pepper&Carrot Episode 19 [es]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Título|1|False|Episodio 19: Polución

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayena|1|True|Vuelvo adentro.
Cayena|2|True|De nada sirve que te siga vigilando...
Cayena|3|False|Entierra todas las pociones fallidas y vuelve a casa pronto para descansar.
Cayena|4|False|Ver tu tarea completa mañana debe ser como un sueño. Pero... ¿quién sabe?
Pimienta|5|True|¡Está bien! Me daré prisa...
Pimienta|6|True|...pero, me pregunto: ¿a qué viene esto de querer enterrarlo todo?
Pimienta|7|False|¿No sería mejor si...?
Cayena|8|False|...¿si QUÉ?

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|True|Emm...
Pimienta|2|True|Bueno, no soy muy experta...
Pimienta|3|True|Pero este año nuestro huerto está rarísimo
Pimienta|4|False|...y afecta también a muchas plantas de alrededor de la casa.
Escritura|5|False|Tomates
Escritura|6|False|Berenjenas
Pimienta|7|False|Por no hablar de las hormigas. Ellas sí que hacen cosas extrañas.
Pimienta|8|True|Así que, bueno...
Pimienta|9|False|He pensado que quizás tenemos un problema de polución. Y creo que deberíamos limpiar todo esto...
Cayena|10|True|¡Escucha, señorita Destrozo-Todas-Mis-Pociones:
Cayena|11|False|tu uniforme de Hippiah se te debe estar subiendo a la cabeza!
Cayena|12|True|¡En Chaosah enterramos nuestros errores!
Cayena|13|True|¡Esa es la tradición desde el principio de los tiempos y no nos importa lo que piensa de ello la MADRE NATURALEZA!
Cayena|14|False|¡¡Así que te callas Y A CAVAR!!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|False|enterramos nuestros errores
Pimienta|2|False|desde el principio de los tiempos
Pimienta|3|False|la tradición
Pimienta|4|False|¡ESO ES!
Zanahoria|5|False|Zzzz
Pimienta|6|False|¡Vamos, más rápido, Zanahoria!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Escritura|6|False|Diario Secreto de Cayena
Pimienta|1|False|"¡Oh, bello guerrero rubio, de cabellos dorados!"
Pimienta|2|False|"...¡Tú que atormentas mi caos!"
Pimienta|3|False|"...Eres la entropía de mis auroras."
Pimienta|4|True|¡Precioso poema, maestra Cayena!
Pimienta|5|False|¡Qué locura todo lo que se puede aprender de las brujas que entierran todos sus errores!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayena|1|True|Todos hemos tenido nuestros errores.
Cayena|2|True|Ya no soy la Cayena que escribió eso.
Cayena|3|False|Ese diario fue enterrado por una razón específica.
Pimienta|4|False|...
Pimienta|5|True|¡De acuerdo!
Pimienta|6|False|¿Pero qué me decís de este, eh?
Escritura|7|False|CHAOSAH SUTRA por Tomillo
Tomillo|8|False|Solo son distracciones de juventud... ¡Y además, esto no es adecuado para tu edad!
Pimienta|9|True|Mm...
Pimienta|10|False|Ya veo...
Pimienta|11|False|No podéis estar avergonzadas por todo esto...

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|True|¡Pero el medio ambiente, la naturaleza!
Pimienta|2|False|¡¡¡No podemos continuar contaminando todo así sin sufrir graves consecuencias!!!
Cayena|3|True|¡Eso nunca ha supuesto un problema hasta ahora!
Cayena|4|True|¡Somos brujas de Chaosah! ¡Y nuestros problemas, los enterramos
Cayena|5|True|PROFUNDAMENTE!
Cayena|6|False|¡No hay discusión con las tradiciones!
Cumino|7|True|¡Oh, mirad lo que acabo de encontrar!
Cumino|8|False|¡No puedo creerlo!
Cumino|9|False|¿Como habrá llegado hasta aquí?
Cumino|10|False|Necesita afinarse un poco, claro está, pero sigue sonando bien.

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cumino|1|False|¿Cómo era esa canción? ¡Cha~Cha Cha, Chaooosah!
Cumino|2|True|¡Jaja, he olvidado la letra!
Cumino|3|False|Seguro que mi libreto de canciones está por aquí...
Cayena|4|True|...entonces, estamos todas de acuerdo. Actualización de las reglas de Chaosah:
Cayena|5|True|¡desde ahora
Cayena|6|True|se clasifica,
Cayena|7|True|se tritura
Cayena|8|True|y se recicla todo!
Cayena|9|False|¡¡¡TODO!!!
Escritura|10|False|vidrio
Escritura|11|False|metal
Narrador|12|False|- FIN -
Créditos|13|False|Septiembre 2016 - www.peppercarrot.com - Dibujo & Guion: David Revoy - Traducción: TheFaico
Créditos|14|False|Script doctor: Craig Maloney. Relectura y apoyo con los diálogos: Valvin, Seblediacre y Alex Gryson. Guion inspirado en: "The book of secrets" de Juan José Segura
Créditos|15|False|Basado en el universo de Hereva, creado por David Revoy con las contribuciones de Craig Maloney. Correcciones de Willem Sonke, Moini, Hali, CGand y Alex Gryson.
Créditos|16|False|Licencia: Creative Commons Attribution 4.0. Software: Krita 3.0.1 e Inkscape 0.91 en Arch Linux XFCE

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Créditos|1|False|Pepper&Carrot es completamente gratuito, de código abierto y financiado gracias al mecenazgo de sus lectores. Para este episodio, gracias a 755 mecenas:
Créditos|2|False|Tú también puedes ser mecenas de Pepper&Carrot en www.patreon.com/davidrevoy
