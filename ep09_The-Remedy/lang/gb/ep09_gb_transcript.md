# Transcript of Pepper&Carrot Episode 09 [gb]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titulo|1|False|Mon 9: Sehagiente

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrator|1|False|Fe din xafe parti...
Pilpil|2|True|Keto? Ixara fe jabal, drevolari ji megu?
Pilpil|3|False|Mas, keseba?
Pilpil|4|False|O, burxanse!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pilpil|2|False|Mi le total wanji na pasivogi "sistema fe anjenya"
Pilpil|5|False|Xosu daif barix, kam no?...
Pilpil|4|True|Nun, minus dento, noce le sen kepul?
Pilpil|6|True|Mi denmo multi asif, mas no hay sungay, kam sahi?
Pilpil|7|False|Mi le jumbigi cay celki moy imi am rucudu sesu bon ganjon xafe hin malokurxey!
Pilpil|1|True|Mafu...
Pilpil|3|True|... mas mi sen daydenmo hox ki uyu le ata!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pilpil|1|False|Bobo Pilpil! To nun sen godomo dyer, moy ete nefra mi...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Yuzu|10|True|Bwaaw
Yuzu|11|False|Saa
Karote|1|False|Myaw-sa-Awsa
Truffel|15|True|Myaaaw
Truffel|16|False|Saaa
Mango|13|True|Kukusa
Mango|14|False|Kukusa
Safran|18|False|Cel syensidom! Hinto ible sen morgiabil!
Safran|17|True|Velosi!
Yuzu|2|True|Bwawsa
Yuzu|3|True|Awsa
Karote|12|False|MyawSa Awsa
Truffel|5|True|MyaaAwsa
Mango|7|True|Kukusa
Mango|8|True|Kukusa
Truffel|6|False|MyaaAwsa!
Mango|9|False|Kukusa!
Yuzu|4|False|Bwawsa!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pilpil|5|False|Fe hataya, to sen asifpul ki imi le no sukses na bloki maxus-efeto de iksir...
Xicimi|6|False|Am no yolyu; xorwala fe belo ji yumaw sen finipul, to xa velosi ruxunjan...
Narrator|7|False|- FIN -
Credits|8|False|July 2015 - Art & Scenario: David Revoy - Translation: Hector Ortega
Safran ji Pilpil|1|True|Suprem!
Xicimi ji Koryandro|3|True|Hura!
Safran ji Pilpil|2|False|Imi sen maxim bon ete!
Xicimi ji Koryandro|4|False|Ete sen sehagido!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Pepper&Carrot is entirely free(libre), open-source and sponsored thanks to the patronage of its readers. For this episode, thank you to the 406 Patrons:
Credits|7|False|https://www.patreon.com/davidrevoy
Credits|6|True|You too can become a patron of Pepper&Carrot for the next episode at
Credits|8|False|License: Creative Commons Attribution 4.0 Source: available at www.peppercarrot.com Software: this episode was 100% drawn with libre software Krita 2.9.6, Inkscape 0.91 on Linux Mint 17
Credits|4|False|Глеб Бузало ★ 无名 ★ 獨孤欣 & 獨弧悦 ★ Adam Mathena ★ Addison Lewis A Distinguished Robot ★ Aina Reich ★ Alan Hardman ★ Albert Westra ★ Alex ★ AlexanderKennedy Alexander Lust ★ Alexander Sopicki ★ Alexandra Jordan ★ Alex Lusco ★ Alex Silver ★ Alex V ★ Alfredo Alien Green ★ Allan Zieser ★ Alok Baikadi ★ Amic ★ Andreas Rieger ★ Andreas Ulmer ★ Andrej Kwadrin Andrew Godfrey ★ Andrey Alekseenko ★ Anna Orlova ★ Antan Karmola ★ Anthony Edlin ★ Ardash Crowfoot Arjun Chennu ★ Arnulf ★ Arturo J. Pérez ★ Austin Knowles ★ Axel Bordelon ★ Bastian Hougaard ★ Ben Evans blacksheep33512 ★ Boonsak Watanavisit ★ BoxBoy ★ Brett Smith ★ Brian Behnke ★ Bryan Butler ★ BS Bui Dang Hai Trieu ★ carlos levischi ★ carolin der keks ★ Cedric Wohlleber ★ Chance Millar ★ Charles★ Chaz Straney Chris ★ Chris Sakkas ★ Christian Howe ★ Christophe Carré ★ Christopher Bates ★ Christopher Rodriguez Christopher Vollick ★ Colby Driedger ★ Damien ★ Daniel ★ Daniel Lynn ★ Danijel ★ David Brennan David Kerdudo ★ David Tang ★ DecMoon ★ Derek Zan ★ Dio Fantasma ★ Dmitry ★ Doug Moen ★ douze12 ★ Drew Fisher Durand D’souza ★ Elijah Brown ★ Elisha Condon ★-epsilon-★ Eric Schulz ★ Faolan Grady ★ francisco dario aviltis Francois Schnell ★ Francou ★ Garret Patterson ★ Gary Thomas ★ Ginny Hendricks ★ GreenAngel5 ★ Grigory Petrov Grzegorz Wozniak ★ G. S. Davis ★ Guillaume ★ Gustav Strömbom ★ happy2ice ★ Happy Mimic ★ Helmar Suschka Henning Döscher ★ Henry Ståhle ★ HobbyThor ★ Ilyas ★ Irina Rempt ★ Jacob ★ James Frazier ★ Jamie Sutherland ★ Janusz Jason ★ Jeffrey Schneider ★ Jessey Wright ★ Jessica Gadling ★ Joao Luiz ★ John ★ John Gholson ★ John Urquhart Ferguson Jonas Peter ★ Jonathan Leroy ★ Jonathan Walsh ★ Justus Kat ★ Kailyce ★ Kai-Ting (Danil) Ko ★ Kari Lehto ★ Kathryn Wuerstl kazakirinyancat ★ Ken Mingyuan Xia ★ Kevin Estalella ★ Kevin Trévien ★ Kingsquee ★ Kurain ★ La Plume ★ Lenod ★ Liang Lise-Lotte Pesonen ★ Lloyd Ash Pyne ★ Lorentz Grip ★ Lorenzo Leonini ★ Magnus Kronnäs ★ Marc et Rick ★ Marco ★ marcus Matt Lichtenwalner ★ Michael F. Schönitzer ★ Michael Gill ★ Michael Polushkin ★ Mike Mosher ★ Mohamed El Banna ★ Nabispace Nazhif ★ Nicholas DeLateur ★ Nicola Angel ★ Oleg Schelykalnov ★ Olga Bikmullina ★ Olivier Amrein ★ Olivier Gavrois ★ Omar Willey Oscar Moreno ★ Pato Acevedo ★ Patrick Dezothez ★ Patrick Gamblin ★ Paul ★ Pavel Semenov ★ Peter Moonen ★ Petr Vlašic Pierre Vuillemin ★ Pranab Shenoy ★ Pummie ★ Raghavendra Kamath ★ Rajul Gupta ★ Ramel Hill ★ Ray Brown ★ Rebecca Morris ResidentEvilArtist ★ Reuben Tracey ★ Roberto Zaghis ★ Roman ★ Rumiko Hoshino ★ Rustin Simons ★ Ryan ★ Sally Bridgewater Sami T ★ Samuel Mitson ★ Scott Petrovic ★ Scott Smiesko ★ ShadowMist ★ shafak ★ Shawn Meyer ★ Soriac ★ Stanislav ★ Stephan Theelke Stephen Bates ★ Steven Bennett ★ Stuart Dickson ★ surt ★ Takao Yamada ★ TamaskanLEM ★ tar8156 ★ TheFaico ★ thibhul ★ Thomas Schwery T.H. Porter ★ Tim Burbank ★ Tim J. ★ Tom Savage ★ Travis Humble ★ Tristy ★ Tyson Tan ★ Venus ★ Vera Vukovic ★ Victoria ★ Victoria White WakoTabacco ★ Wei-Ching Shyu ★ Westen Curry ★ Xavier Claude ★ Yalyn Vinkindo ★ Yaroslav ★ Yasmin ★ Zeni Pong
Credits|5|False|Adrian Lord ★ Ahmad Ali ★ al ★ Alandran ★ Alcide ★ Alexander Bülow Tomassen ★ Alexander Kashev ★ Alex Bradaric ★ Alex Cochrane ★ Alexey Golubev Ali Poulton (Aunty Pol) ★ Amy ★ Andrew ★ Andy Gelme ★ Angelica Beltran ★ anonymous ★ Antoine ★ Antonio Mendoza ★ Antonio Parisi ★ Axel Philipsenburg barbix ★ BataMoth ★ Bela Bargel ★ Bernd ★ Bernhard Saumweber ★ Betsy Luntao ★ Birger Tuer Thorvaldsen ★ blueswag ★ Boris Fauret ★ Brett Bryan Rosander ★ BXS ★ Chris Kastorff ★ Chris Radcliff ★ Christian Gruenwaldner ★ Clara Dexter ★ codl ★ Comics by Shoonyah Studio ★ Conway Scott Smith Coppin Olivier ★ Craig Bogun ★ Crystal Bollinger ★ Cuthbert Williams ★ Cyol ★ Cyrille Largillier ★ Cyril Paciullo ★ Daniel Björkman ★ Dan Norder Dan Stolyarov ★ David ★ Davi Na ★ Dawn Blair ★ Deanna ★ Denis Bolkovskis ★ Dezponia Veil ★ DiCola Jamn ★ Dmitriy Yakimov ★ Donald Hayward Douglas Oliveira Pessoa ★ Duke ★ Eitan Goldshtrom ★ Emery Schulz ★ Enrico Billich ★ Erik Moeller ★ Esteban Manchado Velázquez ★ Fen Yun Fat Fernando Nunes ★ ida nilsen ★ Igor ★ Ivan Korotkov ★ Jamie Hunter ★ Jason Baldus ★ Jazyl Homavazir ★ JDB ★ Jean-Baptiste Hebbrecht Jean-Gabriel LOQUET ★ Jhonny Rosa ★ Jim ★ Jim Street ★ Joerg Raidt ★ Joern Konopka ★ joe rutledge ★ John ★ Jon Brake ★ Jorge Bernal ★ Joseph Bowman Josh ★ Josh Cavalier ★ Juju Mendivil ★ Julian Dauner ★ Julia Velkova ★ Kate ★ Kroet ★ Lars Ivar Igesund ★ Liselle ★ Liska Myers ★ Louis Yung Luc Stepniewski ★ Luke Hochrein ★ Mahwiii ★ Mancy S ★ Manu Järvinen ★ Martin Owens ★ Mary Brownlee ★ Masked Admirer ★ Megan Sandiford ★ Michael Michael Pureka ★ Michelle Pereira Garcia ★ Miroslav ★ mjkj ★ Moonsia ★ Moritz Fuchs ★ Muriah Summer ★ Mylène Cassen ★ Nicholas Terranova Nicole Heersema ★ Nielas Sinclair ★ Nikita Stafeev ★ Noble Hays ★ Noelia Calles Marcos ★ Nora Czaykowski ★ No Reward ★ Nyx ★ Öykü Su Gürler Ozone S. ★ Pat David ★ Patrick Kennedy ★ Paul ★ Pet0r ★ Peter ★ Peter ★ Pierre Geier ★ Pyves & Ran ★ Ray Cruz ★ Raymond Fullon ★ Ray Powell Rebecca ★ Rebekah Hopper ★ Rei ★ Reorx Meng ★ Ret Samys ★ rictic ★ Robin Moussu ★ Sean Adams ★ Sebastien ★ Sevag Bakalian ★ Shadefalcon Simon Isenberg ★ Simon Moffitt ★ Siora ★ Sonja Reimann-Klieber ★ Sonny W. ★ Stanislav German-Evtushenko ★ Stephen Smoogen ★ Surabhi Gaur Taedirk ★ Tas Kartas ★ Terry Hancock (Personal) ★ Thomas Citharel ★ Thomas Werner ★ Thor Galle ★ Thornae ★ Timothy Boersma ★ Tomas Hajek Tomáš Slapnička ★ Tom Dickson ★ tree ★ uglyheroes ★ Umbra Draconis ★ Vitaly Tokarenko ★ Vladislav Kurdyukov ★ Wander ★ Wilhelmine Faust William Crumpler ★ Źmicier Kušnaroŭ ★ zubr kabbi
Credits|3|False|Arne Brix ★ Boudewijn Rempt ★ Brent Houghton ★ Casey Tatum Davis Aites ★ Ejner Fergo ★ Enrique Lopez ★ Francois Didier freecultureftw ★ Jonathan Ringstad ★ Julio Avila ★ Levi Kornelsen Matthew Reynolds ★ Mefflin Ross Bullis-bates ★ Michael Oliveira Nguyen Minh Trung ★ Nicki Aya ★ NinjaKnight Comics ★ Olivier Brun Praveen Bhamidipati ★ Ricardo Muggli ★ RJ van der Weide Roman Burdun ★ Urm
Credits|2|False|Jónatan Nilsson Alex Kotenko ★ Philippe Jean Edward Bateman
