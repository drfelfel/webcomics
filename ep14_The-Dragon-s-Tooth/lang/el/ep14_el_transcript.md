# Transcript of Pepper&Carrot Episode 14 [el]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Επεισόδιο 14: Το Δόντι Του Δράκου

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Κουμίν|5|True|Εδώ είμαστε λοιπόν! Τέρμα οι διακοπές!
Κουμίν|6|False|Ας αρχίσουμε με μια επανάληψη στα αρχαία φίλτρα και τα συστατικά τους.
Κουμίν|7|True|Χμμ... Φτου! Τέλείωσε το αλεσμένο δόντι δράκου!
Κουμίν|8|False|Δεν έχει νόημα να αρχίσουμε το μάθημα χωρίς αυτό...
Writing|9|False|Δόντι Δράκου
Writing|4|False|33
Writing|3|False|ΜΑΓΙΣΣΑΣ
Writing|2|True|ΚΑΤΟΙΚΙΑ
Writing|1|True|ΠΡΟΣΟΧΗ

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Κουμίν|1|False|Καγιέν! Θυμάρα! Μπορείτε να πάτε να μου πάρετε κανά-δυο δόντια δράκου;
Καγιέν|2|False|Με τέτοιο κρύο;! Δεν το νομίζω!
Θυμάρα|3|False|Ναι... Ακριβώς...
Πιπεριά|4|False|Κανένα πρόβλημα, θα σας φέρω εγώ!
Πιπεριά|6|False|Επανέρχομαι...
Πιπεριά|7|True|"Μα το κρύο, το κρύο!"
Πιπεριά|8|False|Τι φοβητσιάρες που είναι!
Πιπεριά|9|False|Ένα γερό σετ τανιάλες, λίγη προστασία, και θα πάρουμε αυτά τα δόντια!
Κουμίν|5|False|Πιπεριά, περίμενε!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Πιπεριά|1|True|Χα Χα!
Πιπεριά|2|False|Ουφ! Μάλλον θα είναι λιγουλάκι πιο δύσκολο από ό,τι φανταζόμουν!
Πιπεριά|3|True|Ωχου!
Πιπεριά|4|False|...και νόμιζα πως θα είναι πιο εύκολο με δράκο του αέρα...

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Πιπεριά|1|True|Καροτούλη!
Πιπεριά|2|True|Έλα πίσω...
Πιπεριά|3|True|...οι δράκοι των βάλτων είναι διάσημοι για την ήμερή τους φύση...
Πιπεριά|4|False|τουλάχιστον αυτό νόμιζα...
Πιπεριά|5|True|Χμμ... Για τον δράκο Κεραυνού...
Πιπεριά|6|False|...δεν είμαι τόσο σίγουρη πως αυτό είναι το κατάλληλο εργαλείο...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Πιπεριά|1|True|Οκ...
Πιπεριά|2|True|Είναι ΠΟΛΥ δύσκολο...
Πιπεριά|3|False|...τα παρατάω.
Πιπεριά|5|False|Η Πιπεριά ποτέ δεν τα παρατά!
Πιπεριά|4|True|Όχι!
Bird|7|False|Κι-Κι-ΡιΚουυου!!!
Narrator|6|False|Την επομένη...
Κουμίν|8|False|!!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrator|10|False|-ΤΕΛΟΣ-
Credits|11|False|01/2016 - Σκίτσο και σενάριο: David Revoy Μετάφραση: Γεώργιος Καρέττας
Writing|6|False|Δράκων|nowhitespace
Writing|5|True|-|nowhitespace
Writing|4|True|Οδοντίατρος
Writing|7|False|ΔΩΡΕΑΝ!
Sound|3|False|pop !
Πιπεριά|1|True|Λοιπόν;! Δεν έχεις εντυπωσιασθεί;
Πιπεριά|2|False|Πρέπει να έχω τουλάχιστον 100 δόντια δράκου μέχρι τώρα...
Κουμίν|8|True|...μα Πιπεριά, το "δόντι δράκου"...
Κουμίν|9|False|είναι φυτό!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Η Πιπεριά και Καροτούλης είναι ένα εντελώς δωρεάν και ανοικτού κώδικα κόμικ, που υποστηρίζεται με χορηγίες. Αυτό το επεισόδιο χρηματοδοτήθηκε από 671 χορηγούς:
Credits|2|True|Και εσύ μπορείς να γίνεις χορηγός για το επόμενο επεισόδιο:
Credits|3|False|https://www.patreon.com/davidrevoy
Credits|4|False|Άδεια : Creative Commons Attribution 4.0 Πηγή : available at www.peppercarrot.com Λογισμικό : αυτό το επεισόδιο δημιουργήθηκε με 100% δωρεάν λογισμικο, όπως Krita 2.9.8, Inkscape 0.91, Blender 2.71, GIMP 2.8.14, G'MIC 1.6.7 σε Linux Mint 17.2
