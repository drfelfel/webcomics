# Transcript of Pepper&Carrot Episode 14 [de]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 14: Der Drachenzahn

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Kümmel|5|True|Gut gut gut! Die Ferien sind vorbei!
Kümmel|6|False|Lass uns mit dem Unterricht über alte Zaubertränke und ihre wichtigsten Zutaten beginnen.
Kümmel|7|True|Hmm... Schade! Kein gemahlener Drachenzahn mehr.
Kümmel|8|False|Es ist absolut sinnlos, ohne ihn anzufangen...
Schrift|9|False|Drachenzahn
Schrift|4|False|33
Schrift|3|False|GRUNDSTÜCK
Schrift|2|True|HEXEN
Schrift|1|True|ACHTUNG

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Kümmel|1|False|Cayenne! Thymian! Kann uns eine von euch etwas Drachenzahn besorgen?
Cayenne|2|False|In dieser Kälte?! Nein Danke!
Thymian|3|False|Brrrr... Ich passe...
Pepper|4|False|Kein Problem, ich kümmere mich darum!
Pepper|6|False|Bin gleich zurück!
Pepper|7|True|"Die Kälte", "die Kälte"!
Pepper|8|False|Diese alten Zimperliesen!
Pepper|9|False|Eine gute Zange, ein wenig Schutzkleidung, und die Drachenzähne sind unser!
Kümmel|5|False|Pepper, warte!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Ah ah!
Pepper|2|False|Huiii! Ich glaube, das wird ein klitzekleines bisschen komplizierter als ich dachte!
Pepper|3|True|Pfff!
Pepper|4|False|... und ich dachte, es ist einfacher bei einem Luftdrachen!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Carrot! ...
Pepper|2|True|Komm zurück...
Pepper|3|True|... Sumpfdrachen sind bekannt für ihr gehorsames Wesen!
Pepper|4|False|zumindest dachte ich das...
Pepper|5|True|Hmm... für den Donnerdrachen...
Pepper|6|False|... haben wir, glaube ich, nicht das richtige Werkzeug...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Ok.
Pepper|2|True|Es ist zu schwer...
Pepper|3|False|...ich gebe auf.
Pepper|5|False|Pepper gibt niemals auf!
Pepper|4|True|Nein!
Vogel|7|False|Kiii keeerrr iiii kiiiii ! ! !|nowhitespace
Erzähler|6|False|Nächster Tag...
Kümmel|8|False|!!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Erzähler|10|False|- ENDE -
Impressum|11|False|01/2016 - Grafik & Handlung: David Revoy Übersetzung: Philipp Hemmer
Schrift|6|False|Zahnarzt|nowhitespace
Schrift|5|True|-|nowhitespace
Schrift|4|True|Drachen-
Schrift|7|False|Kostenlos!
Geräusch|3|False|pop!
Pepper|1|True|Da bist du erstaunt? Nicht wahr?
Pepper|2|False|Ich habe mindestens hundert Drachenzähne!
Kümmel|8|True|... aber Pepper, der Drachenzahn...
Kümmel|9|False|... ist doch eine Pflanze!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Impressum|2|True|Auch Du kannst die nächste Episode von Pepper&Carrot hier unterstützen:
Impressum|3|False|https://www.patreon.com/davidrevoy
Impressum|1|False|Pepper&Carrot ist komplett frei, Open Source und wird durch die Leser unterstützt und finanziert. Für diese Episode geht der Dank an die 671 Förderer:
Impressum|4|False|Lizenz: Creative Commons Namensnennung 4.0 Quelldaten: verfügbar auf www.peppercarrot.com Software: Diese Episode wurde zu 100% mit freier Software erstellt Krita 2.9.10, Inkscape 0.91 auf Linux Mint 17
