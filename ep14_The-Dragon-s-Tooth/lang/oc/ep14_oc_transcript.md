# Transcript of Pepper&Carrot Episode 14 [oc]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Títol|1|False|Episòdi 14 : La Dent de Dragon

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Comin|5|True|Bon ! Las vacanças son acabadas.
Comin|6|False|Començam amb un cors sus las pocions anticas e lors compausants primaris.
Comin|7|True|Mmm... Zut ! I a pas pus de polvera de dent de dragon
Comin|8|False|Val pas lo còp de començar lo cors sens aquel ingredient...
Escritura|9|False|Dent de Dragon
Escritura|4|False|33
Escritura|3|False|PROPRIETAT PRIVADA
Escritura|2|True|MASCA
Escritura|1|True|DANGIÈR

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Comin|1|False|Cayenne ! Frigola ! Me poiriatz anar quèrre de dent de dragon ?
Cayenne|2|False|Amb aquel fred ?! Non, mercé !
Frigola|3|False|Mmmòc... Çò meteis...
Pepper|4|False|Pas de problèma me n'encargui !
Pepper|6|False|Torni lèu lèu !
Pepper|7|True|Lo fred, lo fred !
Pepper|8|False|Mas quala banda de polas mòlas !
Pepper|9|False|De bonas tenalhas, qualques proteccions e las dents de dragon seràn a nosautres !
Comin|5|False|Pepper espèra !

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Ha ha !
Pepper|2|False|Zut ! Serà un pauc mai complicat que çò qu'imaginavi.
Pepper|3|True|Pfff !
Pepper|4|False|...e ieu que pensavi que seriá un pauc mai aisit amb un dragon d'aire !

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Carròt !...
Pepper|2|True|Torna...
Pepper|3|True|...Lo dragon dels paluns es conegut per èstre mai docile pr'aquò
Pepper|4|False|l'ai pas inventat...
Pepper|5|True|Mmm... Pel dragon-lum...
Pepper|6|False|...Soi pas segura qu'aquestas pinças sián adaptadas

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Ok.
Pepper|2|True|Es tròp malaisit...
Pepper|3|False|...abandoni.
Pepper|5|False|Pepper abandona pas jamai !
Pepper|4|True|Non !
Narrator|6|False|L'endeman
Comin|8|False|!!
Aucèl|7|False|CACARACÀÀàààà ! ! !|nowhitespace

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrator|10|False|- FIN -
Crèdits|11|False|01/2016 - Dessenh & Scenari : David Revoy
Escritura|4|True|Dentista
Escritura|5|True|per
Escritura|6|False|Dragons
Escritura|7|False|Gratuit !
Son|3|False|pòp !|nowhitespace
Pepper|1|True|Alara, siás impressionada, non ?
Pepper|2|False|Ai almens un bon centenat de dents de dragon !
Comin|8|True|...mas Pepper, la dent de dragon...
Comin|9|False|...es una planta !

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crèdits|1|False|Pepper&Carrot es completament liure, open source, e esponsorizat mercés al mecenat de sos lectors. Per aqueste episòdi, mercé als 671 mecènas :
Crèdits|4|False|Licéncia : Creative Commons Attribution 4.0 Sorsas : disponiblas sus www.peppercarrot.com Logicials : aqueste episòdi foguèt dessenhat a 100% amb de logicials liures Krita 2.9.10, Inkscape 0.91 sus Linux Mint 17
Crèdits|2|True|Vos tanben, venètz mecèna de Pepper&Carrot per l'episòdi venent sus
Crèdits|3|False|https://www.patreon.com/davidrevoy
