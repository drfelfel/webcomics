# Transcript of Pepper&Carrot Episode 01 [da]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 1: Flyveeliksir

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|...og nu den sidste detalje
Pepper|4|False|...mmm måske ikke stærkt nok
Lyd|2|True|SHH
Lyd|3|False|SHH
Lyd|5|True|PLOF
Lyd|6|False|PLOF

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|ha... perfekt!
Pepper|2|False|NEJ! Du kan godt glemme det!
Lyd|3|False|PLASK

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Er du så tilfreds?!
Credits|2|False|WWW.PEPPERCARROT.COM 05/2014
Credits|3|False|Kunst & script: David Revoy. Oversættelse: Marie Moestrup & Juan José Segura
