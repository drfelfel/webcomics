# Transcript of Pepper&Carrot Episode 01 [oc]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Títol|1|False|Episòdi 1 : Pocion de volada

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|...e lo darrièr ingredient
Pepper|4|False|...mmm benlèu pas pro
Son|2|True|CHH
Son|3|False|CHH
Son|5|True|PLOP
Son|6|False|PLOP

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|a... perfièit
Pepper|2|False|NON ! Fòra de question
Son|3|False|SPLACH

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Siás content ?
Crèdits|2|False|WWW.PEPPERCARROT.COM 05/2014
