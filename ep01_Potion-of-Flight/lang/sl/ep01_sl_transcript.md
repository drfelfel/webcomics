# Transcript of Pepper&Carrot Episode 01 [sl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Naslov|1|False|Epizoda 1: Napoj letenja

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|False|In še to za konec.
Paprika|4|False|Mmm, verjetno ni dovolj močen.
Zvok|2|True|ššš
Zvok|3|False|ššš
Zvok|5|True|BLOP
Zvok|6|False|BLOP

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|False|Ha! Odlično.
Paprika|2|False|Ne! Da ti ne pade na pamet!
Zvok|3|False|PLJUSK

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|False|A si zdaj srečen?!
Zasluge|2|False|WWW.PEPPERCARROT.COM Maj 2014
