# Transcript of Pepper&Carrot Episode 01 [tp]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
nimi|1|False|lipu nanpa wan: telo pi tawa waso

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|False|sin la... pini!
jan Pepa|4|False|...a. ken la ona o mute
kalama|2|True|PA-
kalama|3|False|NA
kalama|5|True|PA
kalama|6|False|AN-

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|False|a... pona a!
jan Pepa|2|False|ike! o pali ala
kalama|3|False|TAWA

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|False|sina pilin pona anu seme ?!
mama|2|False|WWW.PEPPERCARROT.COM 05/2014
