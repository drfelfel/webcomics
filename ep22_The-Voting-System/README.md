# Episode 22: The Voting System

![cover of episode 22](https://www.peppercarrot.com/0_sources/ep22_The-Voting-System/low-res/Pepper-and-Carrot_by-David-Revoy_E22.jpg)

## Comments from the author

Hi! A quick note: this episode is an obvious metaphor of the voting system proposed by any website on the internet; we "like", "downvote", "upvote" and all of this in realtime. This system affect the content a lot. For our witches, they discover here the power of the "cute witches" effect; a parody of a trend I see growing everywhere on Anime culture nowaday, but also the load of "sexy" content I see in digital-art to get more "like" and "upvote" in general... Denunciating this practice with humor wasn't evident, but it was fun to try. :-)

It was mandatory for this story to have Pepper excluded from the participant. I wish I was able to add more reaction shot at the end ( mainly the one of Shichimi and Coriander) but I had to cut to keep my final twist more unexpected. 

From [Author's blog of episode 22](https://www.davidrevoy.com/article612/episode-22-the-voting-system/show#comments)
