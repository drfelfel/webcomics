# Transcript of Pepper&Carrot Episode 30 [oc]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Títol|1|False|Episòdi 30 : Besonh d'una calinhada

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrator|1|False|- FIN -

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|5|True|Podètz tanben venir mecèna de Pepper&Carrot e aver vòstre nom marcat aquí !
Pepper|3|True|Pepper&Carrot es completament liure, gratuit, open-source e esponsorizat mercés au mecenat de sos lectors.
Pepper|4|False|Aqueste episòdi recebèt lo sosten de 973 mecènas !
Pepper|7|True|Anatz sus www.peppercarrot.com per mai d'informacions !
Pepper|6|True|Sèm sus Patreon, Tipeee, PayPal, Liberapay …e d'autres !
Pepper|8|False|Mercé !
Pepper|2|True|O sabiatz ?
Crèdits|1|False|Lo 3 de seteme de 2019 Dessenh, color e scenari : David Revoy. Relectors del storyboard : Alina the Hedgehog, Craig Maloney, Jihoon Kim, Parnikkapore, Martin Disch, Nicolas Artance, Valvin. Version occitana (lengadocian) Traduccion : Aure Séguier Basat sus l'univèrs d'Hereva Creator : David Revoy. Manteneire principal : Craig Maloney. Redactors : Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Correctors : Willem Sonke, Moini, Hali, CGand, Alex Gryson. Logicials : Krita/4.2~git branch, Inkscape 0.92.3 sus Kubuntu 18.04.2. Licéncia : Creative Commons Attribution 4.0. www.peppercarrot.com
