# Episode 30: Need a Hug

![cover of episode 30](https://www.peppercarrot.com/0_sources/ep30_Need-a-Hug/low-res/Pepper-and-Carrot_by-David-Revoy_E30.jpg)

## Comments from the author

It's an episode that focus a bit on Carrot and takes a deeper look at the evenings in Pepper's house.

From [Author's blog of episode 30](https://www.davidrevoy.com/article731/episode-30-need-a-hug/show#comments)
