# Transcript of Pepper&Carrot Episode 36 [pt]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Título|1|False|Episódio 36: O Ataque Surpresa

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Wasabi|1|True|DESCULPAS?
Wasabi|2|False|Você está de brincadeira?
Wasabi|3|False|JOGUEM ESSA TOLA NA PRISÃO!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Som|1|False|Droga!
Pepper|2|True|Fschh...
Pepper|3|False|Eu não consigo fazer nada aqui!
Pepper|4|False|Maldita prisão mágica! Grrrr!
Som|5|False|CLANG!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Como pude ser tão ingênua!
Shichimi|2|True|Shhhhh, Pepper!
Shichimi|3|False|Não faz barulho.
Pepper|4|False|Quem é você?!
Shichimi|5|True|Shhhhh! Fica quieta!
Shichimi|6|True|Vem aqui.
Shichimi|7|False|Eu vim pra te soltar.
Som|8|False|Kshiii...
Pepper|9|False|Shichimi?

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|False|Eu... Eu sinto muito pelo o que aconteceu...
Shichimi|2|False|... sabe, com a nossa briga.
Shichimi|3|True|Eu...
Shichimi|4|False|Eu não tinha escolha.
Pepper|5|True|Não se preocupa - eu sei.
Pepper|6|False|Obrigada por vir.
Shichimi|7|False|Esta cela mágica é mesmo resistente - eles fizeram o pacote completo pra você!
Pepper|8|False|Ha ha!
Shichimi|9|False|Mas fica quieta, eles vão nos ouvir.
Rato|10|True|LAP
Rato|11|True|LAP
Rato|12|False|LAP
Shichimi|13|True|Sabe,
Shichimi|14|True|outro motivo pelo qual eu vim é que eu fui admitida no círculo de confiança da Wasabi depois da cerimônia.
Shichimi|15|False|E eu fiquei sabendo dos planos dela...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Som|16|False|CLANG!
Som|18|True|CLUNK!
Som|19|True|CLANG!
Som|20|True|CLANK!
Som|21|False|CLANG!
Shichimi|1|False|É horrível, Pepper.
Shichimi|2|False|Wasabi quer pura e simplesmente dominar todas as outras escolas de magia...
Shichimi|3|False|Ela partirá amanhã ao amanhecer para Qualicity com um exército de bruxas...
Pepper|4|True|Oh, não!
Pepper|5|True|Coriander!
Pepper|6|False|E seu reino!
Shichimi|7|True|E a magia Zombiah!
Shichimi|8|False|Nós precisamos alertá-la imediatamente!
Shichimi|9|False|Uma pilota e uma dragoa nos esperam na plataforma, para nos tirar daqui.
Shichimi|10|False|Isso, a fechadura finalmente cedeu!
Som|11|False|Ching!
Pepper|12|True|Boa!
Pepper|13|False|Eu vou pegar o Carrot e meu chapéu.
Rato|14|False|squik!
Carrot|15|False|HISSSS!
Rato|17|False|squiiiii!
Pepper|22|False|!!!
Shichimi|23|False|!!!
Guarda|24|True|GUARDA!
Guarda|25|False|ALERTA!
Guarda|26|False|UM INTRUSO ESTÁ LIBERTANDO A PRISIONEIRA!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|False|A gente tava tão perto...
Pepper|2|False|É, tão perto.
Pepper|3|False|Aliás, você sabe por que a Wasabi tá atrás de mim?
Shichimi|4|False|Ela tem medo das bruxas de Chaosah, Pepper...
Shichimi|5|True|Especialmente das reações em cadeia de vocês.
Shichimi|6|False|Ela acredita que esses feitiços sejam uma grande ameaça para os planos dela.
Pepper|7|True|Ah, isso, pfff...
Pepper|8|False|Ela pode ficar fria, eu nunca consegui desencadear uma reação dessas.
Shichimi|9|False|Não brinca?
Pepper|10|False|É, sério, ha ha ha!
Shichimi|11|True|Hi-hi!
Shichimi|12|False|Ela é tão paranóica...

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Rei|1|True|Comandante!
Rei|2|False|Você está certo de que esse é o templo daquela bruxa?
Comandante|3|True|É muito provável, senhor!
Comandante|4|False|Vários de nossos informantes a viram por aqui recentemente.
Rei|5|True|GRrrr...
Rei|6|False|Então é aqui que mora aquela ameaça às nossas artes de guerra e tradições!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Rei|1|False|Celebremos a nossa aliança retaliando e destruindo esse templo da superfície de Hereva.
Som|3|False|Clap
Inimigo|2|False|Bem dito!
Exército|4|True|É!
Exército|5|True|Isso aí!
Exército|6|True|Vamos!
Exército|7|True|É!
Exército|8|True|Urra!
Exército|9|True|Urra!
Exército|10|True|Urra!
Exército|11|True|Vamos!
Exército|12|True|Urra!
Exército|13|False|Urra!
Rei|14|True|CATAPULTAS!
Rei|15|False|FOGO!!!
Som|16|False|ruuuuUUUUOOOOOOO!
Som|17|True|Whush!
Som|18|False|Whush!
Rei|19|False|ATACAAAAAAAR!!!
Pepper|20|True|O que foi isso?
Pepper|21|False|Um ataque?!
Som|22|True|BUUM!
Som|23|False|BUUUM!
Shichimi|24|False|O quê?!
Som|25|True|BUU~UUUM!
Som|26|False|BUUM!

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|cof
Pepper|2|False|cof!!
Pepper|3|False|Shichimi! Você tá bem?
Shichimi|4|True|Sim, tudo bem!
Shichimi|5|True|E você?
Shichimi|6|False|E Carrot?
Pepper|7|False|Tudo bem.
Pepper|8|True|Ora essa...
Pepper|9|False|Não... posso... crer...
Shichimi|10|True|De onde vieram esses soldados?!
Shichimi|11|True|E com catapultas?!
Shichimi|12|False|O que eles querem?
Pepper|13|True|Eu não sei...
Pepper|14|False|Mas eu conheço aqueles dois.
Shichimi|15|False|?
Shichimi|16|False|!!!
Shichimi|17|False|Torreya, aqui!

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|True|Shichimi!
Torreya|2|False|Que os espíritos sejam louvados, você está sã e salva.
Som|3|False|Pega
Torreya|4|True|Eu fiquei tão preocupada quando soube que eles tinham prendido você!
Torreya|5|True|E essa batalha!
Torreya|6|False|Que caos!
Shichimi|7|False|Torreya, é tão bom te ver.
Pepper|8|False|Oh, uau...
Pepper|9|False|Então essa pilota de dragão é a namorada da Shichimi...
Pepper|10|False|Nem imagino o que teria acontecido se eu tivesse me livrado dela.
Pepper|11|False|E essas tropas, eles devem ter me seguido.
Pepper|12|False|Sem eles a gente ainda estaria presa.
Pepper|13|False|Tudo parece tão conectado...
Pepper|14|False|...AH!

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|False|O que está acontecendo com ela?
Shichimi|2|False|Pepper? Tá tudo bem?
Pepper|3|True|Sim, eu tou bem.
Pepper|4|False|Eu só me dei conta de uma coisa.
Pepper|5|False|Tudo o que aconteceu é, direta ou indiretamente, o resultado das minhas ações e escolhas...
Pepper|6|False|...ou seja, minha reação em cadeia!
Shichimi|7|True|Sério?
Shichimi|8|False|Você vai ter que nos explicar.
Torreya|9|True|Chega de papo, estamos no meio de um campo de batalha!
Torreya|10|True|A gente fala sobre isso durante o vôo.
Torreya|11|False|Sobe aí, Pepper!
Shichimi|12|True|Torreya tem razão.
Shichimi|13|False|Precisamos chegar a Qualicity independente de qualquer coisa.
Pepper|14|False|Espera um pouco.
Pepper|15|True|A tropa da Wasabi está prestes a contra-atacar.
Pepper|16|False|Não podemos deixar que se matem assim.
Pepper|17|False|Eu acho que é meu dever encerrar esta batalha.

### P12

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|False|Mas como?
Arra|2|True|Sim, como você pensa em fazê-lo, bruxa?
Arra|3|False|Você recuperou pouca Rea, eu posso sentir.
Pepper|4|True|Você tem toda a razão, mas eu tenho um feitiço que pode consertar tudo
Pepper|5|False|Eu só preciso da sua Rea para conseguir atingir todo o mundo.
Arra|6|True|Dar energia para uma bruxa?
Arra|7|True|É proibido!
Arra|8|False|De jeito nenhum!
Pepper|9|False|Você prefere assistir a uma carnificina, então?
Torreya|10|True|Por favor, Arra. Faça uma exceção. Essas meninas e dragões lutando são nossa escola, nossa família.
Torreya|11|False|E a sua também.
Shichimi|12|False|É, por favor, Arra.
Arra|13|True|Pff! Tá certo!
Arra|14|False|Mas por sua conta e risco!

### P13

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Som|1|False|fshhhh!!
Pepper|2|False|UOOOOU!
Pepper|3|False|Então é assim que é a Rea de um dragão!
Shichimi|4|False|Pepper, rápido! A batalha!
Pepper|5|True|Allus... !
Pepper|6|True|Yuus... !
Pepper|7|True|Needum... !
Pepper|8|True|Est...
Pepper|9|False|...LOVIUS !
Som|10|False|Djjummm!!

### P14

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Som|5|True|Fiizz!
Som|4|True|Dziing!
Som|3|True|Hfhii!
Som|2|True|Schii!
Som|1|True|Schsss!
Som|6|True|Fiizz!
Som|7|True|Dziing!
Som|9|True|Schii!
Som|8|True|Hfheee!
Som|10|False|Schsss!
Pepper|11|False|Esse feitiço foi a minha primeira tentativa quando eu trabalhei com magia anti-guerra.
Pepper|12|False|Ele converte inimigos mortais em amigos...
Pepper|13|False|...e violência em amor e compaixão.
Shichimi|14|True|Uau!
Shichimi|15|True|Mas isso é incrível, Pepper!
Shichimi|16|False|Eles pararam de lutar!
Torreya|17|False|Está funcionando!

### P15

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|True|Mas o que está acontecendo?
Shichimi|2|False|Alguns deles estão se beijando...?!
Torreya|3|True|Hã... Um monte de novos casais!
Torreya|4|False|Isso era mesmo para acontecer, Pepper?
Pepper|5|False|Oh, não! Eu acho que a Rea de dragão ampliou o amor do meu feitiço.
Torreya|6|False|Haha, essa batalha vai entrar direto para os livros de história.
Shichimi|7|True|Hi-hi,
Shichimi|8|False|definitivamente!
Pepper|9|False|Aaahhh, que mico!
Escrita|10|False|- FIM -

### P16

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Créditos|1|False|15 de dezembro de 2021 Arte e roteiro: David Revoy. Leitores beta: Arlo James Barnes, Bhoren, Bobby Hiltz, Craig Maloney, Estefania de Vasconcellos Guimaraes, GunChleoc, Karl Ove Hufthammer, Nicolas Artance, Pierre-Antoine Angelini, Valvin. Tradução para Português: Alexandre E. Almeida, Estefania de Vasconcellos Guimaraes. Baseado no universo de Hereva Criador: David Revoy. Mantenedor principal: Craig Maloney. Escritores: Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Corretores: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Software: Krita 5.0β, Inkscape 1.1 no Kubuntu Linux 20.04. Licença: Creative Commons Attribution 4.0. www.peppercarrot.com
Pepper|2|False|Você sabia?
Pepper|3|False|Pepper&Carrot é totalmente livre, com código aberto e patrocinada graças a gentil contribuição dos seus leitores.
Pepper|4|False|Para este episódio, agradecemos a 1036 patronos!
Pepper|5|False|Você também pode se tornar um(a) patrono(a) de Pepper&Carrot e ter o seu nome aqui!
Pepper|6|False|Nós estamos no Patreon, Tipeee, PayPal, Liberapay... e mais!
Pepper|7|False|Acesse www.peppercarrot.com para mais informações!
Pepper|8|False|Obrigada!
