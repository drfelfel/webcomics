# Transcript of Pepper&Carrot Episode 36 [id]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Judul|1|False|Episode 36: Serangan Kejutan

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Wasabi|1|True|PERMINTAAN MAAF?
Wasabi|2|False|Jangan bercanda!
Wasabi|3|False|MASUKKAN ORANG INI KE PENJARA!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Suara|1|False|Fschh...
Pepper|2|True|Sial!
Pepper|3|False|Aku tak bisa berbuat apa-apa di sini!
Pepper|4|False|Dasar penjara sihir! Grrrr!
Suara|5|False|KLANG!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Kenapa aku begitu bodoh!
Shichimi|2|True|Shhhhh, Pepper!
Shichimi|3|False|Jangan berisik.
Pepper|4|False|Siapa di situ?!
Shichimi|5|True|Shhhhh! Tolong diam!
Shichimi|6|True|Di sini.
Shichimi|7|False|Aku akan mengeluarkanmu.
Suara|8|False|Kshiii...
Pepper|9|False|Shichimi?

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|False|Aku... Aku minta maaf buat yang terjadi...
Shichimi|2|False|...tahu kan, perang kita.
Shichimi|3|True|Aku...
Shichimi|4|False|Aku terpaksa melakukannya.
Pepper|5|True|Tak perlu kuatir – Aku tahu kok
Pepper|6|False|Terima kasih sudah datang.
Shichimi|7|False|Penjara sihir ini sangat kuat – mereka tak tanggung-tanggung buatmu!
Pepper|8|False|Ha ha!
Shichimi|9|False|Pelan-pelan, nanti terdengar mereka.
Tikus|10|True|LAP
Tikus|11|True|LAP
Tikus|12|False|LAP
Shichimi|13|True|Kau tahu,
Shichimi|14|True|alasan lain aku datang ialah aku diundang ke lingkaran dalam Wasabi langsung setelah upacara waktu itu.
Shichimi|15|False|Di situ aku jadi tahu rencananya...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Suara|16|False|KLANG!
Suara|18|True|KLONG!
Suara|19|True|KLANG!
Suara|20|True|KLING!
Suara|21|False|KLANG!
Shichimi|1|False|Mengerikan, Pepper.
Shichimi|2|False|Wasabi mau mendominasi semua aliran sihir yang lain, sesederhana itu...
Shichimi|3|False|Dia akan pergi besok subuh dengan sejumlah tentara penyihir ke Qualicity...
Pepper|4|True|Oh, tidak!
Pepper|5|True|Coriander!
Pepper|6|False|Dan kerajaannya!
Shichimi|7|True|Dan sihir Zombiah!
Shichimi|8|False|Kita harus segera memperingatinya!
Shichimi|9|False|Seorang pilot dengan naga menunggu kita di luar, untuk membawa kita pergi.
Shichimi|10|False|Oke, akhirnya kuncinya terbuka!
Suara|11|False|Ching !
Pepper|12|True|Bravo!
Pepper|13|False|Aku mau ambil Carrot dan topiku.
Tikus|14|False|ciit!
Carrot|15|False|HISSSS!
Tikus|17|False|ciiiit!
Pepper|22|False|!!!
Shichimi|23|False|!!!
Penjaga|24|True|PENJAGA!
Penjaga|25|False|KE SINI!
Penjaga|26|False|ADA PENYUSUP YANG MEMBEBASKAN TAHANAN!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|False|Hampir saja lolos...
Pepper|2|False|Iya, hampir.
Pepper|3|False|Omong-omong, apa kamu tahu kenapa Wasabi mengejarku?
Shichimi|4|False|Dia takut pada penyihir Chaosah, Pepper...
Shichimi|5|True|Terutama dengan reaksi berantai kalian.
Shichimi|6|False|Dia percaya itu ancaman besar untuk rencananya.
Pepper|7|True|Oh, itu, pfff...
Pepper|8|False|Dia tak perlu takut; Aku tidak pernah berhasil melakukannya.
Shichimi|9|False|Benarkah?
Pepper|10|False|Iya, benar, ha ha ha!
Shichimi|11|True|Hihihi!
Shichimi|12|False|Dia terlalu paranoid...

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Raja|1|True|Perwira!
Raja|2|False|Apa kamu yakin ini adalah biara penyihir itu?
Perwira|3|True|Cukup yakin, komandan!
Perwira|4|False|Beberapa pengintai kita melihat dia di sini belakangan.
Raja|5|True|GRrrr...
Raja|6|False|Jadi, ancaman ke tradisi perang kita ada di sini!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Raja|1|False|Mari kita rayakan persekutuan kita dengan membalas dan meratakan biara ini dari permukaan Hereva.
Suara|3|False|Klap
Musuh|2|False|Setuju!
Tentara|4|True|Yeah!
Tentara|5|True|Yeah!
Tentara|6|True|Yeah!
Tentara|7|True|Yeah!
Tentara|8|True|Yeehaaa!
Tentara|9|True|Yarr!
Tentara|10|True|Yeehaw !
Tentara|11|True|Yeee-haw!
Tentara|12|True|Yaaa!
Tentara|13|False|Hurrah!
Raja|14|True|KETAPEL!
Raja|15|False|TEMBAK!!!
Suara|16|False|huUWOOOOOOOOOOO!
Suara|17|True|Wuuussh!
Suara|18|False|Wuuussh!
Raja|19|False|SERAAAAAAANG!!!
Pepper|20|True|Ada apa ini?
Pepper|21|False|Serangan?!
Suara|22|True|BuuM!
Suara|23|False|BUUUM!
Shichimi|24|False|Apa?!
Suara|25|True|BUu~uuuM !
Suara|26|False|BUUM !

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|uhuk
Pepper|2|False|uhuk!!
Pepper|3|False|Shichimi! Kamu tak apa-apa?
Shichimi|4|True|Ya, aku baik-baik saja!
Shichimi|5|True|Kamu?
Shichimi|6|False|Dan Carrot?
Pepper|7|False|Kami baik-baik saja.
Pepper|8|True|Apa-apaan ini...
Pepper|9|False|Aku... tidak... percaya ini...
Shichimi|10|True|Darimana para tentara ini datang?!
Shichimi|11|True|Dan dengan ketapel?!
Shichimi|12|False|Mau apa mereka?
Pepper|13|True|Aku tak tahu...
Pepper|14|False|Tapi dua orang itu aku tahu.
Shichimi|15|False|?
Shichimi|16|False|!!!
Shichimi|17|False|Torreya, di sini!

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|True|Shichimi!
Torreya|2|False|Syukurlah, kamu tidak kenapa-kenapa.
Suara|3|False|Plek
Torreya|4|True|Aku sangat kuatir ketika kudengar mereka memenjarakanmu!
Torreya|5|True|Dan perang ini!
Torreya|6|False|Sungguh kacau!
Shichimi|7|False|Torreya, senangnya bertemu kamu lagi.
Pepper|8|False|Astaga...
Pepper|9|False|Jadi pilot naga ini adalah pacarnya Shichimi's...
Pepper|10|False|Aku tak bisa membayangkan seandainya aku menyingkirkan dia.
Pepper|11|False|Dan para tentara ini, mereka pasti mengikutiku.
Pepper|12|False|Tanpa mereka, kita tidak akan bebas.
Pepper|13|False|Semuanya tersambung...
Pepper|14|False|...OH!

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|False|Kenapa dia?
Shichimi|2|False|Pepper? Semuanya oke?
Pepper|3|True|Ya, tak apa.
Pepper|4|False|Aku baru saja sadar sesuatu.
Pepper|5|False|Semua yang terjadi di sini, langsung atau tidak langsung, adalah hasil dari tindakanku, keputusanku...
Pepper|6|False|...dengan kata lain, reaksi berantaiku!
Shichimi|7|True|Benarkah?
Shichimi|8|False|Kamu harus jelaskan nanti.
Torreya|9|True|Cukup ngobrolnya, kita sekarang di tengah-tengah pertempuran.
Torreya|10|True|Kita bisa lanjutkan setelah kita terbang.
Torreya|11|False|Ayo naik, Pepper!
Shichimi|12|True|Torreya benar.
Shichimi|13|False|Kita perlu ke Qualicity, bagaimanapun juga.
Pepper|14|False|Tunggu sebentar.
Pepper|15|True|Tentara Wasabi baru mau menyerang balik.
Pepper|16|False|Kita tak bisa membiarkan mereka membunuh satu sama lain.
Pepper|17|False|Aku rasa ini adalah tanggung jawabku untuk mengakhiri perang ini.

### P12

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|False|Tapi bagaimana?
Arra|2|True|Ya, bagaimana kau bisa melakukannya, penyihir?
Arra|3|False|Kau belum memulihkan cukup Rea, aku bisa rasakan.
Pepper|4|True|Betul sekali, tapi aku punya mantra yang bisa memperbaiki semuanya.
Pepper|5|False|Aku cuma perlu Rea darimu untuk bisa meraih semua orang.
Arra|6|True|Memberikan energi ke penyihir?
Arra|7|True|Tentu saja tidak!
Arra|8|False|Apa kamu lebih memilih terjadi pertumpahan darah?
Pepper|9|False|Tolong, Arra. Buatlah pengecualian. Penyihir dan naga yang berperang itu, mereka saudara dan teman kita.
Torreya|10|True|Dan saudaramu juga.
Torreya|11|False|Iya, tolonglah, Arra.
Shichimi|12|False|PFF! Baiklah!
Arra|13|True|Tapi tanggung sendiri risikonya!
Arra|14|False|Itu terlarang!

### P13

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Suara|1|False|fshhhh!!
Pepper|2|False|WAAAAH!
Pepper|3|False|Jadi seperti ini ya Rea-nya naga!
Shichimi|4|False|Pepper, cepat! Perangnya!
Pepper|5|True|Allus... !
Pepper|6|True|Yuus... !
Pepper|7|True|Needum... !
Pepper|8|True|Est...
Pepper|9|False|...LOVIUS !
Suara|10|False|Dzziuuu!!

### P14

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Suara|5|True|Fiizz!
Suara|4|True|Dziing!
Suara|3|True|Hfhii!
Suara|2|True|Skiii!
Suara|1|True|Sksss!
Suara|6|True|Fiizz!
Suara|7|True|Dziing!
Suara|9|True|Skiii!
Suara|8|True|Hfhiii!
Suara|10|False|Sksss!
Pepper|11|False|Mantra ini adalah tahap pertama dari mantra anti-perangku.
Pepper|12|False|Ini mengubah musuh menjadi teman...
Pepper|13|False|…dan kekerasan menjadi kasih sayang.
Shichimi|14|True|Wow!
Shichimi|15|True|Ya... ini bagus sekali, Pepper!
Shichimi|16|False|Mereka berhenti berperang!
Torreya|17|False|Mantranya bekerja!

### P15

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|True|Eh tapi apa yang terjadi?
Shichimi|2|False|Mereka mulai berciuman...?!
Torreya|3|True|Uh... Ini banyak pasangan baru!
Torreya|4|False|Memang mantranya seperti itu, Pepper?
Pepper|5|False|Oh, tidak! Aku rasa ini Rea naga yang memperbesar efek kasih sayang di mantranya.
Torreya|6|False|Haha, perang ini pasti tercatat di buku-buku sejarah.
Shichimi|7|True|Hihihi,
Shichimi|8|False|pasti!
Pepper|9|False|Ooohhh, betapa memalukan!
Penulis|10|False|- FIN -

### P16

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Kredit|1|False|15 Desember 2021 Pelukis & skenario: David Revoy. Penguji Bacaan Cerita: Arlo James Barnes, Bhoren, Bobby Hiltz, Craig Maloney, Estefania de Vasconcellos Guimaraes, GunChleoc, Karl Ove Hufthammer, Nicolas Artance, Pierre-Antoine Angelini , Valvin. Versi Bahasa Indonesia Penerjemah: Aldrian Obaja Muis. Berdasarkan alam semesta Hereva Pembuat: David Revoy. Penyunting utama: Craig Maloney. Penulis: Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Correctors: Willem Sonke, Moini, Hali, CGand, Alex Gryson Peranti Lunak: Krita 5.0β, Inkscape 1.1 on Kubuntu Linux 20.04. Izin (Lisensi): Creative Commons Attribution 4.0. www.peppercarrot.com
Pepper|2|False|Tahukah kamu?
Pepper|3|False|Pepper&Carrot sepenuhnya gratis (bebas), sumber terbuka dan disponsori oleh para pembacanya.
Pepper|4|False|Untuk episode ini, terima kasih kepada 1036 relawan!
Pepper|5|False|Kamu juga bisa menjadi sukarelawan Pepper&Carrot dan namamu akan tercantum di sini!
Pepper|6|False|Kami ada di Patreon, Tipeee, PayPal, Liberapay ...dan banyak lagi!
Pepper|7|False|Cek www.peppercarrot.com untuk informasi selanjutnya!
Pepper|8|False|Terima kasih!
