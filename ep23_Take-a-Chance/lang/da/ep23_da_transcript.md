# Transcript of Pepper&Carrot Episode 23 [da]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 23: Grib chancen

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fortæller|1|False|Komonas marked, en uge senere
Skrift|2|False|Kendte
Skrift|3|False|50 000 Ko
Skrift|4|False|Safrans sejr
Skrift|5|False|Mode
Skrift|6|False|Fænomenet Safran
Skrift|7|False|Chic
Skrift|8|False|Safran Special
Lyd|9|False|PLONK!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Tak Carrot…
Pepper|2|False|Ser du, jeg arbejder hårdt hver dag for at blive en god heks…
Pepper|3|True|...med respekt for traditionerne, reglerne…
Pepper|4|False|...det er uretfærdigt, at Safran bliver så populær med den slags metoder...
Pepper|5|False|…Virkelig uretfærdigt.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|1|False|Bonk!
Succesens Ånd|2|True|Goddag!
Succesens Ånd|3|False|Lad mig præsentere mig selv:
Succesens Ånd|4|False|jeg er succesens ånd, til Deres tjeneste fireogtyve timer i døgnet, syv dage om ugen!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Succesens Ånd|1|False|*Tjenesterne kan ikke benyttes i weekender og helligdage. Tilbuddet kan ikke kumuleres. Begrænset antal.
Succesens Ånd|2|False|Øjeblikkelig verdens-omspændende markeds-føringskampagne!
Succesens Ånd|3|False|internationalt anerkendt
Succesens Ånd|4|False|Stylister og frisører inkluderede
Succesens Ånd|5|False|Garanterede resultater!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Succesens Ånd|1|False|Bare en lille underskrift nederst på dokumentet og døren til ære og berømmelse vil åbne sig!
Pepper|2|False|Lige et øjeblik.
Pepper|4|True|Hvad er De for en Deus ex machina ?!
Pepper|3|False|Præcis i det øjeblik hvor det går allerdårligst, falder De ned fra himlen med en mirakelløsning på mit problem?!
Pepper|5|False|Det virker mistænksomt, endda meget mistænksomt!
Pepper|6|True|Kom Carrot.
Pepper|7|False|Måske ville sådanne løjer have virket på os tidligere, men nu er vi ikke længere så dumme at falde for den slags!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fugl|1|True|Pip!
Fugl|2|True|Pip!
Fugl|3|True|Pip!
Fugl|4|False|Pip!
Pepper|5|False|Ser du, Carrot, jeg tror det er en del af at blive voksen.
Pepper|6|False|Undgå de nemme løsninger, se værdien af hårdt arbejde...
Pepper|7|False|...ingen grund til at være jaloux på dem der har fået succes!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Skrift|1|False|Kendte
Skrift|2|False|50 000 Ko
Skrift|3|False|Safrans sejr
Skrift|4|False|Mode
Skrift|5|False|Fænomenet Safran
Pepper|8|False|Lykken skal nok også smile til os en dag!
Skrift|6|False|Chic
Skrift|7|False|Safran Special
Lyd|9|False|Bziooo!
Skrift|10|False|Kendte
Skrift|11|False|Frøken Dues hektiske liv
Skrift|12|False|Mode
Skrift|13|False|Fænomenet Frøken Due
Skrift|14|False|Chic
Skrift|15|False|Frøken Due Special
Fortæller|16|False|- SLUT -

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|08/2017 - www.peppercarrot.com - Tegning og manuskript: David Revoy – Dansk oversættelse: Emmiline Alapetite
Credits|2|False|Genlæsning og hjælp til dialogerne: Alex Gryson, Calimeroteknik, Nicolas Artance et Valvin.
Credits|4|False|Baseret på Hereva-universet skabt af David Revoy med bidrag af Craig Maloney. Rettelser af Willem Sonke, Moini, Hali, CGand og Alex Gryson.
Credits|5|False|Værktøj: Krita 3.1.4, Inkscape 0.92dev på Linux Mint 18.2 Cinnamon
Credits|6|False|Licens: Creative Commons Attribution 4.0
Credits|8|False|Pepper&Carrot er helt gratis, open-source, og sponsoreret af læsere. Til denne episode, tak til de 879 tilhængere:
Credits|7|False|Støt næste episode af Pepper&Carrot på www.patreon.com/davidrevoy
Credits|3|False|Aide au storyboard et mise-en-scène : Calimeroteknik et Craig Maloney.
