# Transcript of Pepper&Carrot Episode 23 [es]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Título|1|False|Episodio 23: Aprovechar la oportunidad

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrador|1|False|Mercado de Komona, una semana después
Escritura|2|False|Una Estrella
Escritura|3|False|50 000 Ko
Escritura|4|False|La victoria de Azafrán
Escritura|5|False|La Moda
Escritura|6|False|El fenómeno Azafrán
Escritura|7|False|Chic
Escritura|8|False|Especial Azafrán
Sonido|9|False|¡PAF!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|False|Gracias, Zanahoria…
Pimienta|2|False|Sabes, trabajo duro cada día para ser una buena bruja…
Pimienta|3|True|…respetando las tradiciones, las reglas…
Pimienta|4|False|…no es justo que Azafrán se haya vuelto tan popular con esta clase de prácticas…
Pimienta|5|False|…es realmente injusto.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Genio del Éxito|2|True|¡Saludos!
Genio del Éxito|3|False|Permítanme presentarme:
Sonido|1|False|¡Bong!
Genio del Éxito|4|False|¡soy el Genio del Éxito, a vuestro servicio las veinticuatro horas del día, los siete días de la semana!*

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Genio del Éxito|1|False|*Excepto domingos y días festivos. Oferta no acumulable, válida hasta agotar stock.
Genio del Éxito|2|False|¡Campaña de comunicación mundial instantánea!
Genio del Éxito|3|False|¡Renombre internacional!
Genio del Éxito|4|False|¡Estilistas y peluquería incluidos!
Genio del Éxito|5|False|¡Resultados inmediatos garantizados!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Genio del Éxito|1|False|¡Solo una firmita al final del documento y las puertas de la fama se te abrirán de par en par!
Pimienta|2|False|¡Eh, un minuto!
Pimienta|4|True|¡¿A qué se debe este Deus ex machina?!
Pimienta|3|False|¡¿Justo en el momento en que estoy de bajón usted cae del cielo con una solución milagrosa a mis problemas?!
Pimienta|5|False|¡Esto me huele mal, es muy sospechoso!
Pimienta|6|True|Vamos, Zanahoria.
Pimienta|7|False|¡Esta treta podría haber funcionado con nosotros antes, pero ya no somos tan ingenuos para caer en este tipo de trampas!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Ave|1|True|¡Gorrr!
Ave|2|True|¡Gorrr!
Ave|3|True|¡Gorrr!
Ave|4|False|¡Gorrr!
Pimienta|5|False|¿Lo ves, Zanahoria? Yo creo que esto es crecer.
Pimienta|6|False|Evitar las trampas fáciles, valorar el esfuerzo…
Pimienta|7|False|…¡de nada sirve envidiar el éxito de los demás!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Escritura|1|False|Una Estrella
Escritura|2|False|50 000 Ko
Escritura|3|False|La victoria de Azafrán
Escritura|4|False|La Moda
Escritura|5|False|El fenómeno Azafrán
Pimienta|8|False|¡La suerte acabará por sonreírnos a nosotros también un día!
Escritura|6|False|Chic
Escritura|7|False|Especial Azafrán
Sonido|9|False|¡Bsiuuu!
Escritura|10|False|Una estrella
Escritura|11|False|La trepidante vida de Srta. Paloma
Escritura|12|False|La Moda
Escritura|13|False|El fenómeno Srta. Paloma
Escritura|14|False|Chic
Escritura|15|False|Especial Srta. Paloma
Narrador|16|False|- FIN -

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Créditos|1|False|08/2017 - www.peppercarrot.com - Dibujo & Guion: David Revoy - Traducción: TheFaico
Créditos|2|False|Relectura y mejora de los diálogos: Alex Gryson, Calimeroteknik, Nicolas Artance y Valvin.
Créditos|4|False|Basado en el universo de Hereva, creado por David Revoy con las contribuciones de Craig Maloney. Correcciones de Willem Sonke, Moini, Hali, CGand y Alex Gryson.
Créditos|5|False|Software: Krita 3.1.4, Inkscape 0.92dev en Linux Mint 18.2 Cinnamon
Créditos|6|False|Licencia: Creative Commons Attribution 4.0
Créditos|8|False|Pepper&Carrot es completamente gratuito, de código abierto y financiado gracias al mecenazgo de sus lectores. Para este episodio, gracias a 879 mecenas:
Créditos|7|False|Tú también puedes ser mecenas de Pepper&Carrot en www.patreon.com/davidrevoy
Créditos|3|False|Ayuda con el guión gráfico y puesta en escena: Calimeroteknik y Craig Maloney.
