# Transcript of Pepper&Carrot Episode 23 [oc]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Títol|1|False|Episòdi 23 : Agantar l'oportunitat

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrator|1|False|Mercat de Komona, una setmana mai tard
Escritura|2|False|Celebritats
Escritura|3|False|50 000 Ko
Escritura|4|False|La victòria de Safran
Escritura|5|False|La Mòda
Escritura|6|False|Lo fenomèn Safran
Escritura|7|False|Gràcia
Escritura|8|False|Especial Safran
Son|9|False|PAF !

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Mercé Carròt…
Pepper|2|False|Sas, trabalhi fòrça, cada jorn, per èstre una bona masca…
Pepper|3|True|…amb lo respècte de las tradicions, de las règlas…
Pepper|4|False|…es pas juste que Safran venga tan populara amb aquela mena de practica…
Pepper|5|False|…De vertat, es pas juste.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Son|1|False|Pang!
Gèni de la capitada|2|False|Adissiatz !
Gèni de la capitada|3|True|Daissatz-me me presentar :
Gèni de la capitada|4|False|soi lo Gèni de la Capidada, al vòstre servici cada jorn de la setmana, a cada ora del jorn !*

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Gèni de la capitada|1|False|*Fòra la dimenjada e los jorns de fèsta, ofèrta pas cumulabla, dins lo limit dels estòcs disponibles.
Gèni de la capitada|2|False|Campanha de comunicacion mondiala instantanèa !
Gèni de la capitada|3|False|Renommada internacionala !
Gèni de la capitada|4|False|Estilistas e cofaires compreses !
Gèni de la capitada|5|False|Resultats garantits e immediats !

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Gèni de la capitada|1|False|Sonque una petita signatura en bas d'aqueste document, e las pòrtas de la gòria se dobriràn per vosautres !
Pepper|2|False|Òu, moment.
Pepper|4|True|De qu'es aquel Deus ex machina ?!
Pepper|3|False|Exactament quand vau pas plan, tombatz del cèl amb la solucion miracle a mon problèma ?!
Pepper|5|False|Me sembla plan dobtós tot aquò, fòrça fòrça dobtós !
Pepper|6|True|Ven Carròt.
Pepper|7|False|Aquel daquòs auriá benlèu foncionat amb nosautres abans, mas ara sèm pas pus pro nècis per tombar dins aquela mena de trapa !

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Aucèl|1|True|Piu !
Aucèl|2|True|Piu !
Aucèl|3|True|Piu !
Aucèl|4|False|Piu !
Pepper|5|False|Veses, Carròt, pensi qu'es aquò créisser.
Pepper|6|False|Evitar las trapèlas aisidas, recobrar la talent de l'esfòrç…
Pepper|7|False|…servís pas a res d'èstre gelós dels que capitan plan !

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Escritura|1|False|Celebritats
Escritura|2|False|50 000 Ko
Escritura|3|False|La victòria de Safran
Escritura|4|False|La Mòda
Escritura|5|False|Lo fenomèn Safran
Pepper|8|False|Acabaram plan per aver bonastre nosautres tanben !
Escritura|6|False|Gràcia
Escritura|7|False|Especial Safran
Son|9|False|Ziiiiiu !
Escritura|10|False|Celebritats
Escritura|11|False|La vida trepidanta de Dauna Colomb
Escritura|12|False|La Mòda
Escritura|13|False|Lo fenomèn Dauna Colomb
Escritura|14|False|Gràcia
Escritura|15|False|Especial Dauna Colomb
Narrator|16|False|- FIN -

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crèdits|1|False|08/2017 - www.peppercarrot.com - Dessenh & Scenari : David Revoy
Crèdits|2|False|Relectura e melhorament dels dialògues : Alex Gryson, Calimeroteknik, Nicolas Artance e Valvin.
Crèdits|4|False|Basat sus l'univèrs d'Hereva creat per David Revoy amb las contribucions de Craig Maloney. Correccions de Willem Sonke, Moini, Hali, CGand e Alex Gryson.
Crèdits|5|False|Logicials : Krita 3.1.4, Inkscape 0.92dev sus Linux Mint 18.2 Cinnamon
Crèdits|6|False|Licéncia : Creative Commons Attribution 4.0
Crèdits|8|False|Pepper&Carrot es completament liure, open source, e esponsorisat mercés al mecenat de sos lectors. Per aqueste episòdi, mercé als 879 mecènas :
Crèdits|7|False|Vos tanben, venètz mecèna de Pepper&Carrot sus www.patreon.com/davidrevoy
Crèdits|3|False|Ajuda al storyboard e mesa en scèna : Calimeroteknik e Craig Maloney.
