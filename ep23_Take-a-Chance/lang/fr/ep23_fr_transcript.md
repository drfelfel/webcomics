# Transcript of Pepper&Carrot Episode 23 [fr]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titre|1|False|Épisode 23 : Saisir la chance

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrateur|1|False|Marché de Komona, une semaine plus tard.
Écriture|2|False|Célébrités
Écriture|3|False|50 000 Ko
Écriture|4|False|La victoire de Safran
Écriture|5|False|La Mode
Écriture|6|False|Le Phénomène Safran
Écriture|7|False|Chic
Écriture|8|False|Spécial Safran
Son|9|False|PAF !

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Merci Carrot…
Pepper|2|False|Tu sais, je travaille dur tous les jours pour être une bonne sorcière…
Pepper|3|True|… dans le respect des traditions, des règles…
Pepper|4|False|… C'est pas juste que Safran devienne aussi populaire avec ce genre de pratique…
Pepper|5|False|… Vraiment pas juste.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Son|1|False|Bong !
Génie du succès|2|False|Bonjour !
Génie du succès|3|True|Permettez-moi de me présenter :
Génie du succès|4|False|je suis le Génie du Succès, à votre service vingt-quatre heures sur vingt-quatre, sept jours sur sept !*

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Génie du succès|1|False|*Hors week-ends et jours fériés, offre non cumulable dans la limite des stocks disponibles.
Génie du succès|2|False|Campagne de communication mondiale instantanée !
Génie du succès|3|False|Renommée internationale !
Génie du succès|4|False|Stylistes et coiffeurs inclus !
Génie du succès|5|False|Résultats garantis et immédiats !

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Génie du succès|1|False|Juste une petite signature en bas de ce document, et les portes de la gloire s'ouvriront à vous !
Pepper|2|False|Eh, minute.
Pepper|4|True|C'est quoi, ce Deus ex machina ?!
Pepper|3|False|Juste au moment où ça ne va pas, vous tombez du ciel avec la solution miracle à mon problème ?!
Pepper|5|False|Ça paraît louche tout ça, même très louche !
Pepper|6|True|Viens Carrot.
Pepper|7|False|Ce truc-là aurait peut-être marché sur nous avant, mais maintenant on n'est plus assez idiots pour tomber dans ce genre de piège !

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Oiseau|1|True|Piou !
Oiseau|2|True|Piou !
Oiseau|3|True|Piou !
Oiseau|4|False|Piou !
Pepper|5|False|Tu vois Carrot, je pense que c'est ça grandir.
Pepper|6|False|Éviter les pièges faciles, reprendre le goût de l'effort…
Pepper|7|False|… Rien ne sert de jalouser ceux qui connaissent le succès !

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Écriture|1|False|Célébrités
Écriture|2|False|50 000 Ko
Écriture|3|False|La victoire de Safran
Écriture|4|False|La Mode
Écriture|5|False|Le Phénomène Safran
Pepper|8|False|La chance finira bien par nous sourire à nous aussi !
Écriture|6|False|Chic
Écriture|7|False|Spécial Safran
Son|9|False|Bziooo !
Écriture|10|False|Célébrités
Écriture|11|False|La vie trépidante de Mlle Pigeon
Écriture|12|False|La Mode
Écriture|13|False|Le Phénomène Mlle Pigeon
Écriture|14|False|Chic
Écriture|15|False|Spécial Mlle Pigeon
Narrateur|16|False|- FIN -

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crédits|1|False|08/2017 - www.peppercarrot.com - Dessin & Scénario : David Revoy
Crédits|2|False|Relecture et amélioration des dialogues : Alex Gryson, Calimeroteknik, Nicolas Artance et Valvin.
Crédits|4|False|Basé sur l'univers d'Hereva créé par David Revoy avec les contributions de Craig Maloney. Corrections de Willem Sonke, Moini, Hali, CGand et Alex Gryson.
Crédits|5|False|Logiciels : Krita 3.1.4, Inkscape 0.92dev sur Linux Mint 18.2 Cinnamon
Crédits|6|False|Licence : Creative Commons Attribution 4.0
Crédits|8|False|Pepper&Carrot est entièrement libre, open-source, et sponsorisé grâce au mécénat de ses lecteurs. Pour cet épisode, merci aux 879 Mécènes :
Crédits|7|False|Vous aussi, devenez mécène de Pepper&Carrot sur www.patreon.com/davidrevoy
Crédits|3|False|Aide au storyboard et mise-en-scène : Calimeroteknik et Craig Maloney.
