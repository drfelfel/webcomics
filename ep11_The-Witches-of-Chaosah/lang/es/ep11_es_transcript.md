# Transcript of Pepper&Carrot Episode 11 [es]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Título|1|False|Episodio 11: Las brujas de Chaosah

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayena|1|False|Pimienta, estás manchando el nombre de Chaosah.
Tomillo|2|False|Cayena tiene razón; una bruja de Chaosah digna de ese título debe inspirar humildad, miedo y respeto.
Cumino|3|False|...y tú, incluso ofreces té con dulces a nuestros demonios*...
Nota|4|False|* ver episodio 8: El cumpleaños de Pimienta.
Pimienta|5|True|Pero...
Pimienta|6|False|...madrinas...

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayena|4|False|¡Silencio!
Cumino|9|False|¡Tacháaan!
Tomillo|1|True|Pimienta, tú tienes mucho talento, pero también eres nuestra única sucesora.
Tomillo|2|False|Nuestro deber es hacer de ti una auténtica bruja malvada de Chaosah.
Pimienta|3|False|¡Pero... yo no quiero ser mala! Eso es... es contrario a lo que....
Cayena|5|True|... ¡o anularemos todos tus poderes!
Cayena|6|False|Y tú volverás a ser la pequeña huérfana idiota de Punta de Ardilla.
Tomillo|7|False|Comino te va a acompañar a todos lados a partir de ahora. Su misión es ayudarte en tu formación e informarnos de tus progresos.
Sonido|8|False|¡Puf!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cumino|1|False|El nuevo rey de Acren es demasiado dulce y bueno con sus súbditos. El pueblo debe temer a su rey.
Cumino|2|False|Tu primera misión será intimidar al monarca para poder manejar sus decisiones.
Cumino|3|False|¡Una auténtica bruja de Chaosah es capaz de influenciar a quien tiene el poder!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|True|¿El rey? ...
Pimienta|2|False|... ¡¿tan joven?!
Pimienta|3|False|Si tendremos la misma edad...
Cumino|4|False|¡¿Pero a que esperas?! ¡Vamos! ¡Despiértalo, dale un sermón, asústalo!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sonido|1|False|Dzzz
Pimienta|2|False|Tú y yo somos tan iguales...
Pimienta|3|True|jóvenes...
Pimienta|4|True|solos en la vida...
Pimienta|5|False|...prisioneros de nuestros destinos.

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tomillo|6|False|Tenemos mucho trabajo que hacer contigo, Pimienta...
Créditos|8|False|Septiembre 2015 - Dibujo y Guión: David Revoy - Traducción: TheFaico
Narrador|7|False|- FIN -
Sonido|1|False|¡Puf!
Pimienta|2|False|?!
Sonido|3|False|¡Fiuuf!
Tomillo|4|True|Primera prueba:
Tomillo|5|True|FRACASO TOTAL

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Créditos|1|False|Pepper&Carrot es completamente gratuito, de código abierto y patrocinado gracias al mecenazgo de sus lectores. Para este episodio, gracias a 502 mecenas:
Créditos|3|False|https://www.patreon.com/davidrevoy
Créditos|2|True|Tú también puedes ser mecenas de Pepper&Carrot para el próximo episodio:
Créditos|4|False|Licencia: Creative Commons Attribution 4.0 Ficheros originales disponibles en www.peppercarrot.com Herramientas: Este episodio ha sido creado al 100% con software libre Krita 2.9.6, G'MIC 1.6.5.2, Inkscape 0.91 en Linux Mint 17
