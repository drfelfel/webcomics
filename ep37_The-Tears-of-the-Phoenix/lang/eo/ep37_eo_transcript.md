# Transcript of Pepper&Carrot Episode 37 [eo]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titolo|1|False|Ĉapitro 37a : La larmoj de la fenikso

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pipro|1|True|Fju...
Pipro|2|False|Tre bone esti ĉi tie !
Pipro|3|False|Ho, estas bazartago.
Pipro|4|False|Ni iru manĝeti, antaŭ ol ni grimpos sur la vulkanon.
Pipro|5|False|Mi sciis, ke vi ŝatus la ideon !

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Skribaĵo|1|False|SERĈATAJ
Skribaĵo|2|False|Nukstaksuso
Skribaĵo|3|False|100 000Ko
Skribaĵo|4|False|Ŝiĉimio
Skribaĵo|5|False|250 000Ko
Skribaĵo|6|False|Pipro
Skribaĵo|7|False|1 000 000Ko

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pipro|1|False|Ili vere afiŝis ĉi tion ĉie.
Pipro|2|False|Eĉ en la plej izolitaj anguloj.
Pipro|3|False|Venu, ni foriru antaŭ ni estu rekonitaj.
Pipro|4|False|Ni nun havas pli grandajn problemojn...
Pipro|5|False|Tiu lumo...
Pipro|6|False|Ni devas esti proksimaj al ĝia nesto.
Pipro|7|False|Prave !

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fenikso|1|False|Kial vi ĝenas min, homo ?
Pipro|2|False|Saluton, ho granda Fenikso !
Pipro|3|False|Mia nomo estas Pipro kaj mi estas sorĉistino de Ĥaosaho.
Pipro|4|False|Mi lastatempe ricevis grandan kvanton da Draka Reo, kaj ekde tiam ĉi tiu aperis kaj kreskas ĉiutage...
Pipro|5|False|Ĝi forigas miajn povojn kaj povus fine mortigi min.

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fenikso|1|True|Hmm...
Fenikso|2|False|Mi vidas...
Fenikso|3|False|... kaj nun vi estas ĉi tie ĉar vi volas feniksajn larmojn por saniĝi, ĉu ?
Pipro|4|False|Jes, ĝi estas mia sola elekteblo.
Fenikso|5|False|*vespiro*
Fenikso|6|False|...nu, kion vi atendas ?
Fenikso|7|False|Klopodu plorigi min.
Fenikso|8|False|Mi donas al vi minuton !
Pipro|9|False|Kio ?!
Pipro|10|False|Plorigi vin ?!
Pipro|11|False|Sed mi ne sciis tion...
Pipro|12|False|Nome... Nur unu minuton ?!
Pipro|13|True|Be...
Pipro|14|True|BONE !
Pipro|15|False|Ni vidu.
Pipro|16|True|Hmm... Pensu pri la monda malsato.
Pipro|17|True|Nuu… atenduatendu !
Pipro|18|True|Mi havas ion pli bonan :
Pipro|19|False|Tiuj, kiuj ne plu estas.
Pipro|20|True|Ankoraŭ nenio ?
Pipro|21|True|Forlasitaj dorlotbestoj ?!
Pipro|22|False|Dorlotbesta forlasado estas malgajega...
Fenikso|23|False|...

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pipro|1|True|Do, kielis ?
Pipro|2|False|Ĉu tio ne plorigas vin ?
Fenikso|3|False|ĈU VERE ?!
Fenikso|4|False|ĈU TIO ESTAS ĈIO, KION VI HAVAS ?!
Fenikso|5|True|Ili almenaŭ provis per poezio !
Fenikso|6|True|Skribante tragediojn !
Fenikso|7|True|ARTON !
Fenikso|8|False|DRAMON !
Fenikso|9|True|SED VI ?!
Fenikso|10|False|VI VENAS NEPREPARITA !
Fenikso|11|True|JES, FARU TION ; FOR !
Fenikso|12|False|IRU HEJMEN KAJ REVENU, KIAM VI PRETU !

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pipro|1|False|Grr ! Ek, Pipro ! Trovu malgajan historion.
Pipro|2|True|Vi povas fari tion.
Pipro|3|False|Vi povas fari tion.
Pipro|4|True|Ne...
Pipro|5|False|Vi ne povas.
Vendisto|6|False|He ! Ho ! Foriru !
Pipro|7|False|!!
Vendisto|8|False|Ne tuŝu per viaj piedoj miajn aferojn, se vi ne povas pagi !
Pipro|9|True|Ho ne, vi ne...
Pipro|10|True|KAROĈJO !
Pipro|11|False|Vi povus helpi min anstataŭ pensi pri via ventro !
Pipro|12|False|Ho ?!
Pipro|13|True|Ho, jees...
Pipro|14|False|Ĉi tio povus funkcii.

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fenikso|1|True|Ha ! Vi revenis.
Fenikso|2|False|Tiel baldaŭ...
Fenikso|3|False|Metala armilo ?!
Fenikso|4|False|Ĉu vere ?!
Fenikso|5|False|Ĉu vi ne scias, ke mi povas fandi ĉian metalon kaj...
Sono|6|False|Plop
Sono|7|False|Plop
Sono|8|False|Plop
Fenikso|9|True|HO NE !
Fenikso|10|False|NE TIO !
Fenikso|11|False|TIO NE JUSTAS !
Pipro|12|True|Rapide, Karoĉjo !
Pipro|13|False|Prenu tiom da larmoj, kiom vi povas !
Sono|14|False|Hak !
Sono|15|False|Hak !
Sono|16|False|Hak !
Titolo|17|False|- FINO -

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Atribuintaro|1|False|3a de aŭgusto, 2022 Arto kaj scenaro : David Revoy. Beta-legantoj : Arlo James Barnes, Benjamin Loubet, Bobby Hiltz, Chloé, Craig Maloney, Estefania de Vasconcellos Guimaraes, GunChleoc, Nicolas Artance, Olivier Jolly, Rhombihexahedron, Valvin. Esperanta traduko : Jorge Maldonado Ventura. Esperanta provlegado : Aleksej Odejĉuk, Tirifto. Provlegado : Craig Maloney. Bazita sur la universo de Hereva Kreinto : David Revoy. Ĉefa daŭriganto : Craig Maloney. Redaktistoj : Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Korektistoj : Willem Sonke, Moini, Hali, CGand, Alex Gryson. Programaro : Krita 5.0.5, Inkscape 1.2 sur Fedora 36 KDE Spin. Permesilo : Creative Commons Attribution 4.0. www.peppercarrot.com
Pipro|2|False|Ĉu vi sciis ?
Pipro|3|False|Pepper&Carrot estas tute libera, malfermitkoda kaj subtenata danke al la mecenateco de siaj legantoj.
Pipro|4|False|Pro ĉi tiu ĉapitro dankon al la 1058 mecenatoj !
Pipro|5|False|Vi ankaŭ povas iĝi mecenato de Pepper&Carrot kaj havi vian nomon skribitan ĉi tie !
Pipro|6|False|Ni estas en Patreon, Tipeee, PayPal, Liberapay ... kaj pli!
Pipro|7|False|Vidu www.peppercarrot.com por pli da informo !
Pipro|8|False|Dankon !
