# Transcript of Pepper&Carrot Episode 37 [id]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Judul|1|False|Episode 37: Air Mata Phoenix

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Fiuh...
Pepper|2|False|Senangya sudah di sini!
Pepper|3|False|Oh, waktunya belanja.
Pepper|4|False|Ayo makan dulu sebelum kita naik ke gunung berapinya.
Pepper|5|False|Aku tahu kamu pasti mau juga!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Penulis|1|False|DICARI
Penulis|2|False|Torreya
Penulis|3|False|100 000Ko
Penulis|4|False|Shichimi
Penulis|5|False|250 000Ko
Penulis|6|False|Pepper
Penulis|7|False|1 000 000Ko

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Mereka benar-benar pasang poster ini di mana-mana.
Pepper|2|False|Bahkan sampai di pelosok.
Pepper|3|False|Ayo kita buruan pindah sebelum kita dikenali.
Pepper|4|False|Kita punya masalah yang lebih besar sekarang...
Pepper|5|False|Sinar itu...
Pepper|6|False|Kita pasti sudah dekat ke sarangnya.
Pepper|7|False|Bingo!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Phoenix|1|False|Untuk apa kamu menggangguku, manusia?
Pepper|2|False|Salam, Phoenix yang agung!
Pepper|3|False|Nama saya Pepper, dan saya penyihir Chaosah.
Pepper|4|False|Saya belakangan baru saja menerima Rea Naga dosis besar, dan sejak itu, ini muncul dan terus menyebar...
Pepper|5|False|Ini meniadakan kekuatan saya dan kemungkinan membunuh saya ke depannya.

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Phoenix|1|True|Hmm...
Phoenix|2|False|Begitu...
Phoenix|3|False|...dan sekarang kamu ke sini karena kamu mau air mata Phoenix sebagai obat, begitu?
Pepper|4|False|Ya, ini satu-satunya pilihan saya.
Phoenix|5|False|*hhh*
Phoenix|6|False|...kalo begitu, tunggu apa lagi?
Phoenix|7|False|Ayo buat aku menangis.
Phoenix|8|False|Aku kasih satu menit!
Pepper|9|False|Apa?!
Pepper|10|False|Buat Anda menangis?!
Pepper|11|False|Tapi aku tidak tahu kalau...
Pepper|12|False|Maksudku... Cuma satu menit?!
Pepper|13|True|Uh...
Pepper|14|True|OK!
Pepper|15|False|Coba ya.
Pepper|16|True|Hmm... Pikirkan tentang isu kelaparan
Pepper|17|True|Eh, tunggutunggu!
Pepper|18|True|Ada yang lebih bagus:
Pepper|19|False|Orang-orang terkasih yang sudah meninggal.
Pepper|20|True|Gak merasa apa-apa?
Pepper|21|True|Peliharaan yang terlantar?!
Pepper|22|False|Binatang terlantar itu menyedihkan...
Phoenix|23|False|...

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Jadi, bagaimana?
Pepper|2|False|Tidak membuat Anda menangis?
Phoenix|3|False|YANG BENAR SAJA?!
Phoenix|4|False|CUMA SEGITU SAJA?!
Phoenix|5|True|Setidaknya mereka mencoba berpuisi!
Phoenix|6|True|Menulis tragedi!
Phoenix|7|True|SENI!
Phoenix|8|False|DRAMA!
Phoenix|9|True|TAPI KAMU?!
Phoenix|10|False|DATANG TANPA PERSIAPAN!
Phoenix|11|True|SUDAHLAH; HUS, HUS!
Phoenix|12|False|PULANG DULU, BALIK LAGI KALAU SUDAH SIAP!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Grr! Ayo, Pepper! Pikirkan cerita sedih.
Pepper|2|True|Kamu pasti bisa.
Pepper|3|False|Kamu pasti bisa.
Pepper|4|True|Uhh...
Pepper|5|False|Aku gak bisa.
Vendor|6|False|Hei! Oh! Pergi!
Pepper|7|False|!!
Vendor|8|False|Jangan pegang-pegang kalau tidak bisa bayar!
Pepper|9|True|Oh tidak, kamu jangan...
Pepper|10|True|CARROT!
Pepper|11|False|Kamu bisa bantuin aku daripada mikirin tentang perut terus!
Pepper|12|False|Oh?!
Pepper|13|True|Oh, ya...
Pepper|14|False|Ini bisa dipakai.

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Phoenix|1|True|Ah! Sudah balik.
Phoenix|2|False|Cepat juga...
Phoenix|3|False|Senjata logam?!
Phoenix|4|False|Yang benar?!
Phoenix|5|False|Memangnya tidak tahu aku bisa melelehkan logam dan ...
Suara|6|False|Plok
Suara|7|False|Plok
Suara|8|False|Plok
Phoenix|9|True|OH TIDAK!
Phoenix|10|False|JANGAN ITU!
Phoenix|11|False|TIDAK ADIL!
Pepper|12|True|Cepetan Carrot!
Pepper|13|False|Tampung air matanya sebanyak-banyaknya!
Suara|14|False|Dok!
Suara|15|False|Dok!
Suara|16|False|Dok!
Judul|17|False|- FIN -

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Kredit|1|False|August 03, 2022 Pelukis & skenario: David Revoy. Penguji bacaan cerita: Arlo James Barnes, Benjamin Loubet, Bobby Hiltz, Chloé, Craig Maloney, Estefania de Vasconcellos Guimaraes, GunChleoc, Nicolas Artance, Olivier Jolly, Rhombihexahedron, Valvin. Versi Bahasa Indonesia Penerjemah: Aldrian Obaja Muis Berdasarkan alam semesta Hereva Pembuat: David Revoy. Penyunting utama: Craig Maloney. Penulis: Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Penyunting: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Peranti Lunak: Krita 5.0.5, Inkscape 1.2 dengan Fedora 36 KDE Spin. Izin (Lisensi): Creative Commons Attribution 4.0. www.peppercarrot.com
Pepper|2|False|Tahukah kamu?
Pepper|3|False|Pepper&Carrot sepenuhnya gratis (bebas), sumber terbuka dan disponsori oleh para pembacanya.
Pepper|4|False|Untuk episode ini, terima kasih kepada 1058 relawan!
Pepper|5|False|Kamu juga bisa menjadi sukarelawan Pepper&Carrot dan namamu akan tercantum di sini!
Pepper|6|False|Kami ada di Patreon, Tipeee, PayPal, Liberapay ... dan lainnya!
Pepper|7|False|Cek www.peppercarrot.com untuk informasi selanjutnya!
Pepper|8|False|Terima kasih!
