# Transcript of Pepper&Carrot Episode 29 [en]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Episode 29: Destroyer of Worlds

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Monster|2|True|At last!
Monster|3|False|An interdimensional rupture!
Monster|4|False|Probably caused by some major cosmic event!
Monster|5|True|Grow! Grow, small breach!
Monster|6|False|And unveil this new world to ENSLAVE and DOMINATE!
Monster|7|True|Oh my…
Monster|8|False|Now that's interesting.
Monster|9|False|This must be my lucky day…
Narrator|1|False|Meanwhile, in another dimension…

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Monster|1|False|A dimension inhabited by intelligent creatures!
Monster|2|False|Faster, small breach!
Monster|3|False|Grow! Muhaha hahaha!
Pepper|4|True|Phew!
Pepper|5|False|What a great party, my friends!
Coriander|6|False|Hey Pepper, Saffron and I just realised we have no clue how your Chaosah magic works.
Saffron|7|False|It's quite the mystery. Would you tell us more about it?

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Haha, it's a little difficult to explain.
Pepper|2|True|Well, you could say that it's based on the understanding of the underlying laws of chaotic systems…
Pepper|3|False|…from the smallest to the largest.
Coriander|4|False|Well, that certainly clears things up.
Pepper|7|False|Hold on, let me show you a simple example.
Pepper|8|False|Just give me a second with some good old Chaosah wisdom…
Pepper|9|False|Gotcha!
Sound|5|True|SCRATCH
Sound|6|False|SCRATCH
Pepper|10|True|See this skewer that was stuck between those cobblestones?
Pepper|11|False|By picking this up I've surely prevented someone from stepping on it.
Pepper|12|True|A small, positive change in the grand chaotic system of existence can have tremendous consequences.
Pepper|13|False|That's what Chaosah is all about!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|False|Oh yuck!
Coriander|2|False|Ewwww! That thing was in someone's mouth!
Saffron|3|False|Ha ha! Impressive as always, Pepper!
Coriander|4|False|Alright Pepper, thanks for this… “explanation”.
Coriander|5|True|It's time to go to sleep now, isn't it?
Coriander|6|False|And for someone to wash their hands.
Pepper|7|False|Hey! Wait up!
Sound|8|False|Flick!
Sound|9|True|Tuck!
Sound|10|False|Tuck!
Sound|11|False|POW!
Carrot|12|False|?

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|False|Ssss!
Sound|2|False|Poof!
Sound|3|False|Tock!
Sound|4|False|CRASH!
Sound|5|False|Shrroof…

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Monster|7|False|ATTACK!!!
Monster|6|True|MuhahaHAHA! Finally!
Monster|9|False|!?
Writing|1|True|FIREWORKS
Writing|2|False|STORAGE
Sound|3|False|ShrrOof!!
Sound|4|False|Shrr!!
Sound|5|False|BOOM!
Sound|8|False|Fizzz!!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|3|False|BoOM!
Sound|2|False|BOOM!
Sound|1|False|CRACK!
Sound|4|False|Fizzz!!
Sound|5|False|PoOF!
Pepper|6|True|Don't worry, Carrot.
Pepper|7|False|That's probably just some people still partying.
Pepper|8|False|Alright, sleep tight!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Monster|5|False|...
Cumin|1|False|Really? She managed to set off such a large-scale chain reaction, without even realizing it?
Cayenne|2|False|No doubt about it.
Thyme|3|False|Ladies, I think this Pepper of ours is finally ready!
Sound|6|False|Fwip!
Sound|4|False|Fshhhh!!
Narrator|7|False|- THE CORONATION OF CORIANDER TRILOGY, THE END -

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|5|True|You too can become a patron of Pepper&Carrot and get your name here!
Pepper|3|True|Pepper&Carrot is entirely free(libre), open-source and sponsored thanks to the patronage of its readers.
Pepper|4|False|For this episode, thanks go to 960 patrons!
Pepper|7|True|Check www.peppercarrot.com for more info!
Pepper|6|True|We are on Patreon, Tipeee, PayPal, Liberapay ...and more!
Pepper|8|False|Thank you!
Pepper|2|True|Did you know?
Credits|1|False|April 25, 2019 Art & scenario: David Revoy. Beta readers: CalimeroTeknik, Craig Maloney, Martin Disch, Midgard, Nicolas Artance, Valvin. English version Translation: CalimeroTeknik, Craig Maloney, Martin Disch, Midgard. Based on the universe of Hereva Creator: David Revoy. Lead maintainer: Craig Maloney. Writers: Craig Maloney, Nartance, Scribblemaniac, Valvin. Correctors: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Software: Krita 4.1.5~appimage, Inkscape 0.92.3 on Kubuntu 18.04.1. License: Creative Commons Attribution 4.0. www.peppercarrot.com
