# Transcript of Pepper&Carrot Episode 38 [it]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titolo|1|False|Episodio 38: La Guaritrice

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Vinya|1|False|Finalmente...
Vinya|2|False|Siamo riusciti a porre fine alle sue sofferenze!
Fritz|3|False|Sì, è stato difficile, il che spiega perché così tanti sono rimasti lì...
Fritz|4|False|Come sta Brasic?
Fritz|5|False|Brasic?...Brasic!
Fritz|6|False|Dannazione, non mi ero accorto che eri così malconcio!
Vinya|7|False|Guaritrice!
Vinya|8|False|Abbiamo bisogno di aiuto, PRESTO!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Fatevi da parte...
Suono|2|False|Dzing
Brasic|3|False|Le mie ferite sono sparite...
Brasic|4|False|...ogni volta è lo stesso miracolo.
Brasic|5|False|Il tuo talento è incredibile, Guaritrice!
Brasic|6|False|Non perdiamo tempo, il bottino ci attende!
Fritz|7|False|Ben detto!
Fritz|8|False|Grazie a lei, siamo invincibili!
Fritz|9|False|In marcia, verso il prossimo mostro!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Brasic|1|False|Prima io!
Vinya|2|False|No, io! Io!
Pepper|3|False|?
Cayenne|4|False|Mi spieghi a che gioco stai giocando?
Cayenne|5|False|Sono mesi che ti stiamo cercando.

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Ehi, non si vede?
Pepper|2|False|Ho semplicemente deciso di mollare tutto e di cambiare lavoro.
Pepper|3|False|D'ora in poi sono la Grande Guaritrice Peph'Ra, come puoi vedere.
Cayenne|4|False|...
Pepper|5|False|Ma perché, ti chiederai?!
Pepper|6|False|Beh, è semplice!
Pepper|7|False|SONO STUFA!
Pepper|8|False|Stufa di essere ricercata, stufa che la gente prenda in giro i miei incantesimi e soprattutto stufa di non trovare lavoro dopo aver studiato per tutto questo tempo!
Pepper|9|False|Qua, almeno, hanno bisogno di me e mi apprezzano per le mie qualità!
Brasic|10|False|Guaritrice!
Brasic|11|False|Non allontanarti troppo!
Brasic|12|False|Vinya si è presa un brutto colpo.
Suono|13|False|Dzing

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Ecco qua.
Pepper|2|False|Qualcosa da aggiungere?!
Cayenne|3|False|Sì.
Cayenne|4|False|Avere il proprio nome su un mandato di cattura è normale per una strega di Chaosah.
Cayenne|5|False|È persino segno di una buona reputazione.
Cayenne|6|False|Ignora le maldicenze e creati un mestiere se nessuno te ne da uno.
Cayenne|7|False|E soprattutto: tu non sei una guaritrice.
Cayenne|8|False|Prima o poi se ne accorgeranno quando finirai quell'incredibile quantità di Lacrime di Fenice che ti porti dietro e che usi per fingere di avere nuovi poteri.
Pepper|9|False|Cos...?!
Pepper|10|False|Come hai...
Pepper|11|False|...indovinato.
Cayenne|12|False|Tze!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Grrr!!!
Pepper|2|False|Va beh, andrò a cercarne altro quando l'avrò finito!
Pepper|3|False|Perché, QUESTO FUNZIONA!
Pepper|4|False|Non come tutti i vostri complicati trucchi di Chaosah!
Pepper|5|False|GRRR...!
Brasic|6|False|Attenzione!
Brasic|7|False|Il mostro sta per metamorfizzarsi!
Mostro|8|False|CRiiiiiiiiiiiii
Mostro|9|False|iiiiiiiiiiiiiiii

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mostro|1|False|CRiiiiiiiiiiiiiiiiiii
Mostro|2|False|iiiiiiiiiiiiiiiiiiiiiiiii
Mostro|3|False|iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii
Suono|4|False|CRRRR ! ! !|nowhitespace
Suono|5|False|CRRRR ! ! !|nowhitespace
Suono|6|False|CRRRR ! ! !|nowhitespace
Suono|7|False|Pok ! ! !|nowhitespace
Suono|8|False|Pok ! ! !|nowhitespace
Suono|9|False|Pok ! ! !|nowhitespace
Suono|10|False|Pok ! ! !|nowhitespace
Suono|11|False|Pok ! ! !|nowhitespace
Suono|12|False|Pok ! ! !|nowhitespace
Mostro|13|False|iiiiiiii...
Suono|14|False|Dziiiooo !!|nowhitespace
Vinya|15|False|Dalla padella alla brace!
Brasic|16|False|Andiamo, gli farò ingoiare il fischietto!
Brasic|17|False|Mi butto!
Brasic|18|False|Guaritrice, tienimi d'occhio nel caso io...
Brasic|19|False|Guaritrice?

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|OPS!
Pepper|2|False|No, no, no!
Brasic|3|False|Buah ah ah!
Brasic|4|False|Guarda qua! Credo che la nostra guaritrice si sia un po' troppo impressionata al grido della bestia!
Brasic|5|False|Oh oh oh!
Fritz|6|False|AHAH AH!
Fritz|7|False|E in più, è tutta blu!
Vinya|8|False|AHAH AH AH!
Vinya|9|False|E pensate che anche quella possa guarire?
Fritz|10|False|Arf, che schifo!
Fritz|11|False|AHAH AH AH!
Brasic|12|False|OHOH OH!
Pepper|13|False|...
Fritz|14|False|EHI!
Brasic|15|False|Aspetta! Stavamo solo scherzando!
Vinya|16|False|Guaritrice!
Cayenne|17|False|Ben tornata tra noi.
Titolo|18|False|- FINE -

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crediti|1|False|26 Aprile 2023 Disegni & sceneggiatura: David Revoy. Lettori della versione beta: Arlo James Barnes, Bobby Hiltz, Craig Maloney, Christian Aribaud, Estefania de Vasconcellos Guimaraes, Frédéric Mora, Erik Mondrian, Nicolas Artance, Rhombihexahedron, Valvin, Vinay. Traduzione in italiano: Matteo Bertini. Correzioni: Elisa Moretti. Basato sull'universo di Hereva Creato da: David Revoy. Amministratore: Craig Maloney. Autori: Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Correzioni: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Software: Krita 5.1.5, Inkscape 1.2 sur Fedora KDE 37 Licenza: Creative Commons Attribution 4.0. www.peppercarrot.com
Pepper|2|False|Lo sapevi?
Pepper|3|False|Pepper&Carrot è completamente libero (gratuito), open-source e sostenuto grazie alle donazioni dei lettori.
Pepper|4|False|Per questo episodio, un grazie va ai 1084 donatori!
Pepper|5|False|Anche tu puoi diventare un sostenitore di Pepper&Carrot ed avere il tuo nome in questo elenco!
Pepper|6|False|Siamo su Patreon, Tipeee, PayPal, Liberapay ...ed altri!
Pepper|7|False|Leggi su www.peppercarrot.com per maggiori informazioni!
Pepper|8|False|Grazie mille!
