# Transcript of Pepper&Carrot Episode 38 [de]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 38: Die Heilerin

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Vinya|1|False|Endlich...
Vinya|2|False|Den haben wir geschafft.
Fritz|3|False|Ja, leicht war das nicht. Nur verständlich, dass so viele Abenteurer hier gescheitert sind.
Fritz|4|False|Wie geht es Brasic?
Fritz|5|False|Brasic? Brasic!
Fritz|6|False|Verdammt! Ich hab gar nicht bemerkt, dass du so schwer verletzt bist!
Vinya|7|False|Heilerin!
Vinya|8|False|Wir brauchen deine Hilfe, SCHNELL!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Zur Seite...
Geräusch|2|False|Bzing
Brasic|3|False|Alle meine Wunden sind verheilt.
Brasic|4|False|Es ist jedes Mal dasselbe Wunder.
Brasic|5|False|Du hast ein unglaubliches Talent, Heilerin!
Brasic|6|False|Nicht trödeln. Der Schatz wartet auf uns!
Fritz|7|False|Hört Hört!
Fritz|8|False|Dank ihr sind wir unbesiegbar!
Fritz|9|False|Lasst es uns mit dem nächsten Monster aufnehmen!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Brasic|1|False|Meins!
Vinya|2|False|Nein, lasst mich machen!
Pepper|3|False|?
Cayenne|4|False|Was glaubst du was du hier tust?
Cayenne|5|False|Wir suchen dich schon seit Monaten.

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Ist das nicht offensichtlich?
Pepper|2|False|Ich habe mich entschlossen, alles aufzugeben und ganz neu anzufangen.
Pepper|3|False|Von nun an bin ich die große Heilerin Peph'Ra, wie du sehen kannst.
Cayenne|4|False|...
Pepper|5|False|Du fragst dich vielleicht "Aber Warum"?!
Pepper|6|False|Ganz Einfach!
Pepper|7|False|Ich hab's SATT!
Pepper|8|False|Ich habe es satt, dauernd gejagt zu werden, ich habe es satt, dass alle über mich lachen, und vor allem habe ich es satt, nach einem so intensiven Studium keine Arbeit zu finden!
Pepper|9|False|Zumindest werde ich hier gebraucht und für meine Fähigkeiten geschätzt!
Brasic|10|False|Heilerin!
Brasic|11|False|Geh nicht so weit weg!
Brasic|12|False|Vinya hat es böse erwischt.
Geräusch|13|False|Bzing

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Darum!
Pepper|2|False|Sonst noch was?
Cayenne|3|False|Ja.
Cayenne|4|False|Für eine Hexe von Chaosāh ist es völlig normal, auf einem Steckbrief genannt zu werden.
Cayenne|5|False|Das macht sogar unseren guten Ruf aus!
Cayenne|6|False|Ignoriere die Nein-Sager, was auch immer sie meinen. Wenn dir keiner eine Karriere bietet, dann gehe deinen eigenen Weg!
Cayenne|7|False|Aber vor allem: Du bist keine Heilerin.
Cayenne|8|False|Spätestens, wenn dein unglaublicher Vorrat an Phönixtränen aufgebraucht ist, den du mit dir rumträgst, werden sie merken, dass du nur vorgaukelst, Heilkräfte zu haben.
Pepper|9|False|Wie... ?
Pepper|10|False|Hast du das...
Pepper|11|False|...erraten.
Cayenne|12|False|Tsss!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Grrr!!!
Pepper|2|False|Na und? Wenn ich mehr brauche werde ich sie mir holen!
Pepper|3|False|Das funktioniert wenigstens!
Pepper|4|False|Im Gegensatz zu all dem komischen Chaosāh Zeugs.
Pepper|5|False|GRRR...!
Brasic|6|False|Pass auf!
Brasic|7|False|Das Monster verändert seine Gestalt!
Monster|8|False|iiiiiiiiiiiiiiii
Monster|9|False|KRiiiiiiiiiiiii

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Monster|1|False|KRiiiiiiiiiiiiiiiiiii
Monster|2|False|iiiiiiiiiiiiiiiiiiiiiiiii
Monster|3|False|iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii
Geräusch|4|False|Krack ! ! !|nowhitespace
Geräusch|5|False|Krack ! ! !|nowhitespace
Geräusch|6|False|Krack ! ! !|nowhitespace
Geräusch|7|False|Klirr ! ! !|nowhitespace
Geräusch|8|False|Klirr ! ! !|nowhitespace
Geräusch|9|False|Klirr ! ! !|nowhitespace
Geräusch|10|False|Klirr ! ! !|nowhitespace
Geräusch|11|False|Klirr ! ! !|nowhitespace
Geräusch|12|False|Klirr ! ! !|nowhitespace
Monster|13|False|iiiiiiii...
Geräusch|14|False|Dziiiooo !!|nowhitespace
Vinya|15|False|Auweia, was für ein Schrei!
Brasic|16|False|Gebt mir Deckung! Ich werde ihm für immer das Maul stopfen!
Brasic|17|False|Jetzt wird's ernst!
Brasic|18|False|Bleib in der Nähe Heilerin, für alle Fälle...
Brasic|19|False|Heilerin?

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|UPS!
Pepper|2|False|Oh nein, nein!
Brasic|3|False|Hey ho ho!
Brasic|4|False|Schaut nur! Der Schrei des Biestes war wohl ein bisschen zu viel für unsere Heilerin!
Brasic|5|False|Ha ha ha!
Fritz|6|False|HAHA HA HA!
Fritz|7|False|Nicht nur das, sie ist komplett blau angelaufen!
Vinya|8|False|HAHA HA HA!
Vinya|9|False|Glaubt ihr das ist auch heilsam?
Fritz|10|False|Wie eklig!
Fritz|11|False|HAHA HA HA!
Brasic|12|False|HUA HA HA!
Pepper|13|False|...
Fritz|14|False|HEH!
Brasic|15|False|Warte! Wir haben doch nur Spaß gemacht!
Vinya|16|False|Heilerin!
Cayenne|17|False|Willkommen zurück.
Titel|18|False|- ENDE -

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Impressum|1|False|26. April 2023 Illustration & Handlung: David Revoy. Beta Leser: Arlo James Barnes, Bobby Hiltz, Craig Maloney, Christian Aribaud, Estefania de Vasconcellos Guimaraes, Frédéric Mora, Erik Mondrian, Nicolas Artance, Rhombihexahedron, Valvin, Vinay. Deutsche Version Übersetzung: Ulrich Greve, Kate. Korrektur: Andrej Ficko. Basierend auf der Hereva-Welt Erstellung: David Revoy. Hauptbetreuer: Craig Maloney. Redakteure: Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Korrektur: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Software: Krita 5.1.5, Inkscape 1.2 auf Fedora KDE 37 Lizenz: Creative Commons Namensnennung 4.0. www.peppercarrot.com
Pepper|2|False|Wusstest du schon?
Pepper|3|False|Pepper&Carrot ist vollständig frei (libre), Open-Source und finanziert durch Spenden von Lesern.
Pepper|4|False|Für diese Episode danken wir 1084 Unterstützern!
Pepper|5|False|Du kannst auch Unterstützer von Pepper&Carrot werden und Deinen Namen hier lesen!
Pepper|6|False|Wir sind auf Patreon, Tipeee, PayPal, Liberapay ...und mehr!
Pepper|7|False|Geh auf www.peppercarrot.com für mehr Informationen!
Pepper|8|False|Dankeschön!
