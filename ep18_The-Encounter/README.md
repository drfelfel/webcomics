﻿# Episode 18: The Encounter

![cover of episode 18](https://www.peppercarrot.com/0_sources/ep18_The-Encounter/low-res/Pepper-and-Carrot_by-David-Revoy_E18.jpg)

## Comments from the author

This is a special episode built around a flashback scene of an important time of the past for Pepper and Carrot.
I really wanted to draw this episode, it was an opportunity to bring many background elements around Pepper; how she started at the Hippiah school, how she met Carrot, how she was found by the witches of Chaosah and a hint about her hidden enormous potential as a witch... So, we go back in time with this flashback, not very far before episode 1; that's why you probably are already familiar with the color of the Hippiah dress.

On a technical point of view, this episode has a big amount of panels, almost two time more than usual episode. All this additional panels were necessary to smooth the storytelling and get a more cinematic feeling (and not a comic-strip one). That's why I'm posting this episode not at the end of July as planed at first , but at the end of the first week of August. A proof I always put priority on quality :)

From [Author's blog of episode 18](https://www.davidrevoy.com/article579/episode-18-the-encounter/show#comments)
