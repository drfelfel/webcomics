# Transcript of Pepper&Carrot Episode 18 [en]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Episode 18: The Encounter

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|OK, you can look now!
Pepper|2|True|TA~DAA~~A!
Pepper|3|True|A Hippiah witch's robes!
Pepper|4|False|It's all they had left in the shop, so I took it while I wait for my new robes to arrive!
Pepper|7|False|Well? Doesn't it remind you of anything?...
Writing|6|False|HIPPIAH

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Writing|1|False|School of Hippiah Witchcraft
Writing|3|False|KITCHENS
Carrot|2|False|Groo
Basilic|4|False|Oregano, good.
Basilic|5|False|Cardamom, good.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Basilic|1|False|Cinnamon, very good.
Basilic|2|False|Camomile, good.
Basilic|3|False|Pepper?!
Pepper|4|False|But I...
Basilic|5|True|Another failure!!!
Basilic|6|False|I remind you that Hippiah is not a magic for making weedkillers!...
Oregano|7|False|HA HAHA HA!
Cardamom|8|False|HA HA!
Cinnamon|9|False|HA HA!
Camomile|10|False|HA HAHA HA!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Oregano|1|False|HAHA HA!
Cardamom|2|False|HuHu Hu!
Cinnamon|9|False|Pfff!
Camomile|10|False|HAHA HA HA!
Pepper|3|True|STOP
Pepper|4|True|!!!
Pepper|5|True|It's not my fault!!!
Pepper|6|True|Come on...
Pepper|7|True|STOP
Pepper|8|False|!!!
Cardamom|12|False|HAHA HA HA!
Oregano|11|False|HuHu Hu!
Camomile|14|False|HAHA HA HA!
Cinnamon|15|False|HAHA HA!!
Pepper|13|False|Grr... Stop... I said...
Pepper|16|True|I said...
Pepper|17|False|STOP!!!
Sound|18|False|BAAAM!!!
Sound|19|False|CRACK!!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayenne|1|True|Ahem...
Cayenne|2|False|If you prefer, we can take care of her...
Cayenne|3|False|It just so happens that we're looking for a student of Chaosah...
Thyme|4|False|... and that one seems the perfect candidate.
Basilic|5|True|Cayenne?!
Basilic|6|True|Thyme?! Cumin?!
Basilic|7|False|But I thought that you were all... ... it's impossible!
Writing|8|False|KITCHENS

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|False|swiiip!
Sound|2|False|Clomp!
Pepper|3|False|Hee hee hee!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Something tells me you and I are going to get along great!
Pepper|2|False|... and I already have a little idea of what to call you!
Pepper|3|False|Well? What does it remind you of?!
Carrot|4|False|Groo?
Writing|5|False|HIPPIAH
Narrator|6|False|- FIN -
Credits|7|False|08/2016 - www.peppercarrot.com - Art & Scenario: David Revoy - English Translation: Alex Gryson
Credits|8|False|Scenarios inspired by two scenarios proposed by Craig Maloney: "You found me, I Choose You" and "Visit from Hippiah".
Credits|9|False|Based on the Hereva universe created by David Revoy with contributions by Craig Maloney. Corrections by Willem Sonke, Moini, Hali, CGand and Alex Gryson.
Credits|10|False|Licence: Creative Commons Attribution 4.0, Software: Krita 3.0, Inkscape 0.91 on Manjaro XFCE

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Pepper&Carrot is entirely free(libre), open-source and sponsored thanks to the patronage of its readers. For this episode, thanks go to 720 Patrons:
Credits|2|False|You too can become a patron of Pepper&Carrot at www.patreon.com/davidrevoy
