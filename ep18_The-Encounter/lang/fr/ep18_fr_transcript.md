# Transcript of Pepper&Carrot Episode 18 [fr]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titre|1|False|Épisode 18 : La Rencontre

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|C'est bon, tu peux regarder !
Pepper|2|True|TA~DAA~~A !
Pepper|3|True|Une tenue de sorcière d'Hippiah !
Pepper|4|False|Il n'y avait plus que ça en magasin, alors je l'ai prise le temps que mes nouveaux vêtements arrivent !
Pepper|7|False|Alors, ça ne te rappelle rien ?...
Écriture|6|False|HIPPIAH

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Écriture|1|False|École de Sorcières Hippiah
Écriture|3|False|CUISINES
Carrot|2|False|Groo
Basilic|4|False|Origan, bien.
Basilic|5|False|Cardamome, bien.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Basilic|1|False|Cannelle, très bien.
Basilic|2|False|Camomille, bien.
Basilic|3|False|Pepper ?!
Pepper|4|False|Mais je...
Basilic|5|True|Encore raté !!!
Basilic|6|False|Je vous rappelle qu'Hippiah n'est pas une magie pour faire des désherbants !...
Origan|7|False|HA HAHA HA !
Cardamome|8|False|HA HA !
Cannelle|9|False|HA HA !
Camomille|10|False|HA HAHA HA !

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Origan|1|False|HAHA HA !
Cardamome|2|False|HuHu Hu !
Cannelle|9|False|Pfff !
Camomille|10|False|HAHA HA HA !
Pepper|3|True|ARRÊTEZ
Pepper|4|True|!!!
Pepper|5|True|C'est pas ma faute !!!
Pepper|6|True|Mais...
Pepper|7|True|ARRÊTEZ
Pepper|8|False|!!!
Cardamome|12|False|HAHA HA HA !
Origan|11|False|HuHu Hu !
Camomille|14|False|HAHA HA HA !
Cannelle|15|False|HAHA HA !!
Pepper|13|False|Gnn... Arrêtez.. Arrê...
Pepper|16|True|J'ai dit ...
Pepper|17|False|STOP !!!
Son|18|False|BAAAM !!!
Son|19|False|CRACK !!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayenne|1|True|Heum Heum...
Cayenne|2|False|Si vous voulez, on peut s'occuper d'elle...
Cayenne|3|False|On recherche justement une élève à Chaosah...
Thym|4|False|... et celle-ci semble être la candidate idéale.
Basilic|5|True|Cayenne ?!
Basilic|6|True|Thym ?! Cumin ?!
Basilic|7|False|Mais je croyais que vous étiez toutes... ... c'est impossible !
Écriture|8|False|CUISINES

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Son|1|False|swiiip !
Son|2|False|Clomp !
Pepper|3|False|Hi hi hi !

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Quelque chose me dit qu'on va s'entendre toi et moi !
Pepper|2|False|... et j'ai déjà ma petite idée pour ton nom !
Pepper|3|False|Alors, alors ! Qu'est-ce que ça te rappelle ?!
Carrot|4|False|Groo ?
Écriture|5|False|HIPPIAH
Narrateur|6|False|- FIN -
Crédits|7|False|08/2016 - www.peppercarrot.com - Dessin & Scénario : David Revoy
Crédits|8|False|Scénarios inspirés de deux scénarios proposés par Craig Maloney : "You found me, I Choose You" et "Visit from Hippiah".
Crédits|9|False|Basé sur l'univers d'Hereva créé par David Revoy avec les contributions de Craig Maloney. Corrections de Willem Sonke, Moini, Hali, CGand et Alex Gryson.
Crédits|10|False|Licence : Creative Commons Attribution 4.0, Logiciels : Krita 3.0, Inkscape 0.91 sur Manjaro XFCE

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crédits|1|False|Pepper&Carrot est entièrement libre, open-source, et sponsorisé grâce au mécénat de ses lecteurs. Pour cet épisode, merci aux 720 Mécènes :
Crédits|2|False|Vous aussi, devenez mécène de Pepper&Carrot sur www.patreon.com/davidrevoy
