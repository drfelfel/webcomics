# Transcript of Pepper&Carrot Episode 18 [sl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Naslov|1|False|Epizoda 18: Prvo srečanje

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|False|V redu, lahko pogledaš!
Paprika|2|True|TA~DAAAAA!
Paprika|3|True|Hipijaška čarovniška obleka!
Paprika|4|False|Samo to so še imeli v trgovini, pa sem jo vzela, medtem ko čakam na novo pošiljko halj!
Paprika|7|False|No? Ti to ne obudi nobenih spominov?
Napis|6|False|HIPIJAH

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Napis|1|False|Hipijaška čarošola
Napis|3|False|KUHINJA
Korenček|2|False|Krul
Bazilika|4|False|Origano, dobro.
Bazilika|5|False|Kardamom, dobro.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bazilika|1|False|Cimetka, zelo dobro.
Bazilika|2|False|Kamilica, dobro.
Bazilika|3|False|Paprika?!
Paprika|4|False|Ampak…
Bazilika|5|True|Spet zamočila!!!
Bazilika|6|False|Naj vas opomnim, da vas v Hipijahu ne vzgajamo za morilke plevela!
Origano|7|False|HA HAHA HA!
Kardamom|8|False|HA HA!
Cimetka|9|False|HA HA!
Kamilica|10|False|HA HAHA HA!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Origano|1|False|HAHA HA!
Kardamom|2|False|HuHu Hu!
Cimetka|9|False|Pfff!
Kamilica|10|False|HAHA HA HA!
Paprika|3|True|NEHAJTE
Paprika|4|True|!!!
Paprika|5|True|Nisem jaz kriva!!!
Paprika|6|True|Pa če…
Paprika|7|True|NEHAJTE
Paprika|8|False|!!!
Kardamom|12|False|HAHA HA HA!
Origano|11|False|HuHu Hu!
Kamilica|14|False|HAHA HA HA!
Cimetka|15|False|HAHA HA!!
Paprika|13|False|Grr! Dovolj! Rekla sem …
Paprika|16|True|Rekla sem, da …
Paprika|17|False|NEHAJTE!!!
Zvok|18|False|BAAAM!!!
Zvok|19|False|ŠKRC!!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Kajenka|1|True|Khm…
Kajenka|2|False|Če vam je ljubše, lahko me prevzamemo skrb zanjo.
Kajenka|3|False|Po naključju iščemo novo čarovniško vajenko za Kaosah ...
Timijana|4|False|In tale je videti kot nalašč.
Bazilika|5|True|Kajenka?!
Bazilika|6|True|Timijana?! Kumina?!
Bazilika|7|False|Ampak mislila sem, da ste … To je nemogoče!
Napis|8|False|KUHINJA

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zvok|1|False|ŠVIS
Zvok|2|False|PAF
Paprika|3|False|Hi hi hi!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|True|Nekaj mi pravi, da se bova midva še dobro ujela!
Paprika|2|False|Že vem, kako te bom klicala!
Paprika|3|False|No? Česa si se spomnil?!
Korenček|4|False|Kruul?
Napis|5|False|HIPIJAH
Pripovedovalec|6|False|- KONEC -
Zasluge|7|False|Avgust 2016 - www.peppercarrot.com - Pisal in risal David Revoy - Prevedla Andrej Ficko in Gorazd Gorup
Zasluge|8|False|Po pisni predlogi Craiga Maloneyja: "You found me, I Choose You" in "Visit from Hippiah".
Zasluge|9|False|Dogaja se v vesolju Hereve avtorja Davida Revoyja s prispevki Craiga Maloneyja. Popravki: Willem Sonke, Moini, Hali, CGand in Alex Gryson.
Zasluge|10|False|Licenca: Creative Commons Priznanje avtorstva 4.0 - Programska oprema: Krita 3.0, Inkscape 0.91 na Manjaru XFCE

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zasluge|1|False|Strip Paprika in Korenček (Pepper&Carrot) je povsem prost in odprtokoden. Njegov nastanek je podprlo 720 bralcev:
Zasluge|2|False|Tudi ti lahko denarno podpreš izdelavo stripa na www.patreon.com/davidrevoy
