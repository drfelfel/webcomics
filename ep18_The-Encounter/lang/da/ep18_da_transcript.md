# Transcript of Pepper&Carrot Episode 18 [da]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 18: Mødet

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Nu må du kigge!
Pepper|2|True|TA~DAA~~A !|nowhitespace
Pepper|3|True|En Hippiahhekse-heksedragt!
Pepper|4|False|Der var ikke andet tilbage i butikken, så jeg tog den, mens jeg venter på mit nye tøj!
Pepper|7|False|Nå, får det dig ikke til at tænke på noget?...
Skrift|6|False|HIPPIAH

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Skrift|1|False|Hippiah hekse-skole
Skrift|3|False|KØKKEN
Carrot|2|False|Rumle
Basilikum|4|False|Oregano, godt.
Basilikum|5|False|Kardemomme, godt.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Basilikum|1|False|Kanel, rigtig godt.
Basilikum|2|False|Kamille, godt.
Basilikum|3|False|Pepper?!
Pepper|4|False|Men jeg...
Basilikum|5|True|Dumpet igen!!!
Basilikum|6|False|Jeg minder dig om, at Hippiah-magi ikke bruges til at lave ukrudtsmiddel!…
Oregano|7|False|HA HAHA HA!
Kardemomme|8|False|HA HA!
Kanel|9|False|HA HA!
Kamille|10|False|HA HAHA HA!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Oregano|1|False|HAHA HA!
Kardemomme|2|False|HiHi Hi!
Kanel|9|False|Fnis!
Kamille|10|False|HAHA HA HA!
Pepper|3|True|LAD VÆRE
Pepper|4|True|!!!
Pepper|5|True|Det er ikke min skyld!!!
Pepper|6|True|Men...
Pepper|7|True|LAD VÆRE
Pepper|8|False|!!!
Kardemomme|12|False|HAHA HA HA!
Oregano|11|False|HuHu Hu!
Kamille|14|False|HAHA HA HA!
Kanel|15|False|HAHA HA!!
Pepper|13|False|Gnn... Lad være.. lad...
Pepper|16|True|Jeg sagde...
Pepper|17|False|STOP!!!
Lyd|18|False|BOOM!!!
Lyd|19|False|KRAAK!!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayenne|1|True|Hm Hm...
Cayenne|2|False|Hvis I vil, kan vi tage os af hende...
Cayenne|3|False|Vi leder faktisk efter en elev til Kaosah…
Timian|4|False|… og hende her ser ud til at være den perfekte kandidat.
Basilikum|5|True|Cayenne?!
Basilikum|6|True|Timian?! Spidskommen?!
Basilikum|7|False|Men jeg troede I alle tre var... det er umuligt!
Skrift|8|False|KØKKEN

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|1|False|swiiip !|nowhitespace
Lyd|2|False|Bonk!
Pepper|3|False|Hi hi hi!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Noget siger mig, at vi vil blive gode venner dig og mig!
Pepper|2|False|...og jeg har allerede en idé til dit navn!
Pepper|3|False|Nå, minder det dig om noget?
Carrot|4|False|Rumle?
Skrift|5|False|HIPPIAH
Fortæller|6|False|- SLUT -
Credits|7|False|08/2016 - www.peppercarrot.com - Tegning og manuskript: David Revoy – Dansk oversættelse: Emmiline Alapetite
Credits|8|False|Manuskript inspireret af to manuskripter foreslået af Craig Maloney: "You found me, I Choose You" og "Visit from Hippiah".
Credits|9|False|Baseret på Hereva-universet skabt af David Revoy med bidrag af Craig Maloney. Rettelser af Willem Sonke, Moini, Hali, CGand og Alex Gryson.
Credits|10|False|Licens: Creative Commons Kreditering 4.0. Værktøj: Krita 3.0, Inkscape 0.91 på Manjaro XFCE

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Pepper&Carrot er helt gratis, open-source, og sponsoreret af læsere. Denne episode kunne ikke være blevet til uden støtte fra de 720 tilhængere:
Credits|2|False|Støt næste episode af Pepper&Carrot på www.patreon.com/davidrevoy
