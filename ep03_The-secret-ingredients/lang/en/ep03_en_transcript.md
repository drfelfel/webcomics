# Transcript of Pepper&Carrot Episode 03 [en]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Episode 3: The Secret Ingredients

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrator|1|False|Komona City; Market day
Pepper|2|False|Good morning Sir, I would like eight pumpkinstars, please.
Vendor|3|False|Here you are, that's 60Ko.*
Pepper|5|False|...Oh dear.
Note|4|False|* Ko = the Komona currency unit

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Uhh sorry, I'll have to take only four...
Vendor|2|False|Grrr...
Saffron|3|False|Greetings, good Sir. Please prepare two dozen of everything for me. Premium quality, as usual.
Vendor|4|False|It's always a pleasure to serve you, Miss Saffron.
Saffron|5|False|Hey look, it's Pepper.
Saffron|6|False|Oh, let me guess business is booming in the countryside?
Pepper|7|False|...
Saffron|8|False|I guess you're preparing the ingredients for tomorrow's potion challenge?
Pepper|9|True|...a potion challenge?
Pepper|10|False|...tomorrow?

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|4|False|Lucky me, I still have a full day to prepare!
Pepper|5|False|Let's win this challenge!
Writing|1|False|Komona Potion Challenge
Writing|2|False|50 000Ko grand prize FOR THE BEST POTION
Writing|3|False|On Azarday, 3 Pinkmoon Grand Plaza of Komona

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|2|False|Oh ...! I know ...
Pepper|3|False|...that's exactly what I need!
Pepper|4|True|Carrot!
Pepper|5|False|Get ready, we'll hunt the ingredients together.
Pepper|1|False|...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|First, I need a few pearls of mist from those black clouds...
Pepper|2|False|...and some red berries from the haunted jungle.

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|...plus egg shells from the Phoenix valley of the volcanos...
Pepper|2|False|...and finally a drop of milk from a young DragonCow.

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|That's it Carrot, I think I have everything I need.
Pepper|2|True|It looks...
Pepper|3|False|...Perfect
Pepper|4|True|Mmm...
Pepper|5|True|Best... Coffee... Ever!
Pepper|6|False|It's everything I'll need to work all night long and make the best potion for tomorrow's challenge.
Narrator|7|False|To be continued...

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|4|False|Алексей, 獨孤欣, Albert Westra, Alejandro Flores Prieto, Alex Lusco, Alex Silver, Alex Vandiver, Alexander Sopicki, Andreas Rieger, Andrew, Andrew Grady, Andrey Alekseenko, Anna Orlova, Antan Karmola, Antoine, Aslak Kjølås-Sæverud, Axel Bordelon, Axel Philipsenburg, Ben Evans, Boonsak Watanavisit, Boudewijn Rempt, carlos levischi, Charlotte Lacombe-bar, Chris Sakkas, Christophe Carré, Clara Dexter, Colby Driedger, Conway Scott Smith, Dmitry, Eitan Goldshtrom, Enrico Billich,-epsilon-, Garret Patterson, Gustav Strömbom, Guy Davis, Helmar Suschka, Ilyas Akhmedov, Inga Huang, Irene C., Jean-Baptiste Hebbrecht, JEM, Jessey Wright, John, Jónatan Nilsson, Joseph Bowman, Juanjo Fernández Monreal, Jurgo van den Elzen, Kai-Ting (Danil) Ko, Kasper Hansen, Kathryn Wuerstl, Ken Mingyuan Xia, Liselle, Lorentz Grip, MacCoy, Mandy, Martin Owens, Maurice-Marie Stromer, Mauricio Vega, mefflin ross bullis-bates, Michelle Pereira Garcia, Morten Hellesø Johansen, Nazhif, Nicki Aya, Nicolae Berbece, Nicole Heersema, No Reward, Noah Summers, Noble Hays, Olivier Amrein, Olivier Brun, Oscar Moreno, Pavel Semenov, Peter Moonen, Pierre Vuillemin, Ret Samys, Reuben Tracey, Rustin Simons, Sami T, Sean Adams, Shadefalcon,
Credits|2|False|This episode couldn't exist without the support of my 93 patrons
Credits|1|False|This webcomic is totally free and open-source ( Creative Commons Attribution 3.0 Unported, hi-res source files available to download )
Credits|3|False|https://www.patreon.com/davidrevoy
Credits|6|False|Special thanks to: David Tschumperlé (G'MIC) and all Krita team! Translation & corrections: Amireeti
Credits|7|False|This episode was made with 100% free(libre) and open-source tools Krita and G'MIC on Xubuntu (GNU/Linux)
Pepper|5|False|Thank you!
