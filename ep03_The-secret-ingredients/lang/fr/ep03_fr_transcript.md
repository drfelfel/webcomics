# Transcript of Pepper&Carrot Episode 03 [fr]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titre|1|False|Épisode 3 : L'ingrédient secret

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Note|4|False|* Ko = unité monétaire de Komona
Narrateur|1|False|Ville de Komona, jour de marché.
Pepper|2|False|Bonjour monsieur, je voudrais huit courgétoiles, s'il vous plaît !
Vendeur|3|False|Et voilà ! Ça fait 60Ko*.
Pepper|5|False|Zut ...

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Heuu, désolée, je vais en prendre que quatre finalement.
Vendeur|2|False|Grrr...
Safran|3|False|Bonjour cher marchand, s'il vous plaît deux dizaines de chaque. Qualité premium, comme d'habitude.
Vendeur|4|False|C'est toujours un plaisir de vous servir, mademoiselle Safran.
Safran|5|False|Hé, mais regardez-moi ça, c'est Pepper !
Safran|6|False|Dis donc, je vois que les affaires marchent bien à la campagne ?
Pepper|7|False|...
Safran|8|False|J'imagine que tu prépares déjà tes ingrédients pour le concours de potions de demain ?
Pepper|9|True|... un concours de potions ?
Pepper|10|False|... demain ?

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|4|False|Quelle chance ce concours ! Et j'ai encore un jour complet pour me préparer...
Pepper|5|False|Bien sûr que je vais participer !
Écriture|1|False|Concours de Potions de Komona
Écriture|2|False|grand prix de 50 000Ko POUR LA MEILLEURE POTION
Écriture|3|False|Azarday, 3 Pinkmoon Grand' place de Komona

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|2|False|Oh ... ! je sais ! ...
Pepper|3|False|... c'est exactement ce qu'il me faut !
Pepper|4|True|Carrot !
Pepper|5|False|Prépare-toi, on part à la chasse aux ingrédients !
Pepper|1|False|...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Premièrement, il me faudrait des perles de brumes de nuages noirs...
Pepper|2|False|... et quelques baies rouges de la forêt maléfique...

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|... puis quelques coquilles d'œuf de Phénix de la vallée des volcans...
Pepper|2|False|... et enfin quelques gouttes de lait d'une jeune Dragonvache.

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Ça y est, Carrot, je pense qu'on a tout.
Pepper|2|True|Ça m'a l'air...
Pepper|3|False|... parfait !
Pepper|4|True|Mmm ...
Pepper|5|True|Best... Café... Ever !
Pepper|6|False|C'est tout ce dont j'avais besoin avant de bosser toute la nuit sur la meilleure potion pour le concours de demain.
Narrateur|7|False|à suivre ...

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crédits|4|False|Алексей, 獨孤欣, Albert Westra, Alejandro Flores Prieto, Alex Lusco, Alex Silver, Alex Vandiver, Alexander Sopicki, Andreas Rieger, Andrew, Andrew Grady, Andrey Alekseenko, Anna Orlova, Antan Karmola, Antoine, Aslak Kjølås-Sæverud, Axel Bordelon, Axel Philipsenburg, Ben Evans, Boonsak Watanavisit, Boudewijn Rempt, carlos levischi, Charlotte Lacombe-bar, Chris Sakkas, Christophe Carré, Clara Dexter, Colby Driedger, Conway Scott Smith, Dmitry, Eitan Goldshtrom, Enrico Billich,-epsilon-, Garret Patterson, Gustav Strömbom, Guy Davis, Helmar Suschka, Ilyas Akhmedov, Inga Huang, Irene C., Jean-Baptiste Hebbrecht, JEM, Jessey Wright, John, Jónatan Nilsson, Joseph Bowman, Juanjo Fernández Monreal, Jurgo van den Elzen, Kai-Ting (Danil) Ko, Kasper Hansen, Kathryn Wuerstl, Ken Mingyuan Xia, Liselle, Lorentz Grip, MacCoy, Mandy, Martin Owens, Maurice-Marie Stromer, Mauricio Vega, mefflin ross bullis-bates, Michelle Pereira Garcia, Morten Hellesø Johansen, Nazhif, Nicki Aya, Nicolae Berbece, Nicole Heersema, No Reward, Noah Summers, Noble Hays, Olivier Amrein, Olivier Brun, Oscar Moreno, Pavel Semenov, Peter Moonen, Pierre Vuillemin, Ret Samys, Reuben Tracey, Rustin Simons, Sami T, Sean Adams, Shadefalcon,
Crédits|2|False|Cet épisode ne pourrait exister sans le soutien de 93 mécènes
Crédits|1|False|ce webcomic est entièrement libre et open-source (Creative Commons Attribution 3.0, fichiers haute résolution disponible au téléchargement)
Crédits|3|False|https://www.patreon.com/davidrevoy
Crédits|6|False|Remerciement spécial Amireeti, David Tschumperlé (G'MIC) et toute la 'team Krita' !
Crédits|7|False|Cet épisode a été réalisé à 100% avec des outils libres, Krita et G'MIC sur Xubuntu (GNU/Linux)
Pepper|5|False|Mille mercis !
