# Transcript of Pepper&Carrot Episode 03 [ld]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zha wudetha|1|False|Wud 3: Woradi Wothesh

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Dedidehá|1|False|* Lo = losh Sháadelol bethu
Loyud|2|False|Sháadelol; sháal wehethu
Behebá|3|False|Wil thal háasháal. Bóo eb ni ashoshimeth nib ledim, lu.
Loyud|5|False|Bíi meham ben nuha wa. Bóo den ne Lo* 60 beth.
Wohíya wodi|4|False|... yana

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Loyud|1|False|...
Behebá|2|False|Wil sha, shawith. Bóo buth ni daleth thabeshin i bim leda, nade nodim, lu.
Loshesh|3|False|Hóoda, dush bel le beth bim wa...
Behebá|4|False|Lhlhlh...
Loshesh|5|False|Bíi loláad le thenath hadihad úwáanú den le ni, Loshesh
Loshesh|6|False|Bóo láad, ham Loyud wa
Loyud|7|False|Bóo di ne ledim: Báada thalehul hal ábeduneha?
Loshesh|8|False|Bíi edeláad le úthú buth ne thesheth úwanú be woyaha-nesherana wora-shonem sháaleya aril. Báa dóon le?
Loyud|9|True|... woya-hanesherana worashon?
Loyud|10|False|... sháal aril?

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Loyud|4|False|Ledaá! Bíi ham wohume wosháal nede noshubewan wáa!
Loyud|5|False|Wil hesho lezh rashoneth hi!
Thod|1|False|Woyahaneshe-rana Worashon Sháadelol bethu
Thod|2|False|Bíi ham mebish Lo 50 000 WOHESHO WORANADIM
Thod|3|False|Hathamesháal sháaya 3, Rahíyahotheha Sháadelol bethu

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Loyud|2|False|Bóo dóham ne neyóoth, bé aril meduredeb nezh shidinal!
Loyud|3|False|Áa...! Lothel le ...
Loyud|4|True|... hi úmú them le wa!
Loyud|5|False|Ámed!
Loyud|1|False|...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Loyud|1|False|Nanede, them le nem shilithuth woloyo woboshumede...
Loyud|2|False|... i wolaya wodalathameth wodóhéeya woholinede

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Loyud|1|False|... i udoya máathuth údehú mehabelid óowababí wohóowabo woyedeha...
Loyud|2|False|... i dóol, laleth nedebe yáalalóowamidede

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Loyud|1|False|Loyud! Bíi lith le úthú nobuth le wothem wodal woho wa.
Loyud|2|True|Bíi...
Loyud|3|False|... shad be wo
Loyud|4|True|Mmm...
Loyud|5|True|Hesho... yob hi... hathehath wa!
Loyud|6|False|Bíi ril thad hal le náaleya obée yobewáan i thad el le wothaleháalish woranath wo. Ib thad hesho le sháaleya aril wo
Dedidehá|7|False|Bé aril dedideth nádideshub...

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Dohiná|4|False|Алексей, 獨孤欣, Albert Westra, Alejandro Flores Prieto, Alex Lusco, Alex Silver, Alex Vandiver, Alexander Sopicki, Andreas Rieger, Andrew, Andrew Grady, Andrey Alekseenko, Anna Orlova, Antan Karmola, Antoine, Aslak Kjølås-Sæverud, Axel Bordelon, Axel Philipsenburg, Ben Evans, Boonsak Watanavisit, Boudewijn Rempt, carlos levischi, Charlotte Lacombe-bar, Chris Sakkas, Christophe Carré, Clara Dexter, Colby Driedger, Conway Scott Smith, Dmitry, Eitan Goldshtrom, Enrico Billich,-epsilon-, Garret Patterson, Gustav Strömbom, Guy Davis, Helmar Suschka, Ilyas Akhmedov, Inga Huang, Irene C., Jean-Baptiste Hebbrecht, JEM, Jessey Wright, John, Jónatan Nilsson, Joseph Bowman, Juanjo Fernández Monreal, Jurgo van den Elzen, Kai-Ting (Danil) Ko, Kasper Hansen, Kathryn Wuerstl, Ken Mingyuan Xia, Liselle, Lorentz Grip, MacCoy, Mandy, Martin Owens, Maurice-Marie Stromer, Mauricio Vega, mefflin ross bullis-bates, Michelle Pereira Garcia, Morten Hellesø Johansen, Nazhif, Nicki Aya, Nicolae Berbece, Nicole Heersema, No Reward, Noah Summers, Noble Hays, Olivier Amrein, Olivier Brun, Oscar Moreno, Pavel Semenov, Peter Moonen, Pierre Vuillemin, Ret Samys, Reuben Tracey, Rustin Simons, Sami T, Sean Adams, Shadefalcon,
Dohiná|2|False|Meden wéedaná i meban ben wothem wolotheth. Áala withedim 93.
Dohiná|1|False|Bíi dínonehóo dademelom hi wa (Creative Commons Attribution 3.0 Unported). Thad bel ne dademeth i thodeth.
Dohiná|3|False|https://www.patreon.com/davidrevoy
Dohiná|6|False|Áalathéle: David Tschumperlé (G'MIC) i olowod Krita bethu woho! Héedan Láadanedim: Yuli i Álo
Dohiná|7|False|Bíi wudeth hi thodeshub wodódin wobodibodenan hi wa: Krita i G'MIC, Xubuntu (GNU/Linux) beha
Loyud|5|False|Áala!
