# Transcript of Pepper&Carrot Episode 02 [jb]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|.i 2 mo'o lisri le litki pe ja'e lo tanbargu
<hidden>|2|False|la .piper. joi la .karot.

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|5|True|.glup.
Sound|6|True|.glup.
Sound|7|False|.glup.
Writing|1|True|.O'I LE TE
Writing|3|False|TI PONSE
Writing|2|True|MAKFA
Writing|4|False|33

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|7|False|.tok.|nowhitespace
Writing|1|False|FAGD
Writing|2|False|CNOXAMSI
Writing|3|False|ZIRPURCI
Writing|4|False|DU'ERBLAN
Writing|5|False|PEN
Writing|6|False|UNRE
Writing|8|False|RARNA
Writing|9|False|PELXU
Writing|10|False|GA'U NARJU
Writing|11|False|FAGDANSU
Writing|12|False|CNOXAMSI
Writing|13|False|ZIRPURCI
Writing|14|False|DU'ERBLANU
Writing|15|False|CMETYPENKA
Writing|16|False|MAGENTA X
Writing|17|False|XUNRE

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|.glup.
Sound|2|True|.glup.
Sound|3|False|.glup.

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|.gulp.
Sound|2|False|.gulp.
Sound|3|True|.gulp.
Sound|4|False|.gulp.
Sound|5|True|.gulp.
Sound|6|False|.gulp.
Sound|20|False|y.|nowhitespace
Sound|19|True|y.|nowhitespace
Sound|18|True|.y.|nowhitespace
Sound|7|True|.s|nowhitespace
Sound|8|True|P|nowhitespace
Sound|9|True|l|nowhitespace
Sound|10|False|urp.|nowhitespace
Sound|11|True|.S|nowhitespace
Sound|12|True|y|nowhitespace
Sound|13|True|c|nowhitespace
Sound|14|True|P|nowhitespace
Sound|15|True|l|nowhitespace
Sound|16|True|a|nowhitespace
Sound|17|False|t.|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|.b
Sound|2|True|lup.|nowhitespace
Sound|3|True|.b
Sound|4|False|lup.|nowhitespace
Sound|7|True|up.|nowhitespace
Sound|6|True|pl|nowhitespace
Sound|5|True|.s
Sound|10|True|up.|nowhitespace
Sound|9|True|pl|nowhitespace
Sound|8|True|.s
Sound|13|False|up.|nowhitespace
Sound|12|True|pl|nowhitespace
Sound|11|True|.s

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|True|This webcomic is open-source and this episode was funded by my 21 patrons on
Credits|2|False|www.patreon.com/davidrevoy
Credits|3|False|Many thanks to
Credits|4|False|made with Krita on GNU/Linux
