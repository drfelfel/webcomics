# Transcript of Pepper&Carrot Episode 02 [pl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tytuł|1|False|Odcinek 2: Tęczowe eliksiry

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Dźwięk|5|True|plum
Dźwięk|6|True|plum
Dźwięk|7|False|plum
Napis|1|True|UWAGA!
Napis|3|False|WIEDŹMY
Napis|2|True|POSESJA
Napis|4|False|33

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Dźwięk|7|False|klank|nowhitespace
Napis|1|False|FIRE D
Napis|2|False|DEEP OCEAN
Napis|3|False|VIOLE(N)T
Napis|4|False|ULTRA BLUE
Napis|5|False|PIN
Napis|6|False|MSON
Napis|8|False|NATURE
Napis|9|False|YELLOW
Napis|10|False|ORANGE TOP
Napis|11|False|FIRE DANCE
Napis|12|False|DEEP OCEAN
Napis|13|False|VIOLE(N)T
Napis|14|False|ULTRA BLUE
Napis|15|False|META PINK
Napis|16|False|MAGENTA X
Napis|17|False|CRIMSON

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Dźwięk|1|True|plum
Dźwięk|2|True|plum
Dźwięk|3|False|plum

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Dźwięk|1|True|gul
Dźwięk|2|False|gul
Dźwięk|3|True|gul
Dźwięk|4|False|gul
Dźwięk|5|True|gul
Dźwięk|6|False|gul
Dźwięk|20|False|m|nowhitespace
Dźwięk|19|True|m|nowhitespace
Dźwięk|18|True|m|nowhitespace
Dźwięk|7|True|t|nowhitespace
Dźwięk|8|True|f|nowhitespace
Dźwięk|9|True|u|nowhitespace
Dźwięk|10|False|uuu !|nowhitespace
Dźwięk|11|True|S|nowhitespace
Dźwięk|12|True|S|nowhitespace
Dźwięk|13|True|S|nowhitespace
Dźwięk|14|True|P|nowhitespace
Dźwięk|15|True|l|nowhitespace
Dźwięk|16|True|a|nowhitespace
Dźwięk|17|False|t|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Dźwięk|1|True|b
Dźwięk|2|True|ul|nowhitespace
Dźwięk|3|True|b
Dźwięk|4|False|ul|nowhitespace
Dźwięk|7|True|ap|nowhitespace
Dźwięk|6|True|hl|nowhitespace
Dźwięk|5|True|c
Dźwięk|10|True|ap|nowhitespace
Dźwięk|9|True|hl|nowhitespace
Dźwięk|8|True|c
Dźwięk|13|False|ap|nowhitespace
Dźwięk|12|True|hl|nowhitespace
Dźwięk|11|True|c

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|True|Ten webcomic jest na licencji open-source.Odcinek został ufundowany przez 21 patronów na:
Credits|2|False|www.patreon.com/davidrevoy
Credits|3|False|Lista zasłużonych:
Credits|4|False|zrobione z Krita na GNU/Linux
