# Transcript of Pepper&Carrot Episode 02 [cs]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|2. díl: Duhové lektvary

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|5|True|Klok
Sound|6|True|Klok
Sound|7|False|Klok
Writing|1|True|VAROVÁNÍ
Writing|3|False|ČARODĚJKY
Writing|2|True|POZEMEK
Writing|4|False|33

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|7|False|ťuk|nowhitespace
Writing|1|False|FIRE D
Writing|2|False|DEEP OCEAN
Writing|3|False|VIOLE(N)T
Writing|4|False|ULTRA BLUE
Writing|5|False|PIN
Writing|6|False|MSON
Writing|8|False|NATURE
Writing|9|False|YELLOW
Writing|10|False|ORANGE TOP
Writing|11|False|FIRE DANCE
Writing|12|False|DEEP OCEAN
Writing|13|False|VIOLE(N)T
Writing|14|False|ULTRA BLUE
Writing|15|False|META PINK
Writing|16|False|MAGENTA X
Writing|17|False|CRIMSON

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|Klok
Sound|2|True|Klok
Sound|3|False|Klok

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|Glo
Sound|2|False|Glo
Sound|3|True|Glo
Sound|4|False|Glo
Sound|5|True|Glo
Sound|6|False|Glo
Sound|20|False|mf|nowhitespace
Sound|19|True|m|nowhitespace
Sound|18|True|M|nowhitespace
Sound|7|False|Prsk!|nowhitespace
Sound|8|False|Blemc|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|B
Sound|2|True|ubly|nowhitespace
Sound|3|True|B
Sound|4|False|ubly|nowhitespace
Sound|7|False|čvacht
Sound|6|True|čvacht
Sound|5|True|čvacht

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|True|Tento komiks je open-source a tuto epizodu finančně podpořilo 21 fanoušků na
Credits|2|False|www.patreon.com/davidrevoy
Credits|3|False|Vřelé díky vám všem:
Credits|4|False|použité nástroje: Krita, GNU/Linux
