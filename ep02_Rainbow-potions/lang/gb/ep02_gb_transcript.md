# Transcript of Pepper&Carrot Episode 02 [gb]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titulo|1|False|Mon 2: Kolorarkoli Iksir

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Soti|5|True|Glup
Soti|6|True|Glup
Soti|7|False|Glup
Eskrixey|1|True|JINGO
Eskrixey|3|False|JUMUNYEN
Eskrixey|2|True|MILKI DE
Eskrixey|4|False|33

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Soti|7|False|tok|nowhitespace
Eskrixey|1|False|ATEXL
Eskrixey|2|False|AMIKU BAHARI
Eskrixey|3|False|VYOLETA
Eskrixey|4|False|SUPRABLUE
Eskrixey|5|False|PIN
Eskrixey|6|False|ESSIM
Eskrixey|8|False|NATURA
Eskrixey|9|False|JALO
Eskrixey|10|False|ORANGE
Eskrixey|11|False|ATEXLI DANSE
Eskrixey|12|False|AMIKU BAHARI
Eskrixey|13|False|VYOLETA
Eskrixey|14|False|SUPRABLUE
Eskrixey|15|False|METAPINKU
Eskrixey|16|False|MAGENTA X
Eskrixey|17|False|KERMESSIM

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Soti|1|True|Glup
Soti|2|True|Glup
Soti|3|False|Glup

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Soti|1|True|Gluglu
Soti|2|False|Gluglu
Soti|3|True|Gluglu
Soti|4|False|Gluglu
Soti|5|True|Gluglu
Soti|6|False|Gluglu
Soti|21|False|m|nowhitespace
Soti|20|True|m|nowhitespace
Soti|19|True|M|nowhitespace
Soti|7|True|E|nowhitespace
Soti|8|True|s|nowhitespace
Soti|9|True|p|nowhitespace
Soti|10|False|lurp!|nowhitespace
Soti|11|True|E|nowhitespace
Soti|12|True|S|nowhitespace
Soti|13|True|S|nowhitespace
Soti|14|True|S|nowhitespace
Soti|15|True|P|nowhitespace
Soti|16|True|l|nowhitespace
Soti|17|True|a|nowhitespace
Soti|18|False|t|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Soti|1|True|B
Soti|2|True|lup|nowhitespace
Soti|3|True|B
Soti|4|False|lup|nowhitespace
Soti|8|True|up|nowhitespace
Soti|7|True|pl|nowhitespace
Soti|6|True|s|nowhitespace
Soti|5|True|e
Soti|12|True|up|nowhitespace
Soti|11|True|pl|nowhitespace
Soti|10|True|s|nowhitespace
Soti|9|True|e
Soti|16|False|up|nowhitespace
Soti|15|True|pl|nowhitespace
Soti|14|True|s|nowhitespace
Soti|13|True|e

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|True|Hin netokomiki sen fe huru mambali kodi, ji hin mon le bepesa misu 21 faydayen in
Credits|2|False|www.patreon.com/davidrevoy
Credits|3|False|Multi xukra tas
Credits|4|False|kreado yon Krita in GNU/Linux
