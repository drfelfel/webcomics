# Transcript of Pepper&Carrot Episode 02 [el]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Επεισόδιο 2: Τα φίλτρα του ουράνιου τόξου

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|5|True|Γκλούπ
Sound|6|True|Γκλούπ
Sound|7|False|Γκλούπ
Writing|1|True|ΠΡΟΣΟΧΗ!
Writing|2|True|ΜΑΓΙΣΣΑ
Writing|3|False|33

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|7|False|τικ|nowhitespace
Writing|1|False|FIRE D
Writing|2|False|DEEP OCEAN
Writing|3|False|VIOLE(N)T
Writing|4|False|ULTRA BLUE
Writing|5|False|PIN
Writing|6|False|MSON
Writing|8|False|NATURE
Writing|9|False|YELLOW
Writing|10|False|ORANGE TOP
Writing|11|False|FIRE DANCE
Writing|12|False|DEEP OCEAN
Writing|13|False|VIOLE(N)T
Writing|14|False|ULTRA BLUE
Writing|15|False|META PINK
Writing|16|False|MAGENTA X
Writing|17|False|CRIMSON

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|Γκλούπ
Sound|2|True|Γκλούπ
Sound|3|False|Γκλούπ

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|Γκλούκ
Sound|2|False|Γκλούκ
Sound|3|True|Γκλούκ
Sound|4|False|Γκλούκ
Sound|5|True|Γκλούκ
Sound|6|False|Γκλούκ
Sound|21|False|... !|nowhitespace
Sound|20|True|μ|nowhitespace
Sound|19|True|μ|nowhitespace
Sound|18|True|M|nowhitespace
Sound|8|True|Μ|nowhitespace
Sound|9|True|π|nowhitespace
Sound|10|True|λ|nowhitespace
Sound|11|False|ιάχ !|nowhitespace
Sound|12|True|Σ|nowhitespace
Sound|13|True|π|nowhitespace
Sound|14|True|λ|nowhitespace
Sound|15|True|ά|nowhitespace
Sound|16|True|τ|nowhitespace
Sound|17|False|ς|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|Μπ
Sound|2|True|λούπ|nowhitespace
Sound|3|True|Μπ
Sound|4|False|λούπ|nowhitespace
Sound|7|True|τς|nowhitespace
Sound|6|True|λά|nowhitespace
Sound|5|True|π
Sound|10|True|τς|nowhitespace
Sound|9|True|λά|nowhitespace
Sound|8|True|π
Sound|13|False|τς|nowhitespace
Sound|12|True|λά|nowhitespace
Sound|11|True|π

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|True|Αυτό το κόμικ είναι ανοικτού κώδικα και πραγματοποιήθηκε χάρη στους 21 χρηματοδότες μου!
Credits|2|False|www.patreon.com/davidrevoy
Credits|3|False|Πολλά ευχαριστώ στους:
Credits|4|False|φτιάχθηκε με Krita στο GNU/Linux
