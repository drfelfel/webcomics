# Transcript of Pepper&Carrot Episode 02 [es]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Título|1|False|Episodio 2: Las pociones arco iris

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sonido|5|True|Glu
Sonido|6|True|Glu
Sonido|7|False|Glu
Escritura|1|True|CUIDADO
Escritura|3|False|PELIGROSA
Escritura|2|True|BRUJA
Escritura|4|False|33

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sonido|7|False|toc|nowhitespace
Escritura|1|False|FLAME
Escritura|2|False|OCÉANO
Escritura|3|False|VIOLE(N)TO
Escritura|4|False|ULTRA AZUL
Escritura|5|False|ROS
Escritura|6|False|RMÍN
Escritura|8|False|NATURAL
Escritura|9|False|AMARILLO
Escritura|10|False|ANARANJADO
Escritura|11|False|FLAMENCO
Escritura|12|False|OCÉANO
Escritura|13|False|VIOLE(N)TO
Escritura|14|False|ULTRA AZUL
Escritura|15|False|META ROSA
Escritura|16|False|MAGENTA X
Escritura|17|False|CARMÍN

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sonido|1|True|Glu
Sonido|2|True|Glu
Sonido|3|False|Glu

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sonido|1|True|Gulp
Sonido|2|False|Gulp
Sonido|3|True|Gulp
Sonido|4|False|Gulp
Sonido|5|True|Gulp
Sonido|6|False|Gulp
Sonido|20|False|m|nowhitespace
Sonido|19|True|m|nowhitespace
Sonido|18|True|M|nowhitespace
Sonido|7|True|B|nowhitespace
Sonido|8|True|l|nowhitespace
Sonido|9|True|e|nowhitespace
Sonido|10|False|rgh !|nowhitespace
Sonido|11|True|S|nowhitespace
Sonido|12|True|S|nowhitespace
Sonido|13|True|S|nowhitespace
Sonido|14|True|P|nowhitespace
Sonido|15|True|l|nowhitespace
Sonido|16|True|o|nowhitespace
Sonido|17|False|p|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sonido|1|True|B
Sonido|2|True|lub|nowhitespace
Sonido|3|True|B
Sonido|4|False|lub|nowhitespace
Sonido|7|True|plof
Sonido|6|True|lof|nowhitespace
Sonido|5|True|p
Sonido|10|False|lof|nowhitespace
Sonido|9|True|p

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Créditos|1|True|Este cómic es libre y de código abierto. Este episodio ha tenido 21 mecenas en
Créditos|2|False|www.patreon.com/davidrevoy
Créditos|3|False|Gracias a :
Créditos|5|False|hecho con Krita en GNU/Linux
Créditos|4|False|Traducción al castellano : TheFaico
