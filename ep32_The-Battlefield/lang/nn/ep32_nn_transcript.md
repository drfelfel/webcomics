# Transcript of Pepper&Carrot Episode 32 [nn]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tittel|1|False|Episode 32: På slagmarka

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Konge|1|True|Adjutant!
Konge|2|False|Fekk du tak i ei heks, slik eg bad om?
Adjutant|3|True|Ja, herre!
Adjutant|4|False|Ho står der, rett ved sida av deg.
Konge|5|False|...?
Pepar|6|True|Hei!
Pepar|7|False|Eg heiter Pep...
Konge|8|False|?!!
Konge|9|True|TOSK!!!
Konge|10|True|Kvifor har du fått tak i eit barn?!
Konge|11|False|Eg treng ei skikkeleg slagmarksheks!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|True|Høyr no her!
Pepar|2|True|Eg er ei ekte kaosah-heks.
Pepar|3|False|Her har eg eit vitnemål som ...
Skrift|4|True|Kaosah-
Skrift|5|False|vitnemål
Skrift|7|False|Kajenne
Skrift|8|False|Karve
Skrift|9|False|Timian
Skrift|6|False|~ for Pepar ~
Konge|10|False|STILLE!
Konge|11|False|Eg har ikkje bruk for ungar i hæren min.
Konge|12|False|Stikk heim og leik med dokkene dine.
Lyd|13|False|Klaps!
Hær|14|True|HA-HA, HA-HA!
Hær|15|True|HA-HA, HA-HA!
Hær|16|True|HA-HA, HA-HA!
Hær|17|False|HA-HA, HA-HA!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|True|Det er ikkje til å tru!
Pepar|2|True|No har eg studert i kjempemange år, men ingen tek meg alvorleg, fordi ...
Pepar|3|False|... eg ikkje ser røynd nok ut!
Lyd|4|False|POFF!!
Pepar|5|False|GULROT!
Lyd|6|False|PAFF!!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|True|Verkeleg, Gulrot?
Pepar|2|True|Eg håpar maten var verd det.
Pepar|3|False|Du ser fæl ut ...
Pepar|4|False|... Du ser ...
Pepar|5|True|Korleis du ser ut!
Pepar|6|False|Sjølvsagt!
Lyd|7|False|Knekk!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|True|HEI!
Pepar|2|False|Eg høyrer de er på utkikk etter ei EKTE HEKS?!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Konge|1|True|Nei, forresten, be den jentungen koma tilbake.
Konge|2|False|Ho her er sikkert altfor dyr ...
Skrift|3|False|FRAMHALD FØLGJER ...

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bidragsytarar|1|False|31. mars 2020 Teikningar og forteljing: David Revoy. Tidleg tilbakemelding: Craig Maloney, Martin Disch, Arlo James Barnes, Nicolas Artance, Parnikkapore, Stephen Paul Weber, Valvin og Vejvej. Omsetjing til nynorsk: Arild Torvund Olsen og Karl Ove Hufthammer. Bygd på Hereva-universet Skapar: David Revoy. Hovudutviklar: Craig Maloney. Forfattarar: Craig Maloney, Nartance, Scribblemaniac og Valvin. Rettingar: Willem Sonke, Moini, Hali, CGand og Alex Gryson. Programvare: Krita 4.2.9-beta og Inkscape 0.92.3 på Kubuntu 19.10. Lisens: Creative Commons Attribution 4.0. www.peppercarrot.com
Pepar|3|True|Pepar & Gulrot er ein heilt fri teikneserie, med opne kjelder, og er sponsa av lesarane.
Pepar|4|False|Takk til dei 1 121 som støtta denne episoden!
Pepar|2|True|Visste du dette?
Pepar|5|True|Du òg kan støtta arbeidet med neste episode av Pepar & Gulrot og få namnet ditt her!
Pepar|7|True|Sjå www.peppercarrot.com for meir informasjon!
Pepar|6|True|Me er på blant anna Patreon, Tipeee, PayPal og Liberapay ...
Pepar|8|False|Tusen takk!
