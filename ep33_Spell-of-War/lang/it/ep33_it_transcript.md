# Transcript of Pepper&Carrot Episode 33 [it]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titolo|1|False|Episodio 33: Incantesimi di guerra

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Enemy|1|False|huUUOOOOOOOOOOO!
Army|2|False|Rawwwr!
Army|5|False|Grrawwr!
Army|4|False|Grrrr!
Army|3|False|Yuurrr!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
King|1|True|OK, giovane strega.
King|2|False|Se hai un incantesimo che ci può aiutare, adesso o mai più!
Pepper|3|True|Eccolo!
Pepper|4|False|Che siate testimoni...
Suono|5|False|Dzziii !!
Pepper|6|False|...DEL MIO CAPOLAVORO!
Suono|7|False|Dzziiii !!
Pepper|8|False|Realitas Hackeris Pepperus!
Suono|9|False|Dzziooo !!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Suono|1|False|Fiizz!
Suono|2|False|Dzii!
Suono|3|False|Schii!
Suono|4|False|Ffhii!
Suono|8|False|Dziing!
Suono|7|False|Fiizz!
Suono|6|False|Schii!
Suono|5|False|Ffhii!
King|9|True|Ebbene, questo incantesimo dà alle nostre spade più potere...
King|10|False|...e indebolisce le armi del nemico?
Suono|11|False|Dzii...
Pepper|12|True|He he.
Pepper|13|True|Lo vedrete!
Pepper|14|False|Tutto quello che posso dirvi è che oggi non perderete nessun soldato.
Pepper|15|False|Ma dovrete comunque combattere e dare il meglio di voi stessi!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
King|1|False|Ci siamo addestrati proprio per questo.
King|2|False|AVAAAANTIIIIIIIIIIII!!!!!
Army|3|False|Yahhhh!
Army|4|False|Yeaahh!
Army|5|False|Yaaah!
Army|6|False|Yaawww!
Army|7|False|Yeaaah!
Army|8|False|Yaaah!
Army|9|False|Yaeeh!
Army|10|False|Yaeeh!
Army|11|False|Yaahhh!
King|12|False|CAAAARICAAAAAAAAA!!!
Army|13|False|Yeaahh!
Army|14|False|Yahhh!
Army|15|False|Yaeeh!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
King|1|False|Yahhhh!!!
Enemy|2|False|YUuurr!!!
Suono|3|False|Schwiiing !!
Suono|4|False|Swoosh !!
Writing|5|False|12
Enemy|6|False|?!!
Writing|7|False|8
King|8|False|?!!
Writing|9|False|64
Writing|10|False|32
Writing|11|False|72
Writing|12|False|0
Writing|13|False|64
Writing|14|False|0
Writing|15|False|56
Army|20|False|Yrrr!
Army|17|False|Yurr!
Army|19|False|Grrr!
Army|21|False|Yaaah!
Army|18|False|Yeaaah!
Army|16|False|Yaawww!
Suono|27|False|Sshing
Suono|25|False|Fffchh
Suono|22|False|wiizz
Suono|23|False|Swoosh
Suono|24|False|Chklong
Suono|26|False|Chkilng

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
King|1|False|?!
Writing|2|False|3
Writing|3|False|24
Writing|4|False|38
Writing|5|False|6
Writing|6|False|12
Writing|7|False|0
Writing|8|False|5
Writing|9|False|0
Writing|10|False|37
Writing|11|False|21
Writing|12|False|62
Writing|13|False|27
Writing|14|False|4
King|15|False|!!
King|16|False|STREGAAAAAA!!!! COSA HAI COMBINATO?!!!!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Tadaaa !
Pepper|2|True|“Questo"
Pepper|3|False|...è il mio capolavoro!
Pepper|4|False|Ho creato un incantesimo che altera la realtà e mostra i punti vita rimanenti dell'avversario.
Writing|5|False|33
Pepper|6|True|Quando hai finito i punti vita, lasci il campo fino alla fine della battaglia.
Pepper|7|True|Il primo esercito che elimina tutti i nemici vince!
Pepper|8|False|Semplice.
Writing|9|False|0
Army|10|False|?
Pepper|11|True|Non è magnifico?
Pepper|12|True|Niente più morti!
Pepper|13|True|Niente più soldati feriti!
Pepper|14|False|La guerra non sarà mai più come l'abbiamo conosciuta!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Adesso che conoscete le regole, fatemi azzerare i punteggi così possiamo ricominciare da capo!
Pepper|2|False|Sarà più corretto.
Writing|3|False|64
Writing|4|False|45
Writing|5|False|6
Writing|6|False|2
Writing|7|False|0
Writing|8|False|0
Writing|9|False|0
Writing|10|False|0
Writing|11|False|9
Writing|12|False|5
Writing|13|False|0
Writing|14|False|0
Writing|15|False|0
Suono|16|False|Sshing!
Suono|17|False|Zooo!
Suono|19|False|tchac!
Suono|18|False|poc!
Pepper|20|True|Corri corri Carrot!
Pepper|21|False|L'incantesimo non durerà molto a lungo!
Narrator|22|False|- FINE -

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crediti|4|False|Giugno 29, 2020 Disegni e sceneggiatura: David Revoy. Co-autori del testo: Arlo James Barnes, Craig Maloney, GunChleoc, Hyperbolic Pain, Karl Ove Hufthammer, Martin Disch, Nicolas Artance, Quetzal2, Valvin. Versione inglese (versione originale) Correzioni di bozze: Craig Maloney, GunChleoc, Karl Ove Hufthammer, Martin Disch. Traduzione in italiano: Carlo Gandolfi. Correzioni: Antonio Parisi. Basto sull'universo di Hereva Creato da: David Revoy. Amministratore: Craig Maloney. Autori: Craig Maloney, Nartance, Scribblemaniac, Valvin. Correzioni: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Software: Krita 4.3, Inkscape 1.0 on Kubuntu 19.10. Licenza: Creative Commons Attribution 4.0. www.peppercarrot.com
Pepper|5|True|Lo sapevi?
Pepper|6|True|Pepper&Carrot è completamente libero (gratuito), open-source e sostenuto grazie alle gentili donazioni dei lettori.
Pepper|7|False|Per questo episodio, grazie ai 1190 donatori!
Pepper|8|True|Anche tu puoi diventare un sostenitore di Pepper&Carrot ed avere il tuo nome in questo elenco!
Pepper|9|True|Siamo su Patreon, Tipeee, Paypal, Liberapay …ed altri!
Pepper|10|True|Leggi www.peppercarrot.com per ulteriori informazioni!
Pepper|11|False|Grazie mille!
