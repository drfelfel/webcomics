# Transcript of Pepper&Carrot Episode 33 [de]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 33: Zauberhafter Krieg

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Feind|1|False|tuuuuUUUUUUUT!
Armee|2|False|Brüll!
Armee|5|False|Raaaah!
Armee|4|False|Grrrr!
Armee|3|False|Juurrr!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
König|1|True|Ok, junge Hexe.
König|2|False|Wenn du einen Zauber hast, der uns helfen kann... jetzt oder nie!
Pepper|3|True|Jawohl!
Pepper|4|False|Macht Euch gefasst auf...
Geräusch|5|False|Tschiing!!
Pepper|6|False|...MEIN MEISTERWERK!
Geräusch|7|False|Tschiiing!!
Pepper|8|False|Realitas Hackeris Pepperus!
Geräusch|9|False|Tschiioo !!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geräusch|1|False|Fiiss!
Geräusch|2|False|Tschii!
Geräusch|3|False|Schii!
Geräusch|4|False|Ffhii!
Geräusch|8|False|Tsching!
Geräusch|7|False|Fiiss!
Geräusch|6|False|Schii!
Geräusch|5|False|Ffhii!
König|9|True|Nun, verstärkt dieser Zauber unsere Schwerter...
König|10|False|...und schwächt die Waffen des Feindes?
Geräusch|11|False|Tschiing...
Pepper|12|True|Hehe.
Pepper|13|True|Ihr werdet sehen!
Pepper|14|False|Ich kann bloß sagen, dass Ihr heute keine Soldaten verlieren werdet.
Pepper|15|False|Aber Ihr müsst immer noch kämpfen und Euer Bestes geben!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
König|1|False|Dafür haben wir trainiert.
König|2|False|AUF MEIN KOMMANDOO!
Armee|3|False|Jawoll!
Armee|4|False|Jaaaa!
Armee|5|False|Los!
Armee|6|False|Los!
Armee|7|False|Jaaa!
Armee|8|False|Los!
Armee|9|False|Jawoll!
Armee|10|False|Jaa!
Armee|11|False|Jawoll!
König|12|False|AAAAAAANGRIIIFF!!!
Armee|13|False|Jaaa!
Armee|14|False|Los!
Armee|15|False|Jawoll!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
König|1|False|Jaaaa!!!
Feind|2|False|JUuurr!!!
Geräusch|3|False|Schwiiing !!
Geräusch|4|False|Wuusch !!
Schrift|5|False|12
Feind|6|False|?!!
Schrift|7|False|8
König|8|False|?!!
Schrift|9|False|64
Schrift|10|False|32
Schrift|11|False|72
Schrift|12|False|0
Schrift|13|False|64
Schrift|14|False|0
Schrift|15|False|56
Armee|20|False|Raah!
Armee|17|False|Jurr!
Armee|19|False|Grrr!
Armee|21|False|Jaaa!
Armee|18|False|Aaah!
Armee|16|False|Jaaa!
Geräusch|27|False|Sching
Geräusch|25|False|Ffsch
Geräusch|22|False|wisch
Geräusch|23|False|Swusch
Geräusch|24|False|Kling
Geräusch|26|False|Kling

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
König|1|False|?!
Schrift|2|False|3
Schrift|3|False|24
Schrift|4|False|38
Schrift|5|False|6
Schrift|6|False|12
Schrift|7|False|0
Schrift|8|False|5
Schrift|9|False|0
Schrift|10|False|37
Schrift|11|False|21
Schrift|12|False|62
Schrift|13|False|27
Schrift|14|False|4
König|15|False|!!
König|16|False|HEXEEEE!!!! WAS HAST DU GETAN?!!!!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Tadaaa!
Pepper|2|True|DAS
Pepper|3|False|...ist mein Meisterwerk!
Pepper|4|False|Es ist ein komplexer Zauber, der die Realität verändert und die Lebenspunkte des Gegners zeigt.
Schrift|5|False|33
Pepper|6|True|Wenn man keine Lebenspunkte mehr hat, verlässt man das Feld für den Rest des Kampfes.
Pepper|7|True|Die erste Armee, die alle gegnerischen Soldaten entfernt, gewinnt!
Pepper|8|False|Einfach!
Schrift|9|False|0
Armee|10|False|?
Pepper|11|True|Ist das nicht großartig?
Pepper|12|True|Keine Tode mehr!
Pepper|13|True|Keine verwundeten Soldaten mehr!
Pepper|14|False|Das wird die Kriegsführung revolutionieren!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Nun, da wir die Regeln kennen, lasst mich die Punkte zurücksetzen, damit wir nochmals beginnen können!
Pepper|2|False|So ist es fairer.
Schrift|3|False|64
Schrift|4|False|45
Schrift|5|False|6
Schrift|6|False|2
Schrift|7|False|0
Schrift|8|False|0
Schrift|9|False|0
Schrift|10|False|0
Schrift|11|False|9
Schrift|12|False|5
Schrift|13|False|0
Schrift|14|False|0
Schrift|15|False|0
Geräusch|16|False|Sching!
Geräusch|17|False|Wusch!
Geräusch|19|False|Tschack!
Geräusch|18|False|Tock!
Pepper|20|True|Lauf schneller, Carrot!
Pepper|21|False|Der Zauber hält nicht mehr lange!
Erzähler|22|False|- ENDE -

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Impressum|4|False|29. Juni, 2020 Illustration & Handlung: David Revoy. Beta-Leser: Arlo James Barnes, Craig Maloney, GunChleoc, Hyperbolic Pain, Karl Ove Hufthammer, Martin Disch, Nicolas Artance, Quetzal2, Valvin. Deutsche Version Übersetzung: Martin Disch. Korrektur: Alina The Hedgehog, GunChleoc, Ret Samys. Basierend auf der Hereva-Welt Erstellung: David Revoy. Hauptbetreuer: Craig Maloney. Redakteure: Craig Maloney, Nartance, Scribblemaniac, Valvin. Korrektur: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Software: Krita 4.3, Inkscape 1.0 auf Kubuntu 19.10. Lizenz: Creative Commons Namensnennung 4.0. www.peppercarrot.com
Pepper|5|True|Wusstest du schon?
Pepper|6|True|Pepper&Carrot ist vollständig frei(libre), Open Source und finanziert durch Spenden von Lesern.
Pepper|7|False|Für diese Episode danken wir 1190 Gönnern!
Pepper|8|True|Du kannst auch Gönner von Pepper&Carrot werden und deinen Namen hier lesen!
Pepper|9|True|Wir sind auf Patreon, Tipeee, PayPal, Liberapay ...und mehr!
Pepper|10|True|Geh auf www.peppercarrot.com für mehr Informationen!
Pepper|11|False|Dankeschön!
