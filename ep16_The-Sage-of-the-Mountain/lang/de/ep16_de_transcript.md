# Transcript of Pepper&Carrot Episode 16 [de]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 16: Der Weise auf dem Berg

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Saffron|10|False|Nein, nein und nochmal nein, Pepper.
Schrift|1|True|Saffron
Schrift|2|True|Hexerei
Schrift|3|False|★★★
Schrift|5|False|Metzgerei
Schrift|6|False|15
Schrift|4|False|13
Schrift|7|False|17
Schrift|8|False|Stern-str.
Schrift|9|False|Haarstudio
Geräusch|11|True|Glug
Geräusch|12|False|Glug

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Saffron|2|False|Ich möchte nicht die Vermittlerin spielen, nicht mal für eine Freundin. So etwas endet immer übel.
Saffron|1|True|Diese Sache musst Du selber klären.
Pepper|3|False|B-Bitte Saffron...
Pepper|4|True|Ich lerne rein gar nichts bei meinen Tanten, das Einzige, was sie machen, ist mein Haus zu übernehmen, sich über ihre alten Knochen beschweren und mich anschreien ...
Saffron|6|True|Hör zu, Du bist eine Hexe, handle wie eine!
Saffron|8|False|Wie ein großes Mädchen!
Saffron|7|True|Rede Klartext mit Ihnen!
Pepper|5|False|Kannst Du wirklich nicht mitkommen und mit ihnen reden... ?
Saffron|9|False|In Ordnung, in Ordnung! Ich könnte schon mitkommen und mit ihnen reden.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Thymian|1|False|Was meinst du mit, du lernst gar nichts?!
Thymian|12|False|Und du bist nicht mal mutig genug, es uns selbst zu sagen?!
Thymian|14|False|Wenn mein Rücken nicht Probleme machen würde, gäbe ich euch eine Lektion. Und keine in Zauberei, wenn ihr versteht, was ich meine.
Cayenne|15|True|Thymian, beruhige dich
Cayenne|16|False|Sie haben recht...
Vogel|2|True|piep
Vogel|3|False|piep
Vogel|4|True|piep
Vogel|5|False|piep
Vogel|6|True|piep
Vogel|7|False|piep
Vogel|8|True|piep
Vogel|9|False|piep
Vogel|10|True|piep
Vogel|11|False|piep
Geräusch|13|False|BAMM!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayenne|1|True|Wir sind nicht mehr so jung wie früher. Jetzt ist eine gute Zeit für eine Lektion.
Cayenne|2|True|Ich schlage ein Treffen vor mit dem
Thymian|5|True|Hi hi hi!
Thymian|6|True|Der Weise?
Thymian|7|False|Sind sie nicht zu jung, um seine Lehren zu verstehen?
Pepper|8|True|Zu jung für was!?
Pepper|9|False|Wir machen's!
Saffron|10|False|"Wir"?
Thymian|11|True|Nun, wir sind hier.
Thymian|12|True|Ihr könnt euch selber bei dem Weisen vorstellen.
Thymian|13|False|Ihr findet ihn oben auf dem Berg.
Thymian|14|True|Bereit für eine
Thymian|15|False|echte Lektion?
Cayenne|3|True|Weisen auf dem Berg
Cayenne|4|False|Sein Rat ist essentiell...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Bereit!
Saffron|2|False|Ich auch!
Thymian|3|False|Großartig! Gute Einstellung!
Pepper|9|False|ANGRIFF!!!
Saffron|6|False|EIN... EIN MONSTER!
Pepper|8|True|ES IST EINE FALLE!
Pepper|7|True|ICH WUSSTE ES!!
Pepper|5|False|Oh nein!
Thymian|4|False|Los!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|GRAVITAS SPIRALIS!
Saffron|2|False|BRASERO INTENSIA!
Geräusch|3|False|Platsch!
Thymian|4|True|Ahh, diese Jugend, wollen immer alles angreifen!
Pepper|7|False|Was für eine verrückte Lektion!
Impressum|11|False|Lizenz: Creative Commons Namensnennung 4.0, Software: Krita, Inkscape auf Ubuntu
Impressum|10|False|Basierend auf dem Hereva Universum von David Revoy mit Unterstützung von Craig Maloney. Korrekturen von Willem Sonke, Moini, Hali, Cgand und Alex Gryson.
Erzähler|8|False|- ENDE -
Impressum|9|False|04/2016 - www.peppercarrot.com - Grafik & Handlung: David Revoy - Deutsche Übersetzung: Philipp Hemmer
Thymian|6|False|Wenn etwas nicht nach euren Wünschen läuft, ist nichts besser als ein schönes heißes Bad! Es ist gut für unser Rheuma und eure Nerven!
Thymian|5|True|Die Lektion des Weisen ist, es ihm nachzumachen!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Impressum|1|False|Pepper&Carrot ist komplett frei, Open Source und wird durch die Leser unterstützt und finanziert. Für diese Episode geht der Dank an die 671 Förderer:
Impressum|2|False|Auch Du kannst Pepper&Carrot unterstützen: www.patreon.com/davidrevoy
